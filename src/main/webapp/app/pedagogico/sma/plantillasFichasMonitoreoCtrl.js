app.controller("plantillasFichasMonitoreoCtrl",["$rootScope","$scope","$sce","NgTableParams","crud","modal", function ($rootScope,$scope,$sce,NgTableParams,crud,modal){        

    $scope.main = true;
    $rootScope.paramsItems = {count: 10};
    $rootScope.settingItems = {counts: []};
    $rootScope.tablaPlantillas = new NgTableParams($rootScope.paramsItems, $rootScope.settingItems);
    $scope.plaId = undefined;
    
    $scope.listarPlantillas = function(){        
        //preparamos un objeto request        
        var request = crud.crearRequest('plantilla_ficha_monitoreo',1,'listarPlantillas');
        //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request
        //y las funciones de exito y error
        
        crud.listar("/sma",request,function(data){              
            $rootScope.settingItems.dataset = data.data;            
            iniciarPosiciones($rootScope.settingItems.dataset);            
            $scope.tablaPlantillas.settings($rootScope.settingItems);
        },function(data){
            console.info(data);
        });        
    };
    
    $scope.crearPlantilla = function(flag, flag2){
        if(flag2){
            $scope.plaId = undefined;
            $scope.nuevaPlantilla = {};
            $scope.nuevoCompromiso = {};
            $scope.nuevoItem = {}; 
            
            $scope.compromisos = [];
        }
        
        
        $scope.main=flag;
        $scope.listarPlantillas();
//        $('#modalCrearPlantilla').modal('show'); 
    };

    //JSON para una plantilla
    $scope.nuevaPlantilla = {};
    $scope.nuevoCompromiso = {};
    $scope.nuevoItem = {};
    
    $scope.compromisos = [];
    
    //Compromiso    
    $scope.agregarCompromiso = function() {
        $scope.nuevoCompromiso.items = [];
        $scope.compromisos.push($scope.nuevoCompromiso);
        $scope.nuevoCompromiso = {};

    };

    $scope.eliminarCompromiso = function(index) {        
        $scope.compromisos.splice(index, 1);      
    };
    
    $scope.editarCompromiso = function(r) {
        if(r.edi){                        
            r.comNom = r.copia.comNom;
            delete r.copia;
            r.edi = false; 
        }
        else{
            r.copia = JSON.parse(JSON.stringify(r));            
            r.edi = true;
        }
    }
    //fin compromisos
  
    //Indicadores
    $scope.agregarItem = function(compromiso) {        

        compromiso.items.push(compromiso.nuevoItem);
        compromiso.nuevoItem = {};
    };

    $scope.eliminarItem = function(index, compromiso) {
        compromiso.items.splice(index, 1);
    };
    $scope.editarItem = function(compromiso, r) {
        if(r.edi){            
            r.iteNom = r.copia.iteNom;
            delete r.copia;
            r.edi = false; 
        }
        else{
            r.copia = JSON.parse(JSON.stringify(r));            
            r.edi = true;
        }
    };
    
    //fin indicadores
    
    //Items
//    $scope.agregarItem = function(indicador) {
//      indicador.items.push(indicador.nuevoItem);
//      indicador.nuevoItem = {};
//    };
//
//    $scope.eliminarItem = function(index, indicador) {      
//      indicador.items.splice(index, 1);
//    };
//    $scope.editarItem = function(r) {
//        if(r.edi){
//            r.iteNom = r.copia.iteNom;
//            r.itePun = r.copia.itePun;
//            delete r.copia;
//            r.edi = false; 
//        }
//        else{
//            r.copia = JSON.parse(JSON.stringify(r));            
//            r.edi = true;
//        }
//    }
    //fin items
    //datos necesarios para guardar una plantilla
    $scope.plantilla={
        codigo:"",
        nombre:"",
        descripcion:"",
    };
    
    //operacion para guardar una plantilla
    $scope.guardarPlantilla = function(){
        var request = crud.crearRequest('plantilla_ficha_monitoreo',1,'registrarPlantillaMonitoreo');
        //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request
        //y las funciones de exito y error        
        request.setData({compromisos:$scope.compromisos, nombre:$scope.plantilla.nombre, descripcion:$scope.plantilla.descripcion});
        crud.insertar("/sma",request,function(response){              
            modal.mensaje("CONFIRMACION",response.responseMsg);
                $scope.crearPlantilla(true, true);
                $scope.listarPlantillas();
//                reiniciarvariables();
        },function(data){
            console.info(data);
        });
    };
    //Fin
    
//    Editar una plantilla
    $scope.editarPlantilla = function(plaide){
        //preparamos un objeto request   
        $scope.plaId = plaide;
        var request = crud.crearRequest('plantilla_ficha_monitoreo',1,'listarPlantilla');
        //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request
        //y las funciones de exito y error
        request.setData({plaId:$scope.plaId});
        crud.listar("/sma",request,function(data){                          
            $scope.plantilla = data.data.pop();
            $scope.compromisos = data.data;            
            $scope.crearPlantilla(false, false);            
        },function(data){
            console.info(data);
        });
    };
    
    $scope.actualizarCabecera = function(){
        modal.mensajeConfirmacion($scope,"seguro que desea cambiar los datos de esta plantilla",function(){            
            var request = crud.crearRequest('plantilla_ficha_monitoreo',1,'actualizarPlantilla');                    
            request.setData({plaId:$scope.plantilla.plaide, 
                            plaNom:$scope.plantilla.nombre,
                            plaDes:$scope.plantilla.descripcion                            
            });

            crud.actualizar("/sma",request,function(response){
                modal.mensaje("CONFIRMACION",response.responseMsg);
                
            },function(data){
                console.info(data);                        
            });            
        }); 
    };
    
    $scope.reportePlantilla = function(){
        
        var objeto = {
            
            nombre:$scope.plantilla.nombre,
            descripcion:$scope.plantilla.descripcion
        };
        
        var request = crud.crearRequest('plantilla_ficha_monitoreo',1,'imprimirPlantilla');
        request.setData({compromisos:$scope.compromisos, objeto:objeto});
        crud.listar("/sma",request,function(data){            
            $scope.dataBase64 = data.data[0].datareporte;
            window.open($scope.dataBase64);
        },function(data){
            console.info(data);
        });
        
    };
    $scope.visualizarPlantilla = function(plaide){
        //preparamos un objeto request   
        $scope.plaId = plaide;
        var request = crud.crearRequest('plantilla_ficha_monitoreo',1,'listarPlantilla');
        //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request
        //y las funciones de exito y error
        request.setData({plaId:$scope.plaId});
        crud.listar("/sma",request,function(data){                          
            $scope.plantilla = data.data.pop();
            $scope.compromisos = data.data;            
            $scope.reportePlantilla();
        },function(data){
            console.info(data);
        });
        
    };
    
    $scope.advertenciaCompromiso = false;
    $scope.verificarPlantilla = function(flag){
        $scope.advertenciaCompromiso = ($scope.compromisos.length > 0 ? false:true);
        return $scope.compromisos.length > 0 ? (flag && true):true;
    };
    
}]);

