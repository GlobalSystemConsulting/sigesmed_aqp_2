/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

angular.module('app')
        .controller('asistenciaEstudiante',['$log','$location','modal','UtilAppServices','NgTableParams','$rootScope','crud',function($log,$location,modal,util,NgTableParams,$rootScope,crud){
        var self = this;
        self.abrirCalendar = false;
        self.openCalendar = function () {
            self.abrirCalendar = true;
        }
        //self.fec = new Date();
        self.dateOptions = {
            formatYear: 'yyyy',
            maxDate: new Date(),
            minDate: new Date(1900,1,1),
            startingDay: 1
        };
        self.estAsist = ["Asistio","Falta","Tardanza"];
        mostrarOpcionesDocente();
        function mostrarOpcionesDocente(){
            var rol = $rootScope.usuMaster.rol;
            if(Number(rol.rolID) === 20){
                self.checkIE = false;
                self.checkClass = true;
            }
        }
        listarNiveles();
        function listarNiveles(){
            var request = crud.crearRequest('anecdotario',1,'listarGrados');
            request.setData({org:$rootScope.usuMaster.organizacion.organizacionID, doc:$rootScope.usuMaster.usuario.usuarioID});
            crud.listar('/maestro',request,function(response){
                if(response.responseSta){
                    self.niveles = response.data.niveles;
                }else{
                    modal.mensaje('ERROR',response.responseMsg);
                }
            },function(errResponse){
            });
        }
        self.mostrarGrados =  function(nivel){
            self.grados = nivel.grados;

        }
        self.mostrarSecciones =  function(grado){
            self.secciones = grado.secciones;

        }
        self.mostrarAreas = function(seccion){
            var request = crud.crearRequest('acomp',1,'listarCursosDocente');
            request.setData({doc:$rootScope.usuMaster.usuario.usuarioID,org:$rootScope.usuMaster.organizacion.organizacionID,gra:self.grado.id,secc:seccion});
            crud.listar('/maestro',request,function(response){
                if(response.responseSta){
                    self.areas = response.data;
                }else{
                    modal.mensaje('ERROR',response.responseMsg);
                }
            },function(errResponse){});
        }
        self.tablaAsistenciaEst = new NgTableParams({count:8},{
            counts: [],
            paginationMaxBlocks: 13,
            paginationMinBlocks: 2,
            dataset:[]
        });

        self.mostrarOpcion = function(row){
            if(row.est === undefined || row.est === null) {
                row.est = 3;
            };
            switch(row.est) {
                case 0: $("#btn-asist" + row.perid).removeClass("btn-warning").addClass("btn-success"); return 'Asistio';
                case 1: $("#btn-asist" + row.perid).removeClass("btn-success").addClass("btn-danger"); return 'Falta';
                case 2: $("#btn-asist" + row.perid).removeClass("btn-danger").addClass("btn-warning"); return 'Tardanza';
                case 3: $("#btn-asist" + row.perid).removeClass("btn-warning").addClass("btn-success"); return 'NN';
            }
        }
        self.desactivarBotones = function(){
            if(self.fec == null) return;
            self.fec.setHours(0,0,0,0);
            var currenDate = new Date();
            currenDate.setHours(0,0,0,0);
            return self.fec.getTime() !== currenDate.getTime();
        }

        self.guardarAsistencia = function()
        {
            var mats = [];
            angular.forEach(self.tablaAsistenciaEst.settings().dataset,function(obj,key){
                mats.push({id:obj.id,est: obj.est,asid:obj.asid});
            });
            var dataSend = {
                org:$rootScope.usuMaster.organizacion.organizacionID,
                doc:$rootScope.usuMaster.usuario.usuarioID,
                fec:new Date().getTime(),
                tip: self.checkIE ? 0 : 1,
                are : self.checkArea && self.area != undefined ? self.area.id : -1,
                mats:mats
            }
            var request = crud.crearRequest('asistencia_estudiante',1,'registrarAsistenciaMasiva'   );
            request.setData(dataSend);
            crud.insertar('/submodulo_academico',request,function(response){
                if(response.responseSta){
                    angular.forEach(self.tablaAsistenciaEst.settings().dataset,function(obj,key){
                        var asis = _.find(response.data,function(o){
                            return o.id == obj.id;
                        });
                        if(asis != null) angular.extend(obj,asis);
                    });
                }else{
                    modal.mensaje('ERROR',response.responseMsg);
                }
            },function(errResponse){});
        }
        self.verReporte = function(){
            var resolve = {
                data : function(){
                    return {
                    }
                }
            }
            var modalInstance = util.openModal('modal_reporte.html','visualizarReporteCtrl','lg','ctrl',resolve);
            modalInstance.result.then(function(){


            },function(response){

            });
        }
        self.cambiarAsistencia = function(row){
            if(row.est + 1 > self.estAsist.length - 1){
                row.est = 0;
            }else{
                row.est += 1;
            }
            switch(row.est){
                case 0: $("#btn-asist" + row.perid).removeClass("btn-warning").addClass("btn-success"); break;
                case 1: $("#btn-asist" + row.perid).removeClass("btn-success").addClass("btn-danger"); break;
                case 2: $("#btn-asist" + row.perid).removeClass("btn-danger").addClass("btn-warning"); break;
            }   
        };
        self.buscarAsistencia = function(){
            if(self.grado == undefined || self.seccion == undefined){
                self.fec = null;
                modal.mensaje('ERROR', 'SE DEBE SELECCIONAR UNA SECCION');
                return;
            }
            var dataSend = {
                org:$rootScope.usuMaster.organizacion.organizacionID,
                gra:self.grado.id,
                secc:self.seccion,
                fec:self.fec.getTime(),
                are: self.checkArea && self.area != undefined ? self.area.id : -1
            };
            var request = crud.crearRequest('asistencia_estudiante',1,'listarAsistencia');
            request.setData(dataSend);
            crud.listar('/submodulo_academico',request,function(response){
                if(response.responseSta){
                    self.tablaAsistenciaEst.settings().dataset = response.data;
                    self.tablaAsistenciaEst.reload();
                    //$log.log('alumnos',self.alumnos);
                }else{
                    modal.mensaje('ERROR',response.responseMsg);
                }
            },function(errResponse){});
        }
        self.justAsistencia = function (row) {
            var resolve = {
                data : function(){
                    return {
                        asi : angular.copy(row)
                    }
                }
            }
            var modalInstance = util.openModal('registroJustificacion.html','registrarJustificacionCtrl','lg','ctrl',resolve);
            modalInstance.result.then(function(){


            },function(response){

            });
        };
    }])
    .controller('registrarJustificacionCtrl',['$log','$uibModalInstance','crud','modal','data',function($log,$uibModalInstance,crud,modal,data){
        var self = this;
        self.titulo = 'Registrar Justificacion';
        self.just = new Object();
        self.just.des = data.asi.desjus;
        self.just.asid = data.asi.asid;
        self.just.perid = data.asi.perid;
        self.cancelar = function(){
            $uibModalInstance.dismiss('cancel');
        }
        self.guardar = function(){
            var request = crud.crearRequest('asistencia_estudiante',1,'registrarJustificacion');
            request.setData(self.just);
            crud.insertar('/submodulo_academico',request,function(response){
                if(response.responseSta){
                    $uibModalInstance.close({desJus:self.just.des,docjus:self.just.nom});
                }else{
                    modal.mensaje('ERROR',response.responseMsg);
                }
            },function(errResponse){modal.mensaje('ERROR','El servidor no responde');});
        }
    }]).controller('visualizarReporteCtrl',['$log','$uibModalInstance','crud','modal','data','$rootScope',function($log,$uibModalInstance,crud,modal,data,$rootScope){
        var self = this;
        self.abrirIniCalendar = false;
        self.abrirFinCalendar = false;
        self.openIniCalendar = function () {self.abrirIniCalendar = true;}
        self.openFinCalendar = function () {
            self.abrirFinCalendar = true;
        }
        self.dateIniOptions = {
            formatYear: 'yyyy',
            maxDate: new Date(),
            minDate: new Date(1990,01,01),
            startingDay: 1
        };
        self.dateFinOptions = {
            formatYear: 'yyyy',
            maxDate: new Date(),
            minDate: new Date(1990,01,01),
            startingDay: 1
        };
        listarNiveles();
        function listarNiveles(){
            var request = crud.crearRequest('anecdotario',1,'listarGrados');
            request.setData({org:$rootScope.usuMaster.organizacion.organizacionID, doc:$rootScope.usuMaster.usuario.usuarioID});
            crud.listar('/maestro',request,function(response){
                if(response.responseSta){
                    self.niveles = response.data.niveles;
                }else{
                    modal.mensaje('ERROR',response.responseMsg);
                }
            },function(errResponse){
            });
        }
        self.mostrarGrados =  function(nivel){
            self.grados = nivel.grados;

        }
        self.mostrarSecciones =  function(grado){
            self.secciones = grado.secciones;

        }
        self.mostrarAreas = function(seccion){
            var request = crud.crearRequest('acomp',1,'listarCursosDocente');
            request.setData({doc:$rootScope.usuMaster.usuario.usuarioID,org:$rootScope.usuMaster.organizacion.organizacionID,gra:self.grado.id,secc:seccion});
            crud.listar('/maestro',request,function(response){
                if(response.responseSta){
                    self.areas = response.data;
                    self.areas.unshift({id:-1,nom:""});
                }else{
                    modal.mensaje('ERROR',response.responseMsg);
                }
            },function(errResponse){});
        }
        self.cancelar = function(){
            $uibModalInstance.dismiss('cancel');
        }
        self.guardar = function(){
            if(self.desde == undefined || self.hasta == undefined){
                modal.mensaje('REPORTE','DEBE SELECCIONAR UN RANGO DE FECHAS');
                return;
            }
            var numDays = Math.abs((self.hasta.getTime() - self.desde.getTime())/(24*60*60*1000)) + 1;
            if(numDays > 31){
                modal.mensaje('REPORTE','SE DEBE SELECCIONAR UN RANGO NO MAYOR A 31 DAS');
                return;
            }
            var request = crud.crearRequest('asistencia_estudiante',1,'reporteAsistencia');

            request.setData({
                ini:self.desde.getTime(),
                fin:self.hasta.getTime(),
                secc:self.seccion,
                org:$rootScope.usuMaster.organizacion.organizacionID,
                gra:self.grado.id,
                are:self.area == undefined ? -1 : self.area.id});
            crud.listar('/submodulo_academico',request,function(response){
                if(response.responseSta){
                    verDocumento(response.data.file);
                    $uibModalInstance.close({});
                }else{
                    modal.mensaje('ERROR',response.responseMsg);
                }
            },function(errResponse){modal.mensaje('ERROR','El servidor no responde');});
        }
    }]);
