app.controller("estudiantesMpfCtrl", ["$rootScope", "$scope", "NgTableParams", '$window', "crud", "modal", function ($rootScope, $scope, NgTableParams, $window, crud, modal) {
        var params = {count: 10};
        var setting = {counts: []};
        $scope.miTablaDatos = new NgTableParams(params, setting);
        
        var params2 = {count: 10};
        var setting2 = {counts: []};
        $scope.miTablaAreas = new NgTableParams(params2, setting2);
        
        var params3 = {count: 10};
        var setting3 = {counts: []};
        $scope.miTablaLista = new NgTableParams(params3, setting3);
        
        var params4 = {count: 10};
        var setting4 = {counts: []};
        $scope.miTablaTareas = new NgTableParams(params4, setting4);
        
        var params5 = {count: 10};
        var setting5 = {counts: []};
        $scope.miTablaAsistencia = new NgTableParams(params5, setting5);
        
        var params6 = {count: 10};
        var setting6 = {counts: []};
        $scope.miTablaNotas = new NgTableParams(params6, setting6);
        
        $scope.asistencias=[];
        $scope.notasEstudiante=[];
        $scope.mostrarLista=false;
        $scope.estadosTarea = [{id:"N",title:"Nuevo"},{id:"E",title:"Enviado"},{id:"C",title:"Calificado"}];
        $scope.estadosAsistencia = [{id:"A",title:"Asistio"},{id:"F",title:"Falta"},{id:"T",title:"Tardanza"}];
        
        $scope.listaEstudiantes = [{personaID:"",matriculaID:"",dni:"",nombre:"",orgId:"",nombreOrganizacion:""}];
        $scope.estudiante={
            nombres:""
            ,nacionalidad:""
            ,dni:""
            ,nacimiento:""
            ,sexo:""
            ,peso:""
            ,talla:""
            ,alergia:""
            ,sangre:""
            ,email:""
            ,numero1:""
            ,apoderadoNombre:""
            ,apoderadoNumero:""
            ,nivel:""
            ,grado:""
            ,seccion:""
            ,turno:""
            ,orden:""
        };
        $scope.matricula={
            
            nivel:""
            ,grado:""
            ,seccion:""
            ,gradoId:""
            ,seccionId:""
            ,turno:""
            ,tutor:""
        };
        $scope.funciones=[{nombre:"Datos Estudiante"},{nombre:"Matricula"},{nombre:"Tareas"}];
        $scope.areas=[{areaID:"",area:""}];
        $scope.sel={areaID:"",plaID:"",matriculaID:"",orgID:"",fechaDesde:"",fechaHasta:"",gradoID:""};
        $scope.asi={nAsis:0,nTjus:0,Tinj:0,nFjus:0,Finj:0};
        
        $scope.listarEstudiantes = function ()
        {
            var request = crud.crearRequest('padreFamilia', 1, 'listarHijos');
            request.setData({usuarioID:$rootScope.usuMaster.usuario.usuarioID});
            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request
            //y las usuarios de exito y error
            crud.listar("/padreFamilia", request, function (data) {
                if (data.responseSta) {
                    $scope.listaEstudiantes = data.data;
                }

            }, function (data) {
                console.info(data);
            });
        };
        
        $scope.datosEstudiante = function (mp,oo)
        {
            $scope.sel.orgID=oo;
            var request = crud.crearRequest('padreFamilia', 1, 'datosEstudiante');
            request.setData({matriculaID:mp});
            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request
            //y las usuarios de exito y error
            crud.listar("/padreFamilia", request, function (data) {
                if (data.responseSta) {
                    $scope.estudiante=data.data;
                    
                    $("#modalDatosEstudiante").modal('show');
                    
                }
                modal.mensaje("CONFIRMACION", data.responseMsg);
            }, function (data) {
                console.info(data);
            });
        };
        
        $scope.imprimirDatosEstudiante = function ()
        {
            var request = crud.crearRequest('padreFamilia', 1, 'reporteFichaEstudiante');
            request.setData({ficha:$scope.estudiante,orgID:$scope.sel.orgID});
            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request
            //y las usuarios de exito y error
            crud.insertar("/padreFamilia", request, function (data) {
                if (data.responseSta) {
                    $scope.dataBase64 = data.data[0].datareporte;
                    window.open($scope.dataBase64);
                    
                }
                modal.mensaje("CONFIRMACION", data.responseMsg);
            }, function (data) {
                console.info(data);
            });
        };
        
        $scope.datosMatricula = function (mp,oo)
        {
            $scope.mostrarLista=false;
            $scope.sel.orgID=oo;
            var request = crud.crearRequest('padreFamilia', 1, 'datosMatricula');
            request.setData({matriculaID:mp});
            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request
            //y las usuarios de exito y error
            crud.listar("/padreFamilia", request, function (data) {
                if (data.responseSta) {
                    $scope.matricula=data.data.matricula;
                    setting.dataset = data.data.matricula;
                    iniciarPosiciones(setting.dataset);
                    $scope.miTablaDatos.settings(setting);
                    setting2.dataset = data.data.areas;
                    iniciarPosiciones(setting2.dataset);
                    $scope.miTablaAreas.settings(setting2);
                    $scope.matricula.observaciones=data.data.obs;
                    $("#modalDatosMatricula").modal('show');
                }
                modal.mensaje("CONFIRMACION", data.responseMsg);
            }, function (data) {
                console.info(data);
            });
        };//
        
        $scope.verUtilesArea = function (a)
        {
            $scope.listaTotal=0;
            var request = crud.crearRequest('padreFamilia', 1, 'listarUtilesByArea');
            request.setData({organizacionID:$scope.sel.orgID,grado:$scope.matricula[0].gradoId,seccion:$scope.matricula[0].seccionId,area:a});
            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request
            //y las usuarios de exito y error
            crud.listar("/padreFamilia", request, function (data) {
                if (data.responseSta) {
                    if(data.data.length==undefined)
                    {
                        $scope.mostrarLista=false;
                        modal.mensaje("VERIFICACION", data.responseMsg);
                    }
                    else
                    {
                        setting3.dataset = data.data;
                        
                        iniciarPosiciones(setting3.dataset);
                        $scope.miTablaLista.settings(setting3);
                        for(var i=0;i<setting3.dataset.length;i++)
                        {
                            $scope.listaTotal+=setting3.dataset[i].precio;
                        }
                        $scope.mostrarLista=true;
                    }
                    
                }
                else
                {
                     $scope.mostrarLista=false;
                }
//                $("#modalDatosTareas").modal('show');
            }, function (data) {
                console.info(data);
            });
        };
        
        $scope.imprimirListaByArea = function (a,aa,d)
        {
            var request = crud.crearRequest('padreFamilia', 1, 'reporteListaUtilesByArea');
            request.setData({organizacionID:$scope.sel.orgID,matricula:$scope.matricula[0],area:aa,areaID:a,docente:d});
            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request
            //y las usuarios de exito y error
            crud.insertar("/padreFamilia", request, function (data) {
                if (data.responseSta) {
                    $scope.dataBase64 = data.data[0].datareporte;
                    window.open($scope.dataBase64);
                    
                }
                modal.mensaje("CONFIRMACION", data.responseMsg);
            }, function (data) {
                console.info(data);
            });
        };
        //imprimirAsistencia
        $scope.imprimirListaUtiles = function ()
        {
            var request = crud.crearRequest('padreFamilia', 1, 'reporteListaUtiles');
            request.setData({matricula:$scope.matricula[0],organizacionID:$scope.sel.orgID});
            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request
            //y las usuarios de exito y error
            crud.insertar("/padreFamilia", request, function (data) {
                if (data.responseSta) {
                    $scope.dataBase64 = data.data[0].datareporte;
                    window.open($scope.dataBase64);
                    
                }
                modal.mensaje("CONFIRMACION", data.responseMsg);
            }, function (data) {
                console.info(data);
            });
        };
        
        $scope.datosTareas = function (mp,oo)
        {
            $scope.sel.areaID="";
            var params4 = {count: 10};
            var setting4 = {counts: []};
            $scope.miTablaTareas = new NgTableParams(params4, setting4);
            $scope.sel.matriculaID=mp;
            var request = crud.crearRequest('padreFamilia', 1, 'listarAreas');
            request.setData({organizacionID:oo,matriculaID:mp});
            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request
            //y las usuarios de exito y error
            crud.listar("/padreFamilia", request, function (data) {
                if (data.responseSta) {
                    $scope.sel.plaID=data.data.planID;
                    $scope.areas=data.data;
                    $("#modalDatosTareas").modal('show');
                }

            }, function (data) {
                console.info(data);
            });
        };//buscarTareas
        
        $scope.buscarTareas = function ()
        {
            if($scope.sel.areaID==="")
                return;
            
            var request = crud.crearRequest('padreFamilia', 1, 'listarBandejaTareaByAlumno');
            request.setData({area:$scope.sel.areaID,matriculaID:$scope.sel.matriculaID});
            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request
            //y las usuarios de exito y error
            crud.listar("/padreFamilia", request, function (data) {
                if (data.responseSta) {
                    if(data.data.length===0)
                        modal.mensaje("VERIFICACION", "No se encontro Tareas Disponibles para el Area Seleccionada");
                    
                    setting4.dataset = data.data;
                    iniciarPosiciones(setting4.dataset);
                    $scope.miTablaTareas.settings(setting4);
                   
                }
                
            }, function (data) {
                console.info(data);
            });
        };
        
        $scope.datosAsistencia = function (pp,gd,oo,mp)
        {
            $scope.sel.areaID="";
            $scope.sel.orgID=oo;
            var params5 = {count: 10};
            var setting5 = {counts: []};
            $scope.miTablaAsistencia = new NgTableParams(params5, setting5);
            $scope.asistencias=[];
            $scope.sel.matriculaID=mp;
            $scope.sel.gradoID=gd;
            $scope.sel.plaID=pp;
            
            var request = crud.crearRequest('padreFamilia', 1, 'listarAreas');
            request.setData({organizacionID:oo,matriculaID:mp});
            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request
            //y las usuarios de exito y error
            crud.listar("/padreFamilia", request, function (data) {
                if (data.responseSta) {
                    $scope.areas=data.data;
                    
                    $("#modalDatosAsistencia").modal('show');
                }

            }, function (data) {
                console.info(data);
            });
        };
        
        $scope.buscarAsistencia = function ()
        {
            if($scope.sel.areaID==="")
                return;
            if($scope.sel.fechaDesde==="")
                return;
            if($scope.sel.fechaHasta==="")
                return;
            
            var request = crud.crearRequest('padreFamilia', 1, 'listarAsistenciaByAlumno');
            request.setData({area:$scope.sel.areaID,matriculaID:$scope.sel.matriculaID,desde:convertirFecha2($scope.sel.fechaDesde),hasta:convertirFecha2($scope.sel.fechaHasta)});
            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request
            //y las usuarios de exito y error
            crud.listar("/padreFamilia", request, function (data) {
                if (data.responseSta) {
                    if(data.data.length===0)
                        modal.mensaje("VERIFICACION", "No se encontro Asistencias");
                    
                    setting5.dataset = data.data.asistencias;
                    $scope.asistencias=data.data.asistencias;
                    iniciarPosiciones(setting5.dataset);
                    $scope.miTablaAsistencia.settings(setting5);
                    $scope.asi=data.data.resumen;
                   
                }
                
            }, function (data) {
                console.info(data);
            });
        };
        
        $scope.imprimirAsistencia = function ()
        {
            if($scope.asistencias.length==0)
            {
                modal.mensaje("VERIFICACION", "Seleccione Area y fechas con asistencias");
                return;
            }
            
            var request = crud.crearRequest('padreFamilia', 1, 'reporteAsistenciaByArea');
            request.setData({organizacionID:$scope.sel.orgID,area:$scope.sel.areaID,matriculaID:$scope.sel.matriculaID,planID:$scope.sel.plaID,gradoID:$scope.sel.gradoID,desde:convertirFecha2($scope.sel.fechaDesde),hasta:convertirFecha2($scope.sel.fechaHasta)});
            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request
            //y las usuarios de exito y error
            crud.insertar("/padreFamilia", request, function (data) {
                if (data.responseSta) {
                    $scope.dataBase64 = data.data[0].datareporte;
                    window.open($scope.dataBase64);
                    
                }
                modal.mensaje("CONFIRMACION", data.responseMsg);
            }, function (data) {
                console.info(data);
            });
        };
        
        $scope.datosNotas = function (mp,oo,pp)
        {
            $scope.sel.matriculaID=mp;
            $scope.sel.orgID=oo;
            $scope.sel.nombre=pp;
            var params6 = {count: 10};
            var setting6 = {counts: []};
            $scope.notasEstudiante=[];
            $scope.miTablaNotas = new NgTableParams(params6, setting6);
//            $scope.sel.matriculaID=mp;
            var request = crud.crearRequest('padreFamilia', 1, 'listarNotas');
            request.setData({organizacionID:oo,matriculaID:mp});
            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request
            //y las usuarios de exito y error
            crud.listar("/padreFamilia", request, function (data) {
                if (data.responseSta) {
                    setting6.dataset = data.data;
                    $scope.notasEstudiante=data.data;
                    iniciarPosiciones(setting6.dataset);
                    $scope.miTablaNotas.settings(setting6);
                }
                $("#modalNotasEstudiante").modal('show');
                modal.mensaje("CONFIRMACION",data.responseMsg);
            }, function (data) {
                console.info(data);
            });
        };//buscarTareas
        
        $scope.imprimirNotas = function ()
        {
            
            if($scope.notasEstudiante.length==0)
            {
                modal.mensaje("CONFIRMACION","No existe Notas Registradas del Alumno")
                return;
            }
            
            var request = crud.crearRequest('padreFamilia', 1, 'reporteBoletaNotas');
            request.setData({organizacionID:$scope.sel.orgID,matriculaID:$scope.sel.matriculaID,persona:$scope.sel.nombre});
            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request
            //y las usuarios de exito y error
            crud.insertar("/padreFamilia", request, function (data) {
                if (data.responseSta) {
                    $scope.dataBase64 = data.data[0].datareporte;
                    window.open($scope.dataBase64);
                    
                }
                modal.mensaje("CONFIRMACION", data.responseMsg);
            }, function (data) {
                console.info(data);
            });
        };
    }]);

