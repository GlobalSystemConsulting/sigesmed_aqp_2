
app.controller("registro_bienCtrl",["$scope","NgTableParams","$location","crud","modal", function ($scope,NgTableParams,$location,crud,modal){
       
       
    /*Codigo de Barras del Bien*/   
     $scope.codigo_barras="";   
        
    /*Datos Catalogo*/
    $scope.item_catalogo= {
        gru_gen_id:0,
        cla_gen_id:0,
        fam_gen_id:0,
        uni_med_id:0,
        cod:0,
        den_bie:""
    };
    /*Datos Movmiento Ingresos*/
    $scope.movimiento = {
        tip_mov_ing:0,
        con_pat_id:0,
        num_res:"",
        fec_res:new Date(),
        obs:""   
    };
    
    
    /*Tipo de Validacion de Registro Bien (Alta,Baja,Salida)*/
   $scope.tipo_validacion ={
       id:0,
       causal_id:0
   };
   $scope.validacion = [
        {id:1,nombre:"ALTA DE BIENES"},
        {id:2,nombre:"BAJA DE BIENES"},
        {id:3,nombre:"SALIDA DE BIENES"}
   ];
    
    /*Listado de Controles Patrimoniales*/
    $scope.controles_patrimoniales = {};
    
    
    /*Datos Detalle Tecnico*/
    $scope.detalle_tecnico = {};
    
    /*Correlativo del Nuevo Bien*/
    $scope.nro_correlativo;
    
    /*Listado de Tipo de Movimientos de Ingreso*/
    $scope.movimientos_ingreso = {};
    
    /*ALTAS*/
    $scope.altas={};
    /*BAJAS*/
    $scope.bajas={};
    /*SALIDAS*/
    $scope.salidas={};
    
    //** Variable para la Actualizacion de un Bien
    $scope.bien_mueble_update;
    
    /*Accion del Bien*///(Registrar,Actualizar)
    $scope.accion_bien="Crear Registro Individual";
    
    /*Lista del Tipo de Causal*/
    $scope.tipo_causal={};
    
    
    
    /*Lista Seleccionada*/
    
    $scope.lista ={
        movimiento:"",
        anexo:"",
        cont_pat:"",
        ambiente:"",
        condicion:""
    };
    
    /*Documentos Adjuntos*/
    $scope.doc_referencia = {nombre:"",archivo:"",nombre_archivo:""};
    $scope.imag_1 = {nombre:"",archivo:"",nombre_archivo:""};
    $scope.imag_2 = {nombre:"",archivo:"",nombre_archivo:""};
    $scope.autopartes = {nombre:"",archivo:"",nombre_archivo:""};

    /*Datos del Bien Mueble*/
    $scope.bien_mueble = {
        /*Id Bien*/    // OJO: Solo para actualizar registro
        id_bien:0,
        /*Id Movimiento Ingresos */ // OJO: Solo para actualizar registro
        mov_ing_id:0,
        /*Id Valor Contable*/ //OJO: Solo para actualizar registro
        val_cont_id:0,
        
        /*Lista de Archivos*/ //OJO:Solo para actualizar Registro 
        doc_referencia:"",
        imag_1:"",
        imag_2:"",
        autopartes:"",
        /***************************/
        
        /*Cabecera*/
        amb_id:0,
        cat_bie_id:"",
        an_id:"",
        des_bie:"",
        cant:0,
        fec_reg:"",
        cod_int:"1",
        rut_doc_bie:"",
        cod_ba_bie:"",
        usu_mod:0,
        org_id:0,
        verificar:"N",
        /*Cuenta Contable*/
        valor_cont:0,
        act_dep:false,
        cod_cuenta:"",   
        /*Detalle Tecnico*/
        marc:"",
        mod:"",
        cond:"",
        dim:0,
        ser:"",
        col:"",
        tip:"",
        nro_mot:"",
        nro_pla:"",
        nro_cha:"",
        raza:"",
        edad:0,
        rut_imag_1:"",
        rut_imag_2:"",
        rut_aut_img:"",
        movimiento:{},
        actualizar_bien:false
    };
    /*Lista de Tipo de Anexo*/
    $scope.tipo_anexo={
        ane_id:"",
        ane_nom:""
    }
    
    /*Lista de Ambientes*/
    $scope.ambientes={
        amb_id:"",
        amb_des:""
    }
    /*Condicion*/
    $scope.condicion = [
        {cond_id:'B',cond_nom:'BUENO'},
        {cond_id:'M',cond_nom:'MALO'},
        {cond_id:'R',cond_nom:'REGULAR'}
    ];
    
    $scope.bien_mueble.cond = $scope.condicion[2];
    
    
    /*...................*/
    /*Variable bandera para saber si es un registro de insercion o actualizacion */
    var es_actualizar = false;
 
 
    /*Variable Bandera para elegir el item de catalogo de bien*/
    var editar_item_catalogo = false;
        
    /*Tabla del Catalogo de Bienes*/
    var params = {count: 5};
    var setting = { counts: []};
    $scope.tabla_catalogo = new NgTableParams(params, setting);
    
    /*Variable Bandera para el Control del Listado*/
    var flag_listas = 1;
    
    
    $scope.mostrarCatalogo = function(){
        
        var request = crud.crearRequest('catalogo_bienes',1,'listar_catalogo_bienes');
        crud.listar("/controlPatrimonial",request,function(data){
            if(data.data){
                flag_listas = flag_listas + 1; //4
                setting.dataset = data.data;
                iniciarPosiciones(setting.dataset);
                $scope.tabla_catalogo.settings(setting);
                $scope.tabla_catalogo.reload();
                $scope.listar_anexos(); 
            }
        },function(data){
            console.info(data);
        });
        
   }
   
   $scope.registrar_bien = function(usuario,orgId){
       
       /*Seleccionamos las Listas*/
       $scope.movimiento.tip_mov_ing=$scope.lista.movimiento.tip_mov_id; 
       $scope.bien_mueble.an_id = $scope.lista.anexo.ane_id;
       $scope.movimiento.con_pat_id = $scope.lista.cont_pat.cp_id;
       $scope.bien_mueble.amb_id = $scope.lista.ambiente.amb_id;
       $scope.bien_mueble.cond = $scope.lista.condicion.cond_id;
       
       /*Asignamos Valores de los Documentos Adjuntos*/
       $scope.bien_mueble.rut_doc_bie = $scope.doc_referencia.archivo;
       $scope.bien_mueble.rut_imag_1 =  $scope.imag_1.archivo;
       $scope.bien_mueble.rut_imag_2 =  $scope.imag_2.archivo;
       $scope.bien_mueble.rut_aut_img = $scope.autopartes.archivo;
       $scope.bien_mueble.org_id = orgId;
       $scope.bien_mueble.movimiento = $scope.movimiento;
       
       
       /*Asignamos los Nombres de los Archivos*/ //OJO: Solo para el caso de Actualizar el Registro
       $scope.bien_mueble.doc_referencia = $scope.doc_referencia.nombre_archivo;
       $scope.bien_mueble.imag_1 = $scope.imag_1.nombre_archivo;
       $scope.bien_mueble.imag_2 = $scope.imag_2.nombre_archivo;
       $scope.bien_mueble.autopartes = $scope.autopartes.nombre_archivo;
       
       
       /*Datos del Usuario*/
        $scope.bien_mueble.usu_mod = usuario.ID;
        
        if($scope.bien_mueble.actualizar_bien== false ){
            $scope.movimiento.fec_res = convertirFecha($scope.movimiento.fec_res);
        }

        /*Verificamos que todos lo campos esten llenados*/
        var accept = true;
        for(var atrib in $scope.bien_mueble){
            if($scope.bien_mueble[atrib]==""){
                if(atrib=="cod_ba_bie"){continue;}
                if(atrib=="id_bien"){continue;}
                if(atrib=="mov_ing_id"){continue;}
                if(atrib=="val_cont_id"){continue;}
                if(atrib=="nro_mot"){continue;}
                if(atrib=="nro_pla"){continue;}
                if(atrib=="nro_cha"){continue;}
                if(atrib=="raza"){continue;}
                if(atrib=="rut_imag_1"){continue;}
                if(atrib=="rut_imag_2"){continue;}
                if(atrib=="rut_aut_img"){continue;} 
                if(atrib=="act_dep"){continue;}
                if(atrib=="actualizar_bien"){continue;}
                if(atrib=="rut_doc_bie"){continue;}
                if(atrib=="rut_imag_1"){continue;}
                if(atrib=="rut_imag_2"){continue;}
                if(atrib=="rut_aut_img"){continue;}
                if(atrib=="doc_referencia"){continue;}
                if(atrib=="imag_1"){continue;}
                if(atrib=="imag_2"){continue;}
                if(atrib=="autopartes"){continue;}
                accept = false;
            }
        }
        
        if(accept==true){
            
            if($scope.bien_mueble.actualizar_bien == false){
                modal.mensajeConfirmacion($scope,"Seguro que desea registrar el Bien Mueble",function(){ 
                var request = crud.crearRequest('ingresos',1,'registrar_bien_mueble');
                request.setData($scope.bien_mueble);        
                crud.insertar("/controlPatrimonial",request,function(response){
                   modal.mensaje("CONFIRMACION",response.responseMsg);
                   if(response.responseSta){
                    //   $scope.area = response.data;}
                    $scope.borrar_valores_bien();   
                   //  $location.path(localStorage.getItem('origen'));    
                   }
                },function(data){
                   console.info(data);
                });

                 });
                
            }
            else{
                modal.mensajeConfirmacion($scope,"Seguro que desea Actualizar el Bien Mueble",function(){ 
                var request = crud.crearRequest('ingresos',1,'registrar_bien_mueble');
                request.setData($scope.bien_mueble);        
                crud.insertar("/controlPatrimonial",request,function(response){
                   modal.mensaje("CONFIRMACION",response.responseMsg);
                   if(response.responseSta){
                    
                    $scope.borrar_valores_bien();   
                    $location.path('verificacion_registros');    
                   }
                },function(data){
                   console.info(data);
                });

                 });
                
            }
                
           
        }else{
               modal.mensaje("ERROR AL REGISTRAR","Verifique que todos los campos esten llenados");
        }

   }
   
   $scope.listar_anexos = function(){
       
       var request = crud.crearRequest('configuracion_patrimonial',1,'listar_anexos');
       crud.listar("/controlPatrimonial",request,function(data){
       if(data.data){
            $scope.tipo_anexo = data.data;
            flag_listas = flag_listas + 1; //5
            
            $scope.listar_tipos_movimiento();
         }    
       },function(data){
            console.info(data);
        });      
   }

   
   $scope.listar_ambientes = function(orgId){
       var request = crud.crearRequest('ingresos',1,'listar_ambientes');
       request.setData({org_id:orgId});
       crud.listar("/controlPatrimonial",request,function(data){
        if(data.data){
            $scope.ambientes = data.data;
            flag_listas = flag_listas + 1; //3
        //    $scope.obtener_correlativo(orgId);
            $scope.mostrarCatalogo();
        }    
       },function(data){
            console.info(data);
        });    
 
   }
   
 
   $scope.ElegirCatalogo = function(cat){
       $scope.bien_mueble.cat_bie_id=cat.id_cat;
       editar_item_catalogo = true;
       var setting2 = { counts: []};
       /*Redimensionamos la tabla de catalogo de bienes*/
       var my_array = new Array();
       my_array[0] = cat;
       
        setting2.dataset = my_array;
        setting.dataset = setting2.dataset;
        iniciarPosiciones(setting.dataset);
        $scope.tabla_catalogo.settings(setting);
        $scope.tabla_catalogo.reload();
        
   }
   
   $scope.buscarCatalogo = function(IdCat){
       var size = setting.dataset.length
       for( var i=0 ; i<size ;i++)
       {
           if(setting.dataset[i].cat_bie_id == IdCat){
                editar_item_catalogo = true;
                var setting2 = { counts: []};
                /*Redimensionamos la tabla de catalogo de bienes*/
                var my_array = new Array();
                my_array[0] = setting.dataset[i];
                setting2.dataset = my_array;
                setting.dataset = setting2.dataset;
                iniciarPosiciones(setting.dataset);
                $scope.tabla_catalogo.settings(setting);
                $scope.tabla_catalogo.reload();
                break;
           }
       }
   }
   $scope.borrar_valores_bien = function(){
        $scope.bien_mueble = {
        /*Cabecera*/
        amb_id:0,
        cat_bie_id:"",
        an_id:"",
        des_bie:"",
        cant:0,
        fec_reg:"",
        cod_int:0,
        estado_bie:"",
        rut_doc_bie:"",
        cod_ba_bie:"",
        usu_mod:0,
        /*Cuenta Contable*/
        valor_cont:0,
        act_dep:false,
        cod_cuenta:"",   
        /*Detalle Tecnico*/
        marc:"",
        mod:"",
        cond:"",
        dim:0,
        ser:"",
        col:"",
        tip:"",
        nro_mot:"",
        nro_pla:"",
        nro_cha:"",
        raza:"",
        edad:0,
        rut_imag_1:"",
        rut_imag_2:"",
        rut_aut_img:""
    };
    $scope.mostrarCatalogo();
 
   }
   
   
   
  // $scope.obtener_correlativo_bien = function(){
      
  // }
        
  $scope.listar_tipo_movimiento = function(){
      
      
      
      
  }  
  
   $scope.listar_controles_patrimoniales = function(orgId){
        
        var request = crud.crearRequest('configuracion_patrimonial',1,'listar_control_patrimonial');
        request.setData({org_id:orgId});
        crud.listar("/controlPatrimonial",request,function(data){
            if(data.data){
                
                $scope.controles_patrimoniales = data.data;
                flag_listas = flag_listas + 1; //2
                $scope.listar_ambientes(orgId);
                
            }
        },function(data){
            console.info(data);
        });
        
        
    }
    
    $scope.validar_bien = function(){
        $('#modalvalidarbien').modal('show');
    }
    
    $scope.listar_causal = function(){
        
        if($scope.tipo_validacion.id == 1){ /*Altas*/
            
            var request = crud.crearRequest('ingresos',1,'listar_causal_alta');
            crud.listar("/controlPatrimonial",request,function(data){
            $scope.tipo_causal = data.data;    
            
            },function(data){
              console.info(data);
            });
          
        }else if($scope.tipo_validacion.id == 2){ /*Bajas*/
            
            var request = crud.crearRequest('ingresos',1,'listar_causal_baja');
            crud.listar("/controlPatrimonial",request,function(data){
            $scope.tipo_causal = data.data;    
            
            },function(data){
              console.info(data);
            });
            
            
        }else if($scope.tipo_validacion.id == 3){ /*Salidas*/

        }
    }
    
    $scope.buscar_bien = function(id_bien,orgId){

            flag_listas = 1;
             var request = crud.crearRequest('ingresos',1,'obtener_bien');
                request.setData({id_bien:id_bien});
                crud.listar("/controlPatrimonial",request,function(data){
                    if(data.data){
                          /*Cabecera*/
                        /*Obtenemos el Ambiente*/ 
                        $scope.obtenerAmbiente(data.data[0].amb_id);  
                        //$scope.bien_mueble.amb_id=data.data[0].amb_id;

                        /*Redimensionamos la Tabla de Catalogo de Bienes*/
                        $scope.bien_mueble.cat_bie_id=(data.data)[0].cat_bie_id;
                        $scope.buscarCatalogo($scope.bien_mueble.cat_bie_id);

                        /*Seleccionamos el Anexo en la Lista*/
                        $scope.obtenerAnexo(data.data[0].an_id);
                        /****************************/

                        $scope.bien_mueble.des_bie=(data.data)[0].des_bie;
                        $scope.bien_mueble.cant=data.data[0].cant;

                        /*Obtenemos el formato de la Fecha */
                        fec_reg = new Date(data.data[0].fec_reg)
                        $scope.bien_mueble.fec_reg=fec_reg;
                        /***********************************/

                        $scope.bien_mueble.cod_int=data.data[0].cod_int;
                    //    $scope.bien_mueble.rut_doc_bie=data.data[0].rut_doc_bie;
                        $scope.doc_referencia.nombre_archivo = data.data[0].doc_ref;
                    
                        $scope.bien_mueble.cod_ba_bie=data.data[0].cod_ba_bie;
                        $scope.bien_mueble.usu_mod=data.data[0].usu_mod;
                        /*Cuenta Contable*/
                        $scope.bien_mueble.valor_cont=data.data[0].valor_cont;
                        if(data.data[0].act_dep=='A'){$scope.bien_mueble.act_dep=true;}
                        else{$scope.bien_mueble.act_dep=false;}

                        $scope.bien_mueble.cod_cuenta=data.data[0].cod_cuenta;  
                        /*Detalle Tecnico*/
                        $scope.bien_mueble.marc=data.data[0].marc;
                        $scope.bien_mueble.mod=data.data[0].mod;
                        /*Seleccionamos la Condicion*/
                        $scope.lista.condicion=$scope.condicion[data.data[0].cond];

                        $scope.bien_mueble.dim=data.data[0].dim;
                        $scope.bien_mueble.ser=data.data[0].ser;
                        $scope.bien_mueble.col=data.data[0].col;
                        $scope.bien_mueble.tip=data.data[0].tip;
                        $scope.bien_mueble.nro_mot=data.data[0].nro_mot;
                        $scope.bien_mueble.nro_pla=data.data[0].nro_pla;
                        $scope.bien_mueble.nro_cha=data.data[0].nro_cha;
                        $scope.bien_mueble.raza=data.data[0].raza;
                        $scope.bien_mueble.edad=data.data[0].edad;
     
                    //    $scope.bien_mueble.rut_imag_1=data.data[0].rut_imag_1;
                        $scope.imag_1.nombre_archivo = data.data[0].rut_imag_1;
                    //    $scope.bien_mueble.rut_imag_2=data.data[0].rut_imag_2;
                        $scope.imag_2.nombre_archivo =  data.data[0].rut_imag_2; 
                    //    $scope.bien_mueble.rut_aut_img=data.data[0].rut_aut_img;
                        $scope.autopartes.nombre_archivo = data.data[0].rut_aut_img;
                        /*Movimiento*/
                        $scope.obtenerTipMovIng(data.data[0].tip_mov_ing);
                        /*Obtener el Control Patrimonial*/
                        $scope.obtenerControl(data.data[0].con_pat_id);

                        $scope.movimiento.num_res =data.data[0].num_res;
                        /*Obtenemos el Formato de la Fecha de Resolucion*/
                        fec_res = new Date(data.data[0].fec_res)
                        $scope.movimiento.fec_res =fec_res;
                        /******************************************/

                        $scope.movimiento.obs =data.data[0].obs;
                        $scope.bien_mueble.id_bien = data.data[0].id_bien;
                        $scope.bien_mueble.mov_ing_id = data.data[0].mov_ing_id;
                        $scope.bien_mueble.val_cont_id = data.data[0].val_cont_id;

                        $scope.bien_mueble.actualizar_bien = true;

                        $scope.accion_bien = "Actualizar Bien";
                    }
                },function(data){
                    console.info(data);
                }); 
   
    }
    
    
    $scope.update_bien = function(){
        
        $scope.bien_mueble_update = JSON.parse( window.atob( localStorage.getItem('update_bien_mueble')) ); 
        if(flag_listas == 6)
        {
            
             $scope.buscar_bien($scope.bien_mueble_update.id_bien,$scope.bien_mueble_update.org_id); 
             localStorage.removeItem('update_bien_mueble');
        }
    }
    
    
    
    $scope.listar_tipos_movimiento = function(){
          var request = crud.crearRequest('ingresos',1,'listar_tipo_movimiento');
            crud.listar("/controlPatrimonial",request,function(data){    
           if(data.data){
               $scope.movimientos_ingreso = data.data;
               flag_listas = flag_listas + 1; //6
               if(flag_listas == 6 && $scope.bien_mueble_update.interfaz == "verificacion_registros")
                {
                    $scope.buscar_bien($scope.bien_mueble_update.id_bien,$scope.bien_mueble_update.org_id); 
                    localStorage.removeItem('update_bien_mueble');
                }
           }     
           },function(data){
            console.info(data);
        }); 
    }

    $scope.obtenerTipMovIng = function(TipMovId){
        
        var size = $scope.movimientos_ingreso.length;
        for(var i =0 ; i< size ; i++){
            if($scope.movimientos_ingreso[i].tip_mov_id == TipMovId){
                $scope.lista.movimiento = $scope.movimientos_ingreso[i];
                break;
            }
        }
    }
    
    $scope.obtenerAnexo = function(AnId){
        
        var size = $scope.tipo_anexo.length;
        for(var i=0;i<size;i++){
            if($scope.tipo_anexo[i].ane_id== AnId){
                $scope.lista.anexo = $scope.tipo_anexo[i];
                break;
            }
        }
    }
    
    $scope.obtenerAmbiente = function(AmbId){
        
        var size = $scope.ambientes.length;
        for(var i =0; i<size ;i++){
            if($scope.ambientes[i].amb_id == AmbId){
                $scope.lista.ambiente = $scope.ambientes[i];
                break;
            }
        }
        
    }
    
    $scope.obtenerControl = function(cpId){
        
        var size = $scope.controles_patrimoniales.length;
        for(var i = 0 ; i< size ; i++){
            if($scope.controles_patrimoniales[i].cp_id == cpId){
                $scope.lista.cont_pat = $scope.controles_patrimoniales[i];
                break;
            }
        }
    }
  
    $scope.actualizar_bien = function(orgId,user){
        
        $scope.bien_mueble.org_id = orgId;
        $scope.bien_mueble.usu_mod = user;
        $scope.bien_mueble.causal_id=$scope.tipo_validacion.causal_id;
        
        if($scope.tipo_validacion.causal_id == 0){
              modal.mensaje("ERROR : Debe elegir un tipo de Causal");
        }
        else{
            if($scope.tipo_validacion.id == 1){ /*REGISTRAMOS LAS ALTAS*/
                    modal.mensajeConfirmacion($scope,"Seguro que desea dar de Alta al Bien Mueble ?",function(){ 
                    var request = crud.crearRequest('ingresos',1,'registrar_altas');
                    request.setData($scope.bien_mueble);        
                    crud.insertar("/controlPatrimonial",request,function(response){
                    modal.mensaje("CONFIRMACION",response.responseMsg);
                    if(response.responseSta){
                       
                       
                    }
                     });
                    });
            }
            if($scope.tipo_validacion.id == 2){ /*REGISTRAMOS LAS BAJAS*/
                    modal.mensajeConfirmacion($scope,"Seguro que desea dar de Baja el Bien Mueble ?",function(){ 
                    var request = crud.crearRequest('salidas',1,'registrar_baja');
                    request.setData($scope.bien_mueble);        
                    crud.insertar("/controlPatrimonial",request,function(response){
                    modal.mensaje("CONFIRMACION",response.responseMsg);
                    if(response.responseSta){
                       
                       
                    }
                     });
                    });
                
                
            }
            if($scope.tipo_validacion.id == 3){ /*REGISTRAMOS LAS SALIDAS*/
                
                
                
                
            }

        }
    }
    
    $scope.verificar_codigo_barras = function(){
        
        var cod_bar = $scope.codigo_barras;
        
        if(cod_bar.length>=9){
            $scope.codigo_barras="";
        }
        
      
    }
    
    $scope.obtener_correlativo = function(org_id){
        var request = crud.crearRequest('ingresos',1,'obtener_correlativo');
          request.setData({org_id:org_id});
        crud.listar("/controlPatrimonial",request,function(data){    
            if(data.data){
                $scope.registrar_correlativo(data.data.num_corr);    
            }
        });
    }
    
    $scope.registrar_correlativo = function(num_corr){
        var nuevo_corr = num_corr + 1 ;
        
        var str_corr ;
        var size_corr = str_corr.length;
      
        for(var i=0 ; i<6-size_corr;i++){
            str_corr = str_corr + "0";
        }
        str_corr = str_corr + nuevo_corr.toString();
        $scope.bien_mueble.cod_int = str_corr;
       
    }
       
        
        
        
}]);


