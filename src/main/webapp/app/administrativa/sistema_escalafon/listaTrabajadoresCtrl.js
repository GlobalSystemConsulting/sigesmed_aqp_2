app.requires.push('angularModalService');
app.requires.push('ngAnimate');
var seApp = angular.module('app');
seApp.config(['$routeProvider',function($routeProvider){
    $routeProvider.when('/actualizar_ficha_escalafonaria/:data',{
            templateUrl:'administrativa/sistema_escalafon/actualizarFichaEscalafonaria.html',
            controller:'actualizarFichaEscalafonariaCtrl',
            controllerAs:'actualizarFichaCtrl'
        }).when('/legajo_personal/:ficha',{
            templateUrl:'administrativa/sistema_escalafon/verLegajoPersonal.html',
            controller:'verLegajoPersonalCtrl',
            controllerAs:'verLegajoCtrl'
        });
}]);
app.controller("listaTrabajadoresCtrl", ["$location", "$rootScope", "$scope", "NgTableParams", "crud", "modal", "ModalService", function ($location, $rootScope, $scope, NgTableParams, crud, modal, ModalService) {

//arreglo donde estan todas las cuentas contables
        $scope.trabajadores = [];
        //variable que servira para crear una nueva cuenta contable
        $scope.trabajadorSel = {};
        //Variables para manejo de la tabla
        $rootScope.paramsTrabajadores = {count: 10};
        $rootScope.settingTrabajadores = {counts: []};
        $rootScope.tablaTrabajadores = new NgTableParams($rootScope.paramsTrabajadores, $rootScope.settingTrabajadores);
        $scope.listarTrabajadores = function () {
            //preparamos un objeto request
            var request = crud.crearRequest('datos_personales', 1, 'listarPorOrganizacion');
            request.setData({orgId: $rootScope.usuMaster.organizacion.organizacionID});
            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request        
            crud.listar("/sistema_escalafon", request, function (data) {
                $rootScope.settingTrabajadores.dataset = data.data;
                //asignando la posicion en el arreglo a cada objeto
                iniciarPosiciones($rootScope.settingTrabajadores.dataset);
                $rootScope.tablaTrabajadores.settings($rootScope.settingTrabajadores);
            }, function (data) {
                console.info(data);
            });
        };
        
        $scope.prepararModificarFichaEscalafonaria = function (t) {
            var trabajadorSel = JSON.parse(JSON.stringify(t));
            trabajadorSel.opcion = 1;
            console.log(trabajadorSel);
            var request = crud.crearRequest('datos_personales',1,'buscarFichaPorDNI');
            request.setData(trabajadorSel);
            crud.listar('/sistema_escalafon',request,function(response){
                if(response.responseSta){
                    console.log(response.data);
                    $location.url('/actualizar_ficha_escalafonaria/'+btoa(JSON.stringify(response.data)));
                }else{
                    modal.mensaje('ERROR',response.responseMsg);
                }
            },function (data) {
                console.info(data);
            });
        };

        $scope.prepararVerLegajoPersonal = function (t) {

            var trabajadorSel = JSON.parse(JSON.stringify(t));
            trabajadorSel.opcion = 200;
            var request = crud.crearRequest('datos_personales',1,'buscarFichaPorDNI');
            request.setData(trabajadorSel);
            crud.listar('/sistema_escalafon',request,function(response){
                if(response.responseSta){
                    $location.url('/legajo_personal/'+btoa(JSON.stringify(response.data)));
                }else{
                    modal.mensaje('ERROR',response.responseMsg);
                }
            },function (data) {
                console.info(data);
            });
        };
        
        $scope.verFichaEscalafonaria = function(t){
            var trabajadorSel = JSON.parse(JSON.stringify(t));
                
            var request = crud.crearRequest('reportes',1,'reporteFichaEscalafonaria');
            request.setData(trabajadorSel);
            crud.insertar('/sistema_escalafon',request,function(response){
                if(response.responseSta){
                    verDocumento(response.data.file);
                }else{
                    modal.mensaje('ERROR',response.responseMsg);
                }
            },function(errResponse){
                modal.mensaje('ERROR','El servidor no responde');
            }); 
        };
        
        $scope.eliminarTrabajador = function(t){   
            modal.mensajeConfirmacion($scope, "¿Seguro que desea eliminar el trabajador?", function () {
                var request = crud.crearRequest('datos_personales', 1, 'eliminarTrabajador');
                request.setData({traId: t.traId});
                
                crud.eliminar('/sistema_escalafon', request, function (response) {
                    if (response.responseSta) {
                        _.remove($rootScope.tablaTrabajadores.settings().dataset, function(item){
                            return t === item;
                        });
                        $rootScope.tablaTrabajadores.reload();
                        modal.mensaje("CONFIRMACION", "La eliminación se realizó correctamente");
                    }else {
                            modal.mensaje("ERROR",response.responseMsg);
                    }
                }, function (error) {
                    modal.mensaje("ERROR","El servidor no responde");
                });
            }, '400'); 
        };
    }]);

app.controller('actualizarFichaEscalafonariaCtrl', ['$routeParams', '$scope', '$rootScope', '$http', 'NgTableParams', 'crud','modal', 'ModalService', function ($routeParams, $scope, $rootScope, $http, NgTableParams, crud, modal, ModalService) {
        
        
        //Variables y funciones para controlar la vista
        $scope.optionsDatosPersonales = [true, false, false, false, false];
        $scope.optionsDatosAcademicos = [true, false, false, false, false, false];
        $scope.optionsExperienciaLaboral = [true, false, false];
        
        $scope.updateOptions = function (numOption, optionsSet) {
            for (var i = 0; i < optionsSet.length; i += 1) {
               optionsSet[i] = false;
            }
           optionsSet[numOption] = true;
        };
        
        $scope.status_1 = {close: true, statusGD: false, statusLD: false};
        $scope.changeDiscapacidadSi = function () {
            $scope.fichaEscalafonariaSel.perDis = true;
        };
        $scope.changeDiscapacidadNo = function () {
            $scope.fichaEscalafonariaSel.perDis = false;
        };
        
        $scope.statusDA = false;
        $scope.changeDASi = function () {
            $scope.statusDA = false;
        };
        $scope.changeDANo = function () {
            $scope.statusDA = true;
        };
        
        function changeAseguradoState(codAutEss) {
            if(codAutEss !== "               ")
                $scope.asegurado = "SI";
            else
                $scope.asegurado = "NO";
        };
        
        function changeSistemaPensionesState(sisPen) {
            if(sisPen === "ONP")
                $scope.optionsSP = "NO";
            else
                $scope.optionsSP = "SI";
        };
        
        function changeDishabilityState(perDis) {
            if(perDis === true)
                $scope.discapacidad = "SI";
            else
                $scope.discapacidad = "NO";
        };

        $scope.parsearBooleano = function (i) {
            var o = "";
            if (i) {
                o = "Si";
            } else{
                o = "No";
            }
            return o;
        };
        
        $scope.parsearBooleano2 = function (i) {
            var o = "";
            if (i) {
                o = "Habilitado";
            } else{
                o = "No Habilitado";
            }
            return o;
        };
        
        $scope.sexo = [{id: "F", title: "Femenino"}, {id: "M", title: "Masculino"}];
        $scope.estadoCivil = [{id: "S", title: "Soltero(a)"}, {id: "C", title: "Casado(a)"}];
        $scope.tipoTrabajador = [{id: "N", title: "Nombrado"}, {id: "V", title: "Contratado"}];
        $scope.grupoOcupacional = [{id: "A", title: "Administrativo"}, {id: "D", title: "Docente"}];
        
        $rootScope.paramsModParientes = {count: 10};
        $rootScope.settingModParientes = {counts: []};
        $rootScope.tablaModParientes = new NgTableParams($rootScope.paramsModParientes, $rootScope.settingModParientes);
        
        $rootScope.paramsModForEdu = {count: 10};
        $rootScope.settingModForEdu  = {counts: []};
        $rootScope.tablaModForEdu  = new NgTableParams($rootScope.paramsModForEdu, $rootScope.settingModForEdu);
        
        $rootScope.paramsModEstCom = {count: 10};
        $rootScope.settingModEstCom  = {counts: []};
        $rootScope.tablaModEstCom  = new NgTableParams($rootScope.paramsModEstCom, $rootScope.settingModEstCom);
        
        $rootScope.paramsModExpPon = {count: 10};
        $rootScope.settingModExpPon  = {counts: []};
        $rootScope.tablaModExpPon  = new NgTableParams($rootScope.paramsModExpPon, $rootScope.settingModExpPon);
        
        $rootScope.paramsModPublicaciones = {count: 10};
        $rootScope.settingModPublicaciones  = {counts: []};
        $rootScope.tablaModPublicaciones = new NgTableParams($rootScope.paramsModPublicaciones, $rootScope.settingModPublicaciones);
        
        $rootScope.paramsModDesplazamientos = {count: 10};
        $rootScope.settingModDesplazamientos  = {counts: []};
        $rootScope.tablaModDesplazamientos= new NgTableParams($rootScope.paramsModDesplazamientos, $rootScope.settingModDesplazamientos);

        $rootScope.paramsModColegiaturas = {count: 10};
        $rootScope.settingModColegiaturas  = {counts: []};
        $rootScope.tablaModColegiaturas = new NgTableParams($rootScope.paramsModModColegiaturas, $rootScope.settingcColegiaturas);
        
        $rootScope.paramsModAscensos = {count: 10};
        $rootScope.settingModAscensos  = {counts: []};
        $rootScope.tablaModAscensos = new NgTableParams($rootScope.paramModsAscensos, $rootScope.settingModAscensos);
        
        $rootScope.paramsModCapacitaciones = {count: 10};
        $rootScope.settingModCapacitaciones  = {counts: []};
        $rootScope.tablaModCapacitaciones = new NgTableParams($rootScope.paramsModCapacitaciones, $rootScope.settingModCapacitaciones);
        
        $rootScope.paramsModReconocimientos= {count: 10};
        $rootScope.settingModReconocimientos  = {counts: []};
        $rootScope.tablaModReconocimientos = new NgTableParams($rootScope.paramsModReconocimientos, $rootScope.settingModReconocimientos);
        
        $rootScope.paramsModEstPos = {count: 10};
        $rootScope.settingModEstPos  = {counts: []};
        $rootScope.tablaModEstPos = new NgTableParams($rootScope.paramsModEstPos, $rootScope.settingModEstPos);
        
        $rootScope.paramsModDemeritos = {count: 10};
        $rootScope.settingModDemeritos  = {counts: []};
        $rootScope.tablaModDemeritos = new NgTableParams($rootScope.paramsModDemeritos, $rootScope.settingModDemeritos);
        
        var datosPersonales = JSON.parse(atob($routeParams.data));

        $scope.personaSel = {
            perId: datosPersonales.persona.perId, 
            apePat: datosPersonales.persona.apePat, 
            apeMat: datosPersonales.persona.apeMat, 
            nom: datosPersonales.persona.nom, 
            dni: datosPersonales.persona.dni, 
            fecNac: new Date(datosPersonales.persona.fecNac), 
            num1: datosPersonales.persona.num1, 
            num2: datosPersonales.persona.num2, 
            fij: datosPersonales.persona.fij, 
            email: datosPersonales.persona.email, 
            sex: datosPersonales.persona.sex, 
            estCiv: datosPersonales.persona.estCiv, 
            depNac: datosPersonales.persona.depNac, 
            proNac: datosPersonales.persona.proNac, 
            disNac: datosPersonales.persona.disNac, 
            estado: 'A'
        };
        
        $scope.direccionDNISel = {
            tipDir: "R", 
            nomDir: "", 
            depDir: "", 
            proDir: "", 
            disDir: "", 
            nomZon: "", 
            desRef: ""
        };
        $scope.direccionDASel = {
            tipDir: "A", 
            nomDir: "", 
            depDir: "", 
            proDir: "", 
            disDir: "", 
            nomZon: "", 
            desRef: ""
        };
        
        var direcciones = [];
        function listarDirecciones(){
            var request = crud.crearRequest('datos_personales',1,'listarDirecciones');
            request.setData({perId:datosPersonales.persona.perId});
            crud.listar('/sistema_escalafon',request,function(response){
                direcciones = response.data;
                changeOptionsDirectionsState();
            },function (data) {
                console.info(data);
            });
        }
        function changeOptionsDirectionsState(){
            switch(direcciones.length){
                case 1: $scope.optionsDirections = "SI";
                        $scope.status_1.statusGD = true;
                        $scope.direccionDNISel.dirId = direcciones[0].dirId;
                        $scope.direccionDNISel.dirId = direcciones[0].dirId;
                        $scope.direccionDNISel.tipDir = direcciones[0].tipDir; 
                        $scope.direccionDNISel.nomDir = direcciones[0].nomDir; 
                        $scope.direccionDNISel.depDir =  direcciones[0].depDir; 
                        $scope.direccionDNISel.proDir = direcciones[0].proDir; 
                        $scope.direccionDNISel.disDir = direcciones[0].disDir; 
                        $scope.direccionDNISel.nomZon = direcciones[0].nomZon;
                        $scope.direccionDNISel.desRef = direcciones[0].desRef;
                    break;
                    
                case 2: $scope.optionsDirections = "NO";
                        $scope.status_1.statusGD = true;
                        $scope.status_1.statusLD = true;
                        $scope.direccionDNISel.dirId = direcciones[0].dirId;
                        $scope.direccionDNISel.dirId = direcciones[0].dirId;
                        $scope.direccionDNISel.tipDir = direcciones[0].tipDir; 
                        $scope.direccionDNISel.nomDir = direcciones[0].nomDir; 
                        $scope.direccionDNISel.depDir = direcciones[0].depDir; 
                        $scope.direccionDNISel.proDir = direcciones[0].proDir; 
                        $scope.direccionDNISel.disDir = direcciones[0].disDir; 
                        $scope.direccionDNISel.nomZon = direcciones[0].nomZon;
                        $scope.direccionDNISel.desRef = direcciones[0].desRef;
                        
                        $scope.direccionDASel.dirId = direcciones[1].dirId;
                        $scope.direccionDASel.dirId = direcciones[1].dirId;
                        $scope.direccionDASel.tipDir = direcciones[1].tipDir; 
                        $scope.direccionDASel.nomDir = direcciones[1].nomDir; 
                        $scope.direccionDASel.depDir = direcciones[1].depDir; 
                        $scope.direccionDASel.proDir = direcciones[1].proDir; 
                        $scope.direccionDASel.disDir = direcciones[1].disDir; 
                        $scope.direccionDASel.nomZon = direcciones[1].nomZon;
                        $scope.direccionDASel.desRef = direcciones[1].desRef;
                    break;
            }
        }
        listarDirecciones();
        
        $scope.trabajadorSel = {
            traId: datosPersonales.trabajador.traId,
            traCon: datosPersonales.trabajador.traCon 
        };
        $scope.fichaEscalafonariaSel = {
            ficEscId: datosPersonales.ficha.ficEscId, 
            autEss: datosPersonales.ficha.autEss, 
            sisPen: datosPersonales.ficha.sisPen,
            nomAfp: datosPersonales.ficha.nomAfp,
            codCuspp: datosPersonales.ficha.codCuspp,
            fecIngAfp: new Date(datosPersonales.ficha.fecIngAfp),
            perDis: datosPersonales.ficha.perDis, 
            regCon: datosPersonales.ficha.regCon,
            gruOcu: datosPersonales.ficha.gruOcu
        };
        
        changeAseguradoState($scope.fichaEscalafonariaSel.autEss);
        changeSistemaPensionesState($scope.fichaEscalafonariaSel.sisPen);
        changeDishabilityState($scope.fichaEscalafonariaSel.perDis);
        
        var tiposFormacion = [
            {id: "1", title: "Estudios Basicos Regulares"}, 
            {id: "2", title: "Estudios Tecnicos"}, 
            {id: "3", title: "Bachiller"}, 
            {id: "4", title: "Universitario"}, 
            {id: "5", title: "Licenciatura"}, 
            {id: "6", title: "Maestria"}, 
            {id: "7", title: "Doctorado"}, 
            {id: "8", title: "Otros"}
        ];
        
        var tiposEstCom = [
            {id: "1", title: "Informatica"}, 
            {id: "2", title: "Idiomas"}, 
            {id: "3", title: "Certificación"},
            {id: "4", title: "Diplomado"},
            {id: "5", title: "Especialización"}, 
            {id: "6", title: "Otros"}
        ];
        
        var niveles = [
            {id: "N", title: "Ninguno"}, 
            {id: "B", title: "Básico"}, 
            {id: "I", title: "Intermedio"},
            {id: "A", title: "Avanzado"}
        ];
        
        var tiposEstPos = [{id: "1", title: "Maestria"}, {id: "2", title: "Doctorado"}];
   
        var tiposDesplazamientos = [
            {id: "1", title: "Designación"},
            {id: "2", title: "Rotación"},
            {id: "3", title: "Reasignación"}, 
            {id: "4", title: "Destaque"}, 
            {id: "5", title: "Permuta"}, 
            {id: "6", title: "Encargo"},
            {id: "7", title: "Comisión de servicio"}, 
            {id: "8", title: "Transferencia"}, 
            {id: "9", title: "Otros"}
        ];
        
        var motivos = [
            {id: "1", title: "Mérito"},
            {id: "2", title: "Felicitación"},
            {id: "3", title: "25 años de servicio"}, 
            {id: "4", title: "30 años de servicio"}, 
            {id: "5", title: "Luto y sepelio"},
            {id: "6", title: "Otros"}
        ];
        
        $http.get('../recursos/json/departamentos.json').success(function (data) {
            $scope.departamentosLugNac = data;
            $scope.departamentosDirDNI = data;
            $scope.departamentosDirDA = data;
        });

        $scope.loadProLugNac = function () {
            $http.get('../recursos/json/provincias.json').success(function (data) {
                var dep = $scope.departamentosLugNac.filter(function (obj) {
                    return obj.codigo_ubigeo === $scope.personaSel.depNac;
                })[0];
                $scope.provinciasLugNac = data[dep.id_ubigeo];
                $scope.distritosLugNac = [];
            });
        };

        $scope.loadDisLugNac = function () {
            $http.get('../recursos/json/distritos.json').success(function (data) {
                var pro = $scope.provinciasLugNac.filter(function (obj) {
                    return obj.codigo_ubigeo === $scope.personaSel.proNac;
                })[0];
                $scope.distritosLugNac = data[pro.id_ubigeo];
            });
        };

        $scope.loadProDirDNI = function () {
            $http.get('../recursos/json/provincias.json').success(function (data) {
                var dep = $scope.departamentosDirDNI.filter(function (obj) {
                    return obj.codigo_ubigeo === $scope.direccionDNISel.depDir;
                })[0];
                $scope.provinciasDirDNI = data[dep.id_ubigeo];
                $scope.distritosDirDNI = [];
            });
        };

        $scope.loadDisDirDNI = function () {
            $http.get('../recursos/json/distritos.json').success(function (data) {
                var pro = $scope.provinciasDirDNI.filter(function (obj) {
                    return obj.codigo_ubigeo === $scope.direccionDNISel.proDir;
                })[0];
                $scope.distritosDirDNI = data[pro.id_ubigeo];
            });
        };

        $scope.loadProDirDA = function () {
            $http.get('../recursos/json/provincias.json').success(function (data) {
                var dep = $scope.departamentosDirDA.filter(function (obj) {
                    return obj.codigo_ubigeo === $scope.direccionDASel.depDir;
                })[0];
                $scope.provinciasDirDA = data[dep.id_ubigeo];
                $scope.distritosDirDA = [];
            });
        };

        $scope.loadDisDirDA = function () {
            $http.get('../recursos/json/distritos.json').success(function (data) {
                var pro = $scope.provinciasDirDA.filter(function (obj) {
                    return obj.codigo_ubigeo === $scope.direccionDASel.proDir;
                })[0];
                $scope.distritosDirDA = data[pro.id_ubigeo];
            });
        };
       
        //Funciones para listas los datos de tablas
        $scope.listarParientes = function () {
            //preparamos un objeto request
            var request = crud.crearRequest('familiares', 1, 'listarParientes');
            request.setData($scope.trabajadorSel);
            
            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request        
            crud.listar("/sistema_escalafon", request, function (data) {
                $rootScope.settingModParientes.dataset = data.data;
                //asignando la posicion en el arreglo a cada objeto
                iniciarPosiciones($rootScope.settingModParientes.dataset);
                $rootScope.tablaModParientes.settings($rootScope.settingModParientes);
            }, function (data) {
                console.info(data);
            });
        };
        
        $scope.listarFormacionesEducativas = function () {
            //preparamos un objeto request
            var request = crud.crearRequest('formacion_educativa', 1, 'listarFormacionesEducativas');
            request.setData($scope.fichaEscalafonariaSel);
            
            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request        
            crud.listar("/sistema_escalafon", request, function (data) {
                data.data.forEach(function (item){
                    item.estConDes = item.estCon?"Si":"No";
                    item.tipDes = buscarContenido(tiposFormacion, 'id', 'title', item.tip);
                });
                $rootScope.settingModForEdu.dataset = data.data;
                //asignando la posicion en el arreglo a cada objeto
                iniciarPosiciones($rootScope.settingModForEdu.dataset);
                $rootScope.tablaModForEdu.settings($rootScope.settingModForEdu);
            }, function (data) {
                console.info(data);
            });
        };
        
        $scope.listarColegiaturas = function () {
            //preparamos un objeto request
            var request = crud.crearRequest('colegiatura', 1, 'listarColegiaturas');
            request.setData($scope.fichaEscalafonariaSel);
            
            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request        
            crud.listar("/sistema_escalafon", request, function (data) {
                data.data.forEach(function (item){
                    item.conRegDes = item.conReg?"Habilitado":"No habilitado";
                });
                $rootScope.settingModColegiaturas.dataset = data.data;
                //asignando la posicion en el arreglo a cada objeto
                iniciarPosiciones($rootScope.settingModColegiaturas.dataset);
                $rootScope.tablaModColegiaturas.settings($rootScope.settingModColegiaturas);
            }, function (data) {
                console.info(data);
            });
        };

        $scope.listarEstudiosComplementarios = function () {
            //preparamos un objeto request
            var request = crud.crearRequest('estudio_complementario', 1, 'listarEstudiosComplementarios');
            request.setData($scope.fichaEscalafonariaSel);
            
            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request        
            crud.listar("/sistema_escalafon", request, function (data) {
                data.data.forEach(function (item){
                    item.tipDes = buscarContenido(tiposEstCom, 'id', 'title', item.tip);
                    item.nivDes = buscarContenido(niveles, 'id', 'title', item.niv);
                });
                $rootScope.settingModEstCom.dataset = data.data;
                //asignando la posicion en el arreglo a cada objeto
                iniciarPosiciones($rootScope.settingModEstCom.dataset);
                $rootScope.tablaModEstCom.settings($rootScope.settingModEstCom);
            }, function (data) {
                console.info(data);
            });
        };
        
        $scope.listarEstudiosPostgrado = function () {
            //preparamos un objeto request
            var request = crud.crearRequest('estudio_postgrado', 1, 'listarEstudiosPostgrado');
            request.setData($scope.fichaEscalafonariaSel);
            
            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request        
            crud.listar("/sistema_escalafon", request, function (data) {
                data.data.forEach(function (item){
                    item.tipDes = buscarContenido(tiposEstPos, 'id', 'title', item.tip);
                });
                $rootScope.settingModEstPos.dataset = data.data;
                //asignando la posicion en el arreglo a cada objeto
                iniciarPosiciones($rootScope.settingModEstPos.dataset);
                $rootScope.tablaModEstPos.settings($rootScope.settingModEstPos);
            }, function (data) {
                console.info(data);
            });
        };
        
        $scope.listarExposiciones = function () {
            //preparamos un objeto request
            var request = crud.crearRequest('exposicion_ponencia', 1, 'listarExposiciones');
            request.setData($scope.fichaEscalafonariaSel);
            
            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request        
            crud.listar("/sistema_escalafon", request, function (data) {
                $rootScope.settingModExpPon.dataset = data.data;
                //asignando la posicion en el arreglo a cada objeto
                iniciarPosiciones($rootScope.settingModExpPon.dataset);
                $rootScope.tablaModExpPon.settings($rootScope.settingModExpPon);
            }, function (data) {
                console.info(data);
            });
        };
        
        $scope.listarPublicaciones = function () {
            //preparamos un objeto request
            var request = crud.crearRequest('publicacion', 1, 'listarPublicaciones');
            request.setData($scope.fichaEscalafonariaSel);
            
            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request        
            crud.listar("/sistema_escalafon", request, function (data) {
                $rootScope.settingModPublicaciones.dataset = data.data;
                //asignando la posicion en el arreglo a cada objeto
                iniciarPosiciones($rootScope.settingModPublicaciones.dataset);
                $rootScope.tablaModPublicaciones.settings($rootScope.settingModPublicaciones);
            }, function (data) {
                console.info(data);
            });
        };
        
        $scope.listarDesplazamientos = function () {
            //preparamos un objeto request
            var request = crud.crearRequest('desplazamiento', 1, 'listarDesplazamientos');
            request.setData($scope.fichaEscalafonariaSel);
            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request        
            crud.listar("/sistema_escalafon", request, function (data) {
                data.data.forEach(function (item){
                    item.tipDes = buscarContenido(tiposDesplazamientos, 'id', 'title', item.tip);
                });
                $rootScope.settingModDesplazamientos.dataset = data.data;
                //asignando la posicion en el arreglo a cada objeto
                iniciarPosiciones($rootScope.settingModDesplazamientos.dataset);
                $rootScope.tablaModDesplazamientos.settings($rootScope.settingModDesplazamientos);
            }, function (data) {
                console.info(data);
            });
        };
        
        $scope.listarAscensos = function () {
            //preparamos un objeto request
            var request = crud.crearRequest('ascenso', 1, 'listarAscensos');
            request.setData($scope.fichaEscalafonariaSel);
            
            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request        
            crud.listar("/sistema_escalafon", request, function (data) {
                $rootScope.settingModAscensos.dataset = data.data;
                //asignando la posicion en el arreglo a cada objeto
                iniciarPosiciones($rootScope.settingModAscensos.dataset);
                $rootScope.tablaModAscensos.settings($rootScope.settingModAscensos);
            }, function (data) {
                console.info(data);
            });
        };
        
        $scope.listarCapacitaciones = function () {
            //preparamos un objeto request
            var request = crud.crearRequest('capacitacion', 1, 'listarCapacitaciones');
            request.setData($scope.fichaEscalafonariaSel);
            
            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request        
            crud.listar("/sistema_escalafon", request, function (data) {
                $rootScope.settingModCapacitaciones.dataset = data.data;
                //asignando la posicion en el arreglo a cada objeto
                iniciarPosiciones($rootScope.settingModCapacitaciones.dataset);
                $rootScope.tablaModCapacitaciones.settings($rootScope.settingModCapacitaciones);
            }, function (data) {
                console.info(data);
            });
        };
        
        $scope.listarReconocimientos = function () {
            //preparamos un objeto request
            var request = crud.crearRequest('reconocimientos', 1, 'listarReconocimientos');
            request.setData($scope.fichaEscalafonariaSel);
            
            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request        
            crud.listar("/sistema_escalafon", request, function (data) {
                data.data.forEach(function (item){
                    item.motDes = buscarContenido(motivos, 'id', 'title', item.mot);
                });
                $rootScope.settingModReconocimientos.dataset = data.data;
                //asignando la posicion en el arreglo a cada objeto
                iniciarPosiciones($rootScope.settingModReconocimientos.dataset);
                $rootScope.tablaModReconocimientos.settings($rootScope.settingModReconocimientos);
            }, function (data) {
                console.info(data);
            });
        };
        
        $scope.listarDemeritos = function () {
            //preparamos un objeto request
            var request = crud.crearRequest('demeritos', 1, 'listarDemeritos');
            request.setData($scope.fichaEscalafonariaSel);
            
            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request        
            crud.listar("/sistema_escalafon", request, function (data) {
                data.data.forEach(function (item){
                    item.sepDes = item.sep?"Si":"No";
                });
                $rootScope.settingModDemeritos.dataset = data.data;
                //asignando la posicion en el arreglo a cada objeto
                iniciarPosiciones($rootScope.settingModDemeritos.dataset);
                $rootScope.tablaModDemeritos.settings($rootScope.settingModDemeritos);
            }, function (data) {
                console.info(data);
            });
        };
        
        $scope.direcciones = [];
        $scope.actualizarDatosGenerales = function () {
            
            if (!$scope.direccionDASel.nomDir.length > 0 && $scope.direccionDNISel.nomDir.length > 0) {
                $scope.direcciones = [$scope.direccionDNISel];
            } else {
                $scope.direcciones = [$scope.direccionDNISel, $scope.direccionDASel];
            }
            var request = crud.crearRequest('datos_personales', 1, 'actualizarDatosPersonales');
            request.setData({persona: $scope.personaSel, trabajador: $scope.trabajadorSel, fichaEscalafonaria:$scope.fichaEscalafonariaSel, direcciones: $scope.direcciones, orgId: $rootScope.usuMaster.organizacion.organizacionID});
            crud.actualizar("/sistema_escalafon", request, function (response) {
                modal.mensaje("CONFIRMACION", response.responseMsg);
                
                $scope.personaSel.apePat = response.data.persona.apePat;
                $scope.personaSel.apeMat = response.data.persona.apeMat;
                $scope.personaSel.nom = response.data.persona.nom;
                $scope.personaSel.dni = response.data.persona.dni;
                $scope.personaSel.fecNac = new Date(response.data.persona.fecNac);
                $scope.personaSel.num1 = response.data.persona.num1;
                $scope.personaSel.num2 = response.data.persona.num2;
                $scope.personaSel.fij = response.data.persona.fij;
                $scope.personaSel.email = response.data.persona.email;
                $scope.personaSel.sex = response.data.persona.sex;
                $scope.personaSel.estCiv = response.data.persona.estCiv;
                $scope.personaSel.depNac = response.data.persona.depNac;
                $scope.personaSel.proNac = response.data.persona.proNac;
                $scope.personaSel.disNac = response.data.persona.disNac;
                
                $scope.trabajadorSel.traCon = response.data.trabajador.traCon;
                
                $scope.fichaEscalafonariaSel.autEss = response.data.ficha.autEss;
                $scope.fichaEscalafonariaSel.sisPen = response.data.ficha.sisPen;
                $scope.fichaEscalafonariaSel.nomAfp = response.data.ficha.nomAfp;
                $scope.fichaEscalafonariaSel.codCuspp = response.data.ficha.codCuspp;
                $scope.fichaEscalafonariaSel.fecIngAfp = new Date(response.data.ficha.fecIngAfp);
                $scope.fichaEscalafonariaSel.perDis = response.data.ficha.perDis;
                $scope.fichaEscalafonariaSel.regCon = response.data.ficha.autEss;
                $scope.fichaEscalafonariaSel.gruOcu = response.data.ficha.gruOcu;
                
                switch(response.data.direcciones.lenght){
                    case 1: $scope.direccionDNISel.tipDir = response.data.direcciones[0].tipDir;
                            $scope.direccionDNISel.nomDir = response.data.direcciones[0].nomDir;
                            $scope.direccionDNISel.depDir = response.data.direcciones[0].depDir;
                            $scope.direccionDNISel.proDir = response.data.direcciones[0].proDir;
                            $scope.direccionDNISel.disDir = response.data.direcciones[0].disDir;
                            $scope.direccionDNISel.nomZon = response.data.direcciones[0].nomZon;
                            $scope.direccionDNISel.desRef = response.data.direcciones[0].desRef;
                            
                            break;
                    case 2: $scope.direccionDNISel.tipDir = response.data.direcciones[0].tipDir;
                            $scope.direccionDNISel.nomDir = response.data.direcciones[0].nomDir;
                            $scope.direccionDNISel.depDir = response.data.direcciones[0].depDir;
                            $scope.direccionDNISel.proDir = response.data.direcciones[0].proDir;
                            $scope.direccionDNISel.disDir = response.data.direcciones[0].disDir;
                            $scope.direccionDNISel.nomZon = response.data.direcciones[0].nomZon;
                            $scope.direccionDNISel.desRef = response.data.direcciones[0].desRef;
                            
                            $scope.direccionDASel.tipDir = response.data.direcciones[1].tipDir;
                            $scope.direccionDASel.nomDir = response.data.direcciones[1].nomDir;
                            $scope.direccionDASel.depDir = response.data.direcciones[1].depDir;
                            $scope.direccionDASel.proDir = response.data.direcciones[1].proDir;
                            $scope.direccionDASel.disDir = response.data.direcciones[1].disDir;
                            $scope.direccionDASel.nomZon = response.data.direcciones[1].nomZon;
                            $scope.direccionDASel.desRef = response.data.direcciones[1].desRef;
                            break;
                }
                
            }, function (data) {
                console.info(data);
            });
        };
        
        $scope.prepararEditarPariente = function (p) {
            var parienteSel = JSON.parse(JSON.stringify(p));
            ModalService.showModal({
                templateUrl: "administrativa/sistema_escalafon/editarPariente.html",
                controller: "editarParienteCtrl",
                inputs: {
                    title: "Editar datos del pariente",
                    pariente: parienteSel,
                    personaId: $scope.personaSel.perId
                }
            }).then(function (modal) {
                modal.element.modal();
            });
        };
        
        $scope.prepararEditarFormacionEducativa = function (fe) {
            var formacionEducativaSel = JSON.parse(JSON.stringify(fe));
            console.log(formacionEducativaSel);
            ModalService.showModal({
                templateUrl: "administrativa/sistema_escalafon/editarFormacionEducativa.html",
                controller: "editarFormacionEducativaCtrl",
                inputs: {
                    title: "Editar datos de Formacion Educativa",
                    formacionEducativa: formacionEducativaSel,
                    fichaEscalafonariaId: $scope.fichaEscalafonariaSel.ficEscId
                }
            }).then(function (modal) {
                modal.element.modal();
            });

        };
        
        $scope.prepararEditarColegiatura = function (c) {
            var colegiaturaSel = JSON.parse(JSON.stringify(c));
            
            ModalService.showModal({
                templateUrl: "administrativa/sistema_escalafon/editarColegiatura.html",
                controller: "editarColegiaturaCtrl",
                inputs: {
                    title: "Editar datos de Colegiatura",
                    colegiatura: colegiaturaSel,
                    fichaEscalafonariaId: $scope.fichaEscalafonariaSel.ficEscId
                }
            }).then(function (modal) {
                modal.element.modal();
            });

        };
        
        $scope.prepararEditarEstudioComplementario = function (e) {
            var estComSel = JSON.parse(JSON.stringify(e));
            
            ModalService.showModal({
                templateUrl: "administrativa/sistema_escalafon/editarEstudioComplementario.html",
                controller: "editarEstudioComplementarioCtrl",
                inputs: {
                    title: "Editar datos de Estudio Complementario",
                    estudioComplementario: estComSel,
                    fichaEscalafonariaId: $scope.fichaEscalafonariaSel.ficEscId
                }
            }).then(function (modal) {
                modal.element.modal();
            });

        };
        
        $scope.prepararEditarEstudioPostgrado = function (e) {
            var estPosSel = JSON.parse(JSON.stringify(e));
            ModalService.showModal({
                templateUrl: "administrativa/sistema_escalafon/editarEstudioPostgrado.html",
                controller: "editarEstudioPostgradoCtrl",
                inputs: {
                    title: "Editar datos de Estudio de Postgrado",
                    estudioPostgrado: estPosSel,
                    fichaEscalafonariaId: $scope.fichaEscalafonariaSel.ficEscId
                }
            }).then(function (modal) {
                modal.element.modal();
            });
        };
        
        $scope.prepararEditarExposicion = function (e) {
            var expSel = JSON.parse(JSON.stringify(e));
            
            ModalService.showModal({
                templateUrl: "administrativa/sistema_escalafon/editarExposicion.html",
                controller: "editarExposicionCtrl",
                inputs: {
                    title: "Editar datos de Exposicion",
                    exposicion: expSel,
                    fichaEscalafonariaId: $scope.fichaEscalafonariaSel.ficEscId
                }
            }).then(function (modal) {
                modal.element.modal();
            });
        };
        
        $scope.prepararEditarPublicacion = function (p) {
            var pubSel = JSON.parse(JSON.stringify(p));
            console.log(pubSel);
            ModalService.showModal({
                templateUrl: "administrativa/sistema_escalafon/editarPublicacion.html",
                controller: "editarPublicacionCtrl",
                inputs: {
                    title: "Editar datos de Publicacion",
                    publicacion: pubSel,
                    fichaEscalafonariaId: $scope.fichaEscalafonariaSel.ficEscId
                }
            }).then(function (modal) {
                modal.element.modal();
            });
        };
        
        $scope.prepararEditarDesplazamiento = function (d) {
            var desSel = JSON.parse(JSON.stringify(d));
            
            ModalService.showModal({
                templateUrl: "administrativa/sistema_escalafon/editarDesplazamiento.html",
                controller: "editarDesplazamientoCtrl",
                inputs: {
                    title: "Editar datos de Desplazamiento",
                    desplazamiento: desSel,
                    fichaEscalafonariaId: $scope.fichaEscalafonariaSel.ficEscId
                }
            }).then(function (modal) {
                modal.element.modal();
            });
        };
        
        $scope.prepararEditarAscenso = function (a) {
            var ascSel = JSON.parse(JSON.stringify(a));
            console.log(ascSel);
            
            ModalService.showModal({
                templateUrl: "administrativa/sistema_escalafon/editarAscenso.html",
                controller: "editarAscensoCtrl",
                inputs: {
                    title: "Editar datos de Ascenso",
                    ascenso: ascSel,
                    fichaEscalafonariaId: $scope.fichaEscalafonariaSel.ficEscId
                }
            }).then(function (modal) {
                modal.element.modal();
            });
        };
        
        $scope.prepararEditarCapacitacion = function (c) {
            var capSel = JSON.parse(JSON.stringify(c));
            
            ModalService.showModal({
                templateUrl: "administrativa/sistema_escalafon/editarCapacitacion.html",
                controller: "editarCapacitacionCtrl",
                inputs: {
                    title: "Editar datos de Capacitacion",
                    capacitacion: capSel,
                    fichaEscalafonariaId: $scope.fichaEscalafonariaSel.ficEscId
                }
            }).then(function (modal) {
                modal.element.modal();
            });
        };
        
        $scope.prepararEditarReconocimiento = function (r) {
            var recSel = JSON.parse(JSON.stringify(r));
            
            ModalService.showModal({
                templateUrl: "administrativa/sistema_escalafon/editarReconocimiento.html",
                controller: "editarReconocimientoCtrl",
                inputs: {
                    title: "Editar datos de Reconocimiento",
                    reconocimiento: recSel,
                    fichaEscalafonariaId: $scope.fichaEscalafonariaSel.ficEscId
                }
            }).then(function (modal) {
                modal.element.modal();
            });
        };
        
        $scope.prepararEditarDemerito = function (d) {
            var demSel = JSON.parse(JSON.stringify(d));
            console.log(demSel);
            
            ModalService.showModal({
                templateUrl: "administrativa/sistema_escalafon/editarDemerito.html",
                controller: "editarDemeritoCtrl",
                inputs: {
                    title: "Editar datos de Demerito",
                    demerito: demSel,
                    fichaEscalafonariaId: $scope.fichaEscalafonariaSel.ficEscId
                }
            }).then(function (modal) {
                modal.element.modal();
            });
        };
        
        $scope.eliminarPariente = function(p){   
            modal.mensajeConfirmacion($scope, "¿Seguro que desea eliminar el pariente?", function () {
                var request = crud.crearRequest('familiares', 1, 'eliminarFamiliar');
                request.setData({parId: p.parId, perId: $scope.personaSel.perId});
                
                crud.eliminar('/sistema_escalafon', request, function (response) {
                    if (response.responseSta) {
                        _.remove($rootScope.tablaModParientes.settings().dataset, function(item){
                            return p === item;
                        });
                        $rootScope.tablaModParientes.reload();
                        modal.mensaje("CONFIRMACION", "La eliminación se realizó correctamente");
                    }else {
                            modal.mensaje("ERROR",response.responseMsg);
                    }
                }, function (error) {
                    modal.mensaje("ERROR","El servidor no responde");
                });
            }, '400'); 
        };
        
    }]);
app.controller('editarParienteCtrl', ['$rootScope','$scope', '$element', 'title', 'pariente', 'personaId', 'close', 'crud', 'modal', function ($rootScope ,$scope, $element, title, pariente, personaId, close, crud, modal) {
        $scope.title = title;
        
        $scope.sexo = [{id: "F", title: "Femenino"}, {id: "M", title: "Masculino"}];
        $scope.tipoPariente = [];
        
        function listarTipoParentesco() {
            var request = crud.crearRequest('familiares', 1, 'listarTipoParentesco');
            crud.listar("/sistema_escalafon", request, function (data) {
                $scope.tipoPariente = data.data;
            }, function (data) {
                console.info(data);
            });
        };
        listarTipoParentesco();
        
        $scope.parienteSel = {
            parId: pariente.parId,
            perId: personaId, 
            tipoParienteId: pariente.tipoParienteId, 
            parentesco: "",
            parApePat: pariente.parApePat, 
            parApeMat: pariente.parApeMat, 
            parNom: pariente.parNom, 
            parDni: pariente.parDni, 
            parFecNac: new Date(pariente.parFecNac), 
            parSex: pariente.parSex, 
            parNum1: pariente.parNum1, 
            parNum2: pariente.parNum2, 
            parFij: pariente.parFij, 
            parEmail: pariente.parEmail
        };
        
        $scope.actualizarPariente = function () {
            var request = crud.crearRequest('familiares', 1, 'actualizarFamiliar');
            request.setData($scope.parienteSel);
            crud.insertar("/sistema_escalafon", request, function (response) {
                modal.mensaje("CONFIRMACION", response.responseMsg);
                if (response.responseSta) {
                    response.data.parentesco = buscarContenido($scope.tipoPariente,'tpaId','tpaDes', response.data.tipoParienteId);
                    response.data.i = pariente.i;
                    response.data.fecNac = new Date(response.data.fecNac);
                    $rootScope.settingModParientes.dataset[pariente.i] = response.data;
                    $rootScope.tablaModParientes.reload();
                }
            }, function (data) {
                console.info(data);
            });
        };
    }]);
app.controller('editarFormacionEducativaCtrl', ['$rootScope','$scope', '$element', 'title', 'formacionEducativa', 'fichaEscalafonariaId', 'close', 'crud', 'modal', function ($rootScope ,$scope, $element, title, formacionEducativa, fichaEscalafonariaId, close, crud, modal) {
        $scope.title = title;
        

        //variable que servira para registrar una nueva formacion academica
        $scope.forEduSel = {
            forEduId: formacionEducativa.forEduId, 
            tip: formacionEducativa.tip,
            tipDes: "",
            niv: formacionEducativa.niv, 
            numTit: formacionEducativa.numTit,
            esp: formacionEducativa.esp, 
            estCon: formacionEducativa.estCon, 
            fecExp: new Date(formacionEducativa.fecExp), 
            cenEst: formacionEducativa.cenEst
        };
        $scope.tiposFormacion = [
            {id: "1", title: "Estudios Basicos Regulares"}, 
            {id: "2", title: "Estudios Tecnicos"}, 
            {id: "3", title: "Bachiller"}, 
            {id: "4", title: "Universitario"}, 
            {id: "5", title: "Licenciatura"}, 
            {id: "6", title: "Maestria"}, 
            {id: "7", title: "Doctorado"}, 
            {id: "8", title: "Otros"}
        ];
        

        $scope.actualizarFormacionEducativa = function () {
            var request = crud.crearRequest('formacion_educativa', 1, 'actualizarFormacionEducativa');
            request.setData($scope.forEduSel);
            crud.insertar("/sistema_escalafon", request, function (response) {
                modal.mensaje("CONFIRMACION", response.responseMsg);
                if (response.responseSta) {
                    response.data.estConDes = response.data.estCon?"Si":"No";
                    response.data.tipDes = buscarContenido($scope.tiposFormacion, 'id', 'title', response.data.tip);
                    response.data.i = formacionEducativa.i;
                    $rootScope.settingModForEdu.dataset[formacionEducativa.i] = response.data;
                    $rootScope.tablaModForEdu.reload();
                }
            }, function (data) {
                console.info(data);
            });
        };
        
    }]);
app.controller('editarColegiaturaCtrl', ['$rootScope','$scope', '$element', 'title', 'colegiatura', 'fichaEscalafonariaId', 'close', 'crud', 'modal', function ($rootScope ,$scope, $element, title, colegiatura, fichaEscalafonariaId, close, crud, modal) {
        $scope.title = title;

        //variable que servira para registrar una nueva formacion academica
        $scope.colegiaturaSel = {
            i: colegiatura.i,
            colId: colegiatura.colId, 
            nomColPro: colegiatura.nomColPro, 
            numRegCol: colegiatura.numRegCol, 
            conReg: colegiatura.conReg
        };

        $scope.actualizarColegiatura = function () {
            var request = crud.crearRequest('colegiatura', 1, 'actualizarColegiatura');
            request.setData($scope.colegiaturaSel);
            crud.insertar("/sistema_escalafon", request, function (response) {
                modal.mensaje("CONFIRMACION", response.responseMsg);
                if (response.responseSta) {
                    response.data.conRegDes = response.data.conReg?"Habilitado":"No Habilitado";
                    response.data.i = colegiatura.i;
                    $rootScope.settingModColegiaturas.dataset[ colegiatura.i] = response.data;
                    $rootScope.tablaModColegiaturas.reload();
                }
            }, function (data) {
                console.info(data);
            });
        };
        
    }]);
app.controller('editarEstudioComplementarioCtrl', ['$rootScope','$scope', '$element', 'title', 'estudioComplementario', 'fichaEscalafonariaId', 'close', 'crud', 'modal', function ($rootScope ,$scope, $element, title, estudioComplementario, fichaEscalafonariaId, close, crud, modal) {
        $scope.title = title;

        //variable que servira para registrar una nueva formacion academica
        $scope.estComSel = {
            i: estudioComplementario.i,
            estComId: estudioComplementario.estComId,
            tip: estudioComplementario.tip, 
            des: estudioComplementario.des, 
            niv: estudioComplementario.niv, 
            insCer: estudioComplementario.insCer, 
            tipPar: estudioComplementario.tipPar, 
            fecIni: new Date(estudioComplementario.fecIni), 
            fecTer: new Date(estudioComplementario.fecTer), 
            horLec: estudioComplementario.horLec
        };
        
        $scope.tiposEstCom = [
            {id: "1", title: "Informatica"}, 
            {id: "2", title: "Idiomas"},
            {id: "3", title: "Certificación"},
            {id: "4", title: "Diplomado"},
            {id: "5", title: "Especialización"}, 
            {id: "6", title: "Otros"}
        ];
        
        $scope.niveles = [
            {id: "N", title: "Ninguno"},
            {id: "B", title: "Básico"}, 
            {id: "I", title: "Intermedio"}, 
            {id: "A", title: "Avanzado"}
        ];
        
        $scope.actualizarEstudioComplementario = function () {
            var request = crud.crearRequest('estudio_complementario', 1, 'actualizarEstudioComplementario');
            request.setData($scope.estComSel);
            crud.insertar("/sistema_escalafon", request, function (response) {
                modal.mensaje("CONFIRMACION", response.responseMsg);
                if (response.responseSta) {
                    response.data.tipDes = buscarContenido($scope.tiposEstCom, 'id', 'title', response.data.tip);
                    response.data.nivDes = buscarContenido($scope.niveles, 'id', 'title', response.data.niv);
                    response.data.i = estudioComplementario.i;
                    $rootScope.settingModEstCom.dataset[ estudioComplementario.i] = response.data;
                    $rootScope.tablaModEstCom.reload();
                }
            }, function (data) {
                console.info(data);
            });
        };
        
    }]);
app.controller('editarEstudioPostgradoCtrl', ['$rootScope','$scope', '$element', 'title', 'estudioPostgrado', 'fichaEscalafonariaId', 'close', 'crud', 'modal', function ($rootScope ,$scope, $element, title, estudioPostgrado, fichaEscalafonariaId, close, crud, modal) {
        $scope.title = title;
        //variable que servira para registrar una nueva formacion academica
        $scope.estPosSel = {
            i: estudioPostgrado.i,
            estPosId: estudioPostgrado.estPosId,
            ficEscId: fichaEscalafonariaId, 
            tip: estudioPostgrado.tip, 
            numRes: estudioPostgrado.numRes,
            fecRes: new Date(estudioPostgrado.fecRes),
            fecIniEst: new Date(estudioPostgrado.fecIniEst), 
            fecTerEst: new Date(estudioPostgrado.fecTerEst), 
            ins: estudioPostgrado.ins
        };
        
        $scope.tiposEstPos = [
            {id: "1", title: "Maestria"}, 
            {id: "2", title: "Doctorado"}
        ];
        
        $scope.actualizarEstudioPostgrado = function () {
            var request = crud.crearRequest('estudio_postgrado', 1, 'actualizarEstudioPostgrado');
            request.setData($scope.estPosSel);
            crud.insertar("/sistema_escalafon", request, function (response) {
                modal.mensaje("CONFIRMACION", response.responseMsg);
                if (response.responseSta) {
                    response.data.tipDes = buscarContenido($scope.tiposEstPos, 'id', 'title', response.data.tip);
                    response.data.i = estudioPostgrado.i;
                    $rootScope.settingModEstPos.dataset[estudioPostgrado.i] = response.data;
                    $rootScope.tablaModEstPos.reload();
                }
            }, function (data) {
                console.info(data);
            });
        };
        
    }]);
app.controller('editarExposicionCtrl', ['$rootScope','$scope', '$element', 'title', 'exposicion', 'fichaEscalafonariaId', 'close', 'crud', 'modal', function ($rootScope ,$scope, $element, title, exposicion, fichaEscalafonariaId, close, crud, modal) {
        $scope.title = title;
        //variable que servira para registrar una nueva formacion academica
        $scope.expPonSel = {
            i: exposicion.i,
            expId: exposicion.expId,
            ficEscId:fichaEscalafonariaId, 
            des: exposicion.des, 
            insOrg: exposicion.insOrg, 
            tipPar: exposicion.tipPar, 
            fecIni: new Date(exposicion.fecIni), 
            fecTer: new Date(exposicion.fecTer), 
            horLec: exposicion.horLec
        };
        
        $scope.actualizarExposicion = function () {
            var request = crud.crearRequest('exposicion_ponencia', 1, 'actualizarExposicion');
            request.setData($scope.expPonSel);
            crud.insertar("/sistema_escalafon", request, function (response) {
                modal.mensaje("CONFIRMACION", response.responseMsg);
                if (response.responseSta) {
                    response.data.i = exposicion.i;
                    $rootScope.settingModExpPon.dataset[exposicion.i] = response.data;
                    $rootScope.tablaModExpPon.reload();
                }
            }, function (data) {
                console.info(data);
            });
        };
        
    }]);
app.controller('editarPublicacionCtrl', ['$rootScope','$scope', '$element', 'title', 'publicacion', 'fichaEscalafonariaId', 'close', 'crud', 'modal', function ($rootScope ,$scope, $element, title, publicacion, fichaEscalafonariaId, close, crud, modal) {
        $scope.title = title;
        //variable que servira para registrar una nueva formacion academica
        $scope.publicacionSel = {
            i: publicacion.i,
            pubId: publicacion.pubId,
            nomEdi: publicacion.nomEdi, 
            tipPub: publicacion.tipPub, 
            titPub: publicacion.titPub, 
            graPar: publicacion.graPar, 
            lug: publicacion.lug, 
            fecPub: new Date(publicacion.fecPub), 
            numReg: publicacion.numReg
        };
        
        $scope.actualizarPublicacion= function () {
            var request = crud.crearRequest('publicacion', 1, 'actualizarPublicacion');
            request.setData($scope.publicacionSel);
            crud.insertar("/sistema_escalafon", request, function (response) {
                modal.mensaje("CONFIRMACION", response.responseMsg);
                if (response.responseSta) {
                    response.data.i = publicacion.i;
                    $rootScope.settingModPublicaciones.dataset[publicacion.i] = response.data;
                    $rootScope.tablaModPublicaciones.reload();
                }
            }, function (data) {
                console.info(data);
            });
        };
        
    }]);
app.controller('editarDesplazamientoCtrl', ['$rootScope','$scope', '$element', 'title', 'desplazamiento', 'fichaEscalafonariaId', 'close', 'crud', 'modal', function ($rootScope ,$scope, $element, title, desplazamiento, fichaEscalafonariaId, close, crud, modal) {
        $scope.title = title;
        //variable que servira para registrar una nueva formacion academica
        $scope.desplazamientoSel = {
            i: desplazamiento.i,
            desId: desplazamiento.desId,
            tip: desplazamiento.tip, 
            numRes: desplazamiento.numRes, 
            fecRes: new Date(desplazamiento.fecRes), 
            insEdu: desplazamiento.insEdu, 
            car: desplazamiento.car, 
            jorLab: desplazamiento.jorLab, 
            fecIni: new Date(desplazamiento.fecIni), 
            fecTer: new Date(desplazamiento.fecTer)
        };
        
        $scope.tiposDesplazamientos = [
            {id: "1", title: "Designación"}, 
            {id: "2", title: "Rotación"},
            {id: "3", title: "Reasignación"},
            {id: "4", title: "Destaque"}, 
            {id: "5", title: "Permuta"}, 
            {id: "6", title: "Encargo"},
            {id: "7", title: "Comisión de servicio"}, 
            {id: "8", title: "Transferencia"}, 
            {id: "9", title: "Otros"},
        ];
        
        $scope.actualizarDesplazamiento = function () {
            var request = crud.crearRequest('desplazamiento', 1, 'actualizarDesplazamiento');
            request.setData($scope.desplazamientoSel);
            crud.insertar("/sistema_escalafon", request, function (response) {
                modal.mensaje("CONFIRMACION", response.responseMsg);
                if (response.responseSta) {
                    response.data.tipDes = buscarContenido($scope.tiposDesplazamientos, 'id', 'title', response.data.tip);
                    response.data.i = desplazamiento.i;
                    $rootScope.settingModDesplazamientos.dataset[desplazamiento.i] = response.data;
                    $rootScope.tablaModDesplazamientos.reload();
                }
            }, function (data) {
                console.info(data);
            });
        };
        
    }]);
app.controller('editarAscensoCtrl', ['$rootScope','$scope', '$element', 'title', 'ascenso', 'fichaEscalafonariaId', 'close', 'crud', 'modal', function ($rootScope ,$scope, $element, title, ascenso, fichaEscalafonariaId, close, crud, modal) {
        $scope.title = title;
        //variable que servira para registrar una nueva formacion academica
        $scope.ascensoSel = {
            i: ascenso.i,
            ascId: ascenso.ascId,
            numRes: ascenso.numRes, 
            fecRes: new Date(ascenso.fecRes), 
            fecEfe: new Date(ascenso.fecEfe), 
            esc: (ascenso.esc).split(" ").join("")
        };
        $scope.escalas = [
            {id:"I", title: "I"}, 
            {id:"II", title: "II"}, 
            {id:"III", title: "III"}, 
            {id:"IV", title: "IV"}, 
            {id:"V", title: "V"}, 
            {id:"VI", title: "VI"},
            {id:"VII", title: "VII"}, 
            {id:"VIII", title: "VIII"}
        ];
        
        $scope.actualizarAscenso = function () {
            var request = crud.crearRequest('ascenso', 1, 'actualizarAscenso');
            request.setData($scope.ascensoSel);
            crud.insertar("/sistema_escalafon", request, function (response) {
                modal.mensaje("CONFIRMACION", response.responseMsg);
                if (response.responseSta) {
                    response.data.i = ascenso.i;
                    $rootScope.settingModAscensos.dataset[ascenso.i] = response.data;
                    $rootScope.tablaModAscensos.reload();
                }
            }, function (data) {
                console.info(data);
            });
        };
        
    }]);
app.controller('editarCapacitacionCtrl', ['$rootScope','$scope', '$element', 'title', 'capacitacion', 'fichaEscalafonariaId', 'close', 'crud', 'modal', function ($rootScope ,$scope, $element, title, capacitacion, fichaEscalafonariaId, close, crud, modal) {
        $scope.title = title;
        //variable que servira para registrar una nueva formacion academica
        $scope.capacitacionSel = {
            i: capacitacion.i,
            capId: capacitacion.capId,
            nom: capacitacion.nom, 
            tip: capacitacion.tip, 
            fec: new Date(capacitacion.fec), 
            cal: capacitacion.cal, 
            lug: capacitacion.lug
        };
        
        $scope.actualizarCapacitacion = function () {
            var request = crud.crearRequest('capacitacion', 1, 'actualizarCapacitacion');
            request.setData($scope.capacitacionSel);
            crud.insertar("/sistema_escalafon", request, function (response) {
                modal.mensaje("CONFIRMACION", response.responseMsg);
                if (response.responseSta) {
                    response.data.i = capacitacion.i;
                    $rootScope.settingModCapacitaciones.dataset[capacitacion.i] = response.data;
                    $rootScope.tablaModCapacitaciones.reload();
                }
            }, function (data) {
                console.info(data);
            });
        };
        
    }]);
app.controller('editarReconocimientoCtrl', ['$rootScope','$scope', '$element', 'title', 'reconocimiento', 'fichaEscalafonariaId', 'close', 'crud', 'modal', function ($rootScope ,$scope, $element, title, reconocimiento, fichaEscalafonariaId, close, crud, modal) {
        $scope.title = title;
        //variable que servira para registrar una nueva formacion academica
        $scope.reconocimientoSel = {
            i: reconocimiento.i,
            recId: reconocimiento.recId,
            mot: reconocimiento.mot, 
            numRes: reconocimiento.numRes, 
            fecRes: new Date(reconocimiento.fecRes), 
            entEmi: reconocimiento.entEmi
        };
        
        $scope.motivos = [
            {id: "1", title: "Mérito"}, 
            {id: "2", title: "Felicitación"}, 
            {id: "3", title: "25 años de servicio"},
            {id: "4", title: "30 años de servicio"},
            {id: "5", title: "Luto y sepelio"}, 
            {id: "6", title: "Otros"}
        ];
        
        $scope.actualizarReconocimiento = function () {
            var request = crud.crearRequest('reconocimientos', 1, 'actualizarReconocimiento');
            request.setData($scope.reconocimientoSel);
            crud.insertar("/sistema_escalafon", request, function (response) {
                modal.mensaje("CONFIRMACION", response.responseMsg);
                if (response.responseSta) {
                    response.data.motDes = buscarContenido($scope.motivos, 'id', 'title', response.data.mot);
                    response.data.i = reconocimiento.i;
                    $rootScope.settingModReconocimientos.dataset[reconocimiento.i] = response.data;
                    $rootScope.tablaModReconocimientos.reload();
                }
            }, function (data) {
                console.info(data);
            });
        };
        
    }]);
app.controller('editarDemeritoCtrl', ['$rootScope','$scope', '$element', 'title', 'demerito', 'fichaEscalafonariaId', 'close', 'crud', 'modal', function ($rootScope ,$scope, $element, title, demerito, fichaEscalafonariaId, close, crud, modal) {
        $scope.title = title;
        //variable que servira para registrar una nueva formacion academica
        $scope.demeritoSel = {
            i: demerito.i,
            demId: demerito.demId,
            entEmi: demerito.entEmi,
            numRes: demerito.numRes, 
            fecRes: new Date(demerito.fecRes), 
            sep: demerito.sep, 
            fecIni: new Date(demerito.fecIni), 
            fecFin: new Date(demerito.fecFin), 
            mot: demerito.mot
        };
        
        
        $scope.actualizarDemerito = function () {
            var request = crud.crearRequest('demeritos', 1, 'actualizarDemerito');
            request.setData($scope.demeritoSel);
            crud.insertar("/sistema_escalafon", request, function (response) {
                modal.mensaje("CONFIRMACION", response.responseMsg);
                if (response.responseSta) {
                    response.data.sepDes = response.data.sep?"Si":"No";
                    response.data.i = demerito.i;
                    $rootScope.settingModDemeritos.dataset[demerito.i] = response.data;
                    $rootScope.tablaModDemeritos.reload();
                }
            }, function (data) {
                console.info(data);
            });
        };
        
    }]);
app.controller('verLegajoPersonalCtrl', ['$routeParams', '$rootScope','$scope', 'crud', 'NgTableParams','ModalService', 'modal', function ($routeParams, $rootScope, $scope, crud, NgTableParams, ModalService, modal) {
        var datos = JSON.parse(atob($routeParams.ficha));
        
        var categorias = [
            {idCat: "1", idSubCat:"1", subCatDes: "Datos personales"},
            {idCat: "1", idSubCat:"2", subCatDes: "Nacimiento"},
            {idCat: "1", idSubCat:"3", subCatDes: "Salud"},
            {idCat: "1", idSubCat:"4", subCatDes: "Direccion"},
            {idCat: "2", idSubCat:"1", subCatDes: "Datos familiares"},
            {idCat: "3", idSubCat:"1", subCatDes: "Formacion educativa"},
            {idCat: "3", idSubCat:"2", subCatDes: "Colegiatura"},
            {idCat: "3", idSubCat:"3", subCatDes: "Estudios complementarios"},
            {idCat: "3", idSubCat:"4", subCatDes: "Estudios postgrado"},
            {idCat: "3", idSubCat:"5", subCatDes: "Exposicion y/o Ponencias"},
            {idCat: "3", idSubCat:"6", subCatDes: "Publicaciones"},
            {idCat: "4", idSubCat:"1", subCatDes: "Desplazamientos"},
            {idCat: "4", idSubCat:"2", subCatDes: "Ascensos"},
            {idCat: "4", idSubCat:"3", subCatDes: "Capacitaciones"},
            {idCat: "5", idSubCat:"1", subCatDes: "Reconocimientos"},
            {idCat: "6", idSubCat:"1", subCatDes: "Demeritos"},
            {idCat: "7", idSubCat:"1", subCatDes: "Otros"}
        ];
        
        $rootScope.paramsLegDatosPersonales = {count: 10};
        $rootScope.settingLegDatosPersonales  = {counts: []};
        $rootScope.tablaLegDatosPersonales = new NgTableParams($rootScope.paramsLegDatosPersonales, $rootScope.settingLegDatosPersonales);
        
        $rootScope.paramsLegDatosFamiliares = {count: 10};
        $rootScope.settingLegDatosFamiliares  = {counts: []};
        $rootScope.tablaLegDatosFamiliares = new NgTableParams($rootScope.paramsLegDatosFamiliares, $rootScope.settingLegDatosFamiliares);
        
        $rootScope.paramsLegDatosAcademicos = {count: 10};
        $rootScope.settingLegDatosAcademicos  = {counts: []};
        $rootScope.tablaLegDatosAcademicos  = new NgTableParams($rootScope.paramsLegDatosAcademicos, $rootScope.settingLegDatosAcademicos);
        
        $rootScope.paramsLegExperienciaLaboral = {count: 10};
        $rootScope.settingLegExperienciaLaboral  = {counts: []};
        $rootScope.tablaLegExperienciaLaboral  = new NgTableParams($rootScope.paramsLegExperienciaLaboral, $rootScope.settingLegExperienciaLaboral);
        
        $rootScope.paramsLegReconocimientos = {count: 10};
        $rootScope.settingLegReconocimientos  = {counts: []};
        $rootScope.tablaLegReconocimientos  = new NgTableParams($rootScope.paramsLegReconocimientos, $rootScope.settingLegReconocimientos);
        
        $rootScope.paramsLegDemeritos = {count: 10};
        $rootScope.settingLegDemeritos   = {counts: []};
        $rootScope.tablaLegDemeritos   = new NgTableParams($rootScope.paramsLegDemeritos , $rootScope.settingLegDemeritos );
        
        $rootScope.paramsLegOtros = {count: 10};
        $rootScope.settingLegOtros   = {counts: []};
        $rootScope.tablaLegOtros   = new NgTableParams($rootScope.paramsLegOtros , $rootScope.settingLegOtros );
        
        var legDatosPersonales = [];
        var legDatosFamiliares = [];
        var legDatosAcademicos = [];
        var legExperienciaLaboral = [];
        var legReconocimientos = [];
        var legDemeritos = [];
        var legOtros = [];
        
        listarLegajos();
        function listarLegajos() {
            //preparamos un objeto request
            var request = crud.crearRequest('legajo_personal', 1, 'listarLegajos');
            request.setData(datos);
            
            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request        
            crud.listar("/sistema_escalafon", request, function (data) {
                data.data.forEach(function (item){
                    var subCategorias = buscarObjetos(categorias, 'idCat', item.catLeg );
                    item.subCatDes = buscarContenido(subCategorias, 'idSubCat', 'subCatDes', item.subCat);
                    
                    switch(item.catLeg){
                        case '1': legDatosPersonales.push(item);
                                  break;
                        case '2': legDatosFamiliares.push(item);
                                  break;
                        case '3': legDatosAcademicos.push(item);
                                  break;
                        case '4': legExperienciaLaboral.push(item);
                                  break;
                        case '5': legReconocimientos.push(item);
                                  break;
                        case '6': legDemeritos.push(item);
                                  break;
                        default: legOtros.push(item);
                    }
                });
                
                $rootScope.settingLegDatosPersonales.dataset = legDatosPersonales;
                iniciarPosiciones($rootScope.settingLegDatosPersonales.dataset);
                $rootScope.tablaLegDatosPersonales.settings($rootScope.settingLegDatosPersonales);
                
                $rootScope.settingLegDatosFamiliares.dataset = legDatosFamiliares;
                iniciarPosiciones($rootScope.settingLegDatosFamiliares.dataset);
                $rootScope.tablaLegDatosFamiliares.settings($rootScope.settingLegDatosFamiliares);
                
                $rootScope.settingLegDatosAcademicos.dataset = legDatosAcademicos;
                iniciarPosiciones($scope.settingLegDatosAcademicos.dataset);
                $rootScope.tablaLegDatosAcademicos.settings($rootScope.settingLegDatosAcademicos);
                
                $scope.settingLegExperienciaLaboral.dataset = legExperienciaLaboral;
                iniciarPosiciones($rootScope.settingLegExperienciaLaboral.dataset);
                $rootScope.tablaLegExperienciaLaboral.settings($rootScope.settingLegExperienciaLaboral);
                
                $rootScope.settingLegReconocimientos.dataset = legReconocimientos;
                iniciarPosiciones($rootScope.settingLegReconocimientos.dataset);
                $rootScope.tablaLegReconocimientos.settings($rootScope.settingLegReconocimientos);
                
                $rootScope.settingLegDemeritos.dataset = legDemeritos;
                iniciarPosiciones($rootScope.settingLegDatosPersonales.dataset);
                $rootScope.tablaLegDemeritos.settings($rootScope.settingLegDemeritos);
                
                $rootScope.settingLegOtros.dataset = legOtros;
                iniciarPosiciones($rootScope.settingLegOtros.dataset);
                $rootScope.tablaLegOtros.settings($rootScope.settingLegOtros);
                
                
            }, function (data) {
                console.info(data);
            });
           
        };
        
        $scope.showNuevoLegajo = function () {
            ModalService.showModal({
                templateUrl: "administrativa/sistema_escalafon/agregarLegajoPersonal.html",
                controller: "agregarNuevoLegajoCtrl",
                inputs: {
                    title: "Nuevo Legajo",
                    fichaEscalafonariaId: datos.ficEscId
                }
            }).then(function (modal) {
                modal.element.modal();
                modal.close.then(function (result) {
                    if (result.flag) {
                    }
                });
            });
        };
        
        $scope.prepararEditarLegajo = function (lp) {
            var legajoSel = JSON.parse(JSON.stringify(lp));
            ModalService.showModal({
                templateUrl: "administrativa/sistema_escalafon/editarLegajo.html",
                controller: "editarLegajoCtrl",
                inputs: {
                    title: "Editar datos del legajo",
                    legajo: legajoSel
                }
            }).then(function (modal) {
                modal.element.modal();
            });
        };
        
        $scope.eliminarLegajo = function(l){   
            modal.mensajeConfirmacion($scope, "¿Seguro que desea eliminar el legajo?", function () {
                var request = crud.crearRequest('legajo_personal', 1, 'eliminarLegajo');
                request.setData({legId: l.legId});
                
                crud.eliminar('/sistema_escalafon', request, function (response) {
                    if (response.responseSta) {
                        _.remove($rootScope.tablaLegOtros.settings().dataset, function(item){
                            return l === item;
                        });
                        $rootScope.tablaLegOtros.reload();
                        modal.mensaje("CONFIRMACION", "La eliminación se realizó correctamente");
                    }else {
                            modal.mensaje("ERROR",response.responseMsg);
                    }
                }, function (error) {
                    modal.mensaje("ERROR","El servidor no responde");
                });
            }, '400'); 
        };
        
    }]);

app.controller('agregarNuevoLegajoCtrl', ['$scope', '$rootScope', '$element','title', 'fichaEscalafonariaId', 'close', 'crud', 'modal', 'ModalService', function ($scope, $rootScope, $element, title, fichaEscalafonariaId, close, crud, modal, ModalService) {
        //variable que servira para registrar un nuevo pariente
        $scope.nuevoLegajo = {
            ficEscId: fichaEscalafonariaId, 
            nomDoc: "", 
            archivo: "", 
            des: "", 
            url: "", 
            catLeg: "7", 
            subCat: "1", 
            codAspOri: "0"
        };
        $scope.title = title;


        $scope.agregarLegajo = function () {
            var request = crud.crearRequest('legajo_personal', 1, 'agregarLegajoPersonal');
            request.setData($scope.nuevoLegajo);
            crud.insertar("/sistema_escalafon", request, function (response) {
                modal.mensaje("CONFIRMACION", response.responseMsg);
                if (response.responseSta) {   
                    insertarElemento($rootScope.settingLegOtros.dataset, response.data);
                    $rootScope.tablaLegOtros.reload();
 
                }
            }, function (data) {
                console.info(data);
            });
        };
    }]);
app.controller('editarLegajoCtrl', ['$rootScope','$scope', '$element', 'title', 'legajo', 'close', 'crud', 'modal', function ($rootScope ,$scope, $element, title, legajo, close, crud, modal) {
        $scope.title = title;
        //variable que servira para registrar una nueva formacion academica
        $scope.legajoSel = {
            i: legajo.i,
            ficEscId: legajo.ficEscId, 
            legId: legajo.legId, 
            nomDoc: legajo.nomDoc, 
            archivo: legajo.archivo, 
            des: legajo.des, 
            url: legajo.url, 
            catLeg: legajo.catleg, 
            subCat: legajo.subCat, 
            codAspOri: legajo.codAspOri
        };
        
        
        $scope.actualizarLegajo = function () {
            var request = crud.crearRequest('legajo_personal', 1, 'actualizarLegajo');
            request.setData($scope.legajoSel);
            crud.actualizar("/sistema_escalafon", request, function (response) {
                modal.mensaje("CONFIRMACION", response.responseMsg);
                if (response.responseSta) {
                    response.data.i = legajo.i;
                    $rootScope.settingLegOtros.dataset[legajo.i] = response.data;
                    $rootScope.tablaLegOtros.reload();
                }
            }, function (data) {
                console.info(data);
            });
        };
        
    }]);