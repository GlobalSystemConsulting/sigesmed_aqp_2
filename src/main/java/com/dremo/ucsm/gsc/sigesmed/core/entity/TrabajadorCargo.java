/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.entity;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author ucsm
 */
@Entity
@Table(name = "trabajador_cargo")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TrabajadorCargo.findAll", query = "SELECT t FROM TrabajadorCargo t"),
    @NamedQuery(name = "TrabajadorCargo.findByCrgTraIde", query = "SELECT t FROM TrabajadorCargo t WHERE t.crgTraIde = :crgTraIde"),
    @NamedQuery(name = "TrabajadorCargo.findByCrgTraNom", query = "SELECT t FROM TrabajadorCargo t WHERE t.crgTraNom = :crgTraNom"),
    @NamedQuery(name = "TrabajadorCargo.findByCrgTraDes", query = "SELECT t FROM TrabajadorCargo t WHERE t.crgTraDes = :crgTraDes")})
public class TrabajadorCargo implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "crg_tra_ide")
    private Short crgTraIde;

    @Column(name = "crg_tra_nom")
    private String crgTraNom;

    @Column(name = "est_reg", length = 1)
    private String estReg;
    @Column(name = "crg_tra_tip", length = 3)
    private String traTip;
    @Size(max = 400)
    @Column(name = "crg_tra_des")
    private String crgTraDes;
    @OneToMany(mappedBy = "traCar")
    private List<Trabajador> trabajadorList;

    public TrabajadorCargo() {
    }

    public TrabajadorCargo(Short crgTraIde) {
        this.crgTraIde = crgTraIde;
    }

    public Short getCrgTraIde() {
        return crgTraIde;
    }

    public void setCrgTraIde(Short crgTraIde) {
        this.crgTraIde = crgTraIde;
    }

    public String getCrgTraNom() {
        return crgTraNom;
    }

    public void setCrgTraNom(String crgTraAli) {
        this.crgTraNom = crgTraAli;
    }

    public String getCrgTraDes() {
        return crgTraDes;
    }

    public void setCrgTraDes(String crgTraDes) {
        this.crgTraDes = crgTraDes;
    }
    
    public String getEstReg() {
        return estReg;
    }

    public void setEstReg(String estReg) {
        this.estReg = estReg;
    }

    public String getTraTip() {
        return traTip;
    }

    public void setTraTip(String traTip) {
        this.traTip = traTip;
    }
    
    @XmlTransient
    public List<Trabajador> getTrabajadorList() {
        return trabajadorList;
    }

    public void setTrabajadorList(List<Trabajador> trabajadorList) {
        this.trabajadorList = trabajadorList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (crgTraIde != null ? crgTraIde.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TrabajadorCargo)) {
            return false;
        }
        TrabajadorCargo other = (TrabajadorCargo) object;
        if ((this.crgTraIde == null && other.crgTraIde != null) || (this.crgTraIde != null && !this.crgTraIde.equals(other.crgTraIde))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.dremo.ucsm.gsc.sigesmed.core.entity.TrabajadorCargo[ crgTraIde=" + crgTraIde + " ]";
    }
    
}
