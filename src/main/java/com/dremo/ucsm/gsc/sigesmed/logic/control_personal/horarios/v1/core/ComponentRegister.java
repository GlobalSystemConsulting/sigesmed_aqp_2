/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.control_personal.horarios.v1.core;

import com.dremo.ucsm.gsc.sigesmed.core.service.WebComponent;
import com.dremo.ucsm.gsc.sigesmed.core.service.constantes.Sigesmed;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.IComponentRegister;
import com.dremo.ucsm.gsc.sigesmed.logic.control_personal.horarios.v1.tx.AsignarHorarioByCargoTx;
import com.dremo.ucsm.gsc.sigesmed.logic.control_personal.horarios.v1.tx.AsignarHorarioPersonalizadoTx;
import com.dremo.ucsm.gsc.sigesmed.logic.control_personal.horarios.v1.tx.BuscarHorarioActualByTrabajadorIdTx;
import com.dremo.ucsm.gsc.sigesmed.logic.control_personal.horarios.v1.tx.BuscarHorarioByTrabajadorIdTx;
import com.dremo.ucsm.gsc.sigesmed.logic.control_personal.horarios.v1.tx.EditarHorarioTx;
import com.dremo.ucsm.gsc.sigesmed.logic.control_personal.horarios.v1.tx.EliminarHorarioTx;
import com.dremo.ucsm.gsc.sigesmed.logic.control_personal.horarios.v1.tx.LiberarHorarioByCargoTx;
import com.dremo.ucsm.gsc.sigesmed.logic.control_personal.horarios.v1.tx.LiberarHorarioPersonalizadoGrupoTx;
import com.dremo.ucsm.gsc.sigesmed.logic.control_personal.horarios.v1.tx.LiberarHorarioPersonalizadoTx;
import com.dremo.ucsm.gsc.sigesmed.logic.control_personal.horarios.v1.tx.ListarCalendarioTx;
import com.dremo.ucsm.gsc.sigesmed.logic.control_personal.horarios.v1.tx.ListarCargosTx;
import com.dremo.ucsm.gsc.sigesmed.logic.control_personal.horarios.v1.tx.ListarDiaSemanaTx;
import com.dremo.ucsm.gsc.sigesmed.logic.control_personal.horarios.v1.tx.ListarHorariosByIdTx;
import com.dremo.ucsm.gsc.sigesmed.logic.control_personal.horarios.v1.tx.ListarHorariosTx;
import com.dremo.ucsm.gsc.sigesmed.logic.control_personal.horarios.v1.tx.NuevoHorarioTx;





/**
 *
 * @author carlos
 */
public class ComponentRegister implements IComponentRegister{
    @Override
    public WebComponent createComponent(){
        //Asiganando el modulo al que pertenece
        WebComponent component = new WebComponent(Sigesmed.MODULO_CONTROL_PERSONAL);        
        
        //Registrnado el Nombre del componente
        component.setName("registroHorario");
        //Version del componente
        component.setVersion(1);
        //Lista de operaciones de logica, propias del componente
        component.addTransactionPOST("editarHorario", EditarHorarioTx.class);
        component.addTransactionGET("listarHorarios", ListarHorariosTx.class);
        component.addTransactionGET("listarHorarioById", ListarHorariosByIdTx.class);
        component.addTransactionGET("listarCalendario", ListarCalendarioTx.class);
        component.addTransactionGET("listarDiaSemana", ListarDiaSemanaTx.class);
        component.addTransactionPOST("nuevoHorario", NuevoHorarioTx.class);
        component.addTransactionDELETE("eliminarHorario", EliminarHorarioTx.class);
        
        component.addTransactionPOST("asignarHorarioByCargo", AsignarHorarioByCargoTx.class);
        component.addTransactionGET("listarCargos", ListarCargosTx.class);
        component.addTransactionPOST("asignarHorarioPersonalizado", AsignarHorarioPersonalizadoTx.class);
 
        component.addTransactionGET("buscarHorarioByTrabajadorId", BuscarHorarioByTrabajadorIdTx.class);
        component.addTransactionGET("buscarHorarioActualByTrabajadorId", BuscarHorarioActualByTrabajadorIdTx.class);
        component.addTransactionPUT("liberarHorarioByCargo", LiberarHorarioByCargoTx.class);
        component.addTransactionPOST("liberarHorarioPersonalizadoGrupo", LiberarHorarioPersonalizadoGrupoTx.class);
        component.addTransactionPOST("liberarHorarioPersonalizado", LiberarHorarioPersonalizadoTx.class);
        //LiberarHorarioPersonalizadoTx
       
        return component;
    }
}
