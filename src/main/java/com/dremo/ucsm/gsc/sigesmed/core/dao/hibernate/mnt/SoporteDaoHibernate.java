/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.mnt;

import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.GenericDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.dao.mnt.SoporteDao;
import com.dremo.ucsm.gsc.sigesmed.core.datastore.HibernateUtil;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mnt.Peticion;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mnt.Soporte;
import org.hibernate.Query;
import org.hibernate.Session;

/**
 *
 * @author Administrador
 */
public class SoporteDaoHibernate  extends GenericDaoHibernate<Soporte> implements SoporteDao{

    @Override
    public Soporte obtenerPorIdMensaje(int menID) {
        Soporte s=null;
        Session session=HibernateUtil.getSessionFactory().openSession();
        try{
            String hql="SELECT p FROM Soporte p LEFT JOIN FETCH p.funcion func  LEFT JOIN FETCH func.subModuloSistema submod LEFT JOIN FETCH submod.moduloSistema mod WHERE p.mensaje.mensajeID=:p1";
            Query q=session.createQuery(hql);
            q.setParameter("p1",menID);
            s=(Soporte)q.uniqueResult();
            q.setMaxResults(1);
        }catch(Exception e){
            System.out.println("No se pudo obtener el soporte "+e.getMessage());
        }finally{
            session.close();
        }
        return s;
    }       
}
