package com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.plan_curricular;

import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.PeriodosPlanEstudios;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Administrador on 26/10/2016.
 */
@Entity
@Table(name = "unidad_didactica",schema = "pedagogico")
@Inheritance(strategy = InheritanceType.JOINED)
public abstract class UnidadDidactica  implements java.io.Serializable{
    @Id
    @Column(name = "uni_did_id")
    @SequenceGenerator(name = "unidad_didactica_uni_did_id_seq",sequenceName = "pedagogico.unidad_didactica_uni_did_id_seq")
    @GeneratedValue(generator = "unidad_didactica_uni_did_id_seq")
    private int uniDidId;
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "pro_anu_id")
    private ProgramacionAnual programacionAnual;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "per_pla_est_id")
    private PeriodosPlanEstudios periodo;
    
    @Column(name="per_pla_est_id" , insertable=false , updatable=false)
    private int per_pla_est_id;
    
    @Column(name = "tit",length = 50,nullable = false)
    private String tit;
    @Column(name = "tip", length = 1)
    private Character tip;
    @Column(name = "sit_sig",length = 100)
    private String sitSig;
    @Temporal(TemporalType.DATE)
    @Column(name = "fec_ini")
    private Date fecIni;
    @Temporal(TemporalType.DATE)
    @Column(name = "fec_fin")
    private Date fecFin;
    @Column(name = "num")
    private Integer num;
    @Column(name = "prod",length = 50)
    private String prod;
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "fec_mod")
    private Date fecMod;
    @Column(name = "usu_mod")
    private Integer usuMod;
    @Column(name = "est_reg", length = 1)
    private Character estReg;

    @OneToMany(mappedBy = "unidad",fetch = FetchType.LAZY)
    private List<ProductosUnidadAprendizaje> productos = new ArrayList<>();
    @OneToMany(mappedBy = "unidad",fetch = FetchType.LAZY)
    private List<EvaluacionesUnidadDidactica> evaluaciones = new ArrayList<>();
    @OneToMany(mappedBy = "unidadDidactica",fetch = FetchType.LAZY)
    private List<MaterialRecursoProgramacion> materiales = new ArrayList<>();
    @OneToMany(mappedBy = "unidad")
    private List<CompetenciasUnidadDidactica> competenciasUnidad;
   /* @OneToOne(mappedBy = "unidadDidactica", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private ProyectoAprendizaje proyectoAprendizaje;

    @OneToOne(mappedBy = "unidadDidactica", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private UnidadAprendizaje unidadAprendizaje;

    @OneToOne(mappedBy = "unidadDidactica", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private ModuloAprendizaje moduloAprendizaje;*/

    public UnidadDidactica() {
    }

    public void setPer_pla_est_id(int per_pla_est_id) {
        this.per_pla_est_id = per_pla_est_id;
    }

    public int getPer_pla_est_id() {
        return per_pla_est_id;
    }

    
    
    public UnidadDidactica(String tit, Character tip, String sitSig, Date fecIni, Date fecFin) {
        this.tit = tit;
        this.tip = tip;
        this.sitSig = sitSig;
        this.fecIni = fecIni;
        this.fecFin = fecFin;

        this.fecMod = new Date();
        this.estReg = 'A';
    }

    public UnidadDidactica(String tit, Character tip, String sitSig, Date fecIni, Date fecFin, Integer num, String prod) {
        this.tit = tit;
        this.tip = tip;
        this.sitSig = sitSig;
        this.fecIni = fecIni;
        this.fecFin = fecFin;
        this.num = num;
        this.prod = prod;

        this.fecMod = new Date();
        this.estReg = 'A';
    }

    public int getUniDidId() {
        return uniDidId;
    }

    public void setUniDidId(int uniDidId) {
        this.uniDidId = uniDidId;
    }

    public Integer getNum() {
        return num;
    }

    public void setNum(Integer num) {
        this.num = num;
    }

    public String getTit() {
        return tit;
    }

    public void setTit(String tit) {
        this.tit = tit;
    }

    public String getSitSig() {
        return sitSig;
    }

    public void setSitSig(String sitSig) {
        this.sitSig = sitSig;
    }

    public PeriodosPlanEstudios getPeriodo() {
        return periodo;
    }

    public void setPeriodo(PeriodosPlanEstudios periodo) {
        this.periodo = periodo;
    }

    public String getProd() {
        return prod;
    }

    public void setProd(String prod) {
        this.prod = prod;
    }

    public Character getTip() {
        return tip;
    }

    public void setTip(Character tip) {
        this.tip = tip;
    }

    public Date getFecIni() {
        return fecIni;
    }

    public void setFecIni(Date fecIni) {
        this.fecIni = fecIni;
    }

    public Date getFecFin() {
        return fecFin;
    }

    public void setFecFin(Date fecFin) {
        this.fecFin = fecFin;
    }

    public Date getFecMod() {
        return fecMod;
    }

    public void setFecMod(Date fecMod) {
        this.fecMod = fecMod;
    }

    public Integer getUsuMod() {
        return usuMod;
    }

    public void setUsuMod(Integer usuMod) {
        this.usuMod = usuMod;
    }

    public Character getEstReg() {
        return estReg;
    }

    public void setEstReg(Character estReg) {
        this.estReg = estReg;
    }

    public ProgramacionAnual getProgramacionAnual() {
        return programacionAnual;
    }

    public void setProgramacionAnual(ProgramacionAnual programacionAnual) {
        this.programacionAnual = programacionAnual;
    }

    public List<ProductosUnidadAprendizaje> getProductos() {
        return productos;
    }

    public void setProductos(List<ProductosUnidadAprendizaje> productos) {
        this.productos = productos;
    }

    public List<EvaluacionesUnidadDidactica> getEvaluaciones() {
        return evaluaciones;
    }

    public void setEvaluaciones(List<EvaluacionesUnidadDidactica> evaluaciones) {
        this.evaluaciones = evaluaciones;
    }

    public List<MaterialRecursoProgramacion> getMateriales() {
        return materiales;
    }

    public List<CompetenciasUnidadDidactica> getCompetenciasUnidad() {
        return competenciasUnidad;
    }

    public void setCompetenciasUnidad(List<CompetenciasUnidadDidactica> competenciasUnidad) {
        this.competenciasUnidad = competenciasUnidad;
    }

    public void setMateriales(List<MaterialRecursoProgramacion> materiales) {
        this.materiales = materiales;
    }

    /*public ProyectoAprendizaje getProyectoAprendizaje() {
        return proyectoAprendizaje;
    }

    public void setProyectoAprendizaje(ProyectoAprendizaje proyectoAprendizaje) {
        this.proyectoAprendizaje = proyectoAprendizaje;
    }

    public UnidadAprendizaje getUnidadAprendizaje() {
        return unidadAprendizaje;
    }

    public void setUnidadAprendizaje(UnidadAprendizaje unidadAprendizaje) {
        this.unidadAprendizaje = unidadAprendizaje;
    }

    public ModuloAprendizaje getModuloAprendizaje() {
        return moduloAprendizaje;
    }

    public void setModuloAprendizaje(ModuloAprendizaje moduloAprendizaje) {
        this.moduloAprendizaje = moduloAprendizaje;
    }*/

    @Override
    public String toString() {
        return "UnidadDidactica{" + "uniDidId=" + uniDidId + ", estReg=" + estReg + '}';
    }
    
    
}
