package com.dremo.ucsm.gsc.sigesmed.core.dao.maestro.plan;

import com.dremo.ucsm.gsc.sigesmed.core.dao.GenericDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.plan_curricular.IndicadorAprendizaje;

import java.util.List;

/**
 * Created by Administrador on 13/10/2016.
 */
public interface IndicadorAprendizajeDao extends GenericDao<IndicadorAprendizaje> {
    IndicadorAprendizaje buscarPorId(int id);
    List<IndicadorAprendizaje> buscarIndicadoresPorCapacidad(int capId);
}
