package com.dremo.ucsm.gsc.sigesmed.logic.maestro.carpeta.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.OrganizacionDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.maestro.carpeta.ContenidoSeccionCarpetaDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Organizacion;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Rol;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.carpeta.ArchivosCarpeta;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.base.EntityUtil;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by Administrador on 02/01/2017.
 */
public class ListarDetalleCarpetaAdminTx implements ITransaction{
    private static Logger logger = Logger.getLogger(ListarDetalleCarpetaDocenteTx.class.getName());
    @Override
    public WebResponse execute(WebRequest wr) {
        JSONObject data = (JSONObject) wr.getData();
        int idCar = data.getInt("car");
        int idOrg = data.getInt("org");
        int idUsu = data.getInt("usu");
        int idRol = data.getInt("rol");
        return listarDetalleCarpeta(idCar,idOrg,idUsu,idRol);
    }

    public WebResponse listarDetalleCarpeta(int idCar, int idOrg, int idUsu,int idRol) {
        try{
            int tipUsu = -1;
            switch (idRol){
                case 2 : tipUsu = 0;break;
                case 3: tipUsu = 1;break;
                case 11: tipUsu = 2;break;
            }
            if(tipUsu != -1){
                OrganizacionDao orgDao = (OrganizacionDao) FactoryDao.buildDao("OrganizacionDao");
                ContenidoSeccionCarpetaDao contDao = (ContenidoSeccionCarpetaDao) FactoryDao.buildDao("maestro.carpeta.ContenidoSeccionCarpetaDao");

                Organizacion organizacion = orgDao.buscarConTipoOrganizacionYPadre(idOrg);
                List<ArchivosCarpeta> archivos = contDao.listarDocumentosCarpetaAdmin(idCar,organizacion.getOrgId(),idUsu,tipUsu);
                JSONArray jsonArchivos = new JSONArray(EntityUtil.listToJSONString(
                        new String[]{"carDigId", "secCarPedId", "ord", "secNom", "conSecCarPedId", "conNom", "rutConCarId", "tipUsu", "pat", "nomFil"},
                        new String[]{"carId", "secId", "secOrd", "secNom", "conId", "conNom", "rutId", "tipUsu", "pat", "nomFil"},
                        archivos
                ));
                return WebResponse.crearWebResponseExito("Se creo con exito la tarea",jsonArchivos);
            }else {
                return WebResponse.crearWebResponseError("Rol Incorrecto del usuario");
            }
        }catch (Exception e){
            logger.log(Level.SEVERE,"listarDetalleCarpeta",e);
            return WebResponse.crearWebResponseError("No se puede listar los detalles de la carpeta");
        }
    }
}
