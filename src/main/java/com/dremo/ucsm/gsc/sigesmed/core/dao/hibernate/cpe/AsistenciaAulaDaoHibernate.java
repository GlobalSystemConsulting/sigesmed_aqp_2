    /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.cpe;

import com.dremo.ucsm.gsc.sigesmed.core.dao.cpe.AsistenciaAulaDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.GenericDaoHibernate;

import com.dremo.ucsm.gsc.sigesmed.core.datastore.HibernateUtil;
import com.dremo.ucsm.gsc.sigesmed.core.entity.cpe.AsistenciaAula;
import com.dremo.ucsm.gsc.sigesmed.core.entity.cpe.Docente;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mech.PlanNivel;
import com.dremo.ucsm.gsc.sigesmed.core.entity.cpe.PlazaMagisterial;
import java.util.Date;

import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author Carlos
 */
public class AsistenciaAulaDaoHibernate extends GenericDaoHibernate<AsistenciaAula> implements AsistenciaAulaDao {

   @Override
    public Boolean registrarHoraSalida(Long idRegistroAsistencia, Date hora,Integer hrs,String relacionHoras) {
        Boolean estado = Boolean.FALSE;
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction miTx = session.beginTransaction();
        try {
            String hql = "UPDATE  AsistenciaAula ra SET  ra.horaSalida=:p2 , ra.horasPedagogicas=:p3,ra.relacionHoras=:p4 WHERE  ra.asistenciaAulaId =:p1 AND ra.estReg<>'E'";
            Query query = session.createQuery(hql);
            query.setParameter("p2", hora);
            query.setParameter("p1", idRegistroAsistencia);
            query.setParameter("p3", hrs);
            query.setParameter("p4", relacionHoras);
            query.executeUpdate();
            estado = Boolean.TRUE;
            miTx.commit();

        } catch (Exception e) {
            miTx.rollback();
            e.printStackTrace();
            estado = Boolean.FALSE;
            System.out.println("No se Registro la Salida \\n " + e.getMessage());
            throw new UnsupportedOperationException("No se Registro la Salida \\n " + e.getMessage());
        } finally {
            session.close();
        }
        return estado;
    
    }

    @Override
    public List<PlanNivel> listarNivelesByOrganizacion(Integer organizacion) {
        List<PlanNivel> lista = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try {
            String hql = "SELECT  pn FROM PlanNivel pn INNER JOIN  pn.planEstudios pe  WHERE pe.orgId=:p1";

            Query query = session.createQuery(hql);
            query.setParameter("p1", organizacion);
            
            lista = query.list();

        } catch (Exception e) {
            System.out.println("No se pudo buscar los niveles \\n " + e.getMessage());
            e.printStackTrace();
            throw new UnsupportedOperationException("No se pudo buscar los niveles \\n " + e.getMessage());
        } finally {
            session.close();
        }

        return lista;

    }

    @Override
    public List<AsistenciaAula> listarAsistenciaAula(Integer docenteId, Date inicio, Date fin) {
       List<AsistenciaAula> lista = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try {
            String hql = "SELECT  asi FROM AsistenciaAula asi WHERE asi.docente=:p1 AND asi.horaIngreso>=:p2 AND asi.horaSalida>=:p2 AND asi.horaIngreso<=:p3 AND asi.horaSalida<=:p3";

            Query query = session.createQuery(hql);
            query.setParameter("p1", new Docente(docenteId));
            query.setParameter("p2", inicio);
            query.setParameter("p3", fin);
            
            lista = query.list();

        } catch (Exception e) {
            System.out.println("No se pudo buscar los niveles \\n " + e.getMessage());
            e.printStackTrace();
            throw new UnsupportedOperationException("No se pudo buscar los niveles \\n " + e.getMessage());
        } finally {
            session.close();
        }

        return lista; 
    }
    
    @Override
    public List<Docente> listarDocenteAndPlazas(Integer organizacion, Integer planNivelId) {
       List<Docente> lista = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try {
            String hql = "SELECT  DISTINCT dc  FROM cpe.Docente dc JOIN FETCH dc.persona pp JOIN FETCH  dc.plazas pz INNER JOIN pz.horarios hd INNER JOIN hd.horario hc  WHERE pz.orgId=:p1 AND hc.plaNivId=:p2 ORDER BY pp.apePat";

            Query query = session.createQuery(hql);
            query.setParameter("p1", organizacion);
            query.setParameter("p2", planNivelId);
            
            lista = query.list();

        } catch (Exception e) {
            System.out.println("No se pudo buscar los niveles \\n " + e.getMessage());
            e.printStackTrace();
            throw new UnsupportedOperationException("No se pudo buscar los niveles \\n " + e.getMessage());
        } finally {
            session.close();
        }

        return lista; 
    }
    
    @Override
    public PlazaMagisterial getPlazaMagisterialAndDistribucion(Integer plazaId) {
        PlazaMagisterial registro = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try {
            String hql = "SELECT pl FROM cpe.PlazaMagisterial pl JOIN FETCH pl.distribucionGrados  dg JOIN FETCH dg.grado WHERE pl.plaMagId=:p1 ";

            Query query = session.createQuery(hql);
            query.setMaxResults(1);
            query.setParameter("p1", plazaId);
            
            registro = (PlazaMagisterial)query.uniqueResult();

        } catch (Exception e) {
            System.out.println("No se pudo buscar la plaza Magisterial \\n " + e.getMessage());
            e.printStackTrace();
            throw new UnsupportedOperationException("No se pudo buscar la Plaza Magisterial \\n " + e.getMessage());
        } finally {
            session.close();
        }

        return registro;

    }

}
