/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.configuracion_inicial.area.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.TipoAreaDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import org.json.JSONObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import com.dremo.ucsm.gsc.sigesmed.core.entity.TipoArea;
import java.util.Date;

/**
 *
 * @author Administrador
 */
public class ActualizarTipoAreaTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
                
        /*
        *   Parte para la lectura, verificacion y validacion de datos
        */
        TipoArea tipoAreaAct = null;
        try{
            JSONObject requestData = (JSONObject)wr.getData();
            int tipoAreaID = requestData.getInt("tipoAreaID");
            String nombre = requestData.getString("nombre");
            String alias = requestData.getString("alias");
            String estado = requestData.getString("estado");
            
            tipoAreaAct = new TipoArea(tipoAreaID, nombre, alias, new Date(), wr.getIdUsuario(), estado.charAt(0));
        
        }catch(Exception e){
            System.out.println(e);
            return WebResponse.crearWebResponseError("No se pudo actualizar, datos incorrectos");
        }
        //Fin
        
        /*
        *  Parte para la operacion en la Base de Datos
        */
        TipoAreaDao areaDao = (TipoAreaDao)FactoryDao.buildDao("TipoAreaDao");
        try{
            areaDao.update(tipoAreaAct);
        
        }catch(Exception e){
            System.out.println("No se pudo actualizar el Tipo de Area\n"+e);
            return WebResponse.crearWebResponseError("No se pudo actualizar el Tipo Area", e.getMessage() );
        }
        //Fin
        
        /*
        *  Repuesta Correcta
        */        
        return WebResponse.crearWebResponseExito("El Tipo de Area se actualizo correctamente");
        //Fin
    }
    
}
