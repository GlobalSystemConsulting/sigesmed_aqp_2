package com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.banco;

import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.Seccion;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.plan_curricular.AreaCurricular;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mech.Grado;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by Administrador on 23/12/2016.
 */
@Entity
@Table(name = "banco_lectura_docente",schema = "pedagogico")
public class BancoLecturaDocente extends BancoLectura implements java.io.Serializable{

    @Column(name = "tem",nullable = true)
    private Integer temp;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "gra_id")
    private Grado grado;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "are_cur_id")
    private AreaCurricular area;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "secc")
    private Seccion seccion;

    public BancoLecturaDocente() {
        super();
    }

    public BancoLecturaDocente(Date fecCre, String des,int year) {
        super(fecCre, des);
        temp = year;
    }

    public BancoLecturaDocente(Date fecCre) {
        super(fecCre);
    }

    public Grado getGrado() {
        return grado;
    }

    public void setGrado(Grado grado) {
        this.grado = grado;
    }

    public AreaCurricular getArea() {
        return area;
    }

    public void setArea(AreaCurricular area) {
        this.area = area;
    }

    public Seccion getSeccion() {
        return seccion;
    }

    public void setSeccion(Seccion seccion) {
        this.seccion = seccion;
    }

    public Integer getTemp() {
        return temp;
    }

    public void setTemp(Integer temp) {
        this.temp = temp;
    }
}
