/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.sma.programacion_monitoreo.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.sma.MonitoreoDetalleDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sma.MonitoreoDetalle;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONObject;

/**
 *
 * @author Yemi
 */
public class ActualizarDetalleMonitoreoTx implements ITransaction {

    private static final Logger logger = Logger.getLogger(ActualizarDetalleMonitoreoTx.class.getName());

    @Override
    public WebResponse execute(WebRequest wr) {
        JSONObject data = (JSONObject) wr.getData();
        Integer usuarioId = wr.getIdUsuario();
        return editarMonitoreoDetalle(data, usuarioId);
    }

    private WebResponse editarMonitoreoDetalle(JSONObject data, int usuarioId) {
        try{
            DateFormat sdi = new SimpleDateFormat("yyyy-MM-dd");
            Integer monDetId = data.getInt("monDetId");
            Integer idEsp = data.getInt("espId");
            Date fecVis = sdi.parse(data.getString("fecVis").substring(0,10));
            String etapaDes = data.getString("etapaDes");
            String docDetalle = data.getString("docDetalle");
            String espDetalle = data.getString("espDetalle");

            MonitoreoDetalleDao monitoreoDao = (MonitoreoDetalleDao) FactoryDao.buildDao("sma.MonitoreoDetalleDao");
            MonitoreoDetalle monitoreoDetalle = monitoreoDao.buscarPorId(monDetId);

            monitoreoDetalle.setIdEsp(idEsp);
            monitoreoDetalle.setFecVis(fecVis);
            monitoreoDetalle.setFecMod(new Date());
            monitoreoDetalle.setUsuMod(usuarioId);
            
            monitoreoDao.update(monitoreoDetalle);
            
            JSONObject oResponse = new JSONObject();
            oResponse.put("fecVis", monitoreoDetalle.getFecVis());
            oResponse.put("etapa", monitoreoDetalle.getEtaMon());
            oResponse.put("etapaDes",etapaDes);
            oResponse.put("docId", monitoreoDetalle.getIdDoc());
            oResponse.put("docDetalle", docDetalle);
            oResponse.put("espId", monitoreoDetalle.getIdEsp());
            oResponse.put("espDetalle", espDetalle);

            return WebResponse.crearWebResponseExito("Se actualizo correctamente el detalle de monitoreo");
        }catch (Exception e){
            logger.log(Level.SEVERE,"actualizarDetalleMonitoreo",e);
            return WebResponse.crearWebResponseError("No se pudo actualizar el detalle de monitoreo");
        }
    }
}
