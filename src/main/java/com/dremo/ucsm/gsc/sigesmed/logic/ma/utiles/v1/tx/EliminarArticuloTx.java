/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.ma.utiles.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.ma.ArticuloEscolarDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.ma.ArticuloEscolar;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author ucsm
 */
public class EliminarArticuloTx implements ITransaction{
    private static final Logger logger = Logger.getLogger(EliminarArticuloTx.class.getName());

    @Override
    public WebResponse execute(WebRequest wr) {
        String idStr = wr.getMetadataValue("id");
        return eliminarArticulo(Integer.parseInt(idStr));
    }
    private WebResponse eliminarArticulo(int idStr){
        try{
            ArticuloEscolarDao articuloEscolarDao = (ArticuloEscolarDao) FactoryDao.buildDao("ma.ArticuloEscolarDao");
            ArticuloEscolar articulo = articuloEscolarDao.buscarPorId(idStr);
            articuloEscolarDao.delete(articulo);             
            return WebResponse.crearWebResponseExito("Operacion realizada con exito",WebResponse.OK_RESPONSE);
        }catch(Exception e){
            logger.log(Level.SEVERE,"eliminarArticulo",e);
            return WebResponse.crearWebResponseError("NO se realizo la operacion",WebResponse.BAD_RESPONSE);
        }
    }
}
