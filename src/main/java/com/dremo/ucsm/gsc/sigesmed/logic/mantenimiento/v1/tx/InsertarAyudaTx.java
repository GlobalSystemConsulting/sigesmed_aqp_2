/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.mantenimiento.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.mnt.AyudaDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mnt.Ayuda;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mnt.PasosAyuda;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.base.FileJsonObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author Administrador
 */
public class InsertarAyudaTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
        
        Ayuda nuevaAyuda=null;
        try{
            JSONObject requestData=(JSONObject)wr.getData();
            int funcionID=(requestData.getInt("funcionId"));
            char ayudaTipo=requestData.getString("ayudaTipo").charAt(0);
            char estReg=requestData.getString("ayudaEstReg").charAt(0);
            JSONArray listaPasos=requestData.getJSONArray("pasos");
            
            nuevaAyuda=new Ayuda(0,funcionID,ayudaTipo,estReg);
            
            //Leyendo pasos
            if(listaPasos.length()>0){
                nuevaAyuda.setPasosAyuda(new ArrayList<PasosAyuda>());
                for(int i=0;i<listaPasos.length();i++){
                    JSONObject pa=listaPasos.getJSONObject(i);
                    String paso_des=pa.getString("pasoDes");
                    char pasoEstReg=pa.getString("pasoEstReg").charAt(0);
                    nuevaAyuda.getPasosAyuda().add(new PasosAyuda(i+1,paso_des,nuevaAyuda,pasoEstReg));
                }
            }
        }catch(Exception e){
            System.out.println(e);
            return WebResponse.crearWebResponseError("No se puedo registrar la ayuda, datos incorrectos", e.getMessage());
        }
          /*
            *Parte de la BD
            */
            AyudaDao ayudaDao=(AyudaDao)FactoryDao.buildDao("mnt.AyudaDao");
            try{
                ayudaDao.insert(nuevaAyuda);
            }catch(Exception e){
                System.out.println("No se puede registrar\n" + e);
                return WebResponse.crearWebResponseError("No se pudo registrar", e.getMessage());
            }
            
            
            //SI OPERAICON FUE EXITOSA
            return WebResponse.crearWebResponseExito("El registro de la ayuda se realizo correctamente");
            
    }
    
}
