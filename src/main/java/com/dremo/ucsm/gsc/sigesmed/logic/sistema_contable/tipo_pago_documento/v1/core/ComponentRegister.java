/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.sistema_contable.tipo_pago_documento.v1.core;

import com.dremo.ucsm.gsc.sigesmed.core.service.WebComponent;
import com.dremo.ucsm.gsc.sigesmed.core.service.constantes.Sigesmed;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.IComponentRegister;
import com.dremo.ucsm.gsc.sigesmed.logic.sistema_contable.tipo_pago_documento.v1.tx.ActualizarTipoDocumentoIdentidadTx;
import com.dremo.ucsm.gsc.sigesmed.logic.sistema_contable.tipo_pago_documento.v1.tx.ActualizarTipoPagoTx;
import com.dremo.ucsm.gsc.sigesmed.logic.sistema_contable.tipo_pago_documento.v1.tx.EliminarTipoDocumentoIdentidadTx;
import com.dremo.ucsm.gsc.sigesmed.logic.sistema_contable.tipo_pago_documento.v1.tx.EliminarTipoPagoTx;
import com.dremo.ucsm.gsc.sigesmed.logic.sistema_contable.tipo_pago_documento.v1.tx.InsertarTipoDocumentoIdentidadTx;
import com.dremo.ucsm.gsc.sigesmed.logic.sistema_contable.tipo_pago_documento.v1.tx.InsertarTipoPagoTx;
import com.dremo.ucsm.gsc.sigesmed.logic.sistema_contable.tipo_pago_documento.v1.tx.ListarTipoDocumentoIdentidadTx;
import com.dremo.ucsm.gsc.sigesmed.logic.sistema_contable.tipo_pago_documento.v1.tx.ListarTipoPagoTx;

/**
 *
 * @author RE
 */
public class ComponentRegister implements IComponentRegister{
    @Override
    public WebComponent createComponent(){
        //Asiganando el modulo al que pertenece
        WebComponent component = new WebComponent(Sigesmed.MODULO_SISTEMA_CONTABLE_INSTITUCIONAL);        
        
        //Registrando el Nombre del componente
        component.setName("contable_datos");
        //Version del componente
        component.setVersion(1);
        //Lista de operaciones de logica, propias del componente        
        component.addTransactionPOST("insertarTipoDocumentoIdentidad", InsertarTipoDocumentoIdentidadTx.class);
        component.addTransactionGET("listarTipoDocumentoIdentidad", ListarTipoDocumentoIdentidadTx.class);
        component.addTransactionDELETE("eliminarTipoDocumentoIdentidad", EliminarTipoDocumentoIdentidadTx.class);
        component.addTransactionPUT("actualizarTipoDocumentoIdentidad", ActualizarTipoDocumentoIdentidadTx.class);
        
        component.addTransactionPOST("insertarTipoPago", InsertarTipoPagoTx.class);
        component.addTransactionGET("listarTipoPago", ListarTipoPagoTx.class);
        component.addTransactionDELETE("eliminarTipoPago", EliminarTipoPagoTx.class);
        component.addTransactionPUT("actualizarTipoPago", ActualizarTipoPagoTx.class);
        
    //    component.addTransactionPOST("insertarBanco", InsertarEmpresaTx.class);
    //    component.addTransactionGET("listarBancos", ListarEmpresasTx.class);
    //    component.addTransactionDELETE("eliminarBanco", EliminarEmpresaTx.class);
    //    component.addTransactionPUT("actualizarBanco", ActualizarEmpresaTx.class);
        
        return component;
    }
}
