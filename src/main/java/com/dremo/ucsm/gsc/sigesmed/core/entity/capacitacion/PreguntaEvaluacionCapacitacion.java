package com.dremo.ucsm.gsc.sigesmed.core.entity.capacitacion;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "pregunta_evaluacion_capacitacion", schema = "pedagogico")
public class PreguntaEvaluacionCapacitacion implements Serializable {

    @Id
    @Column(name = "pre_eva_cap_id", unique = true, nullable = false)
    @SequenceGenerator(name = "secuencia_pregunta_evaluacion_capacitacion", sequenceName = "pedagogico.pregunta_evaluacion_capacitacion_pre_eva_cap_id_seq")
    @GeneratedValue(generator = "secuencia_pregunta_evaluacion_capacitacion")
    private int preEvaCapId;

    @Column(name = "tip", nullable = false, length = 1)
    private Character tip;

    @Column(name = "res", nullable = false)
    private String res;

    @Column(name = "opc", nullable = false)
    private String opc;

    @Column(name = "est_reg", nullable = false, length = 1)
    private Character estReg;

    @Column(name = "usu_mod")
    private Integer usuMod;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "fec_mod")
    private Date fecMod;

    @Column(name = "pun", nullable = false)
    private double pun;

    @Column(name = "nom", nullable = false)
    private String nom;

    @Column(name = "des", nullable = false)
    private String des;

    @Column(name = "sed_cap_id")
    private int sedCapId;

    @Column(name = "eva_cur_cap_id")
    private int evaCurCapId;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumns({
        @JoinColumn(name = "eva_cur_cap_id", referencedColumnName = "eva_cur_cap_id", updatable = false, insertable = false),
        @JoinColumn(name = "sed_cap_id", referencedColumnName = "sed_cap_id", updatable = false, insertable = false)})
    private EvaluacionCursoCapacitacion evaluacionCursoCapacitacion;

    public PreguntaEvaluacionCapacitacion() {
    }

    public PreguntaEvaluacionCapacitacion(Character tip, String res, String opc, Character estReg, Integer usuMod, Date fecMod, double pun, String nom, String des, EvaluacionCursoCapacitacion evaluacionCursoCapacitacion) {
        this.tip = tip;
        this.res = res;
        this.opc = opc;
        this.estReg = estReg;
        this.usuMod = usuMod;
        this.fecMod = fecMod;
        this.pun = pun;
        this.nom = nom;
        this.des = des;
        this.evaCurCapId = evaluacionCursoCapacitacion.getEvaCurCapId();
        this.sedCapId = evaluacionCursoCapacitacion.getSede().getSedCapId();
    }

    public int getPreEvaCapId() {
        return preEvaCapId;
    }

    public void setPreEvaCapId(int preEvaCapId) {
        this.preEvaCapId = preEvaCapId;
    }

    public Character getTip() {
        return tip;
    }

    public void setTip(Character tip) {
        this.tip = tip;
    }

    public String getRes() {
        return res;
    }

    public void setRes(String res) {
        this.res = res;
    }

    public String getOpc() {
        return opc;
    }

    public void setOpc(String opc) {
        this.opc = opc;
    }

    public Character getEstReg() {
        return estReg;
    }

    public void setEstReg(Character estReg) {
        this.estReg = estReg;
    }

    public Integer getUsuMod() {
        return usuMod;
    }

    public void setUsuMod(Integer usuMod) {
        this.usuMod = usuMod;
    }

    public Date getFecMod() {
        return fecMod;
    }

    public void setFecMod(Date fecMod) {
        this.fecMod = fecMod;
    }

    public double getPun() {
        return pun;
    }

    public void setPun(double pun) {
        this.pun = pun;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getDes() {
        return des;
    }

    public void setDes(String des) {
        this.des = des;
    }

    public EvaluacionCursoCapacitacion getEvaluacionCursoCapacitacion() {
        return evaluacionCursoCapacitacion;
    }

    public void setEvaluacionCursoCapacitacion(EvaluacionCursoCapacitacion evaluacionCursoCapacitacion) {
        this.evaluacionCursoCapacitacion = evaluacionCursoCapacitacion;
        this.evaCurCapId = this.evaluacionCursoCapacitacion.getEvaCurCapId();
        this.sedCapId = this.evaluacionCursoCapacitacion.getSede().getSedCapId();
    }

    public int getSedCapId() {
        return sedCapId;
    }

    public void setSedCapId(int sedCapId) {
        this.sedCapId = sedCapId;
    }

    public int getEvaCurCapId() {
        return evaCurCapId;
    }

    public void setEvaCurCapId(int evaCurCapId) {
        this.evaCurCapId = evaCurCapId;
    }
}
