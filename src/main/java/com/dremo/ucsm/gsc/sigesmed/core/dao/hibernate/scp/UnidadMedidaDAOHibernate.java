/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.scp;
import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.GenericDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.datastore.HibernateUtil;
import java.util.List;
import com.dremo.ucsm.gsc.sigesmed.core.dao.scp.UnidadMedidaDAO;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import com.dremo.ucsm.gsc.sigesmed.core.entity.scp.UnidadMedida;
/**
 *
 * @author Administrador
 */
public class UnidadMedidaDAOHibernate extends GenericDaoHibernate<UnidadMedida> implements UnidadMedidaDAO {

    @Override
    public List<UnidadMedida> listarUnidadesMedida() {
        
        List<UnidadMedida> unidades_medida = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{      
            String hql = "SELECT  unidades FROM UnidadMedida unidades";
            Query query = session.createQuery(hql);
            unidades_medida = query.list();
        }catch(Exception e){
              System.out.println("No se pudo Obtener Los Grupos Genericos \\n "+ e.getMessage());
             throw new UnsupportedOperationException("No se pudo Obtener Los Grupos Genericos\\n "+ e.getMessage());
        }
         finally{
            session.close();
        }
        return unidades_medida;
     
      //  throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public UnidadMedida mostrarDetalleUnidadMedida(int un_med_id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    
}
