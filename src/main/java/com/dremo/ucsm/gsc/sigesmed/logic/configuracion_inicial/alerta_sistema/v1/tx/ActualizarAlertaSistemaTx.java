/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.configuracion_inicial.alerta_sistema.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.AlertaSistemaDao;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import org.json.JSONObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import com.dremo.ucsm.gsc.sigesmed.core.entity.AlertaSistema;

/**
 *
 * @author abel
 */
public class ActualizarAlertaSistemaTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
                
        /*
        *   Parte para la lectura, verificacion y validacion de datos
        */
        AlertaSistema alerta = null;
        try{
            JSONObject requestData = (JSONObject)wr.getData();
            int alertaID = requestData.getInt("alertaID");
            int funcionID = requestData.getInt("funcionID");
            String nombre = requestData.getString("nombre");
            //String codigo = requestData.getString("codigo");
            String descripcion = requestData.optString("descripcion");
            String accion = requestData.getString("accion");
            String tipo = requestData.getString("tipo");
            alerta = new AlertaSistema(alertaID,"", nombre, accion, descripcion, tipo.charAt(0),funcionID);
        
        }catch(Exception e){
            System.out.println(e);
            return WebResponse.crearWebResponseError("No se pudo actualizar, datos incorrectos", e.getMessage() );
        }
        //Fin
        
        /*
        *  Parte para la operacion en la Base de Datos
        */
        AlertaSistemaDao funcionDao = (AlertaSistemaDao)FactoryDao.buildDao("AlertaSistemaDao");
        try{
            funcionDao.update(alerta);
        
        }catch(Exception e){
            System.out.println("No se pudo actualizar la Alerta del Sistema \n"+e);
            return WebResponse.crearWebResponseError("No se pudo actualizar la Alerta del Sistema", e.getMessage() );
        }
        //Fin
        
        /*
        *  Repuesta Correcta
        */        
        return WebResponse.crearWebResponseExito("La Alerta del Sistema se actualizo correctamente");
        //Fin
    }
    
}
