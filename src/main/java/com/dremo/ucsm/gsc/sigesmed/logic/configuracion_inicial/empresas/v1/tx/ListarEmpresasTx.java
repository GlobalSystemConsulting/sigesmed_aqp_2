/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.configuracion_inicial.empresas.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.EmpresaDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.cpe.LibroAsistenciaDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Empresa;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import org.json.JSONArray;
import org.json.JSONObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.util.List;

/**
 *
 * @author carlos
 */
public class ListarEmpresasTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
        
       
//        try{
//            
//           
//        }catch(Exception e){
//            return WebResponse.crearWebResponseError("No se pudo verificar la Organizacion", e.getMessage() );
//        }

        
        EmpresaDao empresaDao = (EmpresaDao)FactoryDao.buildDao("EmpresaDao");
        List<Empresa> guiaEmpresas=null;
        try{
           guiaEmpresas=empresaDao.lsitarEmpresas();
        }catch(Exception e){
            return WebResponse.crearWebResponseError("No se pudo Listar el libro de Asistencia ", e.getMessage() );
        }

       JSONArray miArray = new JSONArray();
       int i=1;
       for(Empresa emp:guiaEmpresas)
       {
           JSONObject oRes=new JSONObject();
           oRes.put("i", i++);
           oRes.put("empresaID", emp.getEmpId());
           oRes.put("razonSocial", emp.getRazonSocial());
           oRes.put("ruc", emp.getRuc());
           oRes.put("pagWeb", emp.getWeb());
           oRes.put("telefono", emp.getNum());
           oRes.put("estado", emp.getEstReg()+"");
           miArray.put(oRes);
       }
       
        
        
        return WebResponse.crearWebResponseExito("Se Listo correctamente",miArray);        
        //Fin
    }
    
}

