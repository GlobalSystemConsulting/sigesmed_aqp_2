package com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.capacitacion;

import com.dremo.ucsm.gsc.sigesmed.core.dao.capacitacion.ComentarioTemaCapacitacionDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.GenericDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.datastore.HibernateUtil;
import com.dremo.ucsm.gsc.sigesmed.core.entity.capacitacion.ComentarioTemaCapacitacion;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.json.JSONException;
import org.json.JSONObject;

public class ComentarioTemaCapacitacionDaoHibernate extends GenericDaoHibernate<ComentarioTemaCapacitacion> implements ComentarioTemaCapacitacionDao {

    private static final Logger logger = Logger.getLogger(ComentarioTemaCapacitacionDaoHibernate.class.getName());

    @Override
    public List<ComentarioTemaCapacitacion> listarComentarios(int temCod) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try {
            Criteria query = session.createCriteria(ComentarioTemaCapacitacion.class)
                    .add(Restrictions.eq("estReg", 'P'))
                    .add(Restrictions.eq("temCurCapId", temCod))
                    .addOrder(Order.asc("fec"))
                    .setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);

            return query.list();
        } catch (Exception e) {
            logger.log(Level.SEVERE, "listarComentarios", e);
            throw e;
        } finally {
            session.close();
        }
    }

    @Override
    public List<ComentarioTemaCapacitacion> listarRespuestas(int comCod) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try {
            Criteria query = session.createCriteria(ComentarioTemaCapacitacion.class)
                    .add(Restrictions.eq("estReg", 'H'))
                    .addOrder(Order.asc("fec"))
                    .createCriteria("comentarioRes")
                    .add(Restrictions.eq("comTemCapId", comCod))
                    .setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);

            return query.list();
        } catch (Exception e) {
            logger.log(Level.SEVERE, "listarRespuestas", e);
            throw e;
        } finally {
            session.close();
        }
    }

    @Override
    public JSONObject obtenerSesion(int usuId) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try {

            String queryStr = "SELECT usuario_session.usu_ses_id, persona.ape_mat, persona.ape_pat, persona.nom "
                    + "FROM public.usuario, public.usuario_session, pedagogico.persona "
                    + "WHERE usuario.usu_id = usuario_session.usu_id AND usuario_session.usu_id = persona.per_id AND usuario.usu_id = " + usuId;

            JSONObject object = new JSONObject();
            Object[] answer = (Object[]) session.createSQLQuery(queryStr).uniqueResult();
            object.put("id", Integer.parseInt(answer[0].toString()));
            object.put("nom", answer[2].toString() + " " + answer[1].toString() + " " + answer[3].toString());

            return object;
        } catch (NumberFormatException | JSONException e) {
            logger.log(Level.SEVERE, "obtenerSesion", e);
            throw e;
        } finally {
            session.close();
        }
    }

    @Override
    public ComentarioTemaCapacitacion buscarPorId(int idCom) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try {
            Criteria query = session.createCriteria(ComentarioTemaCapacitacion.class)
                    .add(Restrictions.eq("comTemCapId", idCom));

            return (ComentarioTemaCapacitacion) query.uniqueResult();
        } catch (Exception e) {
            logger.log(Level.SEVERE, "buscarPorId", e);
            throw e;
        } finally {
            session.close();
        }
    }
}
