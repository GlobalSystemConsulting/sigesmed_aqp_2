/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.util;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartUtilities;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.category.CategoryDataset;
import org.jfree.data.general.DefaultPieDataset;
import org.jfree.data.general.PieDataset;

/**
 *
 * @author Administrador
 */
public class MChart {

    public void MPieChart(String nameImage, DefaultPieDataset dataset, String titleChart, int ancho, int alto) throws IOException {
                                
        File pieChart = new File( nameImage ); 
        ChartUtilities.saveChartAsJPEG( pieChart , createPieChart(dataset, titleChart) , ancho , alto );
        
    }   
    
    public void MBarChart(String nameImage, CategoryDataset dataSet, String titleChart, String nombreEjeX, String nombreEjeY , int ancho, int alto) throws IOException {
                                
        File pieChart = new File( nameImage ); 
        ChartUtilities.saveChartAsJPEG( pieChart , createBarChart(dataSet, titleChart, nombreEjeX, nombreEjeY), ancho ,alto );
        
    }
    
    public static JFreeChart createBarChart( CategoryDataset dataSet, String titleChart, String nombreEjeX, String nombreEjeY)
    {
        JFreeChart barChart = ChartFactory.createBarChart(
            titleChart,           
            nombreEjeX,            
            nombreEjeY,            
            dataSet,          
            PlotOrientation.VERTICAL,           
            true, true, false);        
        
        return barChart;
    } 
        
//    private static PieDataset createDataset(String[] names, Double[] values) 
//    {
//        DefaultPieDataset dataset = new DefaultPieDataset( );
//        for(int i = 0; i < names.length; ++i)
//            dataset.setValue(names[i], values[i]);  
//
//        return dataset;         
//    }
    
    public static JFreeChart createPieChart( PieDataset dataset, String titleChart)
    {
        JFreeChart chart = ChartFactory.createPieChart(      
           titleChart,  // chart title 
           dataset,        // data    
           true,           // include legend   
           true, 
           false);

        return chart;
    }   
        
}
