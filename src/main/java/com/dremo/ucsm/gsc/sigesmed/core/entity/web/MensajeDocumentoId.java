/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.entity.web;

import java.io.Serializable;

/**
 *
 * @author abel
 */
public final class MensajeDocumentoId implements Serializable {
    
    private static final long serialVersionUID = 1562260205094677677L;
        private Integer mensaje;
    private int menDocId;

    public MensajeDocumentoId() {
    }

    public MensajeDocumentoId(Integer mensaje, int menDocId) {
        this.setMensaje(mensaje);
        this.setMenDocId(menDocId);
    }

    @Override
    public int hashCode() {
        return ((this.getMensaje() == null
                ? 0 : this.getMensaje().hashCode())
                ^ ((int) this.getMenDocId()));
    }

    @Override
    public boolean equals(Object otherOb) {

        if (this == otherOb) {
            return true;
        }
        if (!(otherOb instanceof MensajeDocumentoId)) {
            return false;
        }
        MensajeDocumentoId other = (MensajeDocumentoId) otherOb;
        return ((this.getMensaje() == null
                ? other.getMensaje() == null : this.getMensaje()
                .equals(other.getMensaje()))
                && (this.getMenDocId() == other.getMenDocId()));
    }

    @Override
    public String toString() {
        return "" + getMensaje() + "-" + getMenDocId();
    }

    public Integer getMensaje() {
        return mensaje;
    }

    public void setMensaje(Integer mensaje) {
        this.mensaje = mensaje;
    }

    public int getMenDocId() {
        return menDocId;
    }

    public void setMenDocId(int menDocId) {
        this.menDocId = menDocId;
    }
    
}
