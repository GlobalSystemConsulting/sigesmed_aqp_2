/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.sistema_contable.tipo_pago_documento.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.sci.TipoPagoDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sci.TipoPago;

import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import org.json.JSONObject;

/**
 *
 * @author RE
 */
public class EliminarTipoPagoTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
        
        /*
        *   Parte para la lectura, verificacion y validacion de datos
        */
        int tipoPagoID = 0;
        try{
            JSONObject requestData = (JSONObject)wr.getData();
            tipoPagoID = requestData.getInt("tipoPagoID");
        
        }catch(Exception e){
            System.out.println(e);
            return WebResponse.crearWebResponseError("No se pudo eliminar, datos incorrectos");
        }
        //Fin
        
        /*
        *  Parte para la operacion en la Base de Datos
        */
       TipoPagoDao tipoPagoDao = (TipoPagoDao)FactoryDao.buildDao("sci.TipoPagoDao");
        try{
            tipoPagoDao.delete(new TipoPago((short)tipoPagoID));
        
        }catch(Exception e){
            System.out.println("No se pudo eliminar el Tipo pago\n"+e);
            return WebResponse.crearWebResponseError("No se pudo eliminar el  Tipo pago", e.getMessage() );
        }
        //Fin
        
        /*
        *  Repuesta Correcta
        */        
        return WebResponse.crearWebResponseExito("El  Tipo pago se elimino correctamente");
        //Fin
    }
}
