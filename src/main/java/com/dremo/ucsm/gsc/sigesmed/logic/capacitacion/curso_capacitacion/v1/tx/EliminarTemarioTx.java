package com.dremo.ucsm.gsc.sigesmed.logic.capacitacion.curso_capacitacion.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.capacitacion.EvaluacionDesarrolloDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.capacitacion.TemarioCursoCapacitacionDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.capacitacion.DesarrolloTemaCapacitacion;
import com.dremo.ucsm.gsc.sigesmed.core.entity.capacitacion.EvaluacionDesarrollo;
import com.dremo.ucsm.gsc.sigesmed.core.entity.capacitacion.TemarioCursoCapacitacion;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONObject;

public class EliminarTemarioTx implements ITransaction {
    
    private static final Logger logger = Logger.getLogger(EliminarTemarioTx.class.getName());

    @Override
    public WebResponse execute(WebRequest wr) {
        try {
            JSONObject data = (JSONObject) wr.getData();
            TemarioCursoCapacitacionDao temarioCursoCapacitacionDao = (TemarioCursoCapacitacionDao) FactoryDao.buildDao("capacitacion.TemarioCursoCapacitacionDao");
            EvaluacionDesarrolloDao evaluacionDesarrolloDao = (EvaluacionDesarrolloDao) FactoryDao.buildDao("capacitacion.EvaluacionDesarrolloDao");
            TemarioCursoCapacitacion tema = temarioCursoCapacitacionDao.buscarEliminar(data.getInt("temCod"));
            
            for(DesarrolloTemaCapacitacion desarrollo: tema.getDesarrollos()) {
                for(EvaluacionDesarrollo evaluacion: desarrollo.getEvaluaciones()) {
                    evaluacion.setEstReg('E');
                    evaluacionDesarrolloDao.update(evaluacion);
                }
            }
            
            tema.setEstReg('E');
            tema.setUsuMod(data.getInt("usuMod"));
            tema.setFecMod(new Date());
            
            temarioCursoCapacitacionDao.update(tema);
            
            return WebResponse.crearWebResponseExito("Exito al eliminar el tema", WebResponse.OK_RESPONSE);
        } catch (Exception e) {
            logger.log(Level.SEVERE, "editarTemaCapacitacion", e);
            return WebResponse.crearWebResponseError("Error al eliminar el tema", WebResponse.BAD_RESPONSE);
        }
    }
}
