package com.dremo.ucsm.gsc.sigesmed.logic.capacitacion.curso_capacitacion.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.capacitacion.DesarrolloTemaCapacitacionDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.capacitacion.EvaluacionDesarrolloDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.capacitacion.AdjuntoEvaluacionDesarrollo;
import com.dremo.ucsm.gsc.sigesmed.core.entity.capacitacion.DesarrolloTemaCapacitacion;
import com.dremo.ucsm.gsc.sigesmed.core.entity.capacitacion.EvaluacionDesarrollo;
import com.dremo.ucsm.gsc.sigesmed.core.entity.capacitacion.Persona;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.constantes.Sigesmed;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

public class ListarEvaluacionesDesarrolloTx implements ITransaction {

    private static final Logger logger = Logger.getLogger(ListarEvaluacionesDesarrolloTx.class.getName());

    @Override
    public WebResponse execute(WebRequest wr) {
        JSONObject data = (JSONObject) wr.getData();
        WebResponse response = null;

        switch (data.getInt("opt")) {
            case 0:
                response = listarParticipantes(data);
                break;

            case 1:
                response = obtenerEvaluacion(data);
                break;
        }

        return response;
    }

    private WebResponse listarParticipantes(JSONObject data) {
        try {
            EvaluacionDesarrolloDao evaluacionDesarrolloDao = (EvaluacionDesarrolloDao) FactoryDao.buildDao("capacitacion.EvaluacionDesarrolloDao");
            DesarrolloTemaCapacitacionDao desarrolloTemaCapacitacionDao = (DesarrolloTemaCapacitacionDao) FactoryDao.buildDao("capacitacion.DesarrolloTemaCapacitacionDao");
            
            List<EvaluacionDesarrollo> evaluaciones = evaluacionDesarrolloDao.buscarParticipantes(data.getInt("desCod"));
            DesarrolloTemaCapacitacion desarrollo = desarrolloTemaCapacitacionDao.buscarPorId(data.getInt("desCod"));
            
            JSONArray array = new JSONArray();
            
            for (EvaluacionDesarrollo evaluation : evaluaciones) {
                JSONObject object = new JSONObject();
                
                if(desarrollo.getFecEnt().before(new Date()) && evaluation.getEstReg() == 'A') {
                    evaluation.setEstReg('N');
                    evaluacionDesarrolloDao.update(evaluation);
                    object.put("state", "N");
                } else 
                    object.put("state", evaluation.getEstReg());
                
                Persona persona = evaluation.getPersona();
                object.put("des", evaluation.getId().getDesTemCapId());
                object.put("doc", evaluation.getId().getPerId());
                object.put("name", persona.getApePat() + " " + persona.getApeMat() + " " + persona.getNom());
                object.put("grade", evaluation.getNotPar());
                        
                JSONArray arrayAttachment = new JSONArray();

                for (AdjuntoEvaluacionDesarrollo attachment : evaluation.getAdjuntos()) {
                    JSONObject objectAttachment = new JSONObject();
                    objectAttachment.put("cod", attachment.getAdjEvaDesId());

                    String nomFile = attachment.getNom();
                    nomFile = nomFile.substring(nomFile.lastIndexOf("_adj_") + 6);
                    nomFile = nomFile.substring(nomFile.indexOf("_") + 1);
                    nomFile = nomFile.substring(0, nomFile.lastIndexOf("."));

                    objectAttachment.put("nom", nomFile);
                    objectAttachment.put("url", data.getString("url") + Sigesmed.UBI_ARCHIVOS + HelpTraining.attachments_ED_Address + attachment.getNom());

                    arrayAttachment.put(objectAttachment);
                }

                object.put("adj", arrayAttachment);
                array.put(object);
           }

            return WebResponse.crearWebResponseExito("Se listo correctamente", WebResponse.OK_RESPONSE).setData(array);
        } catch (Exception e) {
            logger.log(Level.SEVERE, "listarParticipantes", e);
            return WebResponse.crearWebResponseError("Error al las evaluaciones desarrollo", WebResponse.BAD_RESPONSE);
        }
    }

    private WebResponse obtenerEvaluacion(JSONObject data) {
        try {
            EvaluacionDesarrolloDao evaluacionDesarrolloDao = (EvaluacionDesarrolloDao) FactoryDao.buildDao("capacitacion.EvaluacionDesarrolloDao");
            EvaluacionDesarrollo evaluacion = evaluacionDesarrolloDao.buscarPorId(data.getInt("des"), data.getInt("doc"));
            JSONObject object = new JSONObject();
            object.put("est", evaluacion.getEstReg());
            object.put("not", evaluacion.getNotPar());

            if(evaluacion.getEstReg() == 'E' || evaluacion.getEstReg() == 'C') {
                JSONArray arrayAttachment = new JSONArray();
                for (AdjuntoEvaluacionDesarrollo attachment : evaluacion.getAdjuntos()) {
                    JSONObject objectAttachment = new JSONObject();
                    objectAttachment.put("cod", attachment.getAdjEvaDesId());

                    String nomFile = attachment.getNom();
                    nomFile = nomFile.substring(nomFile.lastIndexOf("_adj_") + 6);
                    nomFile = nomFile.substring(nomFile.indexOf("_") + 1);
                    nomFile = nomFile.substring(0, nomFile.lastIndexOf("."));

                    objectAttachment.put("nom", nomFile);
                    objectAttachment.put("url", data.getString("url") + Sigesmed.UBI_ARCHIVOS + HelpTraining.attachments_ED_Address + attachment.getNom());

                    arrayAttachment.put(objectAttachment);
                }

                object.put("adj", arrayAttachment);
            }
            
            return WebResponse.crearWebResponseExito("Se listo correctamente", WebResponse.OK_RESPONSE).setData(object);
        } catch (Exception e) {
            logger.log(Level.SEVERE, "obtenerEvaluacion", e);
            return WebResponse.crearWebResponseError("Error al las evaluaciones desarrollo", WebResponse.BAD_RESPONSE);
        }
    }
}
