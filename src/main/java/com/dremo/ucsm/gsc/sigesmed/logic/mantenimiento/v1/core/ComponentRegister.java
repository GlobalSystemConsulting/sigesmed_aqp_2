/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.mantenimiento.v1.core;

import com.dremo.ucsm.gsc.sigesmed.core.service.WebComponent;
import com.dremo.ucsm.gsc.sigesmed.core.service.constantes.Sigesmed;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.IComponentRegister;
import com.dremo.ucsm.gsc.sigesmed.logic.mantenimiento.v1.tx.ArchivarMensajeTx;
import com.dremo.ucsm.gsc.sigesmed.logic.mantenimiento.v1.tx.BuscarPersonaPorIdTx;
import com.dremo.ucsm.gsc.sigesmed.logic.mantenimiento.v1.tx.EliminarAyudaTx;
import com.dremo.ucsm.gsc.sigesmed.logic.mantenimiento.v1.tx.EliminarMensajeTx;
import com.dremo.ucsm.gsc.sigesmed.logic.mantenimiento.v1.tx.InsertarAyudaTx;
import com.dremo.ucsm.gsc.sigesmed.logic.mantenimiento.v1.tx.InsertarMensajeTx;
import com.dremo.ucsm.gsc.sigesmed.logic.mantenimiento.v1.tx.InsertarPersonalizacionTx;
import com.dremo.ucsm.gsc.sigesmed.logic.mantenimiento.v1.tx.ListarConfiguracionPorUsuarioTx;
import com.dremo.ucsm.gsc.sigesmed.logic.mantenimiento.v1.tx.ListarMensajesEnviadosPeticionTx;
import com.dremo.ucsm.gsc.sigesmed.logic.mantenimiento.v1.tx.ListarMensajesRecibidosTx;
import com.dremo.ucsm.gsc.sigesmed.logic.mantenimiento.v1.tx.ListarPasosDeFuncionalidadTx;

/**
 *
 * @author Administrador
 */
public class ComponentRegister implements IComponentRegister{

    @Override
    public WebComponent createComponent() {
        WebComponent component = new WebComponent(Sigesmed.SUBMODULO_MANTENIMIENTO);
        component.setName("mantenimiento");
        component.setVersion(1);
        component.addTransactionPOST("insertarAyuda",InsertarAyudaTx.class);
        component.addTransactionGET("listarPasosDeFuncionalidad", ListarPasosDeFuncionalidadTx.class);
         component.addTransactionDELETE("eliminarAyuda", EliminarAyudaTx.class);
         component.addTransactionPOST("insertarMensaje", InsertarMensajeTx.class);
         component.addTransactionGET("buscarPersonaPorId", BuscarPersonaPorIdTx.class);
         component.addTransactionGET("listarMensajesEnviadosPeticion",ListarMensajesEnviadosPeticionTx.class);
         component.addTransactionPUT("archivarMensaje",ArchivarMensajeTx.class);
         component.addTransactionDELETE("eliminarMensaje", EliminarMensajeTx.class);
         
         component.addTransactionGET("listarMensajesRecibidos",ListarMensajesRecibidosTx.class);
         component.addTransactionPOST("insertarPersonalizacion",InsertarPersonalizacionTx.class);
         component.addTransactionGET("listarConfiguracionPorUsuario",ListarConfiguracionPorUsuarioTx.class);
         
        return component;
    }
    
}
