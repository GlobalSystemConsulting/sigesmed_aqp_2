/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.util;

/**
 *
 * @author abel
 */
public final class BuildCodigo {
    
    public static String cerosIzquierda(long codigo,int numDigitos){
		
        String nuevoCodigo = String.valueOf( codigo );

        int numCeros = numDigitos - nuevoCodigo.length();

        for (int i=0;i < numCeros;i++)
            nuevoCodigo="0"+nuevoCodigo;

        return nuevoCodigo;
    }
}
