package com.dremo.ucsm.gsc.sigesmed.core.dao;

import com.dremo.ucsm.gsc.sigesmed.core.entity.SubModuloSistema;
import java.util.List;

public interface SubModuloSistemaDao extends GenericDao<SubModuloSistema> {

    public List<SubModuloSistema> buscarConFunciones();

    public List<SubModuloSistema> buscarConModulo();

    public List<SubModuloSistema> buscarConModuloYFunciones();

    public List<SubModuloSistema> listarSubModulos(int modCod);
}
