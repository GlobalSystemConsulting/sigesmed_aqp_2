/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.configuracion_inicial.area.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.AreaDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import org.json.JSONArray;
import org.json.JSONObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Area;
import java.util.List;

/**
 *
 * @author Administrador
 */
public class ListarAreaTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
        
        /*
        *  Parte para la operacion en la Base de Datos
        */
        List<Area> areas = null;
        AreaDao areaDao = (AreaDao)FactoryDao.buildDao("AreaDao");
        try{
            areas = areaDao.buscarConOrganizacionYAreaPadre();
        
        }catch(Exception e){
            System.out.println("No se pudo Listar las Areas\n"+e);
            return WebResponse.crearWebResponseError("No se pudo Listar las Areas", e.getMessage() );
        }
        //Fin
        
        /*
        *  Repuesta Correcta
        */
        JSONArray miArray = new JSONArray();
        for(Area area:areas ){
            JSONObject oResponse = new JSONObject();
            oResponse.put("areaID",area.getAreId() );
            oResponse.put("organizacionID",area.getOrganizacion().getOrgId() );
            if(area.getAreaPadre()!=null){
                oResponse.put("areaPadreID",area.getAreaPadre().getAreId() );
                oResponse.put("areaPadre",area.getAreaPadre().getNom() );
            }
            else
                oResponse.put("areaPadreID",0 );
            oResponse.put("tipoAreaID",area.getTipoArea().getTipAreId() );
            oResponse.put("codigo",area.getCod());
            oResponse.put("nombre",area.getNom());
            oResponse.put("fecha",area.getFecMod().toString());
            oResponse.put("estado",""+area.getEstReg());
            miArray.put(oResponse);
        }
        
        return WebResponse.crearWebResponseExito("Se Listo correctamente",miArray);        
        //Fin
    }
    
}
