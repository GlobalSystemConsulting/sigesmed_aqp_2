/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.ma.utiles.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.ma.ListaUtilesDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.ma.ListaUtiles;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.base.EntityUtil;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.json.JsonObject;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author ucsm
 */
public class ListarListaUtilesTx implements ITransaction{
    private static final Logger logger = Logger.getLogger(ListarListaUtilesTx.class.getName());
    @Override
    public WebResponse execute(WebRequest wr) {
        /*String idOrgStr = wr.getMetadataValue("org");
        String idUser = wr.getMetadataValue("user");*/
        JSONObject data = (JSONObject)wr.getData();
        int idOrg = data.getInt("org");
        int idUsr = data.getInt("usr");
        return listarListas(idOrg,idUsr);
    }
    private WebResponse listarListas(int idOrg,int idUsr){
        try{
            ListaUtilesDao listaUtilesDao = (ListaUtilesDao) FactoryDao.buildDao("ma.ListaUtilesDao");
            List<ListaUtiles> listas = listaUtilesDao.listarListasPorOrganizacionUsuario(idOrg, idUsr);
            JSONArray jsonListas = new JSONArray();
            for(ListaUtiles lista : listas){
                /*String a = EntityUtil.objectToJSONString(
                        new String[]{"listUtiId","anioLista","tituloLista","precioTotal"},
                        new String[]{"id","ani","tit","pre"},lista);*/
                
                JSONObject jsonLista = new JSONObject();
                jsonLista.put("id", lista.getListUtiId());
                jsonLista.put("ani", lista.getAnioLista());
                jsonLista.put("pre",lista.getPrecioTotal());
                jsonLista.put("tit",lista.getTituloLista());
                
                jsonLista.put("nivel",new JSONObject()
                        .put("idn", lista.getNivel().getNivId())
                        .put("nomn", lista.getNivel().getNom()));
                
                jsonLista.put("grado",new JSONObject()
                        .put("idg", lista.getGrado().getGraId())
                        .put("nomg",lista.getGrado().getNom()));
                
                jsonLista.put("area",new JSONObject()
                        .put("ida", lista.getArea().getAreCurId())
                        .put("noma",lista.getArea().getNom()));
                
                logger.log(Level.INFO," Json: {0}",jsonLista.toString());
                JSONObject jsonOrg = new JSONObject(
                       EntityUtil.objectToJSONString(new String[]{"orgId","nom"}, new String[]{"id","nom"},lista.getOrganizacion()));
                jsonLista.put("org",jsonOrg);

               jsonListas.put(jsonLista);
            }
            return WebResponse.crearWebResponseExito("Se obtuvieron con exito",jsonListas);
        }catch(Exception e){
            logger.log(Level.SEVERE,"listarListasDeUtiles",e);
            return WebResponse.crearWebResponseError("Error en listar los datos",WebResponse.BAD_RESPONSE);
        }
    }
}
