/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.rdg;

import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.GenericDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.dao.rdg.ItemFileDetalleDao;
import com.dremo.ucsm.gsc.sigesmed.core.datastore.HibernateUtil;
import com.dremo.ucsm.gsc.sigesmed.core.entity.rdg.ItemFile;
import com.dremo.ucsm.gsc.sigesmed.core.entity.rdg.ItemFileDetalle;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author ucsm
 */
public class ItemFileDetalleDaoHibernate  extends GenericDaoHibernate<ItemFileDetalle> implements ItemFileDetalleDao{

    @Override
    public List<ItemFileDetalle> buscarByArch(ItemFile arch) { 
        List<ItemFileDetalle> listItemsDet = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            //listar requisitos   
             
            //Listado de ItemFileDetalle con sus permisos
            String hql = "SELECT ifd FROM ItemFileDetalle ifd WHERE ifd.ifdIteIde=:p1 and ifd.estReg!='E'";
            Query query = session.createQuery(hql);
            query.setParameter("p1", arch);
            listItemsDet = query.list();
        
        }catch(Exception e){
            System.out.println("No se pudo Listar los item file detalles \\n "+ e.getMessage());
            throw new UnsupportedOperationException("No se pudo Listar los item file detalles \\n "+ e.getMessage());            
        }
        finally{
            session.close();
        }
        return listItemsDet;
        
    }
    
    @Override
    public int eliminarPorIde(int ide) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction miTx = session.beginTransaction();
        int result = 0;
        try{
            String hql = "DELETE FROM ItemFileDetalle ifd WHERE ifd.ifdIde  =:p1";
            Query query = session.createQuery(hql);
            query.setInteger("p1", ide);
            result = query.executeUpdate();    
            miTx.commit();
        
        }catch(Exception e){
            miTx.rollback();
            System.out.println("No se pudo eliminar Totalmente la comparticion : "+e.getMessage());
            throw e;
        }
        finally{
            session.close();
        }
        return result;
    }

    @Override
    public boolean verificarExisteComparticion(int userID, ItemFile item) {
        Session session=HibernateUtil.getSessionFactory().openSession();
        boolean flag=true;
        try{
            String hql="SELECT count(*) FROM ItemFileDetalle itemDet WHERE itemDet.ifdUsu =:p1 and itemDet.ifdIteIde =:p2 and itemDet.estReg='A'";
            Query q=session.createQuery(hql);
            q.setParameter("p1", userID);
            q.setParameter("p2", item);

            Long temp=(Long)q.uniqueResult();
             if(temp>0)
                flag=false;
            else
                flag=true;
        }catch(Exception e){
            System.out.println("No se pudo verificar si el documento ya esta compartido "+e.getMessage());
            throw new UnsupportedOperationException("No se pudo verificar si el documento ya esta compartido"); //To change body of generated methods, choose Tools | Templates.
        }finally{
            session.close();
        }
        return flag;
    }

    @Override
    public boolean esCompartido(ItemFile item) {
        Session session=HibernateUtil.getSessionFactory().openSession();
        boolean flag=false;
        try{
           String hql="SELECT count(*) FROM ItemFileDetalle itemDet WHERE itemDet.ifdIteIde =:p1 and itemDet.estReg='A'";
             Query q=session.createQuery(hql);
            q.setParameter("p1", item);
            Long temp=(Long)q.uniqueResult();    
            

            if(temp>0)
                flag=true;//Es compartido
            else
                flag=false;
        }catch(Exception e){
            System.out.println("No se pudo determinar si el archivo es propio o compartido "+e.getMessage());
            throw new UnsupportedOperationException("No se pudo determinar si el archivo es propio o compartido"); //To change body of generated methods, choose Tools | Templates.
        }finally{
            session.close();
        }        
        return flag;
    }

    @Override
    public List<ItemFileDetalle> buscarCompartidos(int usuCod,ItemFile padre) {
         List<ItemFileDetalle> documentos = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            String hql="SELECT det FROM ItemFileDetalle det JOIN FETCH det.ifdIteIde item "
                    + "WHERE det.estReg!='E' and det.ifdUsu=:p1 and det.ifdIteIde.itePadIde=:p2";
            
            //String hql="SELECT itemFil FROM ItemFile itemFil WHERE itemFil.itePadIde.iteIde=:p1 AND itemFil.estReg='A' AND itemFil.iteMod='N' AND itemFil.iteCodCat!='D' AND itemFil.iteUsuIde=:p2";
            Query query = session.createQuery(hql);
            query.setParameter("p1", usuCod);
            query.setParameter("p2",padre);
            documentos = query.list();        
        }catch(Exception e){
            System.out.println("No se pudo listar los documentos compartidos \\n "+ e.getMessage());
            throw new UnsupportedOperationException("No se pudo listar los documentos compartidos\\n "+ e.getMessage());            
        }
        finally{
            session.close();
        }
        return documentos;
    }

    @Override
    public void eliminarComparticionPorDocYUsu(int ideDoc, int ideUsu) {
        Session session=HibernateUtil.getSessionFactory().openSession();
        Transaction miTx = session.beginTransaction();
        try{
            String hql="UPDATE ItemFileDetalle det SET det.estReg='E' WHERE det.ifdIteIde.iteIde=:p1 AND det.ifdUsu=:p2";
            Query q=session.createQuery(hql);
            q.setParameter("p1",ideDoc);
            q.setParameter("p2",ideUsu);
            q.executeUpdate();
            miTx.commit();
        }catch(Exception e){
            miTx.rollback();
            System.out.println("No se pudo eliminar la compartición "+e.getMessage());
            throw new UnsupportedOperationException("No se pudo eliminar la compartición "+e.getMessage()); 
        }finally{
            session.close();
        }        
    }

    @Override
    public void actualizarComparticionConNuevaVersionDoc(int ideDocNew, int ideDocOld) {
        Session session=HibernateUtil.getSessionFactory().openSession();
        Transaction miTx=session.beginTransaction();
        try{
            String hql="UPDATE ItemFileDetalle det SET det.ifdIteIde.iteIde=:p1 WHERE det.ifdIteIde.iteIde=:p2 and det.estReg='A'";
            Query q=session.createQuery(hql);
            q.setParameter("p1",ideDocNew);
            q.setParameter("p2",ideDocOld);
            q.executeUpdate();
            miTx.commit();
        }catch(Exception e){
            miTx.rollback();
            System.out.println("No se pudo actualizar la nueva version con los usuarios compartidos " +e.getMessage());
            throw new UnsupportedOperationException("No se pudo actualizar la nueva version con los usuarios compartidos " +e.getMessage()); 
        }finally{
            session.close();
        }
        
    }
    
}
