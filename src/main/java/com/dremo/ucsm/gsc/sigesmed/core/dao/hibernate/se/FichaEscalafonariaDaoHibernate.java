/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.se;

import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.GenericDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.datastore.HibernateUtil;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.FichaEscalafonaria;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.FichaEscalafonariaDao;

/**
 *
 * @author gscadmin
 */
public class FichaEscalafonariaDaoHibernate extends GenericDaoHibernate<FichaEscalafonaria> implements FichaEscalafonariaDao{

    @Override
    public FichaEscalafonaria obtenerDatosPersonales(int orgId, int perId) {
        FichaEscalafonaria datosEscalafon = null;        
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction t = session.beginTransaction();
                
        try{   
            String hql = "SELECT fe FROM FichaEscalafonaria AS fe "
                    + "join fetch fe.trabajador t "
                    + "join fetch t.persona p "
                    + "join fetch t.organizacion o "
                    + "WHERE p.perId = " + perId + 
                    " AND o.orgId = " + orgId + 
                    " AND fe.estReg = 'A'" +
                    " AND t.estReg = 'A'";
            
            Query query = session.createQuery(hql);  
            query.setMaxResults(1);
            datosEscalafon = (FichaEscalafonaria)query.uniqueResult();
            t.commit();
        
        }catch(Exception e){
            t.rollback();
            System.out.println("No se pudo Listar los datos escalafon \\n "+ e.getMessage());
            throw new UnsupportedOperationException("No se pudo Listar los datos de escalafon \\n "+ e.getMessage());            
        }
        finally{
            session.close();
        }
        return datosEscalafon;
    }

    @Override
    public List<FichaEscalafonaria> ListarxOrganizacion(int orgId) {
        List<FichaEscalafonaria> fichaEscalafonaria = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction t = session.beginTransaction();
        try{
            String hql = "SELECT fe FROM FichaEscalafonaria as fe "
                    + "join fetch fe.trabajador t "
                    + "join fetch t.persona p "
                    + "join fetch t.organizacion o "
                    + "WHERE o.orgId=" +orgId; 
            Query query = session.createQuery(hql);            
            fichaEscalafonaria = query.list();
            t.commit();
        
        }catch(Exception e){
            t.rollback();
            System.out.println("No se pudo Listar los datos escalafon por organización \\n "+ e.getMessage());
            throw new UnsupportedOperationException("No se pudo Listar los datos escalalfon por organización \\n "+ e.getMessage());            
        }
        finally{
            session.close();
        }
        return fichaEscalafonaria;
    }

    @Override
    public FichaEscalafonaria buscarPorDNI(String perDNI) {
        FichaEscalafonaria fichaEscalafonaria = null;        
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction t = session.beginTransaction();
                
        try{   
            String hql = "SELECT fe FROM FichaEscalafonaria AS fe "
                    + "join fetch fe.trabajador as t "
                    + "join fetch t.persona as p "
                    + "WHERE p.dni= '" + perDNI + "'";
            
            Query query = session.createQuery(hql);
            query.setMaxResults(1);
            fichaEscalafonaria = (FichaEscalafonaria)query.uniqueResult();
            t.commit();
        
        }catch(Exception e){
            t.rollback();
            System.out.println("No se pudo  \\n "+ e.getMessage());
            throw new UnsupportedOperationException("No se pudo  \\n "+ e.getMessage());            
        }
        finally{
            session.close();
        }
        return fichaEscalafonaria;
        
    }

    @Override
    public FichaEscalafonaria buscarPorId(Integer ficEscId) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        FichaEscalafonaria f = (FichaEscalafonaria)session.get(FichaEscalafonaria.class, ficEscId);
        session.close();
        return f;
    }

    @Override
    public FichaEscalafonaria buscarPorUsuId(Integer usuId) {
        FichaEscalafonaria fichaEscalafonaria = null;        
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction t = session.beginTransaction();
                
        try{   
            String hql = "SELECT fe FROM FichaEscalafonaria AS fe"
                    + " join fetch fe.trabajador as t"
                    + " join fetch t.persona as p"
                    + " join fetch p.usuario u"
                    + " WHERE u.usuId =:cod";
            
            Query query = session.createQuery(hql);
            query.setParameter("cod", usuId);
            query.setMaxResults(1);
            fichaEscalafonaria = (FichaEscalafonaria)query.uniqueResult();
            t.commit();
        
        }catch(Exception e){
            t.rollback();
            System.out.println("No se pudo  \\n "+ e.getMessage());
            throw new UnsupportedOperationException("No se pudo  \\n "+ e.getMessage());            
        }
        finally{
            session.close();
        }
        return fichaEscalafonaria;
    }
}
