package com.dremo.ucsm.gsc.sigesmed.logic.maestro.anecdotario.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.maestro.anecdotario.AnecdotarioDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.anecdotario.Anecdotario;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import org.json.JSONObject;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by Administrador on 22/12/2016.
 */
public class EliminarAnecdotaTx implements ITransaction{
    private static Logger logger = Logger.getLogger(EliminarAnecdotaTx.class.getName());
    @Override
    public WebResponse execute(WebRequest wr) {
        JSONObject jsonData = (JSONObject)wr.getData();
        int idAnec = jsonData.getInt("id");
        return eliminarAnecdota(idAnec);
    }

    private WebResponse eliminarAnecdota(int idAnec) {
        try{
            AnecdotarioDao anecDao = (AnecdotarioDao) FactoryDao.buildDao("maestro.anecdotario.AnecdotarioDao");
            Anecdotario anecdota = anecDao.buscarAnecdota(idAnec);
            anecDao.deleteAbsolute(anecdota);
            return WebResponse.crearWebResponseExito("Se elimino correctamente la anecdota");

        }catch (Exception e){
            logger.log(Level.SEVERE,"eliminarAnecdota",e);
            return WebResponse.crearWebResponseError("no se puede eliminar la anecdota");
        }
    }
}
