/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.datos_personales.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.DireccionDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Direccion;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import static com.dremo.ucsm.gsc.sigesmed.util.JSONUtil.getUbigeoLocation;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author Yemi
 */
public class ListarDireccionesPorPersonaTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
        /*
        *  Parte para la operacion en la Base de Datos
        */        
        
        JSONObject requestData = (JSONObject)wr.getData();
        int perId = requestData.getInt("perId");
                
        List<Direccion> direcciones = new ArrayList<Direccion>();
        DireccionDao direccionDao = (DireccionDao)FactoryDao.buildDao("se.DireccionDao");
        
        try{
            direcciones = direccionDao.listarxPersona(perId);
        }catch(Exception e){
            System.out.println("No se pudolListar las direcciones\n"+e);
            return WebResponse.crearWebResponseError("No se pudo las direcciones", e.getMessage() );
        }


        JSONArray miArray = new JSONArray();
        for(Direccion dir:direcciones ){
            JSONObject oResponse = new JSONObject();
            oResponse.put("dirId", dir.getDirId());
            oResponse.put("tipDir", dir.getTip());
            String depDir = dir.getDep() ;
            String proDir = dir.getPro();
            String disDir =  dir.getDis();
            String [] locationDir = getUbigeoLocation(depDir+proDir+disDir);
            
            
            oResponse.put("nomDir", dir.getNom());
            oResponse.put("depDir", depDir);
            oResponse.put("proDir", proDir);
            oResponse.put("disDir", disDir);
            oResponse.put("depDirDes", locationDir[0]);
            oResponse.put("proDirDes", locationDir[1]);
            oResponse.put("disDirDes", locationDir[2]);
            
            oResponse.put("nomZon", dir.getNomZon());
            oResponse.put("desRef", dir.getDesRef());
            miArray.put(oResponse);
        }
        
        return WebResponse.crearWebResponseExito("Se listo correctamente",miArray);
    }
    
}
