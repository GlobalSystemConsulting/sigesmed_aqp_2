package com.dremo.ucsm.gsc.sigesmed.logic.matricula_institucional.traslado_ingreso.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.mmi.EstudianteMMIDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.mmi.GradoSeccionPlanNivelDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mmi.EstudianteMMI;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mmi.OrganizacionMMI;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import org.json.JSONException;
import org.json.JSONObject;

public class BuscarEstudianteTrasladoTx implements ITransaction {

    @Override
    public WebResponse execute(WebRequest wr) {
        EstudianteMMIDaoHibernate dh = new EstudianteMMIDaoHibernate();
        GradoSeccionPlanNivelDaoHibernate orgHb = new GradoSeccionPlanNivelDaoHibernate();
        EstudianteMMI estudianteMmi;
        JSONObject requestData = (JSONObject) wr.getData();
        JSONObject estudiante = new JSONObject();
        String nombre = "";

        try {
            String estDNI = requestData.getString("estDNI");
            estudianteMmi = dh.find4Dni(estDNI);

            if (estudianteMmi == null) {
                return WebResponse.crearWebResponseError("No se encontro el estudiante! ", estudiante);
            } else {
                if ((Long) estudianteMmi.getPerId() != null) {
                    estudiante.put("estPerId", estudianteMmi.getPerId());
                } else {
                    estudiante.put("estPerId", -1);
                }
                if (estudianteMmi.getPersona() != null) {
                    if (estudianteMmi.getPersona().getNom() != null) {
                        nombre = nombre + estudianteMmi.getPersona().getNom() + " ";
                    }
                    if (estudianteMmi.getPersona().getApePat() != null) {
                        nombre = nombre + estudianteMmi.getPersona().getApePat() + " ";
                    }
                    if (estudianteMmi.getPersona().getApeMat() != null) {
                        nombre = nombre + estudianteMmi.getPersona().getApeMat();
                    }
                    estudiante.put("estPerNom", nombre);
                } else {
                    estudiante.put("estPerNom", "Desconocido");
                }
                if (estudianteMmi.getOrgId() != null) {
                    estudiante.put("estOrgOriId", estudianteMmi.getOrgId());
                    estudiante.put("estOrgOriNom", orgHb.getOrganizacionNombre(estudianteMmi.getOrgId()));
                } else {
                    estudiante.put("estOrgOriId", -1);
                    estudiante.put("estOrgOriNom", "I.E. Desconocida");
                }
                if (estudianteMmi.getUltGraCul() != null) {
                        estudiante.put("ultGraCul", estudianteMmi.getUltGraCul());
                        estudiante.put("ultGraCulNom", selectLastGrade(estudianteMmi.getUltGraCul()));
                    } else {
                        estudiante.put("ultGraCul", 0);
                        estudiante.put("ultGraCulNom", "Sin Antecedentes");
                    }
            }

        } catch (NumberFormatException | JSONException e) {
            return WebResponse.crearWebResponseError("No se encontro el estudiante! ", e.getMessage());
        }

        return WebResponse.crearWebResponseExito("Se encontro al estudiante", estudiante);
    }
    
    private String selectLastGrade(String tipo) {
        String returnKey;
        try {
            switch (Integer.parseInt(tipo)) {
                case 0: {
                    returnKey = "Sin Antecedentes";
                    break;
                }
                case 1: {
                    returnKey = "Inicial 0 a 2 años";
                    break;
                }
                case 2: {
                    returnKey = "Inicial 3 a 5 años";
                    break;
                }
                case 3: {
                    returnKey = "1ro de Primaria";
                    break;
                }
                case 4: {
                    returnKey = "2do de Primaria";
                    break;
                }
                case 5: {
                    returnKey = "3ro de Primaria";
                    break;
                }
                case 6: {
                    returnKey = "4to de Primaria";
                    break;
                }
                case 7: {
                    returnKey = "5to de Primaria";
                    break;
                }
                case 8: {
                    returnKey = "6to de Primaria";
                    break;
                }
                case 9: {
                    returnKey = "1ro de Secundaria";
                    break;
                }
                case 10: {
                    returnKey = "2do se Secundaria";
                    break;
                }
                case 11: {
                    returnKey = "3ro de Secundaria";
                    break;
                }
                case 12: {
                    returnKey = "4to de Secundaria";
                    break;
                }
                case 13: {
                    returnKey = "5to de Secundaria";
                    break;
                }
                default: {
                    returnKey = "Sin Antecedentes";
                    break;
                }

            }
        } catch (Exception e) {
            returnKey = "Sin Antecedentes";
        }
        return returnKey;
    }

}
