package com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.scec;

import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.GenericDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.dao.scec.ReunionComisionDao;
import com.dremo.ucsm.gsc.sigesmed.core.datastore.HibernateUtil;
import com.dremo.ucsm.gsc.sigesmed.core.entity.scec.ReunionComision;
import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.Session;
import org.hibernate.criterion.CriteriaSpecification;
import org.hibernate.criterion.Restrictions;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by geank on 30/09/16.
 */
public class ReunionComisionDaoHibernate extends GenericDaoHibernate<ReunionComision> implements ReunionComisionDao{
    private static final Logger logger = Logger.getLogger(ReunionComisionDaoHibernate.class.getName());

    @Override
    public ReunionComision buscarReunionPorId(int id) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            ReunionComision reunion  = (ReunionComision)session.get(ReunionComision.class, id);

            return reunion;
        }catch (Exception e){
            logger.log(Level.SEVERE,"buscarPorId",e);
            throw e;
        }finally {
            session.close();
        }
    }

    @Override
    public ReunionComision buscarActasyAsistenciaPorReunion(int id) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            Criteria criteria = session.createCriteria(ReunionComision.class)
                    .setFetchMode("asistenciasReunion",FetchMode.JOIN)
                    .setFetchMode("comision",FetchMode.JOIN)
                    .add(Restrictions.eq("reuId",id));
            ReunionComision reunion = (ReunionComision)criteria.uniqueResult();
            return reunion;
        }catch (Exception e){
            logger.log(Level.SEVERE," buscarActasyAsistenciaPorReunion",e);
            throw e;
        }finally {
            session.close();
        }
    }

    @Override
    public ReunionComision buscarReunionConActas(int id) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            Criteria criteria = session.createCriteria(ReunionComision.class)
                    .setFetchMode("actasReunion",FetchMode.JOIN);

            criteria.add(Restrictions.eq("reuId",id));
            return  (ReunionComision)criteria.uniqueResult();

        }catch (Exception e){
            logger.log(Level.SEVERE,"buscarReunionConActas",e);
            throw e;
        }finally {
            session.close();
        }
    }

    @Override
    public List<ReunionComision> buscarReunionesPorComision(int com) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            Criteria query = session.createCriteria(ReunionComision.class)
                    .setFetchMode("actasReunion", FetchMode.JOIN);
            query.createCriteria("comision").add(Restrictions.eq("comId",com))
                    .setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY);
            query.add(Restrictions.eq("estReg",'A'));

            return query.list();
        }catch (Exception e){
            logger.log(Level.SEVERE,"buscarReunionesPorComision",e);
            throw e;
        }finally {
            session.close();
        }
    }

    @Override
    public List<ReunionComision> buscarReunionesConAsistencias(int com) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            Criteria query = session.createCriteria(ReunionComision.class)
                    .setFetchMode("asistenciasReunion", FetchMode.JOIN)
                    .createCriteria("comision").add(Restrictions.eq("comId",com));
            query.add(Restrictions.eq("estReg",'A'))
                    .setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY);
            return query.list();
        }catch (Exception e){
            logger.log(Level.SEVERE,"buscarReunionesPorComision",e);
            throw e;
        }finally {
            session.close();
        }
    }
}
