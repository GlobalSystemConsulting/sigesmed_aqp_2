package com.dremo.ucsm.gsc.sigesmed.core.entity.capacitacion;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "evaluaciones_curso_capacitacion", schema = "pedagogico")
public class EvaluacionCursoCapacitacion implements Serializable {

    @Id
    @Column(name = "eva_cur_cap_id", unique = true, nullable = false)
    @SequenceGenerator(name = "secuencia_evaluaciones_curso_capacitacion", sequenceName = "pedagogico.evaluaciones_curso_capacitacion_eva_cur_cap_id_seq")
    @GeneratedValue(generator = "secuencia_evaluaciones_curso_capacitacion")
    private int evaCurCapId;

    @Id
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "sed_cap_id", referencedColumnName = "sed_cap_id", insertable = false, updatable = false)
    private SedeCapacitacion sede;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "fec_ini", nullable = false)
    private Date fecIni;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "fec_fin", nullable = false)
    private Date fecFin;

    @Column(name = "tip", length = 1, nullable = false)
    private Character tip;

    @Column(name = "est_reg", length = 1, nullable = false)
    private Character estReg;

    @Column(name = "obs")
    private String obs;

    @Column(name = "nom", nullable = false)
    private String nom;

    @Column(name = "ord_ale", nullable = false)
    private boolean ordAle;

    @Column(name = "num_int")
    private Integer numInt;
    
    public EvaluacionCursoCapacitacion() {
    }

    public EvaluacionCursoCapacitacion(SedeCapacitacion sede, Date fecIni, Date fecFin, Character tip, Character estReg, String nom) {
        this.sede = sede;
        this.fecIni = fecIni;
        this.fecFin = fecFin;
        this.tip = tip;
        this.estReg = estReg;
        this.nom = nom;
    }

    public int getEvaCurCapId() {
        return evaCurCapId;
    }

    public void setEvaCurCapId(int evaCurCapId) {
        this.evaCurCapId = evaCurCapId;
    }

    public SedeCapacitacion getSede() {
        return sede;
    }

    public void setSedeCap(SedeCapacitacion sede) {
        this.sede = sede;
    }

    public Date getFecIni() {
        return fecIni;
    }

    public void setFecIni(Date fecIni) {
        this.fecIni = fecIni;
    }

    public Date getFecFin() {
        return fecFin;
    }

    public void setFecFin(Date fecFin) {
        this.fecFin = fecFin;
    }

    public Character getTip() {
        return tip;
    }

    public void setTip(Character tip) {
        this.tip = tip;
    }

    public Character getEstReg() {
        return estReg;
    }

    public void setEstReg(Character estReg) {
        this.estReg = estReg;
    }

    public String getObs() {
        return obs;
    }

    public void setObs(String obs) {
        this.obs = obs;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public boolean isOrdAle() {
        return ordAle;
    }

    public void setOrdAle(boolean ordAle) {
        this.ordAle = ordAle;
    }

    public Integer getNumInt() {
        return numInt;
    }

    public void setNumInt(Integer numInt) {
        this.numInt = numInt;
    }
}
