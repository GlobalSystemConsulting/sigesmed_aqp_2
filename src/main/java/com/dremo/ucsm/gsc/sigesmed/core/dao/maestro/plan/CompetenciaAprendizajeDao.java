package com.dremo.ucsm.gsc.sigesmed.core.dao.maestro.plan;

import com.dremo.ucsm.gsc.sigesmed.core.dao.GenericDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.plan_curricular.CompetenciaAprendizaje;

import java.util.List;

/**
 * Created by Administrador on 13/10/2016.
 */
public interface CompetenciaAprendizajeDao extends GenericDao<CompetenciaAprendizaje> {
    List<CompetenciaAprendizaje> buscarCompetenciasConCapacidades();
    CompetenciaAprendizaje buscarCompetenciPorCodigo(int cod);
    List<CompetenciaAprendizaje> buscarCompetenciaPorArea(int idArea);
    CompetenciaAprendizaje buscarCompetenciaPorNombre(String nombre);
}
