/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.archivo_digital.inventario_transferencia.v1.tx;


import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import org.json.JSONObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sad.ArchivosInventarioTransferencia;
import org.json.JSONArray;
import com.dremo.ucsm.gsc.sigesmed.core.service.base.FileJsonObject;
import com.dremo.ucsm.gsc.sigesmed.util.BuildFile;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.text.SimpleDateFormat;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sad.ArchivosInventarioTransferencia;
import com.dremo.ucsm.gsc.sigesmed.core.dao.sad.ArchivoInventarioDAO;
import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;




/**
 *
 * @author Administrador
 */
public class EliminarArchivoTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
        
        ArchivosInventarioTransferencia archivo = null;
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
         ArchivoInventarioDAO archivo_dao = (ArchivoInventarioDAO)FactoryDao.buildDao("sad.ArchivoInventarioDAO");
        try{
            JSONObject requestData = (JSONObject)wr.getData();
            int arc_inv_tra_id = requestData.getInt("arc_inv_tra_id");
            int det_inv_tra = requestData.getInt("det_inv_trans");
            int num_fol = requestData.getInt("num_fol");
            String titulo = requestData.getString("titulo");
           // Date fecha = formatter.parse(requestData.getString("fecha"));
            String cod_expediente = requestData.getString("cod_expediente");
            Date fecha = new Date();
            String nombre_archivo = "";
            archivo = new ArchivosInventarioTransferencia(arc_inv_tra_id,det_inv_tra,num_fol,titulo,fecha,cod_expediente,nombre_archivo);
            
            archivo_dao.deleteAbsolute(archivo);
            
        }catch(Exception e){
              System.out.println(e);
             return WebResponse.crearWebResponseError("No se pudo eliminar el Archivo de Inventario, datos incorrectos", e.getMessage() );
             
        }
          return WebResponse.crearWebResponseExito("El Archivo se elimino correctamente");
     //   throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}

