/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.control_personal.horarios.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.cpe.HorarioDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Organizacion;
import com.dremo.ucsm.gsc.sigesmed.core.entity.cpe.DiasEspeciales;
import com.dremo.ucsm.gsc.sigesmed.core.entity.cpe.HorarioCab;
import com.dremo.ucsm.gsc.sigesmed.core.entity.cpe.HorarioDet;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import org.json.JSONArray;
import org.json.JSONObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author carlos
 */
public class ListarCalendarioTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
        
        Integer org;
        
        try{
            JSONObject requestData = (JSONObject)wr.getData();
            org = requestData.getInt("organizacionID");        
           
        }catch(Exception e){
            return WebResponse.crearWebResponseError("No se pudo verificar la Organizacion", e.getMessage() );
        }
        Organizacion organizacion=new Organizacion(org);
        List<DiasEspeciales> calendario = new ArrayList<>();
        HorarioDao horarioDao = (HorarioDao)FactoryDao.buildDao("cpe.HorarioDao");
        try{
            calendario =horarioDao.listarCalendario(organizacion);
        }catch(Exception e){
            return WebResponse.crearWebResponseError("No se pudo Listar el Calendario", e.getMessage() );
        }

        JSONArray miArray = new JSONArray();
        SimpleDateFormat sdfDate = new SimpleDateFormat("dd/MM/yyyy");//dd/MM/yyyy
        for(DiasEspeciales de:calendario ){
            JSONObject oResponse = new JSONObject();
            oResponse.put("id",de.getDiaEspId());
            oResponse.put("fec",sdfDate.format(de.getFecha()));
            oResponse.put("des",de.getDescripcion());
            if(de.getEstReg().equals("1"))
            {
                oResponse.put("reg","No-Laborable");
            } else if(de.getEstReg().equals("2"))
            {
                oResponse.put("reg","Dia-Recuperacion");
            }
            
            

            miArray.put(oResponse);
        }
        
        return WebResponse.crearWebResponseExito("Se Listo correctamente",miArray);        
        //Fin
    }
    
}

