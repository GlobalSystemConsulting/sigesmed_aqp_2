/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.ejb;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.cpe.AsistenciaDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.cpe.InasistenciaDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Organizacion;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Trabajador;
import com.dremo.ucsm.gsc.sigesmed.core.entity.cpe.DiasEspeciales;
import com.dremo.ucsm.gsc.sigesmed.core.entity.cpe.HorarioDet;
import com.dremo.ucsm.gsc.sigesmed.core.entity.cpe.Inasistencia;
import com.dremo.ucsm.gsc.sigesmed.core.entity.cpe.RegistroAsistencia;
import com.dremo.ucsm.gsc.sigesmed.core.service.constantes.Sigesmed;
import com.dremo.ucsm.gsc.sigesmed.util.DateUtil;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import javax.ejb.Schedule;
import javax.ejb.Singleton;
import javax.ejb.Startup;

/**
 *
 * @author Carlos
 */
@Startup
@Singleton
public class verificacionInasistencias {
   
    @Schedule(hour="23", second="0", minute="55", persistent = false)
    public void getSalaryIncrease() {
        
        AsistenciaDao asistenciaDao = (AsistenciaDao) FactoryDao.buildDao("cpe.AsistenciaDao");
        InasistenciaDao inasistenciaDao = (InasistenciaDao) FactoryDao.buildDao("cpe.InasistenciaDao");
        Date hoy=new Date();
        Calendar cin = Calendar.getInstance();
        cin.setTime(hoy);
        Character dia='D';
        
        switch(cin.get(Calendar.DAY_OF_WEEK))
        {
            case 1: dia='D';
                break;
            case 2: dia='L';
                break;
            case 3: dia='M';
                break;    
            case 4: dia='W';
                break;
            case 5: dia='J';
                break;
            case 6: dia='V';
                break;    
            case 7: dia='S';
                break;    
        }
        List<Organizacion> organizaciones=inasistenciaDao.getOrganizaciones();
        for(Organizacion org:organizaciones)
        {
            List<DiasEspeciales> diasFeriados=inasistenciaDao.getDiasEspeciales(org, hoy, hoy, Sigesmed.ESTADO_DIA_ESPECIAL_NO_LABORABLE);
            if(diasFeriados==null || diasFeriados.size()==0)
            {
                List<Trabajador> trabajadores = asistenciaDao.listarAsistenciaAllTrabajadorByFechaAndOrganizacion(org.getOrgId(),DateUtil.removeTime(hoy), hoy, hoy);
                Boolean flag = Boolean.FALSE;
                for (Trabajador tra : trabajadores) {
                    if (tra.getOrganizacion().getDiasEspeciales().size() == 0) {
                        if (tra.getAsistencias().size() == 0) {
                            if (tra.getHoraCabId() != null) {
                                for (HorarioDet temp : tra.getHoraCabId().getHorarioDetalle()) {
                                    if (temp.getDiaSemId().getDiaSemId().equals(dia)) {
                                        flag = Boolean.TRUE;
                                        break;
                                    }
                                }
                                if (flag) {
                                    Inasistencia nuevaInasistencia = new Inasistencia(hoy, new Date(), "A", tra);
                                    inasistenciaDao.insert(nuevaInasistencia);
                                    flag = Boolean.FALSE;
                                }
                            }
                        } else {
                            for (RegistroAsistencia ra : tra.getAsistencias()) {
                                if (ra.getHoraSalida() == null) {
                                    asistenciaDao.actualizarAsistenciaToFalta(ra);
                                }
                            }
                        }
                    }
                }
            }
        }

    }

    
}
