/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.documentos_comunicacion.ver_documento.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.sdc.DocumentoDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sdc.ContenidoDocumento;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sdc.Documento;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import org.json.JSONArray;
import org.json.JSONObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.util.List;

/**
 *
 * @author Administrador
 */
public class TraerContenidoDocumentoTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
        
        /*
        *   Parte para la lectura, verificacion y validacion de datos
        */
        Integer documentoId;
        Documento doc=null;
        try{
            JSONObject requestData = (JSONObject)wr.getData();
            documentoId = requestData.getInt("documentoId");        
            doc=new Documento(documentoId);
//            plantilla = new Plantilla(plantillaId);
            
        }catch(Exception e){
            return WebResponse.crearWebResponseError("No se pudo Listar los Contenidos del Documento", e.getMessage() );
        }
        //Fin        
        
        /*
        *  Parte para la operacion en la Base de Datos
        */
        List<ContenidoDocumento> contenidosDoc = null;
        DocumentoDao documentoDao = (DocumentoDao)FactoryDao.buildDao("sdc.DocumentoDao");
        try{
           
            contenidosDoc =documentoDao.listarContenidosDocumento(doc);
        }catch(Exception e){
            return WebResponse.crearWebResponseError("No se obtener contenidos del Documento ", e.getMessage() );
        }
        //Fin
        
        /*
        *  Repuesta Correcta
        */
        JSONArray miArray = new JSONArray();
        for(ContenidoDocumento contenidos:contenidosDoc ){
            JSONObject oResponse = new JSONObject();
            oResponse.put("contenidoId",contenidos.getConDocId()); 
            oResponse.put("contenido",contenidos.getConDocCon());
            oResponse.put("contenidoDocTip",contenidos.getConDocTip());
            
            miArray.put(oResponse);
        }
        
        return WebResponse.crearWebResponseExito("Se Listo correctamente",miArray);        
        //Fin
    }
    
}

