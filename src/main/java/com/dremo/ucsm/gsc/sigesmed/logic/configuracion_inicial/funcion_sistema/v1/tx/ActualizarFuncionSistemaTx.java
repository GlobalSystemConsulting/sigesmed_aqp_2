/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.configuracion_inicial.funcion_sistema.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.FuncionSistemaDao;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import org.json.JSONObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import com.dremo.ucsm.gsc.sigesmed.core.entity.FuncionSistema;
import com.dremo.ucsm.gsc.sigesmed.core.entity.SubModuloSistema;
import java.util.Date;

/**
 *
 * @author Administrador
 */
public class ActualizarFuncionSistemaTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
                
        /*
        *   Parte para la lectura, verificacion y validacion de datos
        */
        FuncionSistema funcionAct = null;
        try{
            JSONObject requestData = (JSONObject)wr.getData();
            int funcionID = requestData.getInt("funcionID");
            int subModuloID = requestData.getInt("subModuloID");
            String nombre = requestData.getString("nombre");
            String descripcion = requestData.getString("descripcion");
            String url = requestData.getString("url");
            String clave = requestData.getString("clave");
            String controlador = requestData.getString("controlador");
            String interfaz = requestData.getString("interfaz");
            String icono = requestData.getString("icono");
            String estado = requestData.getString("estado");
            
            funcionAct = new FuncionSistema(funcionID,new SubModuloSistema(subModuloID), nombre, descripcion, url,clave,controlador,interfaz,icono,new Date(), 1, estado.charAt(0),null);
        
        }catch(Exception e){
            System.out.println(e);
            return WebResponse.crearWebResponseError("No se pudo actualizar, datos incorrectos", e.getMessage() );
        }
        //Fin
        
        /*
        *  Parte para la operacion en la Base de Datos
        */
        FuncionSistemaDao funcionDao = (FuncionSistemaDao)FactoryDao.buildDao("FuncionSistemaDao");
        try{
            funcionDao.update(funcionAct);
        
        }catch(Exception e){
            System.out.println("No se pudo actualizar la Funcion del Sistema \n"+e);
            return WebResponse.crearWebResponseError("No se pudo actualizar la Funcion del Sistema", e.getMessage() );
        }
        //Fin
        
        /*
        *  Repuesta Correcta
        */        
        return WebResponse.crearWebResponseExito("La Funcion del Sistema se actualizo correctamente");
        //Fin
    }
    
}
