package com.dremo.ucsm.gsc.sigesmed.logic.maestro.unidad.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.OrganizacionDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.maestro.DocenteDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.maestro.plan.AreaCurricularDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.maestro.plan.ProgramacionAnualDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.maestro.plan.UnidadDidacticaDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Organizacion;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Trabajador;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.Docente;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.plan_curricular.AreaCurricular;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.plan_curricular.ProgramacionAnual;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.plan_curricular.UnidadDidactica;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mech.Grado;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.base.EntityUtil;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by Administrador on 31/10/2016.
 */
public class ListarUnidadesTx implements ITransaction {
    private static final Logger logger = Logger.getLogger(ListarUnidadesTx.class.getName());
    @Override
    public WebResponse execute(WebRequest wr) {
        JSONObject data = (JSONObject) wr.getData();
        int codOrg = data.getInt("org");
        int codUser = data.getInt("usr");
        return listarUnidades(codOrg,codUser);
    }
    private WebResponse listarUnidades(int codOrg, int codUser){
        try{
            OrganizacionDao orgDao = (OrganizacionDao) FactoryDao.buildDao("OrganizacionDao");
            UnidadDidacticaDao unidadDao = (UnidadDidacticaDao) FactoryDao.buildDao("maestro.plan.UnidadDidacticaDao");
            ProgramacionAnualDao progDao = (ProgramacionAnualDao) FactoryDao.buildDao("maestro.plan.ProgramacionAnualDao");
            Organizacion org = orgDao.buscarConTipoOrganizacionYPadre(codOrg);
            String tipo = org.getTipoOrganizacion().getCod().trim().toUpperCase();
            if(tipo.equals("IE")){
                DocenteDao docDao = (DocenteDao) FactoryDao.buildDao("maestro.DocenteDao");
                Docente docente = docDao.buscarDocentePorUsuario(codUser);
                if(docente != null){
                    List<UnidadDidactica> unidades = unidadDao.listarUnidadesPorDocenteYOrganizacion(codOrg, docente.getDoc_id());                    
                    JSONObject response = new JSONObject();
                    response.put("list",crearJSONArray(unidades));
                    response.put("edit",true);
                    ///añadir cursos
                    AreaCurricularDao areaDao = (AreaCurricularDao) FactoryDao.buildDao("maestro.plan.AreaCurricularDao");
                    List<AreaCurricular> areas = areaDao.listar(AreaCurricular.class);
                    response.put("areas",new JSONArray(EntityUtil.listToJSONString(
                            new String[]{"areCurId","nom"},
                            new String[]{"cod","nom"},
                            areas
                    )));
                    //añadir grado
                    List<Grado> grados = progDao.buscarGradosDisenoCurricular(1);
                    response.put("grados",new JSONArray(EntityUtil.listToJSONString(
                            new String[]{"graId","nom"},
                            new String[]{"cod","nom"},
                            grados
                    )));
                    //añadimos los proyectos
                    List<ProgramacionAnual> progs = progDao.buscarProgramacionDocente(docente.getDoc_id());
                    response.put("planes",new JSONArray(EntityUtil.listToJSONString(
                            new String[]{"proAnuId","des"},
                            new String[]{"cod","nom"},
                            progs
                    )));
                    //añadir tipo de unidad
                    response.put("edit",true);
                    return WebResponse.crearWebResponseExito("Se listo con exito las unidades didacticas",response);
                }else{
                    return WebResponse.crearWebResponseError("El usuario debe ser un docente o debe registrar su datos");
                }
            }else{
                return WebResponse.crearWebResponseError("Se debe estar en una IE");
            }
        }catch (Exception e){
            logger.log(Level.SEVERE,"listarUnidades",e);
            return WebResponse.crearWebResponseError("Error al listar Unidades");
        }
    }
    private JSONArray crearJSONArray(List<UnidadDidactica> unidades) throws Exception{
        JSONArray progsArray = new JSONArray();
        for(UnidadDidactica unidad: unidades){
            JSONObject obj = new JSONObject(EntityUtil.objectToJSONString(
                    new String[]{"uniDidId", "tit","tip","fecIni","fecFin"},
                    new String[]{"cod", "tit","tip","ini","fin"},
                    unidad
            )).put("prog",new JSONObject(EntityUtil.objectToJSONString(
                    new String[]{"proAnuId","des"},
                    new String[]{"cod","nom"},
                    unidad.getProgramacionAnual()
            ))).put("are", new JSONObject(EntityUtil.objectToJSONString(
                    new String[]{"areCurId", "nom"},
                    new String[]{"cod", "nom"},
                    unidad.getProgramacionAnual().getAre()
            ))).put("grad",new JSONObject(EntityUtil.objectToJSONString(
                    new String[]{"graId","nom"},
                    new String[]{"cod","nom"},
                    unidad.getProgramacionAnual().getGra()
            ))).put("org",new JSONObject(EntityUtil.objectToJSONString(
                    new String[]{"orgId","nom"},
                    new String[]{"cod","nom"},
                    unidad.getProgramacionAnual().getOrg()
            )));
            progsArray.put(obj);
        }
        return  progsArray;
    }
}
