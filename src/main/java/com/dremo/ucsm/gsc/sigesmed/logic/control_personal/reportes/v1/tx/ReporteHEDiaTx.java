/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.control_personal.reportes.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.cpe.AsistenciaAulaDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.cpe.InasistenciaDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Organizacion;
import com.dremo.ucsm.gsc.sigesmed.core.entity.cpe.AsistenciaAula;
import com.dremo.ucsm.gsc.sigesmed.core.entity.cpe.DiasEspeciales;
import com.dremo.ucsm.gsc.sigesmed.core.entity.cpe.Docente;
import com.dremo.ucsm.gsc.sigesmed.core.entity.cpe.PlazaMagisterial;
import com.dremo.ucsm.gsc.sigesmed.core.service.ServicioREST;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.constantes.Sigesmed;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import com.dremo.ucsm.gsc.sigesmed.util.DateUtil;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.json.JSONObject;

/**
 *
 * @author Carlos
 */
public class ReporteHEDiaTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
        
        /*
        *   Parte para la lectura, verificacion y validacion de datos
        */
        
        Organizacion organizacion;
        DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Date fecha;
        Integer planNivelId;
        JSONObject requestData = (JSONObject)wr.getData();
        String nombreOrg="";
        String nivelNom="";
        try{
            
            Integer organizacionId = requestData.getInt("organizacionID");  
            nombreOrg = requestData.getString("organizacionNombre");  
            String dia=requestData.getString("dia");
            planNivelId=requestData.getInt("nivel");
            nivelNom=requestData.getString("nivelNombre");
            organizacion = new Organizacion(organizacionId);
            fecha=sdf.parse(dia);
    
        }catch(Exception e){
            System.out.println("No se pudo verificar los datos \n"+e);
            return WebResponse.crearWebResponseError("No se pudo verificar los datos ", e.getMessage() );
        }
       
        AsistenciaAulaDao asistenciaAulaDao= (AsistenciaAulaDao)FactoryDao.buildDao("cpe.AsistenciaAulaDao");
        InasistenciaDao inasistenciaDao= (InasistenciaDao)FactoryDao.buildDao("cpe.InasistenciaDao");
       // PersonalDao personalDao= (PersonalDao)FactoryDao.buildDao("cpe.PersonalDao");
      //  AsistenciaDao asistenciaDao= (AsistenciaDao)FactoryDao.buildDao("cpe.AsistenciaDao");
        
        
        List<Docente> docentes=new ArrayList<>();
        
        /**VERIFICANDO DIA NO LABORABLE O SABADO-DOMINGO**/
        try{
            DiasEspeciales diaHoy=inasistenciaDao.verificarDiaEspecial(organizacion, fecha, Sigesmed.ESTADO_DIA_ESPECIAL_NO_LABORABLE);
            if(diaHoy!=null)
            {
                return WebResponse.crearWebResponseError("El dia seleccionado no fue Laborable : "+diaHoy.getDescripcion());
            }
                   
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(fecha);
            int dayOfWeek = calendar.get(Calendar.DAY_OF_WEEK);
            if (dayOfWeek == Calendar.SUNDAY || dayOfWeek == Calendar.SATURDAY)
            {
                return WebResponse.crearWebResponseError("El dia seleccionado no fue Fin de Semana");
            }
                    
            docentes=asistenciaAulaDao.listarDocenteAndPlazas(organizacion.getOrgId(), planNivelId);
            
        }catch(Exception e){
            System.out.println("No se pudo verificar la asistencia \n"+e);
            return WebResponse.crearWebResponseError("No se pudo verificar la asistencia ", e.getMessage() );
        }
        
        
        /*Reporte*/
        String pathFinal = ServicioREST.PATH_SIGESMED+ File.separator +"archivos"+File.separator +"control_personal"+File.separator +"FORMATO_DIA.xlsx";
        System.out.println("Working Directory = " +pathFinal);
           // System.getProperty("user.dir"));
        Date fechaInicio=DateUtil.removeTime(fecha);
        Date fechaFin=DateUtil.removeTime(DateUtil.addDays(fecha, 1));
        InputStream inp;
        try {
            inp = new FileInputStream(pathFinal);
            
            Workbook wb = WorkbookFactory.create(inp);
            Sheet sheet = wb.getSheetAt(0);
            Row rowData1 = sheet.getRow(6);
            rowData1.createCell(2).setCellValue(nombreOrg);
            
            Row rowData2 = sheet.getRow(7);
            rowData2.createCell(2).setCellValue(nivelNom);
            rowData2.createCell(11).setCellValue(DateUtil.convertDateToString(fecha));
            
            int rowIndex=15;
            int nro=1;
            CellStyle cellStyle = wb.createCellStyle();
            cellStyle.setBorderBottom(CellStyle.BORDER_THIN);
            cellStyle.setBorderTop(CellStyle.BORDER_THIN);
            cellStyle.setBorderRight(CellStyle.BORDER_THIN);
            cellStyle.setBorderLeft(CellStyle.BORDER_THIN);
            
            for(Docente data : docentes)
            {
                Row row = sheet.createRow(rowIndex++);
                
                row.createCell(0).setCellValue(nro++);
                row.getCell(0).setCellStyle(cellStyle);
                row.createCell(1).setCellValue(data.getPersona().getNombrePersonaAP());
                row.getCell(1).setCellStyle(cellStyle);
                int jl = 0;
               
                for (PlazaMagisterial pm : data.getPlazas()) {
                    jl = jl + pm.getJornadaLaboral();
                    PlazaMagisterial plaza=asistenciaAulaDao.getPlazaMagisterialAndDistribucion(pm.getPlaMagId());
                    
                    row.createCell(3).setCellValue(plaza.getDistribucionGrados().get(0).getGrado().getAbr()+"");
                    row.getCell(3).setCellStyle(cellStyle);
                    
                    row.createCell(4).setCellValue(plaza.getDistribucionGrados().get(0).getSecId()+"");
                    row.getCell(4).setCellStyle(cellStyle);
                }
                row.createCell(2).setCellValue(jl);
                row.getCell(2).setCellStyle(cellStyle);
                
                row.createCell(5).setCellValue(data.getEsp());
                row.getCell(5).setCellStyle(cellStyle);
                
                List<AsistenciaAula> asistenciasDocente=asistenciaAulaDao.listarAsistenciaAula(data.getDocId(), fechaInicio, fechaFin);
                //Trabajador trab=personalDao.getTrabajadorByPersonaOrganizacion(new Persona(data.getDocId()), organizacion);
                //List<RegistroAsistencia> asistenciaJustificadas=asistenciaDao.buscarRegistroAsistenciaJustificadosByFecha(fechaInicio, fechaFin,trab);
                
                for(int i=0;i<7;i++)
                {
                    row.createCell(6+i).setCellStyle(cellStyle);
                }
                int numTot=0;
                for(AsistenciaAula asi:asistenciasDocente)
                {
                    String horasEfectivas=asi.getRelacionHoras();
                    for(char idx:horasEfectivas.toCharArray())
                    {
                        row.getCell(5+Character.getNumericValue(idx)).setCellValue("X");
                        numTot++;
                    }
                }
                row.createCell(13).setCellValue(numTot);
                row.getCell(13).setCellStyle(cellStyle);
            }
                 
            
            
            
            FileOutputStream fileOut = new FileOutputStream(ServicioREST.PATH_SIGESMED+ File.separator +"archivos"+File.separator +"control_personal"+File.separator+"reporteDia.xlsx");
            wb.write(fileOut);
            fileOut.close();
            
        } catch (FileNotFoundException ex) {
            Logger.getLogger(ReporteHEMesTx.class.getName()).log(Level.SEVERE, null, ex);
            return WebResponse.crearWebResponseError("No se pudo crear el Reporte ", ex.getMessage() );
        } catch (IOException ex) {
            Logger.getLogger(ReporteHEMesTx.class.getName()).log(Level.SEVERE, null, ex);
            return WebResponse.crearWebResponseError("No se pudo crear el Reporte ", ex.getMessage() );
        } catch (InvalidFormatException ex) {
            Logger.getLogger(ReporteHEMesTx.class.getName()).log(Level.SEVERE, null, ex);
            return WebResponse.crearWebResponseError("No se pudo crear el Reporte ", ex.getMessage() );
        } catch (EncryptedDocumentException ex) {
            Logger.getLogger(ReporteHEMesTx.class.getName()).log(Level.SEVERE, null, ex);
            return WebResponse.crearWebResponseError("No se pudo crear el Reporte ", ex.getMessage() );
        }
        
        
        /*Fin Reporte*/
        
        
        

        return WebResponse.crearWebResponseExito("No se pudo realizar el reporte");
        //Fin
    }
}
