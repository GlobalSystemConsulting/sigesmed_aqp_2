package com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.capacitacion;

import com.dremo.ucsm.gsc.sigesmed.core.dao.capacitacion.HorarioSedeCapacitacionDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.GenericDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.datastore.HibernateUtil;
import com.dremo.ucsm.gsc.sigesmed.core.entity.capacitacion.HorarioSedeCapacitacion;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

public class HorarioSedeCapacitacionDaoHibernate extends GenericDaoHibernate<HorarioSedeCapacitacion> implements HorarioSedeCapacitacionDao{

    private static final Logger logger = Logger.getLogger(HorarioSedeCapacitacionDaoHibernate.class.getName());
    
    @Override
    public HorarioSedeCapacitacion buscarPorId(int horCod) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try {
            return (HorarioSedeCapacitacion) session.get(HorarioSedeCapacitacion.class, horCod);
        } catch (Exception e) {
            logger.log(Level.SEVERE, "buscarPorId", e);
            throw e;
        } finally {
            session.close();
        }
    }
    
    @Override
    public List<HorarioSedeCapacitacion> buscarPorSede(int sedCod) {
        Session session = HibernateUtil.getSessionFactory().openSession();

        try {
            Criteria query = session.createCriteria(HorarioSedeCapacitacion.class)
                    .createCriteria("sedeCapacitacion")
                    .add(Restrictions.eq("sedCapId", sedCod))
                    .setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);

            return query.list();
        } catch (Exception e) {
            logger.log(Level.SEVERE, "buscarPorSede", e);
            throw e;
        } finally {
            session.close();
        }
    }
    
    @Override
    public List<HorarioSedeCapacitacion> buscarParaOnTime(int sedCod, int dia, Date tiempo) {
        Session session = HibernateUtil.getSessionFactory().openSession();

        try {
            Criteria query = session.createCriteria(HorarioSedeCapacitacion.class)
                    .add(Restrictions.eq("dia", dia))
                    .add(Restrictions.le("turIni", tiempo))
                    .add(Restrictions.ge("turFin", tiempo))
                    .createCriteria("sedeCapacitacion")
                    .add(Restrictions.eq("sedCapId", sedCod))
                    .setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);

            return query.list();
        } catch (Exception e) {
            logger.log(Level.SEVERE, "buscarParaOnTime", e);
            throw e;
        } finally {
            session.close();
        }
    }
}
