/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.entity.smdg;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Administrador
 */
@Entity
@Table(name = "tipo_item")
@XmlRootElement
//@NamedQueries({
//    @NamedQuery(name = "TipoItem.findAll", query = "SELECT t FROM TipoItem t"),
//    @NamedQuery(name = "TipoItem.findByTitId", query = "SELECT t FROM TipoItem t WHERE t.titId = :titId"),
//    @NamedQuery(name = "TipoItem.findByTitDes", query = "SELECT t FROM TipoItem t WHERE t.titDes = :titDes"),
//    @NamedQuery(name = "TipoItem.findByTitPun", query = "SELECT t FROM TipoItem t WHERE t.titPun = :titPun"),
//    @NamedQuery(name = "TipoItem.findByFecMod", query = "SELECT t FROM TipoItem t WHERE t.fecMod = :fecMod"),
//    @NamedQuery(name = "TipoItem.findByUsuMod", query = "SELECT t FROM TipoItem t WHERE t.usuMod = :usuMod"),
//    @NamedQuery(name = "TipoItem.findByEstReg", query = "SELECT t FROM TipoItem t WHERE t.estReg = :estReg")})
public class TipoItem implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "tit_id")
    private Integer titId;
    @Column(name = "tit_des")
    private String titDes;
    @Column(name = "tit_pun")
    private String titPun;
    @Column(name = "fec_mod")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fecMod;
    @Column(name = "usu_mod")
    private Integer usuMod;
    @Column(name = "est_reg")
    private String estReg;
//    @OneToMany(mappedBy = "titId")
//    private List<GrupoDetalle> grupoDetalleList;

    public TipoItem() {
    }

    public TipoItem(Integer titId) {
        this.titId = titId;
    }

    public Integer getTitId() {
        return titId;
    }

    public void setTitId(Integer titId) {
        this.titId = titId;
    }

    public String getTitDes() {
        return titDes;
    }

    public void setTitDes(String titDes) {
        this.titDes = titDes;
    }

    public String getTitPun() {
        return titPun;
    }

    public void setTitPun(String titPun) {
        this.titPun = titPun;
    }

    public Date getFecMod() {
        return fecMod;
    }

    public void setFecMod(Date fecMod) {
        this.fecMod = fecMod;
    }

    public Integer getUsuMod() {
        return usuMod;
    }

    public void setUsuMod(Integer usuMod) {
        this.usuMod = usuMod;
    }

    public String getEstReg() {
        return estReg;
    }

    public void setEstReg(String estReg) {
        this.estReg = estReg;
    }

//    @XmlTransient
//    public List<GrupoDetalle> getGrupoDetalleList() {
//        return grupoDetalleList;
//    }
//
//    public void setGrupoDetalleList(List<GrupoDetalle> grupoDetalleList) {
//        this.grupoDetalleList = grupoDetalleList;
//    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (titId != null ? titId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TipoItem)) {
            return false;
        }
        TipoItem other = (TipoItem) object;
        if ((this.titId == null && other.titId != null) || (this.titId != null && !this.titId.equals(other.titId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entidades.smdg.TipoItem[ titId=" + titId + " ]";
    }
    
}
