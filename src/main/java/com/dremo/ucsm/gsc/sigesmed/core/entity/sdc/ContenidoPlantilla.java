package com.dremo.ucsm.gsc.sigesmed.core.entity.sdc;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;


@Entity
@Table(name="contenido_plantilla" ,schema="administrativo")
public class ContenidoPlantilla  implements java.io.Serializable {

    @Id
    @Column(name="ctn_pla_id", unique=true, nullable=false)
    @SequenceGenerator(name = "secuencia_contenido_plantilla", sequenceName="administrativo.contenido_plantilla_ctn_pla_id_seq" )
    @GeneratedValue(generator="secuencia_contenido_plantilla")
    private Integer conplaId;
    
    @Column(name="ctn_pla_tip")
    private String conPlaTip;
    
    @Column(name="ctn_pla_con")
    private String conPlaCon;
    
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="ctn_pla_pla_id", nullable=false)
    private Plantilla plaId;
   
    @Column(name="ctn_pla_ali")
    private Integer alineacion;

    @OneToOne(fetch=FetchType.EAGER)
    @JoinColumn(name="ctn_pro_let_id")
    private PropiedadLetra propiedadLetra;

    @Column(name="ctn_pla_bld")
    private Boolean isBold;
    
    @Column(name="ctn_pla_cur")
    private Boolean isCursiva;
    
    @Column(name="ctn_pla_sub")
    private Boolean isSubrayado;
    
    @Column(name="ctn_pla_tam")
    private Integer tamanho;
    
    
    public ContenidoPlantilla() {
    }

    public ContenidoPlantilla(Integer conplaId) {
        this.conplaId = conplaId;
    }

    public ContenidoPlantilla(String conPlaTip, String conPlaCon, Plantilla plaId, Integer alineacion, PropiedadLetra propiedadLetra, Boolean isBold, Boolean isCursiva,Boolean isSubrayado, Integer tamanho) {
        this.conPlaTip = conPlaTip;
        this.conPlaCon = conPlaCon;
        this.plaId = plaId;
        this.alineacion = alineacion;
        this.propiedadLetra = propiedadLetra;
        this.isBold = isBold;
        this.isCursiva = isCursiva;
        this.isSubrayado=isSubrayado;
        this.tamanho = tamanho;
    }

    public Integer getConplaId() {
        return conplaId;
    }

    public void setConplaId(Integer conplaId) {
        this.conplaId = conplaId;
    }

    public String getConPlaTip() {
        return conPlaTip;
    }

    public void setConPlaTip(String conPlaTip) {
        this.conPlaTip = conPlaTip;
    }

    public String getConPlaCon() {
        return conPlaCon;
    }

    public void setConPlaCon(String conPlaCon) {
        this.conPlaCon = conPlaCon;
    }

    public Plantilla getPlaId() {
        return plaId;
    }

    public void setPlaId(Plantilla PlaId) {
        this.plaId = PlaId;
    }
    
    public PropiedadLetra getPropiedadLetra() {
        return propiedadLetra;
    }

    public void setPropiedadLetra(PropiedadLetra propiedadLetra) {
        this.propiedadLetra = propiedadLetra;
    }

    public Boolean getIsBold() {
        return isBold;
    }

    public void setIsBold(Boolean isBold) {
        this.isBold = isBold;
    }

    public Boolean getIsCursiva() {
        return isCursiva;
    }

    public void setIsCursiva(Boolean isCursiva) {
        this.isCursiva = isCursiva;
    }
    
    public Integer getAlineacion() {
        return alineacion;
    }

    public void setAlineacion(Integer alineacion) {
        this.alineacion = alineacion;
    }

    public Integer getTamanho() {
        return tamanho;
    }

    public void setTamanho(Integer tamanho) {
        this.tamanho = tamanho;
    }

    public Boolean getIsSubrayado() {
        return isSubrayado;
    }

    public void setIsSubrayado(Boolean isSubrayado) {
        this.isSubrayado = isSubrayado;
    }
    
}


