/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.smdg.plantilla_ficha.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.smdg.PlantillaFichaInstitucionalDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.smdg.PlantillaGrupoDao;

import com.dremo.ucsm.gsc.sigesmed.core.entity.smdg.PlantillaFichaInstitucional;
import com.dremo.ucsm.gsc.sigesmed.core.entity.smdg.PlantillaGrupo;
import com.dremo.ucsm.gsc.sigesmed.core.entity.smdg.PlantillaIndicadores;

import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.util.List;
import java.util.Set;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author Administrador
 */
public class ListarPlantillaTx implements ITransaction{
    @Override
    public WebResponse execute(WebRequest wr) {        
        /*
        *  Parte para la operacion en la Base de Datos
        */               
        JSONObject requestData = (JSONObject)wr.getData();
        int plaId = requestData.getInt("plaId");
                
        PlantillaFichaInstitucional plantilla = null;
        PlantillaFichaInstitucionalDao plantillasDao = (PlantillaFichaInstitucionalDao)FactoryDao.buildDao("smdg.PlantillaFichaInstitucionalDao");        
        
        try{
            plantilla = plantillasDao.obtenerPlantilla(plaId);
        }catch(Exception e){            
            return WebResponse.crearWebResponseError("No se encontro la plantilla", e.getMessage() );
        }
        //Fin
             
        /*
        *  Repuesta Correcta
        */               
        
        JSONArray oplantilla = new JSONArray();        
        List<PlantillaGrupo> grupos = plantilla.getGrupos();
        PlantillaGrupoDao grupoDao = (PlantillaGrupoDao)FactoryDao.buildDao("smdg.PlantillaGrupoDao");
                
        for(PlantillaGrupo g:grupos){
//            int gruId = g.getPgrId();
            JSONObject ogrupo = new JSONObject();
            ogrupo.put("gruId",g.getPgrId());
            ogrupo.put("gruNom",g.getPgrDes());
            ogrupo.put("Tipo",g.getTipoGrupo().getTipId());
            
            JSONArray oindicadores = new JSONArray();        
            List<PlantillaIndicadores> indicadores = grupoDao.obtenerIndicadores(g.getPgrId());            
            for(PlantillaIndicadores i:indicadores){
                JSONObject oindicador = new JSONObject();
                oindicador.put("indId",i.getPinId());
                oindicador.put("indNom",i.getPinDes());
                oindicadores.put(oindicador);
            }
            ogrupo.put("indicadores",oindicadores);            
            oplantilla.put(ogrupo);
            
        }
        
        JSONObject cabecera = new JSONObject();
        
        cabecera.put("plaide", plantilla.getPfiInsId());
        cabecera.put("codigo", plantilla.getPfiCod());
        cabecera.put("nombre", plantilla.getPfiNom());
        cabecera.put("tipo", plantilla.getTipo().getPtiId());
        cabecera.put("tipoNom", plantilla.getTipo().getPtiNom());
        
        oplantilla.put(cabecera);
        
        return WebResponse.crearWebResponseExito("Se Listo correctamente",oplantilla);        
                
    }    
}
