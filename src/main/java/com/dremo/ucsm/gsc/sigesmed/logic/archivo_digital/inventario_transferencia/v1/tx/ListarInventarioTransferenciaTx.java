/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.archivo_digital.inventario_transferencia.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.sad.InventarioTransferenciaDAO;
import com.dremo.ucsm.gsc.sigesmed.core.dao.sad.UnidadOrganicaDAO;
import com.dremo.ucsm.gsc.sigesmed.core.dao.AreaDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sad.InventarioTransferencia;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sad.UnidadOrganica;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sad.DetalleInventarioTransferencia;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Area;

import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import org.json.JSONObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;


import org.json.JSONArray;
import com.dremo.ucsm.gsc.sigesmed.core.service.base.FileJsonObject;
import com.dremo.ucsm.gsc.sigesmed.util.BuildFile;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 *
 * @author admin
 */
public class ListarInventarioTransferenciaTx implements ITransaction {

    @Override
    public WebResponse execute(WebRequest wr) {
        
        int ser_doc_id = 0;
        try{
             JSONObject requestData = (JSONObject)wr.getData();
             ser_doc_id = requestData.getInt("serie_id");
            
        }catch(Exception e){
            System.out.println(e);
            return WebResponse.crearWebResponseError("No se pudo Listar los Inventarios de Transferencia", e.getMessage() );
        }
             
        
       List<InventarioTransferencia> inv_trans = null;
       
       InventarioTransferenciaDAO inv_tra_dao = (InventarioTransferenciaDAO)FactoryDao.buildDao("sad.InventarioTransferenciaDAO");
       try{
           
           inv_trans = inv_tra_dao.buscarPorInventarioTransferencia();
       }catch(Exception e){
            System.out.println("No se pudo Listar los Inventarios de Transferencia\n"+e);
            return WebResponse.crearWebResponseError("No se pudo Listar los Inventarios de Transferencia", e.getMessage() );
       }
        JSONArray miArray = new JSONArray();
        for(InventarioTransferencia it :inv_trans ){
            JSONObject oResponse = new JSONObject();
            oResponse.put("inv_tra_id",it.getInvTransId());
            oResponse.put("ser_doc_id",it.getSerDocId());
            oResponse.put("cod",it.getCodigo());
            oResponse.put("fec",it.getFecha());
           
            oResponse.put("serie",it.getSerieDocumental().getNombre());
                     
            int uniOrgID = it.getSerieDocumental().getUniOrgId();
            List<UnidadOrganica> uni_org = null;
            UnidadOrganicaDAO uni_orgDAO = (UnidadOrganicaDAO)FactoryDao.buildDao("sad.UnidadOrganicaDAO");
            uni_org = uni_orgDAO.buscarPorCodigo(uniOrgID);
            for(UnidadOrganica uo:uni_org){
                
                oResponse.put("un_org",uo.getnombre() );
                oResponse.put("area",uo.getArea().getNom());
            }
            List<DetalleInventarioTransferencia>dit;
            dit = it.getInvTransDet();
            for (DetalleInventarioTransferencia det : dit){
                oResponse.put("det_inv_trans",det.getdet_inv_tra());
                oResponse.put("asu",det.getAsu());
                oResponse.put("met_lin",det.getMet_lin());
                oResponse.put("alm",det.getAlm());
                oResponse.put("est",det.getEst());
                oResponse.put("bal",det.getBal());
                oResponse.put("fil",det.getFil());
                oResponse.put("cue",det.getCue());
                oResponse.put("caj",det.getCaj());
            }
            
          
            miArray.put(oResponse);
        }
               
        return WebResponse.crearWebResponseExito("Se Listo los Inventarios de Transferencia Correctamente",miArray); 
     //   throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
