/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.cuadro_horas.cuadro_horas.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.mech.DistribucionHoraGradoDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mech.DistribucionHoraArea;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mech.DistribucionHoraGrado;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import org.json.JSONArray;
import org.json.JSONObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author abel
 */
public class BuscarDistribucionPorDocenteTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
        
        /*
        *   Parte para la lectura, verificacion y validacion de datos
        */
        int planID = 0;
        int docenteID = 0;
        try{
            
            JSONObject requestData = (JSONObject)wr.getData();
            planID = requestData.getInt("planID");
            docenteID = requestData.getInt("docenteID");
        
        }catch(Exception e){
            System.out.println(e);
            return WebResponse.crearWebResponseError("No se pudo Listar la distribucion horas por docente, datos incorrectos", e.getMessage() );
        }
        //Fin        
        
        /*
        *  Parte para la operacion en la Base de Datos
        */
        List<DistribucionHoraGrado> distribucion = null;
        
        try{
            DistribucionHoraGradoDao disDao = (DistribucionHoraGradoDao)FactoryDao.buildDao("mech.DistribucionHoraGradoDao");
            
            distribucion = disDao.buscarDistribucionHorasGradoPorPlanYDocente(planID,docenteID);
        
        }catch(Exception e){
            System.out.println("No se pudo Listar Listar la distribucion horas por docente\n"+e);
            return WebResponse.crearWebResponseError("No se pudo Listar la distribucion de horas por docente", e.getMessage() );
        }
        //Fin        
        /*
        *  Repuesta Correcta
        */
        
        JSONArray miArray = new JSONArray();
        List<Integer> grados = new ArrayList<Integer>();
        
        JSONObject oDisGrado = null;
        JSONArray aDisSeccion = null;
        
        for(DistribucionHoraGrado d:distribucion ){
            
            JSONObject oDisSeccion = new JSONObject();            
            oDisSeccion.put("disGradoID",d.getDisHorGraId());
            oDisSeccion.put("hora",d.getHorAsi());
            oDisSeccion.put("gradoID",d.getGraId());
            oDisSeccion.put("plazaID",d.getPlaMagId());
            oDisSeccion.put("seccionID",""+d.getSecId());
            
            boolean entro = false;
            for(int i=0;i<grados.size();i++){
                if(d.getGraId() == grados.get(i)){
                    entro = true;
                    break;
                }
            }
            if(entro){
                aDisSeccion.put(oDisSeccion);
            }
            else{
                aDisSeccion = new JSONArray();
                aDisSeccion.put(oDisSeccion);
                
                oDisGrado = new JSONObject();                
                oDisGrado.put("gradoID", d.getGraId());
                oDisGrado.put("secciones", aDisSeccion);
                grados.add(d.getGraId());
                
                miArray.put(oDisGrado);
            }
            
            JSONArray aDisA = new JSONArray();
            for(DistribucionHoraArea da : d.getDistribucionAreas()){
                JSONObject oDisA = new JSONObject();
                
                oDisA.put("disAreaID", da.getDisHorAreId() );
                oDisA.put("hora", da.getHorAsi() );
                oDisA.put("areaID", da.getAreCurId() );
                oDisA.put("disGradoID", da.getDisHorGraId() );
                aDisA.put(oDisA);
            }
            //verificando si tiene asignado horas de tutoria
            if(d.getHorTut()>0){
                JSONObject oDisA = new JSONObject();
                oDisA.put("disAreaID", 0 );
                oDisA.put("hora", d.getHorTut() );
                oDisA.put("areaID", -1 );
                oDisA.put("disGradoID", d.getDisHorGraId() );
                aDisA.put(oDisA);
            }
            oDisSeccion.put("areas", aDisA);
        }
        
        return WebResponse.crearWebResponseExito("La distribucion de las plazas se listo correctamente",miArray);        
        //Fin
    }
    
}

