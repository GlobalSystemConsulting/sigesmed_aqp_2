package com.dremo.ucsm.gsc.sigesmed.logic.matricula_institucional.matricula_individual.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.mmi.MatriculaInsDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.mmi.MatriculaMMIDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mmi.MatriculaMMI;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mmi.PlanNivelMMI;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import com.dremo.ucsm.gsc.sigesmed.util.DateUtil;
import org.json.JSONObject;

public class BuscarMatriculaTx implements ITransaction {

    @Override
    public WebResponse execute(WebRequest wr) {
        JSONObject requestData = (JSONObject) wr.getData();
        long estId = requestData.getLong("estId");

        PlanNivelMMI tempPlanNivel;

        MatriculaMMIDaoHibernate hb = new MatriculaMMIDaoHibernate();
        MatriculaInsDaoHibernate pln = new MatriculaInsDaoHibernate();
        JSONObject matricula = new JSONObject();
        String apoNom, regNom, estNom, plaNivNom;
        try {
            MatriculaMMI myMatricula = hb.getMatriculaByEstId(estId);
            if (myMatricula != null) {
                if ((Long) myMatricula.getMatId() != null) {
                    matricula.put("matId", myMatricula.getMatId());
                } else {
                    matricula.put("matId", -1);
                }
                if (myMatricula.getPersonaByApoId() != null) {
                    matricula.put("apoId", myMatricula.getPersonaByApoId().getPerId());
                    apoNom = myMatricula.getPersonaByApoId().getNom() + " "
                            + myMatricula.getPersonaByApoId().getApePat() + " "
                            + myMatricula.getPersonaByApoId().getApeMat();
                    matricula.put("apoNom", apoNom);
                } else {
                    matricula.put("apoId", -1);
                    matricula.put("apoNom", "Desconocido");
                }
                if (myMatricula.getMatEst() != null) {
                    matricula.put("matEst", myMatricula.getMatEst());
                    matricula.put("matEstNom", getEstadomatricula(myMatricula.getMatEst()));
                } else {
                    matricula.put("matEst", -1);
                    matricula.put("matEstNom", getEstadomatricula(-1));
                }
                if (myMatricula.getMatSit() != null) {
                    matricula.put("sitMat", myMatricula.getMatSit());
                    matricula.put("sitMatNom", getSituacionMatricula(myMatricula.getMatSit()));
                } else {
                    matricula.put("sitMat", -1);
                    matricula.put("sitMatNom", getSituacionMatricula(-1));
                }
                if (myMatricula.getConMat() != null) {
                    matricula.put("conMat", myMatricula.getConMat());
                    matricula.put("conMatNom", getCondicionMatricula(myMatricula.getConMat()));
                } else {
                    matricula.put("conMat", 1);
                    matricula.put("conMatNom", getCondicionMatricula(-1));
                }
                if (myMatricula.getObs() != null) {
                    matricula.put("obs", myMatricula.getObs());
                } else {
                    matricula.put("obs", "");
                }
                if (myMatricula.getMatFec() != null) {
                    matricula.put("matFec", DateUtil.getString2Date(myMatricula.getMatFec()));
                } else {
                    matricula.put("matFec", "-");
                }
                if (myMatricula.getPersonaByRegId() != null) {
                    matricula.put("regId", myMatricula.getPersonaByRegId().getPerId());
                    regNom = myMatricula.getPersonaByRegId().getNom() + " "
                            + myMatricula.getPersonaByRegId().getApePat() + " "
                            + myMatricula.getPersonaByRegId().getApeMat();
                    matricula.put("regNom", regNom);
                } else {
                    matricula.put("regId", (long) -1);
                    matricula.put("regNom", "Desconocido");
                }
                if (myMatricula.getEstudiante() != null) {
                    matricula.put("estId", myMatricula.getEstudiante().getPerId());
                    estNom = myMatricula.getEstudiante().getPersona().getNom() + " "
                            + myMatricula.getEstudiante().getPersona().getApePat() + " "
                            + myMatricula.getEstudiante().getPersona().getApeMat();
                    matricula.put("estNom", estNom);
                    matricula.put("estDNI", myMatricula.getEstudiante().getPersona().getDni());
                } else {
                    matricula.put("estId", -1);
                    matricula.put("estNom", "Desconocido");
                    matricula.put("estDNI", "Desconocido");
                }
                if (myMatricula.getOrganizacionByOrgOriId() != null) {
                    matricula.put("orgOriId", myMatricula.getOrganizacionByOrgOriId().getOrgId());
                    matricula.put("orgOriNom", myMatricula.getOrganizacionByOrgOriId().getNom());
                } else {
                    matricula.put("orgOriId", -1);
                    matricula.put("orgOriNom", "Desconocido");
                }
                if (myMatricula.getOrganizacionByOrgDesId() != null) {
                    matricula.put("orgDesId", myMatricula.getOrganizacionByOrgDesId().getOrgId());
                    matricula.put("orgDesNom", myMatricula.getOrganizacionByOrgDesId().getNom());
                } else {
                    matricula.put("orgDesId", -1);
                    matricula.put("orgDesNom", "Desconocido");
                }
                if (myMatricula.getGraId() != null) {
                    matricula.put("graId", myMatricula.getGraId());
                    matricula.put("graNom", getGradoDes("" + myMatricula.getGraId()));
                } else {
                    matricula.put("graId", -1);
                    matricula.put("graNom", getGradoDes(""));
                }

                if (myMatricula.getSecId() != null) {
                    matricula.put("secId", myMatricula.getSecId().toString());
                } else {
                    matricula.put("secId", "-");
                }

                if (myMatricula.getPlaNivId() != null) {
                    tempPlanNivel = pln.loadPlanNivel(myMatricula.getPlaNivId());

                    plaNivNom = tempPlanNivel.getJornadaEscolar().getAbr() + ": "
                            + tempPlanNivel.getDes();

                    matricula.put("planNivId", myMatricula.getPlaNivId());
                    matricula.put("nivId", tempPlanNivel.getJornadaEscolar().getNivel().getNivId());
                    matricula.put("nivNom", tempPlanNivel.getJornadaEscolar().getNivel().getNom());
                    matricula.put("JornadaId", tempPlanNivel.getJornadaEscolar().getJorEscId());
                    matricula.put("JornadaNom", tempPlanNivel.getJornadaEscolar().getNom());
                    matricula.put("turnoId", tempPlanNivel.getTurno().getTurId());
                    matricula.put("turnoNom", tempPlanNivel.getTurno().getNom());

                } else {
                    matricula.put("planNivId", -1);
                    matricula.put("nivId", -1);
                    matricula.put("nivNom", "Desconocido");
                    matricula.put("JornadaId", -1);
                    matricula.put("JornadaNom", "Desconocido");
                    matricula.put("turnoId", "-");
                    matricula.put("turnoNom", "Desconocido");
                }

                return WebResponse.crearWebResponseExito("Se encontro matricula", matricula);

            } else {
                return WebResponse.crearWebResponseError("No se encontro matricula! ", matricula);
            }
        } catch (Exception e) {
            return WebResponse.crearWebResponseError("No se encontro matriculaa! ", e.getMessage());
        }
    }

    private String getEstadomatricula(int estMat) {
        switch (estMat) {
            case 1: {
                return "En Proceso";
            }
            case 2: {
                return "Aseptada";
            }
            default: {
                return "En Proceso";
            }
        }
    }

    private String getCondicionMatricula(int conMat) {
        switch (conMat) {
            case 1: {
                return "Gratuita";
            }
            case 2: {
                return "Pagante";
            }
            case 3: {
                return "Beca Completa";
            }
            case 4: {
                return "Media Beca";
            }
            default: {
                return "Gratuita";
            }
        }
    }

    private String getSituacionMatricula(int matSit) {
        switch (matSit) {
            case 1: {
                return "Ingresante";
            }
            case 2: {
                return "Promovido";
            }
            case 3: {
                return "Repitente";
            }
            case 4: {
                return "Reentrante";
            }
            case 5: {
                return "Reingresante";
            }
            case 6: {
                return "Otra";
            }
            default: {
                return "Otra";
            }
        }
    }

    private String getGradoDes(String graId) {
        switch (graId) {
            case "0": {
                return "-";
            }
            case "1": {
                return "Inicial 0-2";
            }
            case "2": {
                return "Inicial 3-5";
            }
            case "3": {
                return "1ro ";
            }
            case "4": {
                return "2do ";
            }
            case "5": {
                return "3ro ";
            }
            case "6": {
                return "4to ";
            }
            case "7": {
                return "5to ";
            }
            case "8": {
                return "6to ";
            }
            case "9": {
                return "1ro ";
            }
            case "10": {
                return "2do ";
            }
            case "11": {
                return "3ro ";
            }
            case "12": {
                return "4to ";
            }
            case "13": {
                return "5to ";
            }
            default: {
                return "-";
            }
        }
    }
}
