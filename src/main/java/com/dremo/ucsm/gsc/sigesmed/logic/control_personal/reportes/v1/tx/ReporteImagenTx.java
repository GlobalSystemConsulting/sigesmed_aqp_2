/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.control_personal.reportes.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.logic.documentos_gestion.entorno_inicio.v1.tx.*;
import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.OrganizacionDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.PersonaDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.cpe.AsistenciaDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.rdg.TipoEspecializadoDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Organizacion;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Persona;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Trabajador;
import com.dremo.ucsm.gsc.sigesmed.core.entity.cpe.RegistroAsistencia;
import com.dremo.ucsm.gsc.sigesmed.core.entity.rdg.TipoEspecializado;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.base.FileJsonObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import com.dremo.ucsm.gsc.sigesmed.util.DateUtil;
import com.dremo.ucsm.gsc.sigesmed.util.GTabla;
import com.dremo.ucsm.gsc.sigesmed.util.Mitext;
import com.itextpdf.layout.border.Border;
import com.itextpdf.layout.element.Cell;
import com.itextpdf.layout.element.Paragraph;
import com.itextpdf.layout.element.Table;
import com.itextpdf.layout.property.TextAlignment;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author Administrador
 */
public class ReporteImagenTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
        
        /*
        *   Parte para la lectura, verificacion y validacion de datos
        */
        FileJsonObject miGrafico = null;//miGrafico = new FileJsonObject( requestData.optJSONObject("grafico")  );
         Organizacion organizacion;
        String dni="";
        DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String desde="";
        String hasta="";
        String persona="";
        Date fechaDesde;
        Date fechaHasta;
        JSONObject requestData = (JSONObject)wr.getData();
        String nombreOrg = "";
        try{
            miGrafico = new FileJsonObject( requestData.optJSONObject("grafico")  );
            Integer organizacionId = requestData.getInt("orgId");  
            dni=requestData.getString("perDni");
            desde=requestData.getString("desde");
            hasta=requestData.getString("hasta");
            nombreOrg=requestData.getString("nomOrg");
            persona=requestData.getString("nom");
            fechaDesde=sdf.parse(desde);
            fechaHasta=sdf.parse(hasta);
            organizacion=new Organizacion(organizacionId);
    
        }catch(Exception e){
            System.out.println("No se pudo verificar los datos \n"+e);
            return WebResponse.crearWebResponseError("No se pudo verificar los datos ", e.getMessage() );
        }
       
        AsistenciaDao asistenciaDao= (AsistenciaDao)FactoryDao.buildDao("cpe.AsistenciaDao");
        List<Trabajador> trabajadores=new ArrayList<>();
        try{
           
            trabajadores=asistenciaDao.listarAsistenciaByFecha(fechaDesde, fechaHasta, organizacion, dni);
            
        }catch(Exception e){
            System.out.println("No se pudo verificar la asistencia \n"+e);
            return WebResponse.crearWebResponseError("No se pudo verificar la asistencia ", e.getMessage() );
        }
        
        
        
        //Creando el reporte....        
        Mitext m = null;        
        try {
            m = new Mitext(false);
            String titulo = "CUADRO DE ASISTENCIA E INASISTENCIA DEL PERSONAL DOCENTE Y AUXILIARES ";
            m.setStyle(1, 13, false, false, false);
            m.agregarParrafoMyEstilo(titulo, 1);
            m.newLine(1);
        } catch (Exception ex) {
            System.out.println("No se pudo crear el documento \n"+ex);
            Logger.getLogger(ReporteListadoAsistenciasTx.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        
        float[] columnWidthsD={4,6,5};
        Table tabla = new Table(columnWidthsD);
        tabla.setWidthPercent(100);
        tabla.addCell(new Cell(1,1).setBorder(Border.NO_BORDER).add(new Paragraph("INSTITUCION EDUCATIVA").setFontSize(12).setTextAlignment(TextAlignment.LEFT)));
        tabla.addCell(new Cell(1,1).setBorder(Border.NO_BORDER).add(new Paragraph(" : "+nombreOrg).setFontSize(12)).setTextAlignment(TextAlignment.LEFT));
        tabla.addCell(new Cell(1,1).setBorder(Border.NO_BORDER));

        tabla.addCell(new Cell(1,1).setBorder(Border.NO_BORDER).add(new Paragraph("DE").setFontSize(12).setTextAlignment(TextAlignment.LEFT)));
        tabla.addCell(new Cell(1,1).setBorder(Border.NO_BORDER).add(new Paragraph(" : "+DateUtil.convertDateToString(fechaDesde)).setFontSize(12)).setTextAlignment(TextAlignment.LEFT));
        tabla.addCell(new Cell(1,5).setBorder(Border.NO_BORDER));

        tabla.addCell(new Cell(1,1).setBorder(Border.NO_BORDER).add(new Paragraph("HASTA").setFontSize(12)).setTextAlignment(TextAlignment.LEFT));
        tabla.addCell(new Cell(1,1).setBorder(Border.NO_BORDER).add(new Paragraph(" : "+DateUtil.convertDateToString(fechaHasta)).setFontSize(12)).setTextAlignment(TextAlignment.LEFT));
        tabla.addCell(new Cell(1,1).setBorder(Border.NO_BORDER));

        tabla.addCell(new Cell(1,1).setBorder(Border.NO_BORDER).add(new Paragraph("APELLIDOS Y NOMBRES").setFontSize(12)).setTextAlignment(TextAlignment.LEFT));
        tabla.addCell(new Cell(1,1).setBorder(Border.NO_BORDER).add(new Paragraph(" : "+persona).setFontSize(12)).setTextAlignment(TextAlignment.LEFT));
        tabla.addCell(new Cell(1,5).setBorder(Border.NO_BORDER));
        
        float[] columnWidths = new float[]{1,5,5,5,5,5,5};
        
        GTabla t = new GTabla(columnWidths);
        String encabezados[]={"N","FECHA","CARGO","CONDICION","JORNADA LABORAL","ESTADO","OBSERVACIONES"};
        String detalle[]={"","","","","","",""};
        
        try {
            t.build(encabezados);
            
            t.setWidthPercent(100);
        } catch (IOException ex) {
            Logger.getLogger(ReporteListadoAsistenciasTx.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        int cont=0;
        for (Trabajador tra : trabajadores) {

            for (RegistroAsistencia ra : tra.getAsistencias()) {
                detalle[0]=++cont+"";
                
                detalle[1]=ra.getHoraIngreso() != null ? DateUtil.convertDateToString(ra.getHoraIngreso()) : DateUtil.convertDateToString(ra.getHoraSalida());
                if (tra.getTraCar() == null) {
                    detalle[2]="";
                } else {
                    detalle[2]=tra.getTraCar().getCrgTraNom();
                }
                if (tra.getCondicion() == null) {
                    detalle[3]="";
                } else if (tra.getCondicion().equals('T')) {
                    detalle[3]="NOMBRADO";
                } else if (tra.getCondicion().equals('V')) {
                    detalle[3]="CONTRATADO";
                }

                if (tra.getJornada() == null) {
                    detalle[4]="0";
                } else {
                    detalle[4]=tra.getJornada()+"";
                }

                //asistencias  
                
                if (ra.getEstReg().equals("1")) {
                    detalle[5]="ASISTIO";
                } else if (ra.getEstReg().equals("2")) {
                    detalle[5]="TARDANZA";
                } else {
                    detalle[5]="FALTA";
                }
                if (ra.getJustificacion() == null) {
                    detalle[6]="";
                } else {
                    detalle[6]="JUSTIFICADA";
                }
                t.processLine(detalle);
                detalle[0]="";
                detalle[1]="";
                detalle[2]="";
                detalle[3]="";
                detalle[4]="";
                detalle[5]="";
                detalle[6]="";
            }
            
        }      
    
        try {
            //fin tabla
            m.agregarTabla(tabla);
            m.agregarParrafo("");
            m.agregarTabla(t);
            m.agregarParrafo("");
            
            m.agregarImagen64(miGrafico.getData());
            m.cerrarDocumento();
        } catch (Exception ex) {
        }
        JSONArray miArray = new JSONArray();
        JSONObject oResponse = new JSONObject();
        oResponse.append("datareporte", m.encodeToBase64());
        
        miArray.put(oResponse);

        return WebResponse.crearWebResponseExito("No se pudo realizar el reporte", miArray);
        //Fin
    }
}
