/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.scp;

import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.GenericDaoHibernate;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import com.dremo.ucsm.gsc.sigesmed.core.dao.scp.CausalBajaDAO;
import com.dremo.ucsm.gsc.sigesmed.core.entity.scp.CausalBaja;
import com.dremo.ucsm.gsc.sigesmed.core.datastore.HibernateUtil;
/**
 *
 * @author Administrador
 */
public class CausalBajaDAOHibernate extends GenericDaoHibernate<CausalBaja> implements CausalBajaDAO{

    @Override
    public List<CausalBaja> listarCausalBaja() {
        
        List<CausalBaja> cb = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{

            String hql = "SELECT cb FROM CausalBaja cb  WHERE cb.est_reg!='E'";

            Query query = session.createQuery(hql); 
            cb = query.list();
            
        }catch(Exception e){
             System.out.println("No se pudo Mostrar las Causales de Baja \\n "+ e.getMessage());
             throw new UnsupportedOperationException("No se pudo Mostrar las Causales de Baja \\n "+ e.getMessage());
        }
        finally{
            session.close();
        }
        return cb; 
        
      //  throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    
}
