package com.dremo.ucsm.gsc.sigesmed.logic.maestro.proys.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.smdg.ProyectosDao;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.constantes.Sigesmed;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.List;

/**
 * Created by Administrador on 10/01/2017.
 */
public class ListarProyectosDocenteTx implements ITransaction{
    @Override
    public WebResponse execute(WebRequest wr) {
        /*
        *  Parte para la operacion en la Base de Datos
        */

        JSONObject requestData = (JSONObject)wr.getData();
        int docId = requestData.getInt("doc");
        int orgId = requestData.getInt("org");

        return listarProyectos(docId,orgId);

    }

    private WebResponse listarProyectos(int docId, int orgId) {
        try{
            ProyectosDao proyectosDao = (ProyectosDao) FactoryDao.buildDao("smdg.ProyectosDao");
            List<Object[]> proyectos = proyectos = proyectosDao.listarProyectosDocente(docId, orgId);
            JSONArray miArray = new JSONArray();
            for(Object[] p : proyectos){
                JSONObject oResponse = new JSONObject();
                oResponse.put("proid",p[0]);
                oResponse.put("pronom",p[1]);
                oResponse.put("prores",p[2]);
                oResponse.put("proini",p[3]);
                oResponse.put("profin",p[4]);
                oResponse.put("prodoc",p[5]);
                oResponse.put("proresnom",p[6]+ " " +p[7]+ " " +p[8]);
                oResponse.put("proresdni",p[9]);
                oResponse.put("url", Sigesmed.UBI_ARCHIVOS+"/smdg/");
                oResponse.put("proava",p[10]);

                miArray.put(oResponse);
            }

            return WebResponse.crearWebResponseExito("Se Listo correctamente",miArray);
        }catch (Exception e){
            return WebResponse.crearWebResponseError("No se pudo Listar las proyectos", e.getMessage() );
        }
    }

}
