/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.entity.scp;


import java.util.Date;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;


/**
 *
 * @author Jeferson
 */
@Entity
@Table(name="catalogo_bienes", schema="administrativo")
public class CatalogoBienes implements java.io.Serializable {
    
    @Id
    @Column(name="cat_bie_id", unique= true , nullable=false)
    @SequenceGenerator(name="secuencia_det_inv_tra",sequenceName="administrativo.catalogo_bienes_cat_bie_id_seq")
    @GeneratedValue(generator="secuencia_det_inv_tra")
    private int cat_bie_id;
    
    /*
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="gru_gen_id" , insertable=false , updatable=false)
    private GrupoGenerico grupo_generico;
    
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="cla_gen_id" , insertable=false , updatable=false)
    private ClaseGenerica clase_generica;
    
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="fam_gen_id" , insertable=false , updatable=false)
    private FamiliaGenerica familia_generica;
    */
    
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumns({
        @JoinColumn(name="gru_gen_id",referencedColumnName="gru_gen_id",insertable=false,updatable=false),
        @JoinColumn(name="cla_gen_id",referencedColumnName="cla_gen_id",insertable=false,updatable=false),
        @JoinColumn(name="fam_gen_id",referencedColumnName="fam_gen_id",insertable=false,updatable=false)    
    })
    private FamiliaGenerica familia;

    public void setFamilia(FamiliaGenerica familia) {
        this.familia = familia;
    }

    public FamiliaGenerica getFamilia() {
        return familia;
    }
    
    
    
    
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="uni_med_id" , insertable=false , updatable=false)
    private UnidadMedida unidad_medida;

    public void setUnidad_medida(UnidadMedida unidad_medida) {
        this.unidad_medida = unidad_medida;
    }

    public UnidadMedida getUnidad_medida() {
        return unidad_medida;
    }
    
    @Column(name="gru_gen_id")
    private int gru_gen_id;
    
    @Column(name="cla_gen_id")
    private int cla_gen_id;
    
    @Column(name="fam_gen_id")
    private int fam_gen_id;
    
    @Column(name="cod")
    private String cod;
    
    
    
    @Column(name="den_bie")
    private String den_bie;
    
    @Column(name="fec_cre")
    private Date fec_cre;
    
    @Column(name="num_res")
    private String num_res;
    
    @Column(name="uni_med_id")
    private short uni_med_id;
    
    @Column(name="fla_sbn")
    private boolean fla_sbn;
    
    @Column(name="fec_mod")
    private Date fec_mod;
    
    @Column(name="usu_mod")
    private int usu_mod;
    
    @Column(name="est_reg")
    private char est_reg;

    public void setCat_bie_id(int cat_bie_id) {
        this.cat_bie_id = cat_bie_id;
    }

    public void setDen_bie(String den_bie) {
        this.den_bie = den_bie;
    }

    public void setFec_cre(Date fec_cre) {
        this.fec_cre = fec_cre;
    }

    public void setNum_res(String num_res) {
        this.num_res = num_res;
    }

    public void setUni_med_id(short uni_med_id) {
        this.uni_med_id = uni_med_id;
    }

    public void setFla_sbn(boolean fla_sbn) {
        this.fla_sbn = fla_sbn;
    }

    public void setFec_mod(Date fec_mod) {
        this.fec_mod = fec_mod;
    }

    public void setUsu_mod(int usu_mod) {
        this.usu_mod = usu_mod;
    }

    public void setEst_reg(char est_reg) {
        this.est_reg = est_reg;
    }

    public int getCat_bie_id() {
        return cat_bie_id;
    }

   
    public String getCod() {
        return cod;
    }

    
    public String getDen_bie() {
        return den_bie;
    }

    public Date getFec_cre() {
        return fec_cre;
    }

    public String getNum_res() {
        return num_res;
    }

    public short getUni_med_id() {
        return uni_med_id;
    }

    public boolean isFla_sbn() {
        return fla_sbn;
    }

    public Date getFec_mod() {
        return fec_mod;
    }

    public int getUsu_mod() {
        return usu_mod;
    }

    public char getEst_reg() {
        return est_reg;
    }

    public CatalogoBienes(int cat_bie_id, String cod, String den_bie, Date fec_cre, String num_res, short uni_med_id, boolean fla_sbn, Date fec_mod, int usu_mod, char est_reg ,int gru_gen , int cla_gen , int fam_gen) {
        this.cat_bie_id = cat_bie_id;
        this.cod = cod;
        
        this.den_bie = den_bie;
        this.fec_cre = fec_cre;
        this.num_res = num_res;
        this.uni_med_id = uni_med_id;
        this.fla_sbn = fla_sbn;
        this.fec_mod = fec_mod;
        this.usu_mod = usu_mod;
        this.est_reg = est_reg;
        this.gru_gen_id = gru_gen;
        this.cla_gen_id = cla_gen;
        this.fam_gen_id = fam_gen;
    }

    /*
    public CatalogoBienes(int cat_bie_id, GrupoGenerico grupo_generico, ClaseGenerica clase_generica, FamiliaGenerica familia_generica, UnidadMedida unidad_medida) {
        this.cat_bie_id = cat_bie_id;
        this.grupo_generico = grupo_generico;
        this.clase_generica = clase_generica;
        this.familia_generica = familia_generica;
        this.unidad_medida = unidad_medida;
    }
    
    */

    public CatalogoBienes() {
    }
    
    
    
}
