package com.dremo.ucsm.gsc.sigesmed.core.entity.web;

import java.util.Date;

public class BandejaTareaModel{

    public int banTarId;    
    public int nota;
    
    public Date fecEnt;
    public Date fecVis;    
    
    public String nombres;
    public String apellido1;
    public String apellido2;
    
    public char estado;

    public int per_id;
    
    public String tip_per;
    
    public int id_per; 
      
    public BandejaTareaModel() {
    }
    public BandejaTareaModel(int banTarId) {
        this.banTarId = banTarId;
    }
    
    public BandejaTareaModel(int banTarId, Date fecEnt,Date fecVis, int nota,char estado,String nombres,String apellido1,String apellido2 , int per_id,String tip_per ,int id_per) {
       this.banTarId = banTarId;
       this.fecVis = fecVis;
       this.fecEnt = fecEnt;
       this.nota = nota;
       this.estado = estado;
       this.nombres = nombres;
       this.apellido1 = apellido1;
       this.apellido2 = apellido2;
       this.per_id = per_id;
       this.tip_per = tip_per;
       this.id_per = id_per;
    }
}


