package com.dremo.ucsm.gsc.sigesmed.logic.scec.reporte.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.scec.ActasReunionComisionDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.scec.ComisionDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.scec.ReunionComisionDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Persona;
import com.dremo.ucsm.gsc.sigesmed.core.entity.scec.*;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import com.dremo.ucsm.gsc.sigesmed.util.GTabla;
import com.dremo.ucsm.gsc.sigesmed.util.Mitext;
import com.itextpdf.io.font.FontConstants;
import com.itextpdf.kernel.font.PdfFont;
import com.itextpdf.kernel.font.PdfFontFactory;
import com.itextpdf.layout.element.Paragraph;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by Administrador on 15/10/2016.
 */
public class ReporteReunionTx implements ITransaction {
    private static final Logger logger = Logger.getLogger(ReporteReunionTx.class.getName());
    @Override
    public WebResponse execute(WebRequest wr) {
        JSONObject data = (JSONObject) wr.getData();
        return reporteReunion(data.getInt("reu"),data.getInt("com"));
    }

    public WebResponse reporteReunion(int reu, int com) {
        try{
            ReunionComisionDao reunionDao = (ReunionComisionDao) FactoryDao.buildDao("scec.ReunionComisionDao");
            ComisionDao comisionDao = (ComisionDao) FactoryDao.buildDao("scec.ComisionDao");

            ReunionComision reunion = reunionDao.buscarActasyAsistenciaPorReunion(reu);

            String reportb64 = crearReporteMemoria(reunion);
            return WebResponse.crearWebResponseExito("Se creo el reporte satisfactoriamente",WebResponse.OK_RESPONSE).setData(
                    new JSONObject().put("b64",reportb64)
            );
            //return null;
        }catch (Exception e){
            logger.log(Level.SEVERE,"reporte reunion",e);
            return WebResponse.crearWebResponseError("No se puede crear el reporte",WebResponse.BAD_RESPONSE);
        }

    }
    private String crearReporteMemoria(ReunionComision reunion) throws Exception {

        Mitext mitext = new Mitext(false,"SIGESMED");
        mitext.agregarTitulo("Reporte de Reunion");


        JSONObject jsonSub = new JSONObject();
        jsonSub.put("Hora Reunion",new SimpleDateFormat("HH:mm a").format(reunion.getFecReu()));
        jsonSub.put("Comision",reunion.getComision().getNomCom());
        jsonSub.put("Fecha de Reunion",new SimpleDateFormat("dd/MM/yyyy").format(reunion.getFecReu()));
        mitext.agregarSubtitulos(jsonSub);

        PdfFont bold = PdfFontFactory.createFont(FontConstants.HELVETICA_BOLD);
        Paragraph p = new Paragraph("Asistencia de participantes");
        p.setKeepTogether(true);
        p.setFont(bold).setFontSize(12);
        mitext.getDocument().add(p);

        GTabla gtabla = new GTabla(new float[]{1.5f,2.0f,2.0f,1.5f,1.5f,1.5f});
        gtabla.build(new String[]{"DNI","Apellido Pat.","Apellido Mat.","Nombre","Cargo","Asistencia"});
        for(AsistenciaReunionComision asistencia : reunion.getAsistenciasReunion()){
            IntegranteComision integrante = asistencia.getIntegranteComision();
            CargoComision cargo = integrante.getCargoComision();
            if(!cargo.getNomCar().toLowerCase().equals("participante")){
                String asisStr = asistencia.getEst() ? "Asistio" : "No asistio";
                Persona persona = integrante.getPersona();
                //imprimir en una tabla
                gtabla.processLine(
                        new String[]{
                                String.valueOf(persona.getDni()),
                                persona.getApePat(),
                                persona.getApeMat(),
                                persona.getNom(),
                                cargo.getNomCar(),
                                asisStr
                        });
            }

        }
        mitext.agregarTabla(gtabla);

        ////
        p = new Paragraph("Actas de la reunion");
        p.setKeepTogether(true);
        p.setFont(bold).setFontSize(12);
        mitext.getDocument().add(p);

        ActasReunionComisionDao actasDao = (ActasReunionComisionDao) FactoryDao.buildDao("scec.ActasReunionComisionDao");
        List<ActasReunionComision> actas = actasDao.buscarPorReunionConAcuerdos(reunion.getReuId());
        for(ActasReunionComision acta : actas){
            mitext.agregarParrafo(acta.getDes());
            gtabla = new GTabla(new float[]{3.5f,3.5f,3.0f});
            gtabla.build(new String[]{"Acuerdo","Cumplido","Fecha de cumplimiento"});
            for(AcuerdosActaComision acuerdo : acta.getAcuerdos()){
                String cumStr = acuerdo.getEstCumAcu() ? "Cumplido":"No Cumplido";
                String fecCum = acuerdo.getEstCumAcu() ? new SimpleDateFormat("dd/MM/yyyy").format(acuerdo.getFecCumAcu()) : "";
                gtabla.processLine(new String[]{
                        acuerdo.getDesAcu(),
                        cumStr,
                        fecCum
                });
            }
            mitext.agregarTabla(gtabla);
        }
        mitext.cerrarDocumento();

        return mitext.encodeToBase64();
    }
}
