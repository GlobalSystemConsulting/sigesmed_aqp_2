/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.entity.scp;

import java.util.Date;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.OneToOne;
/**
 *
 * @author Jeferson
 */

@Entity
@Table(name="caracteristicas_tecnicas", schema="administrativo")
public class CaracteristicasTecnicas implements java.io.Serializable {
    
    @Id
    @Column(name="car_tec_id", unique= true , nullable=false)
    @SequenceGenerator(name="secuencia_det_inv_tra",sequenceName="administrativo.caracteristicas_tecnicas_car_tec_id_seq")
    @GeneratedValue(generator="secuencia_det_inv_tra")
    private int car_tec_id;
    
    @Id
    @OneToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="cat_bie_id" , insertable=false , updatable=false)
    private CatalogoBienes catalogo_bienes;

    
    @Column(name="des_car")
    private String des_car;
    
    @Column(name="fec_mod")
    private Date fec_mod;
    
    @Column(name="usu_mod")
    private int usu_mod;
    
    @Column(name="est_reg")
    private char est_reg;

    public void setCar_tec_id(int car_tec_id) {
        this.car_tec_id = car_tec_id;
    }

    public void setCatalogo_bienes(CatalogoBienes catalogo_bienes) {
        this.catalogo_bienes = catalogo_bienes;
    }

    public void setDes_car(String des_car) {
        this.des_car = des_car;
    }

    public void setFec_mod(Date fec_mod) {
        this.fec_mod = fec_mod;
    }

    public void setUsu_mod(int usu_mod) {
        this.usu_mod = usu_mod;
    }

    public void setEst_reg(char est_reg) {
        this.est_reg = est_reg;
    }

    public int getCar_tec_id() {
        return car_tec_id;
    }

    public CatalogoBienes getCatalogo_bienes() {
        return catalogo_bienes;
    }

    public String getDes_car() {
        return des_car;
    }

    public Date getFec_mod() {
        return fec_mod;
    }

    public int getUsu_mod() {
        return usu_mod;
    }

    public char getEst_reg() {
        return est_reg;
    }
    
}
