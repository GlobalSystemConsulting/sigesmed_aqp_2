/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.entity.mpf;

/**
 *
 * @author carlos
 */
// Generated 13/02/2017 02:15:53 PM by Hibernate Tools 4.3.1
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity(name="com.dremo.ucsm.gsm.sigesmed.core.entity.mpf.Pais")
@Table(name = "pais", schema = "pedagogico"
)
public class Pais implements java.io.Serializable {

    @Id
    @Column(name = "pai_id", unique = true, nullable = false)
    private int paiId;
    
    @Column(name = "pai_nom", length = 60)
    private String paiNom;
    
    @Column(name = "usu_mod")
    private Integer usuMod;
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "fec_mod", length = 29)
    private Date fecMod;
    
    @Column(name = "est_reg", length = 1)
    private Character estReg;
    
    @OneToMany(mappedBy = "pais")
    private List<DatosNacimiento> datosNacimientos;

    public Pais() {
    }

    public Pais(int paiId) {
        this.paiId = paiId;
    }

    public Pais(int paiId, String paiNom, Integer usuMod, Date fecMod, Character estReg, List<DatosNacimiento> datosNacimientos) {
        this.paiId = paiId;
        this.paiNom = paiNom;
        this.usuMod = usuMod;
        this.fecMod = fecMod;
        this.estReg = estReg;
        this.datosNacimientos = datosNacimientos;
    }

    public int getPaiId() {
        return this.paiId;
    }

    public void setPaiId(int paiId) {
        this.paiId = paiId;
    }

    
    public String getPaiNom() {
        return this.paiNom;
    }

    public void setPaiNom(String paiNom) {
        this.paiNom = paiNom;
    }

    
    public Integer getUsuMod() {
        return this.usuMod;
    }

    public void setUsuMod(Integer usuMod) {
        this.usuMod = usuMod;
    }

    
    public Date getFecMod() {
        return this.fecMod;
    }

    public void setFecMod(Date fecMod) {
        this.fecMod = fecMod;
    }

    
    public Character getEstReg() {
        return this.estReg;
    }

    public void setEstReg(Character estReg) {
        this.estReg = estReg;
    }

    
    public List<DatosNacimiento> getDatosNacimientos() {
        return this.datosNacimientos;
    }

    public void setDatosNacimientos(List<DatosNacimiento> datosNacimientos) {
        this.datosNacimientos = datosNacimientos;
    }

}
