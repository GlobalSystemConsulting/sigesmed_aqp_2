package com.dremo.ucsm.gsc.sigesmed.core.entity.cpe;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="sistema_biometrico" ,schema="administrativo")
public class SistemaBiometrico  implements java.io.Serializable {


    @Id
    @Column(name="sis_bio_id", unique=true, nullable=false)
    @SequenceGenerator(name = "secuencoa_sistema_biometrico", sequenceName="administrativo.sistema_biometrico_sis_bio_id_seq" )
    @GeneratedValue(generator="secuencoa_sistema_biometrico")
    private Integer sisBioId;
    
    @Column(name="sis_bio_des")
    private String sisBioDes;
    
    @Column(name="sis_bio_ip")
    private String sisBioIp;
    
    @Column(name="sis_bio_pue")
    private String sisBioPue;
    
    @Column(name="est_reg")
    private String estReg;
    
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="con_per_id", nullable=false )
    private ConfiguracionControl sisBioConPerId;  

    public SistemaBiometrico(String sisBioDes, String sisBioIp, String sisBioPue, String estReg, ConfiguracionControl sisBioConPerId) {
        this.sisBioDes = sisBioDes;
        this.sisBioIp = sisBioIp;
        this.sisBioPue = sisBioPue;
        this.estReg = estReg;
        this.sisBioConPerId = sisBioConPerId;
    }

    public Integer getSisBioId() {
        return sisBioId;
    }

    public void setSisBioId(Integer sisBioId) {
        this.sisBioId = sisBioId;
    }

    public String getSisBioDes() {
        return sisBioDes;
    }

    public void setSisBioDes(String sisBioDes) {
        this.sisBioDes = sisBioDes;
    }

    public String getSisBioIp() {
        return sisBioIp;
    }

    public void setSisBioIp(String sisBioIp) {
        this.sisBioIp = sisBioIp;
    }

    public String getSisBioPue() {
        return sisBioPue;
    }

    public void setSisBioPue(String sisBioPue) {
        this.sisBioPue = sisBioPue;
    }

    public String getEstReg() {
        return estReg;
    }

    public void setEstReg(String estReg) {
        this.estReg = estReg;
    }

    public ConfiguracionControl getSisBioConPerId() {
        return sisBioConPerId;
    }

    public void setSisBioConPerId(ConfiguracionControl sisBioConPerId) {
        this.sisBioConPerId = sisBioConPerId;
    }
    
    
   
}


