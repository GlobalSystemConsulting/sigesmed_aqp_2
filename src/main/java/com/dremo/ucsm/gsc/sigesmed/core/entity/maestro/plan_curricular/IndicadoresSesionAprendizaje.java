package com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.plan_curricular;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by Administrador on 29/11/2016.
 */
@Entity
@Table(name = "indicadores_sesion_aprendizaje", schema = "pedagogico")
public class IndicadoresSesionAprendizaje implements java.io.Serializable {
    @Embeddable
    public static class Id implements java.io.Serializable{
        @Column(name = "ses_apr_id", nullable = false)
        protected int sesAprId;
        @Column(name = "uni_did_id", nullable = false)
        protected int unididId;
        @Column(name = "com_id", nullable = false)
        protected int comId;
        @Column(name = "cap_id", nullable = false)
        protected int capId;
        @Column(name = "ind_apr_id", nullable = false)
        protected int indAprId;

        public Id() {
        }

        public Id(int sesAprId, int unididId, int comId, int capId, int indAprId) {
            this.sesAprId = sesAprId;
            this.unididId = unididId;
            this.comId = comId;
            this.capId = capId;
            this.indAprId = indAprId;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (!(o instanceof Id)) return false;

            Id id = (Id) o;

            if (sesAprId != id.sesAprId) return false;
            if (unididId != id.unididId) return false;
            if (comId != id.comId) return false;
            if (capId != id.capId) return false;
            return indAprId == id.indAprId;

        }

        @Override
        public int hashCode() {
            int result = sesAprId;
            result = 31 * result + unididId;
            result = 31 * result + comId;
            result = 31 * result + capId;
            result = 31 * result + indAprId;
            return result;
        }
    }
    @EmbeddedId
    @AttributeOverrides({
            @AttributeOverride(name = "sesAprId", column = @Column(name = "ses_apr_id", nullable = false)),
            @AttributeOverride(name = "unididId", column = @Column(name = "uni_did_id", nullable = false)),
            @AttributeOverride(name = "comId", column = @Column(name = "com_id", nullable = false)),
            @AttributeOverride(name = "capId", column = @Column(name = "cap_id", nullable = false)),
            @AttributeOverride(name = "indAprId", column = @Column(name = "ind_apr_id", nullable = false))

    })
    private Id id = new Id();

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "ses_apr_id", insertable = false, updatable = false)
    private SesionAprendizaje sesion;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "uni_did_id", insertable = false, updatable = false)
    private UnidadDidactica unidad;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "com_id", insertable = false, updatable = false)
    private CompetenciaAprendizaje competencia;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "cap_id", insertable = false, updatable = false)
    private CapacidadAprendizaje capacidad;
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "ind_apr_id", insertable = false, updatable = false)
    private IndicadorAprendizaje indicador;

    @Column(name = "usu_mod")
    private Integer usuMod;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "fec_mod")
    private Date fecMod;

    @Column(name = "est_reg", length = 1)
    private Character estReg;

    public IndicadoresSesionAprendizaje() {
    }

    public IndicadoresSesionAprendizaje(SesionAprendizaje sesion, UnidadDidactica unidad, CompetenciaAprendizaje competencia, CapacidadAprendizaje capacidad, IndicadorAprendizaje indicador) {
        this.sesion = sesion;
        this.unidad = unidad;
        this.competencia = competencia;
        this.capacidad = capacidad;
        this.indicador = indicador;

        this.id.sesAprId = sesion.getSesAprId();
        this.id.unididId = unidad.getUniDidId();
        this.id.comId = competencia.getComId();
        this.id.capId = capacidad.getCapId();
        this.id.indAprId = indicador.getIndAprId();


        this.estReg = 'A';
        this.fecMod = new Date();
    }

    public Character getEstReg() {
        return estReg;
    }

    public void setEstReg(Character estReg) {
        this.estReg = estReg;
    }

    public Date getFecMod() {
        return fecMod;
    }

    public void setFecMod(Date fecMod) {
        this.fecMod = fecMod;
    }

    public Integer getUsuMod() {
        return usuMod;
    }

    public void setUsuMod(Integer usuMod) {
        this.usuMod = usuMod;
    }

    public SesionAprendizaje getSesion() {
        return sesion;
    }

    public UnidadDidactica getUnidad() {
        return unidad;
    }

    public CompetenciaAprendizaje getCompetencia() {
        return competencia;
    }

    public CapacidadAprendizaje getCapacidad() {
        return capacidad;
    }

    public IndicadorAprendizaje getIndicador() {
        return indicador;
    }

    @Override
    public String toString() {
        return "IndicadoresSesionAprendizaje{" + " sesion=" + sesion + ", unidad=" + unidad + ", competencia=" + competencia + ", capacidad=" + capacidad + ", indicador=" + indicador + ", usuMod=" + usuMod + ", fecMod=" + fecMod + ", estReg=" + estReg + '}';
    }
    
    
}
