/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.sistema_contable.cuenta_contable.v1.tx;

/**
 *
 * @author ucsm
 */

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.sci.CuentaContableDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.dao.sci.CuentaContableDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sci.CuentaContable;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.math.BigDecimal;
import java.util.Date;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author ucsm
 */
public class InsertarCuentaContableTx implements ITransaction{   
    
    @Override    
    public WebResponse execute(WebRequest wr)  {
                
        /*
        *   Parte para la lectura, verificacion y validacion de datos
        */
        CuentaContable nuevaCuentaContable = null;
        CuentaContable verificar=null;
    
        try{            
             
            JSONObject requestData = (JSONObject)wr.getData();           
            String numero = requestData.getString("numero");            
            String tipo =  Integer.toString(requestData.getInt("tipo"));           
            String subClase =  Integer.toString(requestData.getInt("subClase"));                 
          
            String nombre = requestData.getString("nombre");

            boolean encabezado = requestData.getBoolean("encabezado");
            boolean efectivo = requestData.getBoolean("efectivo");
          
            boolean iva = requestData.getBoolean("iva");
         
                     
                        
            nuevaCuentaContable = new CuentaContable(0,numero, tipo.charAt(0),subClase.charAt(0), nombre,  encabezado, efectivo,  iva,new Date(), wr.getIdUsuario(), 'A');
                           
        }catch(Exception e){
          
            return WebResponse.crearWebResponseError("No se pudo registrar, datos incorrectos", e.getMessage() );
        }
         //Fin
        
        /*
        *   Parte de Logica de Negocio    
        *
        */
        
     

        CuentaContableDao cuentaContableDao = new CuentaContableDaoHibernate();
        //Fin
        
        /*
        *  Parte para la operacion en la Base de Datos
        */
        try{
            verificar=cuentaContableDao.buscarCuentaPorNumero(nuevaCuentaContable.getNumCue());
            
            if(verificar==null){              
                cuentaContableDao.insert(nuevaCuentaContable);        
            }
        }catch(Exception e){
            return WebResponse.crearWebResponseError("No se pudo registrar la cuenta contable ", e.getMessage() );
        }
        //Fin
        
      
        /*
        *  Repuesta Correcta
        */
        JSONObject oResponse = new JSONObject();

        if(verificar==null){
            oResponse.put("cuentaContableID",nuevaCuentaContable.getCueConId());
            oResponse.put("numero",nuevaCuentaContable.getNumCue());
            oResponse.put("nombre",nuevaCuentaContable.getNomCue());
            oResponse.put("tipo",nuevaCuentaContable.getTip());
            oResponse.put("encabezado",nuevaCuentaContable.getCueEnc());
            oResponse.put("efectivo",nuevaCuentaContable.getEfeReg());
            oResponse.put("subClase",nuevaCuentaContable.getSubCla());
            oResponse.put("iva", nuevaCuentaContable.getUsaInfIva());
            
            oResponse.put("estado",""+nuevaCuentaContable.getEstReg());
        }

        return WebResponse.crearWebResponseExito(verificar==null?"El registro de la Cuenta Contable se realizo correctamente":"Ya existe una cuenta con el mismo número: INGRESE otro numero de cuenta!" , oResponse);
        //Fin
    }    
    
    
}
