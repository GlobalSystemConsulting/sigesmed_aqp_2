package com.dremo.ucsm.gsc.sigesmed.core.dao.capacitacion;

import com.dremo.ucsm.gsc.sigesmed.core.dao.GenericDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.capacitacion.AdjuntoTemaCapacitacion;

public interface AdjuntoTemaCapacitacionDao extends GenericDao<AdjuntoTemaCapacitacion> {
    
}
