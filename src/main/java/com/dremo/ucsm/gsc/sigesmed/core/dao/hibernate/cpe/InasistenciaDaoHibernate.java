    /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.cpe;

import com.dremo.ucsm.gsc.sigesmed.core.dao.cpe.CalendarioDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.cpe.HorarioDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.cpe.InasistenciaDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.cpe.JustificacionInasistenciaDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.GenericDaoHibernate;

import com.dremo.ucsm.gsc.sigesmed.core.datastore.HibernateUtil;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Organizacion;
import com.dremo.ucsm.gsc.sigesmed.core.entity.cpe.DiasEspeciales;
import com.dremo.ucsm.gsc.sigesmed.core.entity.cpe.HorarioCab;
import com.dremo.ucsm.gsc.sigesmed.core.entity.cpe.Inasistencia;
import com.dremo.ucsm.gsc.sigesmed.core.entity.cpe.JustificacionInasistenciaTrabajador;
import java.util.Date;

import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;

/**
 *
 * @author Carlos
 */
public class InasistenciaDaoHibernate extends GenericDaoHibernate<Inasistencia> implements InasistenciaDao {
    @Override
    public List<DiasEspeciales> getDiasEspeciales(Organizacion org,Date inicio,Date fin,String estado){
        List<DiasEspeciales> lista = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try {
            String hql = "SELECT  de FROM DiasEspeciales de  WHERE de.organizacionId=:p1 AND de.fecha>=:p2 AND de.fecha<=:p3 AND de.estReg=:p4";

            Query query = session.createQuery(hql);
            query.setParameter("p1", org);
            query.setParameter("p2", inicio);
            query.setParameter("p3", fin);
            query.setParameter("p4", estado);
            
            lista = query.list();

        } catch (Exception e) {
            System.out.println("No se pudo buscar los niveles \\n " + e.getMessage());
            e.printStackTrace();
            throw new UnsupportedOperationException("No se pudo buscar los niveles \\n " + e.getMessage());
        } finally {
            session.close();
        }

        return lista;

    }

    @Override
    public List<Organizacion> getOrganizaciones() {
        List<Organizacion> lista = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try {
            String hql = "SELECT  oo FROM Organizacion oo  WHERE oo.estReg<>'E'";

            Query query = session.createQuery(hql);
            lista = query.list();

        } catch (Exception e) {
            System.out.println("No se pudo buscar las organizaciones \\n " + e.getMessage());
            e.printStackTrace();
            throw new UnsupportedOperationException("No se pudo buscar las organizaciones \\n " + e.getMessage());
        } finally {
            session.close();
        }
        return lista;
    }

    @Override
    public DiasEspeciales verificarDiaEspecial(Organizacion org, Date fecha, String estado) {
       DiasEspeciales rspta=null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try {
            String hql = "SELECT  oo FROM DiasEspeciales oo  WHERE oo.estReg=:p3 AND oo.organizacionId=:p1 AND oo.fecha=:p2";

            Query query = session.createQuery(hql);
            query.setParameter("p1", org);
            query.setParameter("p2", fecha);
            query.setParameter("p3", estado);
            if(query.list().size()>0)
                rspta=(DiasEspeciales)query.list().get(0);

        } catch (Exception e) {
            System.out.println("No se pudo buscar el dia especial \\n " + e.getMessage());
            e.printStackTrace();
            throw new UnsupportedOperationException("No se pudo buscar el dia especial \\n " + e.getMessage());
        } finally {
            session.close();
        }
        return rspta;
    
    }
}
