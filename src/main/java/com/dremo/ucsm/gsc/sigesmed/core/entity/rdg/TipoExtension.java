/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.entity.rdg;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author ucsm
 */
@Entity
@Table(name = "tipo_extension",schema="institucional")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TipoExtension.findAll", query = "SELECT t FROM TipoExtension t"),
    @NamedQuery(name = "TipoExtension.findByTexIde", query = "SELECT t FROM TipoExtension t WHERE t.texIde = :texIde"),
    @NamedQuery(name = "TipoExtension.findByTexNom", query = "SELECT t FROM TipoExtension t WHERE t.texNom = :texNom"),
    @NamedQuery(name = "TipoExtension.findByTexDes", query = "SELECT t FROM TipoExtension t WHERE t.texDes = :texDes")})
public class TipoExtension implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "tex_ide")
    @SequenceGenerator(name = "secuencia_tipext", sequenceName="institucional.tipo_extension_tex_ide_seq" )
    @GeneratedValue(generator="secuencia_tipext")
    private Short texIde;
    @Size(max = 5)
    @Column(name = "tex_nom")
    private String texNom;
    @Size(max = 200)
    @Column(name = "tex_des")
    private String texDes;


    public TipoExtension() {
    }

    public TipoExtension(Short texIde) {
        this.texIde = texIde;
    }

    public Short getTexIde() {
        return texIde;
    }

    public void setTexIde(Short texIde) {
        this.texIde = texIde;
    }

    public String getTexNom() {
        return texNom;
    }

    public void setTexNom(String texNom) {
        this.texNom = texNom;
    }

    public String getTexDes() {
        return texDes;
    }

    public void setTexDes(String texDes) {
        this.texDes = texDes;
    }

  

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (texIde != null ? texIde.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
       
        return true;
    }

    @Override
    public String toString() {
        return "com.dremo.ucsm.gsc.sigesmed.core.entity.TipoExtension[ texIde=" + texIde + " ]";
    }
    
}
