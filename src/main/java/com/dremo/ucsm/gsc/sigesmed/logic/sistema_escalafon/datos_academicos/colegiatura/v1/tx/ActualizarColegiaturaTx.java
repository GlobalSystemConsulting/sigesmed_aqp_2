/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.datos_academicos.colegiatura.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.ColegiaturaDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Colegiatura;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONObject;

/**
 *
 * @author Yemi
 */
public class ActualizarColegiaturaTx implements ITransaction{

    private static final Logger logger = Logger.getLogger(ActualizarColegiaturaTx.class.getName());

    @Override
    public WebResponse execute(WebRequest wr) {
        try{
                 
            JSONObject requestData = (JSONObject)wr.getData();
        
            Integer colId = requestData.getInt("colId");
            String nomColPro = requestData.optString("nomColPro");
            String numRegCol = requestData.optString("numRegCol");
            Boolean conReg = requestData.getBoolean("conReg");      
            
            return actualizarColegiatura(colId, nomColPro, numRegCol, conReg);
        }catch (Exception e){
            System.out.println(e);
            logger.log(Level.SEVERE,"Actualizar colegiatura",e);
            return WebResponse.crearWebResponseError("No se pudo actualizar, datos incorrectos", e.getMessage());
        } 
    }
    
    private WebResponse actualizarColegiatura(Integer colId, String nomColPro, String numRegCol, Boolean conReg) {
        try{
            ColegiaturaDao colegiaturaDao = (ColegiaturaDao)FactoryDao.buildDao("se.ColegiaturaDao");        
            Colegiatura colegiatura = colegiaturaDao.buscarPorId(colId);

            colegiatura.setNomColPro(nomColPro);
            colegiatura.setNumRegCol(numRegCol);
            colegiatura.setConReg(conReg);
            
            colegiaturaDao.update(colegiatura);
            
            

            JSONObject oResponse = new JSONObject();
            oResponse.put("colId", colegiatura.getColId());
            oResponse.put("nomColPro", colegiatura.getNomColPro());
            oResponse.put("numRegCol", colegiatura.getNumRegCol());
            oResponse.put("conReg", colegiatura.getConReg());
            oResponse.put("conRegDes", "");
            return WebResponse.crearWebResponseExito("Colegiatura actualizada exitosamente",oResponse);
            
        }catch (Exception e){
            logger.log(Level.SEVERE,"actualizarColegiatura",e);
            return WebResponse.crearWebResponseError("Error, la colegiatura no fue actualizada");
        }
    } 
    
}
