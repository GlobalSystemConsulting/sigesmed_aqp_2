
package com.dremo.ucsm.gsc.sigesmed.core.dao;

import com.dremo.ucsm.gsc.sigesmed.core.entity.Organizacion;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Persona;
import java.util.List;

/**
 *
 * @author abel
 */
public interface PersonaDao extends GenericDao<Persona>{
    
    public Persona buscarPorDNI(String dni);
    public Persona buscarPorID(int usuarioID);
    public Persona buscarPorCod(int cod);
    public List<Persona> buscarPorNombre(String nombreUser);
    List<Persona> buscarPorApellido(String apellidoPaterno, String apellidoMaterno);
    Persona buscarPorOrganizacionConUsuario(String dni,int idOrg);
    List<Persona> buscarPorOrganizacionConUsuario(String apellidoPaterno, String apellidoMaterno,int idOrg);
}
