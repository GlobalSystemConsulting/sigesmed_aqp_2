package com.dremo.ucsm.gsc.sigesmed.core.dao;

import com.dremo.ucsm.gsc.sigesmed.core.entity.Organizacion;
import java.util.List;

public interface OrganizacionDao extends GenericDao<Organizacion> {

    public String buscarUltimoCodigo();

    public List<Organizacion> buscarConTipoOrganizacionYPadre();

    public List<Organizacion> buscarPorTipoOrganizacion(int tipoOrganizacion);

    public Organizacion buscarConTipoOrganizacionYPadre(int orgID);

    List<Organizacion> buscarHijosOrganizacion(int orgId);

    public Organizacion buscarConTipoOrganizacion(int orgId);

    List<Integer> listarOrganizacionesCalendario(int orgCod);
}
