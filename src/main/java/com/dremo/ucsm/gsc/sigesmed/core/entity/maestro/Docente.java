package com.dremo.ucsm.gsc.sigesmed.core.entity.maestro;

import com.dremo.ucsm.gsc.sigesmed.core.entity.Persona;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Trabajador;

import javax.persistence.*;

/**
 * Created by Administrador on 10/10/2016.
 */
@Entity(name = "DocenteMaestro")
@Table(name = "docente", schema = "pedagogico")
public class Docente implements java.io.Serializable {
    @Id
    @GeneratedValue(generator = "docenteKeyGenerator")
    @org.hibernate.annotations.GenericGenerator(
            name = "docenteKeyGenerator",
            strategy = "foreign",
            parameters = @org.hibernate.annotations.Parameter(
                    name = "property",value = "per"
            )
    )
    private int doc_id;

    @OneToOne(fetch = FetchType.LAZY,optional = false)
    @PrimaryKeyJoinColumn
    private Persona per;

    @Column(name = "cod_mod", nullable = false,length = 10)//codigo modular
    private String codMod;

    @Column(name = "aut_ess")//
    private String autEss;

    @Column(name = "reg_lab")//regimen laboral
    private String regLab;

    @Column(name = "reg_pen")//regimen pension
    private String regPen;

    @Column(name = "num_col")
    private String numCol;//numero de colegiatura

    @Column(name = "esp")
    private String esp;

    @Column(name = "niv")
    private Integer niv;

    public Docente() {
    }

    public Docente(Persona per) {
        this.per = per;
    }

    public Docente(String autEss, String regLab, String regPen, String numCol, String esp, Integer niv, String codMod, Persona per) {
        this.autEss = autEss;
        this.regLab = regLab;
        this.regPen = regPen;
        this.numCol = numCol;
        this.esp = esp;
        this.niv = niv;
        this.codMod = codMod;
        this.per = per;
        this.doc_id = per.getPerId();
    }

    public int getDoc_id() {
        return doc_id;
    }

    public Persona getPer() {
        return per;
    }

    public void setPer(Persona per ) {
        this.per = per;
    }

    public String getCodMod() {
        return codMod;
    }

    public void setCodMod(String codMod) {
        this.codMod = codMod;
    }

    public String getAutEss() {
        return autEss;
    }

    public void setAutEss(String autEss) {
        this.autEss = autEss;
    }

    public String getRegLab() {
        return regLab;
    }

    public void setRegLab(String regLab) {
        this.regLab = regLab;
    }

    public String getRegPen() {
        return regPen;
    }

    public void setRegPen(String regPen) {
        this.regPen = regPen;
    }

    public String getNumCol() {
        return numCol;
    }

    public void setNumCol(String numCol) {
        this.numCol = numCol;
    }

    public String getEsp() {
        return esp;
    }

    public void setEsp(String esp) {
        this.esp = esp;
    }

    public Integer getNiv() {
        return niv;
    }

    public void setNiv(Integer niv) {
        this.niv = niv;
    }
}
