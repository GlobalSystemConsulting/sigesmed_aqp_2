/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.web.EvaluacionEscolar.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.web.EvaluacionEscolarDao;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import org.json.JSONArray;
import org.json.JSONObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import com.dremo.ucsm.gsc.sigesmed.core.entity.web.MensajeCalificacion;
import java.util.List;

/**
 *
 * @author abel
 */
public class ListarMensajesCalificacionTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
        
        /*
        *   Parte para la lectura, verificacion y validacion de datos
        */
        int evaluacionID = 0;
        try{
            
            JSONObject requestData = (JSONObject)wr.getData();
            
            evaluacionID = requestData.getInt("evaluacionID");
        
        }catch(Exception e){
            System.out.println(e);
            return WebResponse.crearWebResponseError("No se pudo Listar los mensajes de la evaluacion, datos incorrectos", e.getMessage() );
        }
        //Fin        
        
        /*
        *  Parte para la operacion en la Base de Datos
        */
        List<MensajeCalificacion> mensajes = null;
        EvaluacionEscolarDao evaluacionDao = (EvaluacionEscolarDao)FactoryDao.buildDao("web.EvaluacionEscolarDao");
        try{
            mensajes = evaluacionDao.buscarMensajesPorEvaluacion(evaluacionID);
        
        }catch(Exception e){
            System.out.println("No se pudo Listar los mensajes de la evaluacion\n"+e);
            return WebResponse.crearWebResponseError("No se pudo Listar los mensajes de la evaluacion", e.getMessage() );
        }
        //Fin        
        /*
        *  Repuesta Correcta
        */
        JSONArray miArray = new JSONArray();
        int i = 0;
        for(MensajeCalificacion m:mensajes ){
            JSONObject oResponse = new JSONObject();
            oResponse.put("mensajeID",m.getMenCalId() );
            oResponse.put("mensaje",m.getMensaje() );
            oResponse.put("puntos",m.getPuntos() );
            
            oResponse.put("i",i++);
            
            miArray.put(oResponse);
        }
        
        return WebResponse.crearWebResponseExito("Se Listaron los mensajes correctamente",miArray);        
        //Fin
    }
    
}

