/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.se;

import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.GenericDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.PublicacionDao;
import com.dremo.ucsm.gsc.sigesmed.core.datastore.HibernateUtil;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Publicacion;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author gscadmin
 */
public class PublicacionDaoHibernate extends GenericDaoHibernate<Publicacion> implements PublicacionDao{

    @Override
    public List<Publicacion> listarxFichaEscalafonaria(int ficEscId) {
        List<Publicacion> publicaciones = null;
        
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction t = session.beginTransaction();
        try{
            
            String hql = "SELECT p from Publicacion as p "
                    + "join fetch p.fichaEscalafonaria as fe "
                    + "WHERE fe.ficEscId=" + ficEscId + " AND p.estReg='A'";
            Query query = session.createQuery(hql);
            publicaciones = query.list();
            t.commit();
                    
        }catch(Exception e){
            t.rollback();
            System.out.println("No se pudo listar las exposiciones \\n "+ e.getMessage());
            throw new UnsupportedOperationException("No se pudo listar las exposiciones \\n "+ e.getMessage());            
        }
        finally{
            session.close();
        }
        
        return publicaciones;
    }

    @Override
    public Publicacion buscarPorId(Integer pubId) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Publicacion pub = (Publicacion)session.get(Publicacion.class, pubId);
        session.close();
        return pub;
    }
    
}
