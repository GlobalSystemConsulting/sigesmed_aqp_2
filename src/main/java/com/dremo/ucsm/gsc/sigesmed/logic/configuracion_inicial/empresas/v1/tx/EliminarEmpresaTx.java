/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.configuracion_inicial.empresas.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.EmpresaDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.cpe.LibroAsistenciaDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Empresa;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import org.json.JSONArray;
import org.json.JSONObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;

/**
 *
 * @author carlos
 */
public class EliminarEmpresaTx implements ITransaction {

    @Override
    public WebResponse execute(WebRequest wr) {

        Empresa empresa = null;
        try {
            JSONObject requestData = (JSONObject) wr.getData();
            int empresaId=requestData.getInt("empresaID");
            empresa=new Empresa(empresaId);
        } catch (Exception e) {
            return WebResponse.crearWebResponseError("No se pudo verificar los datos", e.getMessage());
        }

        EmpresaDao empresaDao = (EmpresaDao) FactoryDao.buildDao("EmpresaDao");

        try {
            empresaDao.delete(empresa);
        } catch (Exception e) {
            return WebResponse.crearWebResponseError("No se pudo Eliminar ", e.getMessage());
        }

      

        return WebResponse.crearWebResponseExito("Se Elimino correctamente");
        //Fin
    }

}
