/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.sad;

import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.GenericDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.datastore.HibernateUtil;
import com.dremo.ucsm.gsc.sigesmed.core.dao.sad.InventarioTransferenciaDAO;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sad.InventarioTransferencia;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sad.DetalleInventarioTransferencia;

import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author Jeferson
 */
public class InventarioTransferenciaDAOHibernate extends GenericDaoHibernate<InventarioTransferencia> implements InventarioTransferenciaDAO {

    @Override
    public List<InventarioTransferencia> buscarPorOrganizacion() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<InventarioTransferencia> buscarPorInventarioTransferencia() {
        List<InventarioTransferencia> inv_trans = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try {
            String hql = "SELECT  it FROM InventarioTransferencia it JOIN FETCH it.inv_tra_det JOIN FETCH it.serie";
            Query query = session.createQuery(hql);
            inv_trans = query.list();

        } catch (Exception e) {
            System.out.println("No se pudo Listar los Inventarios de Transferencia \\n " + e.getMessage());
            throw new UnsupportedOperationException("No se pudo Listar los Inventarios de Transferencia \\n " + e.getMessage());
        } finally {
            session.close();
        }

        return inv_trans;

    }

    @Override
    public void registrarDocumentos(List documentos) {

        // throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override

    public List<InventarioTransferencia> buscarPorInventarioTransferencia(int inv_tra_id) {

        List<InventarioTransferencia> inv_trans = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try {
            String hql = "SELECT  it FROM InventarioTransferencia it JOIN FETCH it.inv_tra_det WHERE it.inv_tra_id=:p1";
            Query query = session.createQuery(hql);
            query.setParameter("p1", inv_tra_id);
            inv_trans = query.list();
            if (inv_trans == null) { /*En caso que este vacio la lista*/

                return inv_trans;
            }
        } catch (Exception e) {
            System.out.println("No se pudo Listar los Inventarios de Transferencia \\n " + e.getMessage());
            throw new UnsupportedOperationException("No se pudo Listar los Inventarios de Transferencia \\n " + e.getMessage());
        } finally {
            session.close();
        }
        return inv_trans;

        //   throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<InventarioTransferencia> buscarPorSerieDoc(int ser_doc_id) {

        List<InventarioTransferencia> inv_trans = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try {
            String hql = "SELECT DISTINCT it FROM InventarioTransferencia WHERE it.serie.ser_doc_id=:p1";
            Query query = session.createQuery(hql);
            query.setParameter("p1", ser_doc_id);
            inv_trans = query.list();

        } catch (Exception e) {
            System.out.println("No se pudo Listar los Inventarios de Transferencia \\n " + e.getMessage());
            throw new UnsupportedOperationException("No se pudo Listar los Inventarios de Transferencia \\n " + e.getMessage());
        } finally {
            session.close();
        }

        return inv_trans;

        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void registrarDetalleIventarioTrans(DetalleInventarioTransferencia dit) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public InventarioTransferencia buscarUltimoInventario(String cod) {

        InventarioTransferencia it = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try {
            String hql = "SELECT DISTINCT it FROM InventarioTransferencia it WHERE it.codigo=:p1";
            Query query = session.createQuery(hql);
            query.setParameter("p1", cod);
            it = ((InventarioTransferencia) query.uniqueResult());

        } catch (Exception e) {
            System.out.println("No se pudo encontrar el Ultimo Inventario de Transferencia \\n " + e.getMessage());
            throw new UnsupportedOperationException("No se pudo encontrar el Ultimo Inventario de Transferencia \\n " + e.getMessage());
        }

        return it;
        // throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
