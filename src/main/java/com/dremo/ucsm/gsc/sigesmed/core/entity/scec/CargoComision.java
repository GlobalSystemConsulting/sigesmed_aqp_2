/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.entity.scec;

import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;
import java.util.Date;

/**
 *
 * @author ucsm
 */
@Entity
@Table(name="cargo_comision",schema="pedagogico")
@DynamicUpdate(value=true)
public class CargoComision implements java.io.Serializable{
    @Id
    @Column(name="car_com_id", unique=true, nullable=false)
    @SequenceGenerator(name = "secuencia_cargo_comision", sequenceName="pedagogico.cargo_comision_car_com_id_seq" )
    @GeneratedValue(generator="secuencia_cargo_comision")
    private int carComId;
    
    @Column(name="nom_car",length = 20)
    private String nomCar;
    
    @Column(name="des_car")
    private String desCar;

    @Column(name = "usu_mod")
    private Integer usuMod;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "fec_mod", length=29)
    private Date fecMod;

    @Column(name = "est_reg",length = 1)
    private Character estReg;

    public CargoComision() {
    }

    public CargoComision(String nomCar, String desCar) {
        this.nomCar = nomCar;
        this.desCar = desCar;
    }
    
    public CargoComision(String nomCar, String desCar, int usuMod, Date fecMod, char estReg) {
        this.nomCar = nomCar;
        this.desCar = desCar;
        this.usuMod = usuMod;
        this.fecMod = fecMod;
        this.estReg = estReg;
    }

    public int getCarComId() {
        return carComId;
    }

    public void setCarComId(int carComId) {
        this.carComId = carComId;
    }

    public String getNomCar() {
        return nomCar;
    }

    public void setNomCar(String nomCar) {
        this.nomCar = nomCar;
    }

    public String getDesCar() {
        return desCar;
    }

    public void setDesCar(String desCar) {
        this.desCar = desCar;
    }

    public int getUsuMod() {
        return usuMod;
    }

    public void setUsuMod(int usuMod) {
        this.usuMod = usuMod;
    }

    public Date getFecMod() {
        return fecMod;
    }

    public void setFecMod(Date fecMod) {
        this.fecMod = fecMod;
    }

    public char getEstReg() {
        return estReg;
    }

    public void setEstReg(char estReg) {
        this.estReg = estReg;
    }
}
