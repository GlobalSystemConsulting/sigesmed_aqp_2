package com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.capacitacion;

import com.dremo.ucsm.gsc.sigesmed.core.dao.capacitacion.TecnicaCursoCapacitacionDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.GenericDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.entity.capacitacion.TecnicaCursoCapacitacion;

public class TecnicaCursoCapacitacionDaoHibernate extends GenericDaoHibernate<TecnicaCursoCapacitacion> implements TecnicaCursoCapacitacionDao {
    
}
