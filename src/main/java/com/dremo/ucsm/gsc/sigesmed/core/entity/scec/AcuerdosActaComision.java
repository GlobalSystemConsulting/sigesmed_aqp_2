package com.dremo.ucsm.gsc.sigesmed.core.entity.scec;


import javax.persistence.*;
import java.util.Date;

/**
 * Created by admin on 15/09/16.
 */
@Entity
@Table(name = "acuerdos_acta", schema = "pedagogico")
public class AcuerdosActaComision implements java.io.Serializable {
    @Id
    @Column(name = "acu_act_id", nullable = false)
    @SequenceGenerator(name = "acuerdos_acta_sequence", sequenceName = "pedagogico.acuerdos_acta_acu_act_id_seq")
    @GeneratedValue (generator = "acuerdos_acta_sequence")
    private int acuActId;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "act_com_id")
    private ActasReunionComision actaReunion;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumns( {
            @JoinColumn(name = "per_id", referencedColumnName = "per_id"),
            @JoinColumn(name = "com_id", referencedColumnName = "com_id") })
    private IntegranteComision integranteComision;

    @Column(name = "des_acu")
    private String desAcu;

    @Temporal(TemporalType.DATE)
    @Column(name = "fec_cum_acu")
    private Date fecCumAcu;

    @Column(name = "est_cum_acu")
    private Boolean estCumAcu;

    @Column(name = "num_vot_fav", nullable = true)
    private Integer numVotFav;
    
    @Column(name = "num_vot_con", nullable = true)
    private Integer numVotCon;
    
    @Column(name = "obs", length = 256, nullable = true)
    private String obs;
    @Column(name = "usu_mod")
    private Integer usuMod;
     
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "fec_mod", length=29)
    private Date fecMod;

    @Column(name = "est_reg",length = 1)
    private Character estReg;

    public AcuerdosActaComision() {
    }

    
    public AcuerdosActaComision(int id,String desAcu, Date fecCumAcu, Boolean estCumAcu,ActasReunionComision acta) {
        this.acuActId = id;
        this.desAcu = desAcu;
        this.fecCumAcu = fecCumAcu;
        this.estCumAcu = estCumAcu;
        this.actaReunion = acta;

        this.fecMod = new Date();
        this.estReg = 'A';
    }

    public AcuerdosActaComision(int acuActId, String desAcu, Date fecCumAcu, Boolean estCumAcu, Integer numVotFav, Integer numVotCon, String obs,ActasReunionComision actaReunion) {
        this.acuActId = acuActId;
        this.actaReunion = actaReunion;
        this.desAcu = desAcu;
        this.fecCumAcu = fecCumAcu;
        this.estCumAcu = estCumAcu;
        this.numVotFav = numVotFav;
        this.numVotCon = numVotCon;
        this.obs = obs;
        
        this.fecMod = new Date();
        this.estReg = 'A';
    }

    
    public AcuerdosActaComision(String desAcu, Date fecCumAcu, Boolean estCumAcu, Integer numVotFav, Integer numVotCon, String obs) {
        this.desAcu = desAcu;
        this.fecCumAcu = fecCumAcu;
        this.estCumAcu = estCumAcu;
        this.numVotFav = numVotFav;
        this.numVotCon = numVotCon;
        this.obs = obs;
        
        this.fecMod = new Date();
        this.estReg = 'A';
    }
    
    public AcuerdosActaComision(String desAcu, Date fecCumAcu, Boolean estCumAcu) {
        this.desAcu = desAcu;
        this.fecCumAcu = fecCumAcu;
        this.estCumAcu = estCumAcu;

        this.fecMod = new Date();
        this.estReg = 'A';
    }

    public AcuerdosActaComision(String desAcu, Date fecCumAcu, Boolean estCumAcu, ActasReunionComision actCom, IntegranteComision resAcu) {
        this.desAcu = desAcu;
        this.fecCumAcu = fecCumAcu;
        this.estCumAcu = estCumAcu;
        this.actaReunion = actCom;
        this.integranteComision = resAcu;
    }

    public int getAcuActId() {
        return acuActId;
    }

    public void setAcuActId(int acuActId) {
        this.acuActId = acuActId;
    }

    public ActasReunionComision getActaReunion() {
        return actaReunion;
    }

    public void setActaReunion(ActasReunionComision actaReunion) {
        this.actaReunion = actaReunion;
    }

    public String getDesAcu() {
        return desAcu;
    }

    public void setDesAcu(String desAcu) {
        this.desAcu = desAcu;
    }

    public Date getFecCumAcu() {
        return fecCumAcu;
    }

    public void setFecCumAcu(Date fecCumAcu) {
        this.fecCumAcu = fecCumAcu;
    }

    public Boolean getEstCumAcu() {
        return estCumAcu;
    }

    public void setEstCumAcu(Boolean estCumAcu) {
        this.estCumAcu = estCumAcu;
    }

    public int getUsuMod() {
        return usuMod;
    }

    public void setUsuMod(int usuMod) {
        this.usuMod = usuMod;
    }

    public Date getFecMod() {
        return fecMod;
    }

    public void setFecMod(Date fecMod) {
        this.fecMod = fecMod;
    }

    public char getEstReg() {
        return estReg;
    }

    public void setEstReg(char estReg) {
        this.estReg = estReg;
    }

    public IntegranteComision getIntegranteComision() {
        return integranteComision;
    }

    public void setIntegranteComision(IntegranteComision integranteComision) {
        this.integranteComision = integranteComision;
    }

    public Integer getNumVotFav() {
        return numVotFav;
    }

    public void setNumVotFav(Integer numVotFav) {
        this.numVotFav = numVotFav;
    }

    public Integer getNumVotCon() {
        return numVotCon;
    }

    public void setNumVotCon(Integer numVotCon) {
        this.numVotCon = numVotCon;
    }

    public String getObs() {
        return obs;
    }

    public void setObs(String obs) {
        this.obs = obs;
    }
}
