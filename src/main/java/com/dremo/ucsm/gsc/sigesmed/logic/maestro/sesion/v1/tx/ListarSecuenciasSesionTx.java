package com.dremo.ucsm.gsc.sigesmed.logic.maestro.sesion.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.maestro.plan.SesionAprendizajeDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.plan_curricular.ActividadSeccionSequenciaDidactica;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.plan_curricular.SeccionSequenciaDidactica;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.plan_curricular.SeccionSequenciaDidacticaSesion;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.plan_curricular.SesionSequenciaDidactica;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.base.EntityUtil;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by Administrador on 06/12/2016.
 */
public class ListarSecuenciasSesionTx implements ITransaction{
    private static Logger logger = Logger.getLogger(ListarSecuenciasSesionTx.class.getName());
    @Override
    public WebResponse execute(WebRequest wr) {
        JSONObject data = (JSONObject) wr.getData();
        int idSesion = data.getInt("ses");
        return listarSecuenciasDidacticas(idSesion);
    }

    public WebResponse listarSecuenciasDidacticas(int idSesion) {
        try{
            SesionAprendizajeDao sesionDao = (SesionAprendizajeDao) FactoryDao.buildDao("maestro.plan.SesionAprendizajeDao");
            List<SeccionSequenciaDidactica> secciones = sesionDao.listarSeccionesSequenciaDidactica();
            List<SesionSequenciaDidactica> secuencias = sesionDao.listarSecuenciasDidacticas(idSesion);
            JSONArray jsonSecuencias = new JSONArray();
            logger.log(Level.INFO,"size secuencias {0}",secuencias.size());
            for(SesionSequenciaDidactica secuencia : secuencias){
                JSONObject jsonSecuencia = new JSONObject(EntityUtil.objectToJSONString(
                        new String[]{"sesSeqDidId","nom","durTot"},new String[]{"id","nom","dur"},secuencia
                ));
                JSONArray parts = new JSONArray();
                for(SeccionSequenciaDidactica seccion : secciones){
                    JSONObject jsonPart = new JSONObject();
                    SeccionSequenciaDidacticaSesion scds = sesionDao.getSeccioneSequencia(secuencia.getSesSeqDidId(), seccion.getSecSeqDid());
                    jsonPart.put("nom",seccion.getNomSec());
                    if(scds != null){
                        jsonPart.put("dur",scds.getDur());
                        if(scds.getActividades().size() > 0){
                            JSONArray jsonActividades = new JSONArray(EntityUtil.listToJSONString(
                                    new String[]{"actSeqDidId","nom","des"},new String[]{"id","nom","des"},scds.getActividades()
                            ));
                            jsonPart.put("activs",jsonActividades);
                        }else {
                            jsonPart.put("activs",new JSONArray());
                        }
                    }else{
                        jsonPart.put("dur",0);
                        jsonPart.put("activs",new JSONArray());
                    }

                    parts.put(jsonPart);
                }
                jsonSecuencia.put("cont",parts);
                jsonSecuencias.put(jsonSecuencia);
            }
            return WebResponse.crearWebResponseExito("Exito al listar las secuencias",jsonSecuencias);
        }catch (Exception e){
            logger.log(Level.SEVERE,"listarSecuencias",e);
            return WebResponse.crearWebResponseError("Error al listar las secuencias");
        }

    }
}
