package com.dremo.ucsm.gsc.sigesmed.core.dao.maestro.carpeta;

import com.dremo.ucsm.gsc.sigesmed.core.dao.GenericDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.carpeta.RutaContenidoCarpeta;

/**
 * Created by Administrador on 30/12/2016.
 */
public interface RutaContenidoCarpetaDao extends GenericDao<RutaContenidoCarpeta> {
    RutaContenidoCarpeta buscarRutaPorId(int id);
}
