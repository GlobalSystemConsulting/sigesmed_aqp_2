package com.dremo.ucsm.gsc.sigesmed.logic.ma.notas.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.maestro.plan.UnidadDidacticaDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.plan_curricular.IndicadorAprendizaje;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.base.EntityUtil;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by Administrador on 13/01/2017.
 */
public class ListarIndicadoresUnidadTx implements ITransaction{
    private static Logger logger = Logger.getLogger(ListarIndicadoresUnidadTx.class.getName());
    @Override
    public WebResponse execute(WebRequest wr) {
        JSONObject data = (JSONObject) wr.getData();
        int idUnidad = data.getInt("uni");
        int idComp = data.getInt("comp");
        int idCap = data.getInt("cap");
        return listarIndicadores(idUnidad,idComp,idCap);
    }

    private WebResponse listarIndicadores(int idUnidad, int idComp, int idCap) {
        try{
            UnidadDidacticaDao unidadDao = (UnidadDidacticaDao) FactoryDao.buildDao("maestro.plan.UnidadDidacticaDao");
            List<IndicadorAprendizaje> indicadores = unidadDao.listarIndicadoresUnidadPorCapacidad(idUnidad,idComp,idCap);
            JSONArray indicadoresJSON = new JSONArray(EntityUtil.listToJSONString(
                    new String[]{"indAprId","nomInd"},
                    new String[]{"id","nom"},
                    indicadores
            ));
            return WebResponse.crearWebResponseExito("SE LISTO  CON EXITO",indicadoresJSON);
        }catch (Exception e){
            logger.log(Level.SEVERE,"listarIndicadores",e);
            return WebResponse.crearWebResponseError("No se pudo listar los datos");
        }
    }
}
