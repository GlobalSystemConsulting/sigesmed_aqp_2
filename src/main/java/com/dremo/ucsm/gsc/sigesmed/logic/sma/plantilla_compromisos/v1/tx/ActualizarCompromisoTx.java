/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.sma.plantilla_compromisos.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.sma.CompromisosGestionDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sma.CompromisosGestion;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sma.PlantillaFicha;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import org.json.JSONObject;

/**
 *
 * @author Administrador
 */
public class ActualizarCompromisoTx implements ITransaction{
    @Override
    public WebResponse execute(WebRequest wr) {
                
        /*
        *   Parte para la lectura, verificacion y validacion de datos
        */
        CompromisosGestion grupoAct = null;
        try{
            
            JSONObject requestData = (JSONObject)wr.getData();
            int gruId = requestData.getInt("gruId");
            String gruDes = requestData.optString("gruNom");
            int plaId = requestData.getInt("plaId");
            
            grupoAct = new CompromisosGestion(gruId, gruDes, new PlantillaFicha(plaId));
            
        }catch(Exception e){
            System.out.println(e);
            return WebResponse.crearWebResponseError("No se pudo actualizar, datos incorrectos", e.getMessage() );
        }
        //Fin
        
        /*
        *  Parte para la operacion en la Base de Datos
        */
        CompromisosGestionDao dexDao = (CompromisosGestionDao)FactoryDao.buildDao("sma.CompromisosGestionDao");
        try{
            dexDao.update(grupoAct);
        
        }catch(Exception e){
            System.out.println("No se pudo actualizar el compromiso\n"+e);
            return WebResponse.crearWebResponseError("No se pudo actualizar el compromiso", e.getMessage() );
        }
        //Fin
        
        /*
        *  Repuesta Correcta
        */        
        return WebResponse.crearWebResponseExito("El compromiso se actualizo correctamente");
        //Fin
    }
    
}
