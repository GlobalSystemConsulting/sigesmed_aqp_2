package com.dremo.ucsm.gsc.sigesmed.util;

import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.mmi.GenericMMIDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.mmi.MatriculaInsDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Organizacion;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mmi.EstudianteMMI;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mmi.MatriculaMMI;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mmi.OrganizacionMMI;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mmi.PersonaMMI;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mmi.TrasladoIngreso;
import com.dremo.ucsm.gsc.sigesmed.core.service.ServicioREST;
import com.itextpdf.io.font.FontConstants;
import com.itextpdf.kernel.color.Color;
import com.itextpdf.layout.Style;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

public class MatriculaReporter {

    private final String PATH_CONSTANCIA_MATRICULA = ServicioREST.PATH_SIGESMED + "\\archivos\\matricula_institucional\\constanciaMatricula\\";
    private final String PATH_CONSTANCIA_VACANTE = ServicioREST.PATH_SIGESMED + "\\archivos\\matricula_institucional\\constanciaVacante\\";
    private final String PATH_FICHA_UNICA_MATRICULA = ServicioREST.PATH_SIGESMED + "\\archivos\\matricula_institucional\\fichaMatricula\\";
    private final String PATH_NOMINA_MATRICULA = ServicioREST.PATH_SIGESMED + "\\archivos\\matricula_institucional\\nominaMatricula\\";

    private final String URL_FONT_ARIAL = ServicioREST.PATH_SIGESMED + "\\archivos\\matricula_institucional\\templates\\fonts\\arial.ttf";
    private final String URL_MINISTERIO_EDU_LOGO = ServicioREST.PATH_SIGESMED + "\\archivos\\matricula_institucional\\templates\\imgs\\min_edu_cab.png";
    private final String URL_TIGER_LOGO = ServicioREST.PATH_SIGESMED + "\\archivos\\matricula_institucional\\templates\\imgs\\tiger_extend.png";

    private DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
    private ITextMatricula myITextMat;
    private Style fontArialNormal12;
    private Style fontArialBold12;
    private Style fontArialBold14;
    private Style fontArialBold16;
    private Style fontTimesRomanBold22;
    private Style fontTimesRomanBold8;
    private Style fontTimesRomanNormal8;
    private Style fontTimesRomanNormal6;

    public MatriculaReporter() {
        fontArialNormal12 = ITextUtil.getFont(URL_FONT_ARIAL, 12, Color.BLACK, false, false, false);
        fontArialBold12 = ITextUtil.getFont(URL_FONT_ARIAL, 12, Color.BLACK, true, false, false);
        fontArialBold14 = ITextUtil.getFont(URL_FONT_ARIAL, 14, Color.BLACK, true, false, false);
        fontArialBold16 = ITextUtil.getFont(URL_FONT_ARIAL, 16, Color.BLACK, true, false, false);

        fontTimesRomanBold22 = ITextUtil.getFontConstants(FontConstants.TIMES_ROMAN, 22, true, false, false);
        fontTimesRomanBold8 = ITextUtil.getFontConstants(FontConstants.TIMES_ROMAN, 8, true, false, false);
        fontTimesRomanNormal8 = ITextUtil.getFontConstants(FontConstants.TIMES_ROMAN, 8, false, false, false);
        fontTimesRomanNormal6 = ITextUtil.getFontConstants(FontConstants.TIMES_ROMAN, 6, false, false, false);

        dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
    }

    /**
     * Los datos adicionales a la matricula estan mapeados por codigo, debido a
     * eso no se puede accesar adecuadamente a la data y se tiene que
     * inicializar como parametros
     *
     * @param matricula Matricula para el Reporte
     * @param nivel Nivel Educativo (Inicial - Primaria - Secundaria)
     * @param jornadaAbr Jornada Academica Abreviatura
     * @param jornada Jornada Academica Completa
     * @param turno Turno
     * @param grado Grado
     */
    public void makeConstanciaMatricula(MatriculaMMI matricula, String nivel,
            String jornadaAbr, String jornada, String turno, String grado) {
        EstudianteMMI estudiante = matricula.getEstudiante();
        PersonaMMI personaEstudiante = estudiante.getPersona();
        PersonaMMI apoderado = matricula.getPersonaByApoId();
        OrganizacionMMI colegio = matricula.getOrganizacionByOrgDesId();

        String nameDocument = "constanciaMatricula_" + personaEstudiante.getDni() + ".pdf";
        String dateHeader = "Fecha: " + dateFormat.format(new Date()) + "\nPagina 1 de 1";

        String estId, estNom, orgId, orgNom, apoderadoDNI, apoderadoNom;

        char seccion;
        try {
            estId = estudiante.getCodEst();
            estNom = personaEstudiante.getNom() + " "
                    + personaEstudiante.getApePat() + " "
                    + personaEstudiante.getApeMat();
            orgId = colegio.getCod();
            orgNom = colegio.getNom();
            seccion = matricula.getSecId();
            apoderadoDNI = apoderado.getDni();
            apoderadoNom = apoderado.getNom() + " "
                    + apoderado.getApePat() + " "
                    + apoderado.getApeMat();

        } catch (Exception e) {
            estId = "";
            estNom = "";
            orgId = "";
            orgNom = "";
            seccion = ' ';
            apoderadoDNI = "";
            apoderadoNom = "";
        }

        myITextMat = new ITextMatricula(PATH_CONSTANCIA_MATRICULA, nameDocument);

        //myITextMat.addDocumentHeader(URL_MINISTERIO_EDU_LOGO, URL_TIGER_LOGO);
        myITextMat.addDocumentHeader(URL_MINISTERIO_EDU_LOGO, dateHeader, ITextUtil.getFont(URL_FONT_ARIAL, 12, Color.BLACK, true, false, false));

        myITextMat.addLineJump();
        myITextMat.addLineJump();
        myITextMat.addLineJump();
        myITextMat.addParagraph("Constancia de Matricula", ITextUtil.getFont(URL_FONT_ARIAL, 22, Color.BLACK, true, false, false), ITextUtil.ALIGN_CENTER);
        myITextMat.addLineJump();

        myITextMat.insertTableConstanciaMatricula(
                ITextUtil.getFont(URL_FONT_ARIAL, 11, Color.BLACK, true, false, false),
                ITextUtil.getFont(URL_FONT_ARIAL, 11, Color.BLACK, false, false, false),
                estId, estNom,
                orgId, orgNom, nivel, jornadaAbr, jornada,
                turno, grado, seccion, apoderadoDNI, apoderadoNom);

        myITextMat.closeDocument();
    }

    /**
     * Los datos adicionales al traslado estan mapeados por codigo, debido a eso
     * no se puede accesar adecuadamente a la data y se tiene que inicializar
     * como parametros
     *
     * @param traslado Traslado de Ingreso
     * @param orgDesId Id organizacion Destino
     * @param orgDesNom Nombre organizacion Destino
     * @param nivel Nivel Educativo
     * @param jornadaAbr Jornada Abreviatura
     * @param jornada Jornada Nombre
     * @param turno Turno
     * @param grado Grado
     */
    public void makeConstanciaVacante(TrasladoIngreso traslado,
            String orgDesId, String orgDesNom, String nivel,
            String jornadaAbr, String jornada, String turno, String grado) {

        EstudianteMMI estudiante = traslado.getEstudiante();
        PersonaMMI personaEstudiante = estudiante.getPersona();

        String nameDocument = "constanciaVacante_" + personaEstudiante.getDni() + ".pdf";
        String dateHeader = "Fecha: " + dateFormat.format(new Date()) + "\nPagina 1 de 1";

        String estId, estNom;
        char seccion;
        try {
            estId = estudiante.getCodEst();
            estNom = personaEstudiante.getNom() + " "
                    + personaEstudiante.getApePat() + " "
                    + personaEstudiante.getApeMat();

            seccion = traslado.getSecId();

        } catch (Exception e) {
            estId = "";
            estNom = "";
            seccion = ' ';
        }

        myITextMat = new ITextMatricula(PATH_CONSTANCIA_VACANTE, nameDocument);

        //myITextMat.addDocumentHeader(URL_MINISTERIO_EDU_LOGO, URL_TIGER_LOGO);
        myITextMat.addDocumentHeader(URL_MINISTERIO_EDU_LOGO, dateHeader, ITextUtil.getFont(URL_FONT_ARIAL, 12, Color.BLACK, true, false, false));

        myITextMat.addLineJump();
        myITextMat.addLineJump();
        myITextMat.addLineJump();
        myITextMat.addParagraph("Constancia de Vacante", ITextUtil.getFont(URL_FONT_ARIAL, 22, Color.BLACK, true, false, false), ITextUtil.ALIGN_CENTER);
        myITextMat.addLineJump();

        myITextMat.insertTableConstanciaVacante(
                ITextUtil.getFont(URL_FONT_ARIAL, 11, Color.BLACK, true, false, false),
                ITextUtil.getFont(URL_FONT_ARIAL, 11, Color.BLACK, false, false, false),
                estId, estNom, orgDesId, orgDesNom, nivel, jornadaAbr,
                jornada, turno, grado, seccion);

        myITextMat.closeDocument();
    }

    /**
     * Es imperante que la organizacion tenga padre
     *
     * @param myOrganizacion
     * @param nombreSeccion
     * @param periodoLecIni
     * @param periodoLecFin
     * @param dpto
     * @param prov
     * @param dist
     * @param jornadaEscNom
     * @param orgId
     * @param nivelId
     * @param jornadaEscId
     * @param turnoId
     * @param gradoId
     * @param secId
     */
    public void makeNominaMatricula(Organizacion myOrganizacion, Organizacion myPadre, String nombreSeccion, String periodoLecIni, String periodoLecFin,
            String dpto, String prov, String dist, String jornadaEscNom, int orgId, int nivelId, int jornadaEscId, char turnoId, int gradoId, char secId, List<Object[]> alumnos) {
        String nameDocument = "nominaMatricula_" + orgId + nivelId + jornadaEscId + turnoId + gradoId + secId + ".pdf";

        myITextMat = new ITextMatricula(PATH_NOMINA_MATRICULA, nameDocument, true, 0.5, 0.5, 0.5, 0.5);
        myITextMat.addParagraph("Nomina de Matricula", fontTimesRomanBold22, ITextUtil.ALIGN_CENTER);
        myITextMat.addLineJump();

        String padreCod = myPadre.getCod();
        String padreName = myPadre.getNom();

        myITextMat.insertTableNominaMatricula(fontTimesRomanBold8, fontTimesRomanNormal8,
                myOrganizacion.getNom(), myOrganizacion.getOrgGes(), myOrganizacion.getCod(),
                myOrganizacion.getOrgCar(), myOrganizacion.getOrgPro(), "RD N° 0006",
                myOrganizacion.getOrgFor(), getNivel(nivelId), getGrado(gradoId), myOrganizacion.getOrgVar(),
                "" + secId, nombreSeccion, "" + turnoId, "EBR", jornadaEscNom,
                periodoLecIni, periodoLecFin, dpto, prov, dist, myOrganizacion.getDir(),
                padreCod, padreName, alumnos);

        char sex;
        int numHombres = 0;
        int numMujeres = 0;
        for (Object[] alumno : alumnos) {
            sex = alumno[6].toString().charAt(0);
            if (sex == 'H') {
                numHombres = numHombres + 1;
            }
            if (sex == 'M') {
                numMujeres = numMujeres + 1;
            }
        }

        myITextMat.addParagraph("", fontTimesRomanNormal8);

        myITextMat.addParagraph("", fontTimesRomanNormal8);

        myITextMat.insertTableIndiceNomina(fontTimesRomanNormal6);

        myITextMat.addParagraph("", fontTimesRomanNormal8);

        myITextMat.addParagraph("", fontTimesRomanNormal8);

        myITextMat.insertPiePaginaNomina(numMujeres, numHombres, fontTimesRomanBold8, fontTimesRomanNormal8);

        myITextMat.closeDocument();

    }

    private String getNivel(int nivel) {
        switch (nivel) {
            case 1: {
                return "INI";
            }
            case 2: {
                return "PRI";
            }
            case 3: {
                return "SEC";
            }
            default: {
                return "-";
            }
        }
    }

    private String getGrado(int grado) {
        switch (grado) {
            case 3: {
                return "1";
            }
            case 4: {
                return "2";
            }
            case 5: {
                return "3";
            }
            case 6: {
                return "4";
            }
            case 7: {
                return "5";
            }
            case 8: {
                return "6";
            }
            case 9: {
                return "1";
            }
            case 10: {
                return "2";
            }
            case 11: {
                return "3";
            }
            case 12: {
                return "4";
            }
            case 13: {
                return "5";
            }
            default: {
                return "-";
            }
        }
    }

}
