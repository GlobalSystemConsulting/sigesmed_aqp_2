package com.dremo.ucsm.gsc.sigesmed.logic.capacitacion.curso_capacitacion.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.capacitacion.ComentarioTemaCapacitacionDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.capacitacion.AdjuntoComentarioTemaDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.dao.web.MensajeElectronicoDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.capacitacion.AdjuntoComentarioTema;
import com.dremo.ucsm.gsc.sigesmed.core.entity.capacitacion.ComentarioTemaCapacitacion;
import com.dremo.ucsm.gsc.sigesmed.core.entity.web.MensajeElectronico;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.base.FileJsonObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import com.dremo.ucsm.gsc.sigesmed.util.BuildFile;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

public class RegistrarComentarioTemaTx implements ITransaction {
    
    private static final Logger logger = Logger.getLogger(RegistrarComentarioTemaTx.class.getName());

    @Override
    public WebResponse execute(WebRequest wr) {
        try {
            JSONObject data = (JSONObject) wr.getData();    
            int sede = data.getInt("sed");
            int docente = data.getInt("doc");
            int tema = data.getInt("tem");
            
            ComentarioTemaCapacitacionDao comentarioTemaCapacitacionDao = (ComentarioTemaCapacitacionDao) FactoryDao.buildDao("capacitacion.ComentarioTemaCapacitacionDao");
            ComentarioTemaCapacitacion comentario = new ComentarioTemaCapacitacion(tema,
                                                                                    sede,
                                                                                    docente,
                                                                                    data.getJSONObject("com").getString("com"),
                                                                                    docente,
                                                                                    new Date(),
                                                                                    'P');
            
            if(data.getInt("est") == 1) {
                comentario.setEstReg('H');
                ComentarioTemaCapacitacion padre = new ComentarioTemaCapacitacion();
                padre.setComTemCapId(data.getInt("res"));
                comentario.setComentarioRes(padre);
            }
            
            comentarioTemaCapacitacionDao.insert(comentario);
            
            JSONArray adjArray = data.getJSONObject("com").getJSONArray("adjuntos");

            if (adjArray.length() > 0) {
                AdjuntoComentarioTemaDaoHibernate adjuntoComentarioTemaDao = (AdjuntoComentarioTemaDaoHibernate) FactoryDao.buildDao("capacitacion.AdjuntoComentarioTemaDao");

                for (int i = 0; i < adjArray.length(); i++) {
                    AdjuntoComentarioTema adjunto = new AdjuntoComentarioTema("Nombre_archivo", comentario, docente);
                    adjuntoComentarioTemaDao.insert(adjunto);
                    
                    FileJsonObject file = new FileJsonObject(adjArray.getJSONObject(i).optJSONObject("arc"),
                            "tem_" + data.getInt("tem") + "_com_" + comentario.getComTemCapId() + "_adj_" + adjunto.getAdjComTemId() + "_" + adjArray.getJSONObject(i).getString("nom"));
                    
                    BuildFile.buildFromBase64(HelpTraining.attachments_Comment, file.getName(), file.getData());
                    adjunto.setNom(file.getName());
                    adjuntoComentarioTemaDao.update(adjunto);
                }
            }
            
            
            if(data.getInt("est") == 1) {
                JSONObject object = comentarioTemaCapacitacionDao.obtenerSesion(data.getInt("rep"));
                if(object.getInt("id") != data.getInt("ses")) {
                    MensajeElectronicoDao mensajeDao = (MensajeElectronicoDao)FactoryDao.buildDao("web.MensajeElectronicoDao");
                    String msm = object.getString("nom") + " ha respondido a un comentario que realizaste en la capacitación " + data.getString("cap");
                    MensajeElectronico mensaje = new MensajeElectronico(0, "RESPUESTA COMENTARIO", msm, "", "", new Date(), data.getInt("ses"));
                    mensajeDao.enviarMensaje(mensaje, object.getInt("id"));
                }
            }
            
            return WebResponse.crearWebResponseExito("El comentario del tema fue creado correctamente", WebResponse.OK_RESPONSE);
        } catch (Exception e){
            logger.log(Level.SEVERE,"registrarComentario",e);
            return WebResponse.crearWebResponseError("No se pudo registrar el comentario del tema",WebResponse.BAD_RESPONSE);
        }
    }
}
