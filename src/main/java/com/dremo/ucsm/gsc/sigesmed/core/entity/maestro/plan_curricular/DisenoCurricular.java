package com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.plan_curricular;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Administrador on 19/10/2016.
 */
@Entity(name = "plan_curricular.DisenoCurricular")
@Table(name = "diseno_curricular")
public class DisenoCurricular implements java.io.Serializable{
    @Id
    @Column(name = "dis_cur_id", nullable = false, unique = true)
    @SequenceGenerator(name = "diseno_curricular_dis_cur_id_sequence", sequenceName = "diseño_curricular_dis_cur_id_seq")
    @GeneratedValue(generator = "diseno_curricular_dis_cur_id_sequence")
    private int disCurId;

    @Column(name = "nom", length = 64)
    private String nom;
    @Column(name = "des", length = 256)
    private String des;
    @Column(name = "res_dir", length = 16)
    private String resDir;
    @Column(name = "tip", length = 1)
    private Character tip;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "fec_cre")
    private Date fecCre;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "fec_mod")
    private Date fecMod;
    @Column(name = "usu_mod")
    private Integer usuMod;
    @Column(name = "est_reg",length = 1)
    private Character estReg;

    @OneToMany(mappedBy = "disenoCurr",fetch = FetchType.LAZY,cascade = CascadeType.PERSIST)
    private List<AreaCurricular> areasCurriculares = new ArrayList<>();

    public DisenoCurricular() {
    }

    public DisenoCurricular(String nom, String des) {
        this.nom = nom;
        this.des = des;

        this.fecCre = new Date();
        this.fecMod = new Date();
        this.estReg = 'A';
    }

    public DisenoCurricular(String nom, String des, String resDir, Character tip) {
        this.nom = nom;
        this.des = des;
        this.resDir = resDir;
        this.tip = tip;

        this.fecCre = new Date();
        this.fecMod = new Date();
        this.estReg = 'A';
    }

    public int getDisCurId() {
        return disCurId;
    }

    public void setDisCurId(int disCurId) {
        this.disCurId = disCurId;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getDes() {
        return des;
    }

    public void setDes(String des) {
        this.des = des;
    }

    public String getResDir() {
        return resDir;
    }

    public void setResDir(String resDir) {
        this.resDir = resDir;
    }

    public Character getTip() {
        return tip;
    }

    public void setTip(Character tip) {
        this.tip = tip;
    }

    public Date getFecCre() {
        return fecCre;
    }

    public void setFecCre(Date fecCre) {
        this.fecCre = fecCre;
    }

    public Date getFecMod() {
        return fecMod;
    }

    public void setFecMod(Date fecMod) {
        this.fecMod = fecMod;
    }

    public Integer getUsuMod() {
        return usuMod;
    }

    public void setUsuMod(Integer usuMod) {
        this.usuMod = usuMod;
    }

    public Character getEstReg() {
        return estReg;
    }

    public void setEstReg(Character estReg) {
        this.estReg = estReg;
    }

    public List<AreaCurricular> getAreasCurriculares() {
        return areasCurriculares;
    }

    public void setAreasCurriculares(List<AreaCurricular> areasCurriculares) {
        this.areasCurriculares = areasCurriculares;
    }
}
