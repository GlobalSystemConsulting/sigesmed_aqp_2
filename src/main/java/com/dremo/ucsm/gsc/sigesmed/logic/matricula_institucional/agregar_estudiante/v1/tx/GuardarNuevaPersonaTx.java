package com.dremo.ucsm.gsc.sigesmed.logic.matricula_institucional.agregar_estudiante.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.mmi.GenericMMIDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mmi.DatosNacimiento;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mmi.EstudianteMMI_datosGenerales;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mmi.GradoInstruccion;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mmi.Lengua;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mmi.Pais;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mmi.PersonaMMI;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mmi.PersonaMMI_datosGenerales;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import com.dremo.ucsm.gsc.sigesmed.util.DateUtil;
import java.util.Date;
import org.json.JSONObject;

import java.util.logging.Level;
import java.util.logging.Logger;

public class GuardarNuevaPersonaTx implements ITransaction {

    private static Logger logger = Logger.getLogger(GuardarNuevaPersonaTx.class.getName());

    @Override
    public WebResponse execute(WebRequest wr) {
        JSONObject datosNuevaPersona = (JSONObject) wr.getData();

        String perDni, perNom, perApePat, perApeMat, perEstCiv, perOcu, perDir, perTel;
        long perId;
        int perPai, perGraIns;
        Character perSex;
        Date perFecNac;

        try {
            perDni = datosNuevaPersona.getString("perDni");
            perNom = datosNuevaPersona.getString("perNom");
            perApePat = datosNuevaPersona.getString("perApePat");
            perApeMat = datosNuevaPersona.getString("perApeMat");
            perEstCiv = datosNuevaPersona.getString("perEstCiv");
            perOcu = datosNuevaPersona.getString("perOcu");
            perDir = datosNuevaPersona.getString("perDir");
            perTel = datosNuevaPersona.getString("perTel");

            perGraIns = datosNuevaPersona.getInt("perGraIns");
            perSex = datosNuevaPersona.getString("perSex").charAt(0);
            perFecNac = DateUtil.getDate2String(datosNuevaPersona.getString("perFecNac"));

        } catch (Exception e) {
            logger.log(Level.SEVERE, "Error al cargar datos persona", e);
            return WebResponse.crearWebResponseError("Nose pudo insertar persona");
        }

        GenericMMIDaoHibernate<PersonaMMI> dhpersona = new GenericMMIDaoHibernate<>();

        try {
            long k = (Long) dhpersona.llave(PersonaMMI.class, "perId");
            perId = k + 1;

            PersonaMMI nuevaPersona = new PersonaMMI();
            nuevaPersona.setPerId(perId);
            nuevaPersona.setNom(perNom);
            nuevaPersona.setApePat(perApePat);
            nuevaPersona.setApeMat(perApeMat);
            nuevaPersona.setDni(perDni);
            nuevaPersona.setEstCiv(perEstCiv);
            nuevaPersona.setOcu(perOcu);
            nuevaPersona.setPerDir(perDir);
            nuevaPersona.setNum1(perTel);

            nuevaPersona.setGradoInstruccion(new GradoInstruccion(perGraIns));
            nuevaPersona.setSex(perSex);
            nuevaPersona.setFecNac(perFecNac);
            nuevaPersona.setEstReg('A');

            dhpersona.insert(nuevaPersona);

            return WebResponse.crearWebResponseExito("Se guardo a la persona exitosamente", datosNuevaPersona);
        } catch (Exception e) {
            logger.log(Level.SEVERE, "Error Insertar persona", e);
            return WebResponse.crearWebResponseError("Nose pudo insertar persona");
        }
    }
}
