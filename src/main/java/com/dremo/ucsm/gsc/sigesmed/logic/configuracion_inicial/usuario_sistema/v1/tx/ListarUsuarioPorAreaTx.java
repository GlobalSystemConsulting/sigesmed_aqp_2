/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.configuracion_inicial.usuario_sistema.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.UsuarioDao;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import org.json.JSONArray;
import org.json.JSONObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import com.dremo.ucsm.gsc.sigesmed.core.entity.UsuarioSession;
import java.util.List;

/**
 *
 * @author abel
 */
public class ListarUsuarioPorAreaTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
        int areaID = 0;
        try{
            JSONObject requestData = (JSONObject)wr.getData();
            areaID = requestData.getInt("areaID");
        
        }catch(Exception e){
            System.out.println(e);
            return WebResponse.crearWebResponseError("No se pudo listar usuarios por area, datos incorrectos", e.getMessage() );
        }
        /*
        *  Parte para la operacion en la Base de Datos
        */
        List<UsuarioSession> usuarios = null;
        UsuarioDao usuarioDao = (UsuarioDao)FactoryDao.buildDao("UsuarioDao");
        try{
            usuarios = usuarioDao.buscarConRolPorArea(areaID);
        
        }catch(Exception e){
            System.out.println("No se pudo Listar los usuarios del Sistema por Area\n"+e);
            return WebResponse.crearWebResponseError("No se pudo Listar los usuarios del Sistema por Area", e.getMessage() );
        }
        //Fin
        
        /*
        *  Repuesta Correcta
        */
        JSONArray miArray = new JSONArray();
        for(UsuarioSession session:usuarios ){
            JSONObject oResponse = new JSONObject();
            oResponse.put("usuarioID",session.getPersona().getPerId());
            oResponse.put("DNI",session.getPersona().getDni());
            oResponse.put("nombre",session.getPersona().getNom());
            oResponse.put("rolID",session.getRol().getRolId() );
            oResponse.put("rol",session.getRol().getNom() );
            oResponse.put("sessionID",session.getUsuSesId() );
            
            oResponse.put("estado",""+session.getEstReg());
            miArray.put(oResponse);
        }
        
        return WebResponse.crearWebResponseExito("Se Listo correctamente",miArray);        
        //Fin
    }
    
}



