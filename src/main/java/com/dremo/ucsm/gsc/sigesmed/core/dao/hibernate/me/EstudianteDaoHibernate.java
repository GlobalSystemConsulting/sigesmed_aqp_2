/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.me;

import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.GenericDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.dao.me.EstudianteDao;
import com.dremo.ucsm.gsc.sigesmed.core.datastore.HibernateUtil;
import com.dremo.ucsm.gsc.sigesmed.core.entity.me.Estudiante;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;

/**
 *
 * @author abel
 */
public class EstudianteDaoHibernate extends GenericDaoHibernate<Estudiante> implements EstudianteDao{
    
    @Override
    public List<Object[]> buscarEstudiantesIdsPorGradoYSeccion(int organizacionID,int gradoID,char seccionID){
        List<Object[]> lista = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            //listar jornadas escolares
            String hql = "SELECT m.est_id,us.usu_ses_id FROM pedagogico.matricula m inner join pedagogico.grado_ie_estudiante gie on m.mat_id=gie.mat_id "
                        + "inner join usuario_session us on m.est_id=us.usu_id "
                        + "WHERE m.org_des_id=:p1 and m.act=true and gie.gra_id=:p2 and gie.sec_id=:p3 and us.rol_id=9"; //por defecto verificamos que sea rol estudiante
            Query query = session.createSQLQuery(hql);
            query.setParameter("p1", organizacionID);
            query.setParameter("p2", gradoID);
            query.setParameter("p3", seccionID);
            
            lista = query.list();
        
        }catch(Exception e){
            System.out.println("No se pudo Listar los ids del estudiantes por grado y seccion\n "+ e.getMessage());
            throw new UnsupportedOperationException("No se pudo Listar los ids del estudiante por grado y seccion\\n "+ e.getMessage());            
        }
        finally{
            session.close();
        }
        return lista;
    }
}
