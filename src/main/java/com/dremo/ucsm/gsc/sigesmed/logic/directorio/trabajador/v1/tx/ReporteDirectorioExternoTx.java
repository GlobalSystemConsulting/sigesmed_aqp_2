/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.directorio.trabajador.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.di.DirectorioExternoDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.di.DirectorioExternoDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.di.ParientesDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.util.GTabla;
import com.dremo.ucsm.gsc.sigesmed.util.MChart;
import com.dremo.ucsm.gsc.sigesmed.util.Mitext;
import com.dremo.ucsm.gsc.sigesmed.core.entity.di.Parientes;
import com.dremo.ucsm.gsc.sigesmed.core.entity.di.DirectorioExterno;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import com.sun.org.apache.xalan.internal.xsltc.compiler.util.Type;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.codec.binary.Base64;
import org.jfree.data.general.DefaultPieDataset;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author Administrador
 */
public class ReporteDirectorioExternoTx implements ITransaction{
    
    @Override
    public WebResponse execute(WebRequest wr) {        
        /*
        *  Parte para la operacion en la Base de Datos
        */
        
        JSONObject requestData = (JSONObject)wr.getData();
        
        JSONObject objeto = requestData.getJSONObject("objeto");
        JSONArray campos = requestData.getJSONArray("campos");
        JSONArray titulos = requestData.getJSONArray("titulos");
        JSONArray pesos = requestData.getJSONArray("pesos");
               
        
        String _campos = campos.toString().replace("[", "").replace("]", "").replace("\"", "");
        String[] _keys = _campos.split(",");
//        for(int i=0; i < _keys.length; ++i) _keys[i] =  _keys[i].substring(2);
                
        String[] _titulos = titulos.toString().replace("[", "").replace("]", "").replace("\"", "").split(",");
        String[] _pesos = pesos.toString().replace("[", "").replace("]", "").replace("\"", "").split(",");
        float [] _p = new float[_pesos.length];
        for(int i = 0; i< _pesos.length; ++i){
             _p[i] = Float.parseFloat(_pesos[i]);
        }
        
        List dirExt = null;
//        DirectorioExternoDao dirExtDao = (DirectorioExternoDao)FactoryDao.buildDao("di.DirectorioExternoDao");
        DirectorioExternoDaoHibernate dirExtDao = new DirectorioExternoDaoHibernate();
        
        try{
            dirExt = dirExtDao.ListarDirectorioExterno(_campos);
        
        }catch(Exception e){
            System.out.println("No se pudo Listar el directorio externo \n"+e);
            return WebResponse.crearWebResponseError("No se pudo Listar el directorio externo ", e.getMessage() );
        }
        
        //Creando el reporte....        
        Mitext m = null;        
        try {
            m = new Mitext();
            m.newLine(2);
            m.agregarTitulo("REPORTE DE DIRECTORIO EXTERNO");
            m.newLine(2);
            m.agregarSubtitulos(objeto);
            m.newLine(2);
        } catch (Exception ex) {
            System.out.println("No se pudo crear el documento \n"+ex);
            Logger.getLogger(ReporteDirectorioExternoTx.class.getName()).log(Level.SEVERE, null, ex);
        }
                
        //agregar tabla
        GTabla t = new GTabla(_p);
        
        try {
            t.build(_titulos);
        } catch (IOException ex) {
            Logger.getLogger(ReporteDirectorioExternoTx.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        for(Object object:dirExt)
        {            
            Map row = (Map)object;
            
            String[] _row = new String[_keys.length];
            for(int i=0; i<_keys.length; ++i){
                if(row.get(_keys[i])==null)    _row[i] = "";
                else    _row[i] = row.get(_keys[i]).toString();
            }            
            t.processLine(_row);
        }
        
        //fin tabla
                
        m.agregarTabla(t);
        
//        agregar grafico
        
//        MChart chart = new MChart();
//        
//        DefaultPieDataset dataset = new DefaultPieDataset( );
//        dataset.setValue( "IPhone 5s" , new Double( 20 ) );  
//        dataset.setValue( "SamSung Grand" , new Double( 20 ) );   
//        dataset.setValue( "MotoG" , new Double( 40 ) );    
//        dataset.setValue( "Nokia Lumia" , new Double( 10 ) );  
//        
//        try {
//            m.agregarGrafico(chart.createPieChart(dataset, "Grafica de Ejemplo"), 400, 300);
//        } catch (IOException ex) {
//            System.out.println("No se pudo agregar el grafico \n"+ex);
//            Logger.getLogger(ReporteTx.class.getName()).log(Level.SEVERE, null, ex);
//        }
        
        m.cerrarDocumento();  
                                
        JSONArray miArray = new JSONArray();
        JSONObject oResponse = new JSONObject();        
        oResponse.put("datareporte",m.encodeToBase64());
        miArray.put(oResponse);                       
        
        return WebResponse.crearWebResponseExito("Se genero el reporte correctamente",miArray);        
        
    }
    
    
}
