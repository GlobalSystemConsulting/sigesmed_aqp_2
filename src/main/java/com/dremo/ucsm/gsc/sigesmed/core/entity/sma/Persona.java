/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.entity.sma;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author Yemi
 */
@Entity(name = "com.dremo.ucsm.gsm.sigesmed.core.entity.sma.Persona")
@Table(name = "persona", schema= "pedagogico")
public class Persona implements Serializable {

    @Id
    @Column(name="per_id", unique=true, nullable=false)
    @SequenceGenerator(name = "secuencia_persona", sequenceName="pedagogico.persona_per_id_seq" )
    @GeneratedValue(generator="secuencia_persona")
    private Integer perId;
    
    @Column(name = "per_cod")
    private String perCod;
    
    @Column(name = "ape_mat", nullable=false, length=60)
    private String apeMat;
    
    @Column(name = "ape_pat", nullable=false, length=60)
    private String apePat;
    
    @Column(name = "nom", nullable=false, length=60)
    private String nom;
    
    @Column(name = "fec_nac", nullable=false)
    @Temporal(TemporalType.DATE)
    private Date fecNac;
    
    @Column(name = "dni", nullable=false, length=8)
    private String dni;
    
    @Column(name = "email", length=60)
    private String email;
    
    @Column(name = "num_1", length=10)
    private String num1;
    
    @Column(name = "num_2", length=10)
    private String num2;
    
    @Column(name = "fij", length=10)
    private String fij;
    
    @Column(name = "per_dir")
    private String perDir;
    
    @Column(name = "sex")
    private Character sex;
    
    @Column(name = "est_civ")
    private Character estCiv;
    
    @Column(name = "dep_nac", length=2)
    private String depNac;
    
    @Column(name = "pro_nac", length=2)
    private String proNac;
    
    @Column(name = "dis_nac", length=2)
    private String disNac;
    
    @Column(name = "usu_mod")
    private Integer usuMod;
    
    @Column(name = "fec_mod")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fecMod;
    
    @Column(name = "est_reg")
    private Character estReg;
    
    @OneToMany(fetch=FetchType.LAZY, mappedBy = "persona")
    Set<Trabajador> trabajadores = new HashSet(0);
    
    public Persona() {
    }

    public Persona(Integer perId) {
        this.perId = perId;
    }

    //Constructor para insertar un trabajador
    public Persona(String apePat, String apeMat,  String nom, String dni, Date fecNac, String num1, String num2, String fijo, String email, Character sex, Character estCiv, String depNac, String proNac, String disNac, Integer usuMod, Date fecMod, Character estReg) {
        this.apePat = apePat;
        this.apeMat = apeMat;
        this.nom = nom;
        this.dni = dni;
        this.fecNac = fecNac;
        this.num1 = num1;
        this.num2 = num2;
        this.fij = fijo;
        this.email = email;
        this.sex = sex;
        this.estCiv = estCiv;
        this.depNac = depNac;
        this.proNac = proNac;
        this.disNac = disNac;
        this.fecMod = fecMod;
        this.usuMod = usuMod;
        this.estReg = estReg;
    }
    
    //Constructor para insertar un pariente
    public Persona(String apePat, String apeMat, String nom, String dni, Date fecNac, String num1, String num2, String fijo, String email, Character sex, Integer usuMod, Date fecMod, Character estReg) {
        this.apePat = apePat;
        this.apeMat = apeMat;
        this.nom = nom;
        this.dni = dni;
        this.fecNac = fecNac;
        this.num1 = num1;
        this.num2 = num2;
        this.fij = fijo;
        this.email = email;
        this.sex = sex;
        this.fecMod = fecMod;
        this.usuMod = usuMod;
        this.estReg = estReg;
    }

    public Integer getPerId() {
        return perId;
    }

    public void setPerId(Integer perId) {
        this.perId = perId;
    }

    public String getPerCod() {
        return perCod;
    }

    public void setPerCod(String perCod) {
        this.perCod = perCod;
    }

    public String getApeMat() {
        return apeMat;
    }

    public void setApeMat(String apeMat) {
        this.apeMat = apeMat;
    }

    public String getApePat() {
        return apePat;
    }

    public void setApePat(String apePat) {
        this.apePat = apePat;
    }
    
    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public Date getFecNac() {
        return fecNac;
    }

    public void setFecNac(Date fecNac) {
        this.fecNac = fecNac;
    }

    public String getDni() {
        return dni;
    }

    public void setDni(String dni) {
        this.dni = dni;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getNum1() {
        return num1;
    }

    public void setNum1(String num1) {
        this.num1 = num1;
    }

    public String getNum2() {
        return num2;
    }

    public void setNum2(String num2) {
        this.num2 = num2;
    }

    public String getFij() {
        return fij;
    }

    public void setFij(String fij) {
        this.fij = fij;
    }

    public String getPerDir() {
        return perDir;
    }

    public void setPerDir(String perDir) {
        this.perDir = perDir;
    }

    public Character getSex() {
        return sex;
    }

    public void setSex(Character sex) {
        this.sex = sex;
    }
    
    public Character getEstCiv() {
        return estCiv;
    }

    public void setEstCiv(Character estCiv) {
        this.estCiv = estCiv;
    }
    
    public String getDepNac() {
        return depNac;
    }

    public void setDepNac(String depNac) {
        this.depNac = depNac;
    }
    
    public String getProNac() {
        return proNac;
    }

    public void setProNac(String proNac) {
        this.proNac = proNac;
    }
    
    public String getDisNac() {
        return disNac;
    }

    public void setDisNac(String disNac) {
        this.disNac = disNac;
    }
    
    public Integer getUsuMod() {
        return usuMod;
    }

    public void setUsuMod(Integer usuMod) {
        this.usuMod = usuMod;
    }

    public Date getFecMod() {
        return fecMod;
    }

    public void setFecMod(Date fecMod) {
        this.fecMod = fecMod;
    }

    public Character getEstReg() {
        return estReg;
    }

    public void setEstReg(Character estReg) {
        this.estReg = estReg;
    }

    @Override
    public String toString() {
        return "Persona{" + "perId=" + perId + '}';
    }
    
}
