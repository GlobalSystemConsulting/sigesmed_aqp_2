package com.dremo.ucsm.gsc.sigesmed.core.entity.cpe;

import com.dremo.ucsm.gsc.sigesmed.core.entity.sdc.ImagenPlantilla;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="dia_semana" ,schema="public")
public class DiaSemana  implements java.io.Serializable {


    @Id
    @Column(name="dia_sem_id", unique=true, nullable=false)
    private Character diaSemId;
    
    @Column(name="nom")
    private String nombre;

   public DiaSemana() {
        
    }
    public DiaSemana(Character diaSemId) {
        this.diaSemId = diaSemId;
        
    }
    
    public DiaSemana(Character diaSemId, String nombre) {
        this.diaSemId = diaSemId;
        this.nombre = nombre;
    }

    public DiaSemana(String nombre) {
        this.nombre = nombre;
    }

    public Character getDiaSemId() {
        return diaSemId;
    }

    public void setDiaSemId(Character diaSemId) {
        this.diaSemId = diaSemId;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

//    public List<HorarioDet> getHorarioDet() {
//        return horarioDet;
//    }
//
//    public void setHorarioDet(List<HorarioDet> horarioDet) {
//        this.horarioDet = horarioDet;
//    }
//    

}


