package com.dremo.ucsm.gsc.sigesmed.logic.matricula_institucional.agregar_estudiante.v1.core;

import com.dremo.ucsm.gsc.sigesmed.core.service.WebComponent;
import com.dremo.ucsm.gsc.sigesmed.core.service.constantes.Sigesmed;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.IComponentRegister;
import com.dremo.ucsm.gsc.sigesmed.logic.matricula_institucional.agregar_estudiante.v1.tx.BuscarPersonaTx;
import com.dremo.ucsm.gsc.sigesmed.logic.matricula_institucional.agregar_estudiante.v1.tx.BuscarEstudianteTx;
import com.dremo.ucsm.gsc.sigesmed.logic.matricula_institucional.agregar_estudiante.v1.tx.EliminarDomicilioTx;
import com.dremo.ucsm.gsc.sigesmed.logic.matricula_institucional.agregar_estudiante.v1.tx.EliminarParienteEstudianteTx;
import com.dremo.ucsm.gsc.sigesmed.logic.matricula_institucional.agregar_estudiante.v1.tx.ListarLenguasTx;
import com.dremo.ucsm.gsc.sigesmed.logic.matricula_institucional.agregar_estudiante.v1.tx.ListarPaisesTx;
import com.dremo.ucsm.gsc.sigesmed.logic.matricula_institucional.agregar_estudiante.v1.tx.GuardarDatosGeneralesTx;
import com.dremo.ucsm.gsc.sigesmed.logic.matricula_institucional.agregar_estudiante.v1.tx.GuardarDomicilioTx;
import com.dremo.ucsm.gsc.sigesmed.logic.matricula_institucional.agregar_estudiante.v1.tx.GuardarNuevaPersonaTx;
import com.dremo.ucsm.gsc.sigesmed.logic.matricula_institucional.agregar_estudiante.v1.tx.GuardarParienteEstudianteTx;
import com.dremo.ucsm.gsc.sigesmed.logic.matricula_institucional.agregar_estudiante.v1.tx.ListarDomiciliosTx;
import com.dremo.ucsm.gsc.sigesmed.logic.matricula_institucional.agregar_estudiante.v1.tx.ListarGradoInstruccionTx;
import com.dremo.ucsm.gsc.sigesmed.logic.matricula_institucional.agregar_estudiante.v1.tx.ListarParientesTx;
import com.dremo.ucsm.gsc.sigesmed.logic.matricula_institucional.agregar_estudiante.v1.tx.ListarTipoParienteTx;

public class ComponentRegister implements IComponentRegister {

    @Override

    public WebComponent createComponent() {

        WebComponent UComponent = new WebComponent(Sigesmed.SUBMODULO_MATRICULA_INSTITUCIONAL);
        UComponent.setName("agregarEstudiante");
        UComponent.setVersion(1);
        UComponent.addTransactionGET("buscarEstudiante", BuscarEstudianteTx.class);
        UComponent.addTransactionGET("listarLenguas", ListarLenguasTx.class);
        UComponent.addTransactionGET("listarPaises", ListarPaisesTx.class);
        UComponent.addTransactionPOST("guardarDatosGenerales", GuardarDatosGeneralesTx.class);
        UComponent.addTransactionGET("listarDomicilios", ListarDomiciliosTx.class);
        UComponent.addTransactionPOST("guardarDomicilio", GuardarDomicilioTx.class);
        UComponent.addTransactionDELETE("eliminarDomicilio", EliminarDomicilioTx.class);
        UComponent.addTransactionGET("listarParientes", ListarParientesTx.class);
        UComponent.addTransactionGET("listarTipoPariente", ListarTipoParienteTx.class);
        UComponent.addTransactionGET("listarGradoInstruccion", ListarGradoInstruccionTx.class);
        UComponent.addTransactionGET("buscarPersona", BuscarPersonaTx.class);
        UComponent.addTransactionPOST("guardarNuevaPersona", GuardarNuevaPersonaTx.class);
        UComponent.addTransactionPOST("guardarParienteEstudiante", GuardarParienteEstudianteTx.class);
        UComponent.addTransactionDELETE("eliminarParienteEstudiante", EliminarParienteEstudianteTx.class);
        
        return UComponent;
    }
}
