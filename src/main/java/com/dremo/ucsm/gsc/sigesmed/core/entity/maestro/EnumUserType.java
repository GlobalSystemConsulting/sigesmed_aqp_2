package com.dremo.ucsm.gsc.sigesmed.core.entity.maestro;

/**
 * Created by Administrador on 13/10/2016.
 */
import java.io.Serializable;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.Properties;
import org.hibernate.HibernateException;
import org.hibernate.engine.spi.SessionImplementor;
import org.hibernate.usertype.ParameterizedType;
import org.hibernate.usertype.UserType;

/**
 *
 * @author geank
 */
public class EnumUserType<E extends PersistentEnum> implements UserType{
    private Class<E> clazz = null;
    private static final int[] SQL_TYPES = {Types.VARCHAR};

    protected EnumUserType(Class<E> c) {
        this.clazz = c;
    }
    @Override
    public int[] sqlTypes() {
        return SQL_TYPES;
    }
    @Override
    public Class returnedClass() {
        return clazz;
    }
    @Override
    public Object nullSafeGet(ResultSet resultSet, String[] names, SessionImplementor si, Object owner) throws HibernateException, SQLException {
        String name = resultSet.getString(names[0]);

        if (resultSet.wasNull())
            return null;

        for(Object value : returnedClass().getEnumConstants()) {
            if(name.equals(((PersistentEnum)value).getValue())) {
                return value;
            }
        }
        throw new IllegalStateException("Unknown " + returnedClass().getSimpleName() + " id");
    }

    @Override
    public void nullSafeSet(PreparedStatement preparedStatement, Object value, int index, SessionImplementor si) throws HibernateException, SQLException {
        if (null == value) {
            preparedStatement.setNull(index, Types.VARCHAR);
        } else {
            preparedStatement.setString(index, ((PersistentEnum)value).getValue());
        }
    }

    @Override
    public Object deepCopy(Object o) throws HibernateException {
        return o;
    }

    @Override
    public boolean isMutable() {
        return false;
    }

    @Override
    public Serializable disassemble(Object o) throws HibernateException {
        return (Serializable)o;
    }

    @Override
    public Object assemble(Serializable cached, Object o) throws HibernateException {
        return cached;
    }

    @Override
    public Object replace(Object original, Object target, Object owner) throws HibernateException {
        return original;
    }
    @Override
    public int hashCode(Object x) throws HibernateException {
        return x.hashCode();
    }
    @Override
    public boolean equals(Object x, Object y) throws HibernateException {
        if (x == y)
            return true;
        if (null == x || null == y)
            return false;
        return x.equals(y);
    }
}