/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.smdg.item_file.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.smdg.ObjetivosDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.smdg.ObjetivosDetalleDao;

import com.dremo.ucsm.gsc.sigesmed.core.entity.smdg.Objetivos;
import com.dremo.ucsm.gsc.sigesmed.core.entity.smdg.ObjetivosDetalle;

import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;

import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import com.dremo.ucsm.gsc.sigesmed.logic.smdg.proyectos.v1.tx.RegistrarProyectosTx;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author Administrador
 */
public class GuardarObjetivosTx implements ITransaction{
    @Override
    public WebResponse execute(WebRequest wr) {                
        /*
        *   Parte para la lectura, verificacion y validacion de datos
        */
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        
        JSONObject requestData = (JSONObject)wr.getData();
        JSONArray objetivos = requestData.getJSONArray("objetivos");
        JSONArray eliminados = requestData.getJSONArray("eliminados");
        
        int iteide = requestData.optInt("iteide");
        String tipo = requestData.getString("tipo");

        Date objfec = null;
        
        Objetivos objetivo = null;
        ObjetivosDetalle detalle = null;
        
        ObjetivosDao objetivosDao = (ObjetivosDao)FactoryDao.buildDao("smdg.ObjetivosDao");
        ObjetivosDetalleDao detalleDao = (ObjetivosDetalleDao)FactoryDao.buildDao("smdg.ObjetivosDetalleDao");
        
        for(int i = 0; i < objetivos.length(); ++i){            
            try {                
                objfec = formatter.parse(objetivos.getJSONObject(i).optString("objfec"));            
            } catch (ParseException ex) {
                Logger.getLogger(RegistrarProyectosTx.class.getName()).log(Level.SEVERE, null, ex);
            }
            
//            si es un objetivo nuevo
            if (objetivos.getJSONObject(i).optInt("objide") == 0) {                 
                objetivo = new Objetivos(tipo, objetivos.getJSONObject(i).optString("objtip"), 
                                    objetivos.getJSONObject(i).optString("objdes"), 
                                    objetivos.getJSONObject(i).optDouble("objava"),
                                    objfec,
                                    objetivos.getJSONObject(i).optJSONObject("objusu").optInt("traId"),
                                    iteide);
                objetivosDao.insert(objetivo);
//                Si es un objetivo cuantitativo
                if(new String(objetivos.getJSONObject(i).optString("objtip")).equals("c")){
                    detalle = new ObjetivosDetalle(objetivos.getJSONObject(i).optString("indicador"), 
                                                objetivos.getJSONObject(i).optInt("meta"), 
                                                objetivo.getObjId(), objetivos.getJSONObject(i).optDouble("odeava"));
                    detalleDao.insert(detalle);
                }
                
            }
//            si se va actualizar un objetivo
            else{
                objetivo = new Objetivos(objetivos.getJSONObject(i).optInt("objide"),
                                    tipo, objetivos.getJSONObject(i).optString("objtip"), 
                                    objetivos.getJSONObject(i).optString("objdes"), 
                                    objetivos.getJSONObject(i).optDouble("objava"),
                                    objfec,
                                    objetivos.getJSONObject(i).optJSONObject("objusu").optInt("traId"),
                                    iteide);
                objetivosDao.update(objetivo);
                
                if(new String(objetivos.getJSONObject(i).optString("objtip")).equals("c")){
    
                    detalle = detalleDao.listar(objetivos.getJSONObject(i).optInt("objide"));
                    detalle.setOdeInd(objetivos.getJSONObject(i).optString("indicador"));
                    detalle.setOdeMet(objetivos.getJSONObject(i).optInt("meta"));
                    detalle.setOdeAva(objetivos.getJSONObject(i).optDouble("odeava"));

                    detalleDao.update(detalle);
                    
                }                
            }                        
        }
                
        objetivo = null;
        for(int i = 0; i < eliminados.length(); ++i){
            objetivo = objetivosDao.load(Objetivos.class, eliminados.getJSONObject(i).optInt("objide"));
            objetivo.setEstReg("E");
            objetivosDao.update(objetivo);
        }
        //Fin
        /*
        *  Repuesta Correcta
        */
        JSONObject oResponse = new JSONObject();
        oResponse.put("estado",true);
        return WebResponse.crearWebResponseExito("Se actualizo el proyecto correctamente", oResponse);
        //Fin
    }
}
