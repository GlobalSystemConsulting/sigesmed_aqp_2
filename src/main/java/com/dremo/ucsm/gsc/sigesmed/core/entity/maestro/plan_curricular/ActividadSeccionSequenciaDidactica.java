package com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.plan_curricular;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by Administrador on 02/12/2016.
 */
@Entity
@Table(name = "actividad_seccion_sequencia_didactica", schema = "pedagogico")
public class ActividadSeccionSequenciaDidactica implements java.io.Serializable {
    @SequenceGenerator(name = "actividad_seccion_sequencia_didactica_act_sec_did_id_seq", sequenceName = "pedagogico.actividad_seccion_sequencia_didactica_act_sec_did_id_seq")
    @GeneratedValue(generator = "actividad_seccion_sequencia_didactica_act_sec_did_id_seq")
    @Id
    @Column(name="act_sec_did_id",nullable = false)
    private int actSeqDidId;

    /*@ManyToOne(fetch = FetchType.EAGER)
    @JoinColumns( {
            @JoinColumn(name = "sec_sec_did", referencedColumnName = "sec_sec_did",insertable = false, updatable = false),
            @JoinColumn(name = "ses_sec_did_id", referencedColumnName = "ses_sec_did_id",insertable = false, updatable = false) })
    private SeccionSequenciaDidacticaSesion seccionSequenciaDidactica;*/
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "sec_sec_did")
    private SeccionSequenciaDidactica seccion;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn (name = "ses_sec_did_id")
    private SesionSequenciaDidactica sequencia;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumns( {
            @JoinColumn(name = "sec_sec_did", referencedColumnName = "sec_sec_did",insertable = false, updatable = false),
            @JoinColumn(name = "ses_sec_did_id", referencedColumnName = "ses_sec_did_id",insertable = false, updatable = false) })
    private SeccionSequenciaDidacticaSesion seccionSequenciaDidactica;
    @Column(name = "nom", nullable = false, length = 256)
    private String nom;
    @Column(name = "des", length = 256)
    private String des;
    @Column(name = "usu_mod")
    private Integer usuMod;
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "fec_mod", length=29)
    private Date fecMod;
    @Column(name = "est_reg",length = 1)
    private Character estReg;

    public ActividadSeccionSequenciaDidactica() {
    }

    public ActividadSeccionSequenciaDidactica(SeccionSequenciaDidactica seccion,SesionSequenciaDidactica sequencia, String nom, String des) {
        this.seccion = seccion;
        this.sequencia = sequencia;

        this.nom = nom;
        this.des = des;
        this.fecMod = new Date();
        this.estReg = 'A';
    }

    public int getActSeqDidId() {
        return actSeqDidId;
    }

    public void setActSeqDidId(int actSeqDidId) {
        this.actSeqDidId = actSeqDidId;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }


    public SeccionSequenciaDidactica getSeccion() {
        return seccion;
    }

    public void setSeccion(SeccionSequenciaDidactica seccion) {
        this.seccion = seccion;
    }

    public SesionSequenciaDidactica getSequencia() {
        return sequencia;
    }

    public void setSequencia(SesionSequenciaDidactica sequencia) {
        this.sequencia = sequencia;
    }

    public String getDes() {
        return des;
    }

    public void setDes(String des) {
        this.des = des;
    }

    public Integer getUsuMod() {
        return usuMod;
    }

    public void setUsuMod(Integer usuMod) {
        this.usuMod = usuMod;
    }

    public Date getFecMod() {
        return fecMod;
    }

    public void setFecMod(Date fecMod) {
        this.fecMod = fecMod;
    }

    public Character getEstReg() {
        return estReg;
    }

    public void setEstReg(Character estReg) {
        this.estReg = estReg;
    }
}
