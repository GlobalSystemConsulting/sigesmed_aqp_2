/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.entity.sci;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author RE
 */
@Entity
@Table(name="empresa" ,schema="administrativo" )
public class Empresa  implements java.io.Serializable {

    @Id
    @Column(name="empresa_id", unique=true, nullable=false)
    @SequenceGenerator(name = "secuencia_empresa", sequenceName="administrativo.empresa_emp_id_seq" )
    @GeneratedValue(generator="secuencia_empresa")
    private int empId;
    @Column(name="num", length=16)
    private String num;
    @Column(name="raz_soc", nullable=false, length=128)
    private String razSoc;
    @Column(name="dir", nullable=false, length=256)
    private String dir;
    @Column(name="tip", nullable=false, length=1)
    private String tip;
    
     @Temporal(TemporalType.DATE)
    @Column(name="fec_mod", length=13)
     private Date fecMod;
    
    @Column(name="usu_mod")
     private Integer usuMod;
    
    @Column(name="est_reg", length=1)
     private Character estReg;

    public Empresa() {
    }

    public Empresa(int empId) {
        this.empId = empId;
    }

    public Empresa(int empId, String num, String razSoc, String dir, String tip, Date fecMod, Integer usuMod, Character estReg) {
        this.empId = empId;
        this.num = num;
        this.razSoc = razSoc;
        this.dir = dir;
        this.tip = tip;
        this.fecMod = fecMod;
        this.usuMod = usuMod;
        this.estReg = estReg;
    }

    public int getEmpId() {
        return empId;
    }

    public void setEmpId(int empId) {
        this.empId = empId;
    }

    public String getNum() {
        return num;
    }

    public void setNum(String num) {
        this.num = num;
    }

    public String getRazSoc() {
        return razSoc;
    }

    public void setRazSoc(String razSoc) {
        this.razSoc = razSoc;
    }

    public String getDir() {
        return dir;
    }

    public void setDir(String dir) {
        this.dir = dir;
    }

    public String getTip() {
        return tip;
    }

    public void setTip(String tip) {
        this.tip = tip;
    }

    public Date getFecMod() {
        return fecMod;
    }

    public void setFecMod(Date fecMod) {
        this.fecMod = fecMod;
    }

    public Integer getUsuMod() {
        return usuMod;
    }

    public void setUsuMod(Integer usuMod) {
        this.usuMod = usuMod;
    }

    public Character getEstReg() {
        return estReg;
    }

    public void setEstReg(Character estReg) {
        this.estReg = estReg;
    }
  
}
