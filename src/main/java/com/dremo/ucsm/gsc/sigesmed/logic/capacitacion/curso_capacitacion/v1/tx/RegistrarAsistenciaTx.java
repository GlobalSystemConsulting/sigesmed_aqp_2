package com.dremo.ucsm.gsc.sigesmed.logic.capacitacion.curso_capacitacion.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.capacitacion.AsistenciaParticipanteDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.capacitacion.HorarioSedeCapacitacionDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.capacitacion.AsistenciaParticipante;
import com.dremo.ucsm.gsc.sigesmed.core.entity.capacitacion.HorarioSedeCapacitacion;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

public class RegistrarAsistenciaTx implements ITransaction {
    
    private static final Logger logger = Logger.getLogger(RegistrarAsistenciaTx.class.getName());

    @Override
    public WebResponse execute(WebRequest wr) {
        try {
            JSONObject data = (JSONObject) wr.getData();
            
            HorarioSedeCapacitacionDao horarioSedeCapacitacionDao = (HorarioSedeCapacitacionDao) FactoryDao.buildDao("capacitacion.HorarioSedeCapacitacionDao");
            AsistenciaParticipanteDao asistenciaParticipanteDao = (AsistenciaParticipanteDao) FactoryDao.buildDao("capacitacion.AsistenciaParticipanteDao");
            Calendar calendar = Calendar.getInstance();
            
            List<HorarioSedeCapacitacion> horarios = horarioSedeCapacitacionDao.buscarParaOnTime(data.getInt("sed"), calendar.get(Calendar.DAY_OF_WEEK), new Date());
            JSONObject answer = new JSONObject();
            answer.put("est", (horarios.size() > 0));
            JSONArray array = new JSONArray();
            
            for(HorarioSedeCapacitacion shift: horarios) {
                JSONObject est = new JSONObject();
                JSONArray states = new JSONArray();
                
                List<AsistenciaParticipante> asistencias = asistenciaParticipanteDao.buscar(data.getInt("doc"), shift.getHorSedCapId(), new Date());
                est.put("est",(asistencias.size() > 0));
                
                for(AsistenciaParticipante attendance: asistencias) {
                    JSONObject time = new JSONObject();
                    time.put("time", (new Date()).getTime());
                    states.put(time);
                    attendance.setUsuMod(data.getInt("doc"));
                    attendance.setEstReg('A');
                    asistenciaParticipanteDao.update(attendance);
                }
                
                est.put("states", states);
                array.put(est);
            }
            
            answer.put("shifts", array);
            
            return WebResponse.crearWebResponseExito("El desarrollo del tema del curso de capacitación fue creado correctamente", WebResponse.OK_RESPONSE).setData(answer);
        } catch (Exception e) {
            logger.log(Level.SEVERE, "registrarDesarrollo", e);
            return WebResponse.crearWebResponseError("No se pudo registrar rl desarrollo del tema del curso de capacitación", WebResponse.BAD_RESPONSE);
        }
    }
}
