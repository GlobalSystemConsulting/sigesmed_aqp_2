package com.dremo.ucsm.gsc.sigesmed.core.entity.mech;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="horario_detalle" ,schema="institucional" )
public class HorarioDetalle  implements java.io.Serializable {

    @Id
    @Column(name="hor_det_id", unique=true, nullable=false)
    @SequenceGenerator(name = "secuencia_horariodetalle", sequenceName="institucional.horario_detalle_hor_det_id_seq" )
    @GeneratedValue(generator="secuencia_horariodetalle")
    private int horDetId;
    
    @Column(name="hor_ini")
    @Temporal(TemporalType.TIME)
    private Date horIni;
    @Temporal(TemporalType.TIME)
    @Column(name="hor_fin")
    private Date horFin;
    
    @Column(name="hor_esc_id")
    private int horarioId;
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="hor_esc_id",updatable = false,insertable = false)
    private HorarioEscolar horario;
    
    @Column(name="aul_id")
    private int aulId;
    
    @Column(name="dia_sem_id")
    private char diaId;
    
    @Column(name="pla_mag_id")
    private int plaMagId;
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="pla_mag_id",updatable = false,insertable = false)
    private PlazaMagisterial plazaMagisterial;
    
    @Column(name="are_cur_id")
    private int areCurId;
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="are_cur_id",updatable = false,insertable = false)
    private AreaCurricular area;

    public HorarioDetalle() {
    }
    public HorarioDetalle(int horDetId) {
        this.horDetId = horDetId;
    }
    public HorarioDetalle(int horDetId,Date horIni,Date horFin,int horarioId,int plaMagId,int areCurId,int aulId, char diaId) {
       this.horDetId = horDetId;
       
       this.horIni = horIni;
       this.horFin = horFin;
       
       this.horarioId = horarioId;
       this.plaMagId = plaMagId;
       this.areCurId = areCurId;
       this.aulId = aulId;
       this.diaId = diaId;
    }
     
    public int getHorDetId() {
        return this.horDetId;
    }    
    public void setHorDetId(int horDetId) {
        this.horDetId = horDetId;
    }
    
    public Date getHorIni() {
        return this.horIni;
    }
    public void setHorIni(Date horIni) {
        this.horIni = horIni;
    }
    
    public Date getHorFin() {
        return this.horFin;
    }
    public void setHorFin(Date horFin) {
        this.horFin = horFin;
    }
    
    public int getHorarioId() {
        return this.horarioId;
    }    
    public void setHorarioId(int horarioId) {
        this.horarioId = horarioId;
    }    
    public HorarioEscolar getHorarioEscolar() {
        return this.horario;
    }
    public void setHorarioEscolar(HorarioEscolar horario) {
        this.horario = horario;
    }
    
    public int getPlaMagId() {
        return this.plaMagId;
    }    
    public void setPlaMagId(int plaMagId) {
        this.plaMagId = plaMagId;
    }    
    public PlazaMagisterial getPlazaMagisterial() {
        return this.plazaMagisterial;
    }
    public void setPlazaMagisterial(PlazaMagisterial plazaMagisterial ) {
        this.plazaMagisterial = plazaMagisterial;
    }
    
    public int getAreCurId() {
        return this.areCurId;
    }    
    public void setAreCurId(int areCurId) {
        this.areCurId = areCurId;
    }
    
    public AreaCurricular getArea() {
        return this.area;
    }
    public void setArea(AreaCurricular area) {
        this.area = area;
    }
    
    public int getAulId() {
        return this.aulId;
    }    
    public void setAulId(int aulId) {
        this.aulId = aulId;
    }
    
    public char getDisId() {
        return this.diaId;
    }    
    public void setDia(char diaId) {
        this.diaId = diaId;
    }
}


