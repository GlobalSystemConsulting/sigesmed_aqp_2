/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.web.Grupo.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.web.GrupoDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.web.Grupo;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import org.json.JSONObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;

/**
 *
 * @author abel
 */
public class EliminarGrupoTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
        
        /*
        *   Parte para la lectura, verificacion y validacion de datos
        */
        int moduloID = 0;
        try{
            JSONObject requestData = (JSONObject)wr.getData();
            moduloID = requestData.getInt("grupoID");
        
        }catch(Exception e){
            System.out.println(e);
            return WebResponse.crearWebResponseError("No se pudo eliminar, datos incorrectos", e.getMessage() );
        }
        //Fin
        
        /*
        *  Parte para la operacion en la Base de Datos
        */
        GrupoDao moduloDao = (GrupoDao)FactoryDao.buildDao("web.GrupoDao");
        try{
            moduloDao.delete(new Grupo(moduloID));
        
        }catch(Exception e){
            System.out.println("No se pudo eliminar el Grupo\n"+e);
            return WebResponse.crearWebResponseError("No se pudo eliminar el Grupo", e.getMessage() );
        }
        //Fin
        
        /*
        *  Repuesta Correcta
        */        
        return WebResponse.crearWebResponseExito("El Grupo se elimino correctamente");
        //Fin
    }
}
