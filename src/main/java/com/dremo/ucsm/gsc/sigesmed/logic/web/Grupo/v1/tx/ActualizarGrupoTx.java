/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.web.Grupo.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.web.GrupoDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.UsuarioSession;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import org.json.JSONObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import com.dremo.ucsm.gsc.sigesmed.core.entity.web.Grupo;
import java.util.ArrayList;
import java.util.Date;
import org.json.JSONArray;

/**
 *
 * @author abel
 */
public class ActualizarGrupoTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
                
        /*
        *   Parte para la lectura, verificacion y validacion de datos
        */
        Grupo actualizarGrupo = null;
        try{
            JSONObject requestData = (JSONObject)wr.getData();
            int grupoID = requestData.getInt("grupoID");
            boolean tipo = requestData.getBoolean("tipo");
            String nombre = requestData.getString("nombre");
            int organizacionID = requestData.getInt("organizacionID");
            String estado = requestData.getString("estado");
            
            JSONArray roles = requestData.optJSONArray("usuarios");
            
            actualizarGrupo = new Grupo(grupoID, tipo, nombre, organizacionID, new Date(), wr.getIdUsuario(), estado.charAt(0));
            if( roles != null ){
                actualizarGrupo.setUsuarios(new ArrayList<UsuarioSession>());
                for(int i = 0; i < roles.length();i++){
                    JSONObject bo = roles.getJSONObject(i);
                    actualizarGrupo.getUsuarios().add( new UsuarioSession( bo.getInt("sessionID") ) );
                }
            }
        
        }catch(Exception e){
            System.out.println("No se pudo actualizar, datos incorrectos\n"+e);
            return WebResponse.crearWebResponseError("No se pudo actualizar, datos incorrectos", e.getMessage() );
        }
        //Fin
        
        /*
        *  Parte para la operacion en la Base de Datos
        */
        GrupoDao moduloDao = (GrupoDao)FactoryDao.buildDao("web.GrupoDao");
        try{
            moduloDao.update(actualizarGrupo);
        
        }catch(Exception e){
            System.out.println("No se pudo actualizar el grupo\n"+e);
            return WebResponse.crearWebResponseError("No se pudo actualizar el grupo", e.getMessage() );
        }
        //Fin
        
        /*
        *  Repuesta Correcta
        */        
        return WebResponse.crearWebResponseExito("El Grupo se actualizo correctamente");
        //Fin
    }
    
}
