/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.entity.se;

import java.io.Serializable;
import javax.persistence.Column;

/**
 *
 * @author Administrador
 */

public class ParientesPK implements Serializable {
    
    @Column(name = "par_id")
    private Integer parId;
    
    @Column(name = "per_id")
    private Integer perId;

    public ParientesPK() {
    }

    public ParientesPK(Integer parId, Integer perId) {
        this.parId = parId;
        this.perId = perId;
    }

    public long getParId() {
        return parId;
    }

    public void setParId(Integer parId) {
        this.parId = parId;
    }

    public long getPerId() {
        return perId;
    }

    public void setPerId(Integer perId) {
        this.perId = perId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) parId;
        hash += (int) perId;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ParientesPK)) {
            return false;
        }
        ParientesPK other = (ParientesPK) object;
        if (this.parId != other.parId) {
            return false;
        }
        if (this.perId != other.perId) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.dremo.ucsm.gsm.sigesmed.core.entity.se.ParientePK[ parId=" + parId + ", perId=" + perId + " ]";
    }
    
}
