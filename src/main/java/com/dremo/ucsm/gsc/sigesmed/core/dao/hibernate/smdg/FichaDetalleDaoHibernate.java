/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.smdg;

import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.GenericDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.dao.smdg.FichaDetalleDao;
import com.dremo.ucsm.gsc.sigesmed.core.datastore.HibernateUtil;
import com.dremo.ucsm.gsc.sigesmed.core.entity.smdg.FichaDetalle;
import com.dremo.ucsm.gsc.sigesmed.core.entity.smdg.PlantillaFichaInstitucional;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author Administrador
 */
public class FichaDetalleDaoHibernate extends GenericDaoHibernate<FichaDetalle> implements FichaDetalleDao{
    @Override
    public String buscarUltimoCodigo() {
        String codigo = null;
//        Session session = HibernateUtil.getSessionFactory().openSession();
//        try{
//            //listar ultimo codigo de tramite
//            String hql = "SELECT a.apoId FROM Apoderado a ORDER BY a.apoId DESC ";
//            Query query = session.createQuery(hql);
//            query.setMaxResults(1);
//            codigo = ((String)query.uniqueResult());
//            
//            //solo cuando no hay ningun registro aun
//            if(codigo==null)
//                codigo = "0000";
//        
//        }catch(Exception e){
//            System.out.println("No se pudo encontrar el ultimo codigo \\n "+ e.getMessage());
//            throw new UnsupportedOperationException("No se pudo encontrar el ultimo codigo \\n "+ e.getMessage());            
//        }
//        finally{
//            session.close();
//        }
        return codigo;
    }
    @Override
    public List<FichaDetalle> obtenerDetalles(int fevDocId){
        List<FichaDetalle> detalles = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction t = session.beginTransaction();
        try{
            
//            String hql = "SELECT fd FROM FichaDetalle fd "
//                    + "join fetch fd.plantilla p "
//                    + "join fetch fd.grupo g "
//                    + "join fetch fd.indicador i "
//                    + "join fetch fd.ficha f "
//                    + "WHERE fd.fevDocId = " + fevDocId;
            String hql = "SELECT fd FROM FichaDetalle fd "
                    + "join fetch fd.indicador i "
                    + "join fetch i.grupo g "
                    + "join fetch g.plantilla p "                    
                    + "join fetch fd.ficha f "
                    + "WHERE fd.fevDocId = " + fevDocId + " "
                    + "order by g.pgrId, i.pinId";
            Query query = session.createQuery(hql);            
//            query.setParameter("p1", org);
            detalles = query.list();
            t.commit();
        
        }catch(Exception e){
            t.rollback();
            System.out.println("No se pudo Listar los detalles de la ficha \\n "+ e.getMessage());
            throw new UnsupportedOperationException("No se pudo Listar los detalles de la ficha \\n "+ e.getMessage());            
        }
        finally{
            session.close();
        }
        return detalles;
    }
    
    @Override
    public FichaDetalle obtenerDetalle(int indId, int ficId){
        Session session = HibernateUtil.getSessionFactory().openSession();
        FichaDetalle detalle = null;
        Transaction t = session.beginTransaction();
        
//        session.enableFilter("filtroplantilla");
//        session.enableFilter("filtrogrupo");
        
        try{                                        
            String hql = "SELECT d FROM FichaDetalle d "           
                    + "WHERE d.pinId = " + indId + " AND d.fevDocId = " + ficId ;
            
            Query query = session.createQuery(hql);
            query.setMaxResults(1);
            detalle = (FichaDetalle)query.uniqueResult();            
            t.commit();
                      
        }catch(Exception e){
            t.rollback();
            System.out.println("No se pudo obtener el detalle \\n "+ e.getMessage());
            throw new UnsupportedOperationException("No se pudo obtener el detalle \\n "+ e.getMessage());            
        }
        finally{
//            session.disableFilter("filtroplantilla");
//            session.disableFilter("filtrogrupo");
            session.close();
        }
        
        return detalle;
    }
}
