package com.dremo.ucsm.gsc.sigesmed.logic.maestro.acomp.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.maestro.DocenteDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.plan_curricular.AreaCurricular;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.base.EntityUtil;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by Administrador on 09/01/2017.
 */
public class ListarCursosDocenteTx implements ITransaction{
    private static Logger logger = Logger.getLogger(ListarCursosDocenteTx.class.getName());
    @Override
    public WebResponse execute(WebRequest wr) {
        JSONObject data = (JSONObject)wr.getData();
        int idGrado = data.getInt("gra");
        String idSeccion = data.optString("secc", "");
        int idDoc = data.getInt("doc");
        int idOrg = data.getInt("org");
        return listarAreasDocente(idDoc,idOrg,idGrado,idSeccion);
    }

    private WebResponse listarAreasDocente(int doc, int org,int idGrado,String idSeccion) {
        try{
            DocenteDao docDao = (DocenteDao) FactoryDao.buildDao("maestro.DocenteDao");
            List<AreaCurricular> areas = idSeccion.isEmpty()? docDao.listarAreasDocente(org,doc,idGrado): docDao.listarAreasDocente(org,doc,idGrado,idSeccion.charAt(0));
            ///List<AreaCurricular> areas = docDao.listarAreasDocente(org,doc,idGrado,idSeccion);
            JSONArray areasJSON = new JSONArray(EntityUtil.listToJSONString(
                    new String[]{"areCurId","nom"},
                    new String[]{"id","nom"},
                    areas
            ));
            return WebResponse.crearWebResponseExito("Se listo con exito",areasJSON);
        }catch (Exception e){
            logger.log(Level.SEVERE,"listarAreasDocente",e);
            return WebResponse.crearWebResponseError("No se puede listar los curso");
        }
    }

}
