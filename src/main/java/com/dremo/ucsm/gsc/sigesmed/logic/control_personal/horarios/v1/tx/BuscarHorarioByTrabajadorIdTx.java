/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.control_personal.horarios.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.logic.control_personal.registro_trabajador.v1.tx.*;
import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.cpe.HorarioDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.cpe.LibroAsistenciaDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.cpe.PersonalDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Organizacion;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Trabajador;
import com.dremo.ucsm.gsc.sigesmed.core.entity.TrabajadorCargo;
import com.dremo.ucsm.gsc.sigesmed.core.entity.cpe.HorarioCab;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import org.json.JSONArray;
import org.json.JSONObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author carlos
 */
public class BuscarHorarioByTrabajadorIdTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {

        TrabajadorCargo worker=null;
      
        try{
            
            JSONObject requestData = (JSONObject)wr.getData();
           
            Integer idCargoTrab = requestData.getInt("idCargo"); 
            worker=new TrabajadorCargo(Short.valueOf(idCargoTrab+""));
            
        }catch(Exception e){
            return WebResponse.crearWebResponseError("No se pudo verificar los datos", e.getMessage() );
        }
        
        HorarioDao horarioDao = (HorarioDao)FactoryDao.buildDao("cpe.HorarioDao");
        HorarioCab horario=null;
        try{
            
            horario =horarioDao.buscarHorarioCargoByTrabajador(worker);
        }catch(Exception e){
            return WebResponse.crearWebResponseError("No se pudo Listar los Cargos de los Trabajadores", e.getMessage() );
        }
        
        JSONObject oResponse = new JSONObject();
        if(horario==null)
        {
            oResponse.put("id", "");
            oResponse.put("des", "");
            oResponse.put("idCargo", "");
        }
        else
        {
            oResponse.put("id", horario.getHorCabId());
            oResponse.put("des", horario.getHorCabDes());
            oResponse.put("idCargo", horario.getHorCabCar().getCrgTraIde());
        }

        return WebResponse.crearWebResponseExito("Se Listo correctamente", oResponse);
        //Fin
    }
    
}

