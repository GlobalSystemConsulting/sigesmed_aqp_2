package com.dremo.ucsm.gsc.sigesmed.core.entity.mep;
// Generated 15/07/2016 01:43:01 PM by Hibernate Tools 4.3.1


import org.hibernate.annotations.DynamicUpdate;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.*;

/**
 * @author geank
 */
@Entity
@Table(name="indicador_ficha_evaluacion",schema="pedagogico")
@DynamicUpdate(value=true)
public class IndicadorFichaEvaluacion  implements java.io.Serializable {

    @Id 
    @Column(name="ind_fic_eva_id", unique=true, nullable=false)
    @SequenceGenerator(name = "secuencia_indicador_ficha_evaluacion", sequenceName="pedagogico.indicador_ficha_evaluacion_ind_fic_eva_id_seq" )
    @GeneratedValue(generator="secuencia_indicador_ficha_evaluacion")
    private int indFicEvaId;
    
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="con_fic_eva_id")
    private ContenidoFichaEvaluacion contenidoFichaEvaluacion;
    
    @Column(name="nom", nullable=false)
    private String nom;
    
    @Column(name="des", nullable=false)
    private String des;
     
    @Column(name="doc_ver", length=20)
    private String docVer;
    
    @Column(name="usu_mod")
    private Integer usuMod;
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="fec_mod", length=29)
    private Date fecMod;
    
    @Column(name="est_reg", length=1)
    private Character estReg;
    
     @OneToMany(fetch=FetchType.LAZY, mappedBy="indicadorFichaEvaluacion")
    private Set<IndicadoresEvaluarPersonal> indicadoresEvaluarPersonals = new HashSet(0);

    @Transient
    private short value;
    public IndicadorFichaEvaluacion() {
    }
    public IndicadorFichaEvaluacion(String nom, short value) {
        this.nom = nom;
        this.value = value;
    }

    public IndicadorFichaEvaluacion(String nom, String des, String docVer, ContenidoFichaEvaluacion cont) {
        this.nom = nom;
        this.des = des;
        this.docVer = docVer;
        this.contenidoFichaEvaluacion = cont;
    }
    public IndicadorFichaEvaluacion(String nom, String des, String docVer) {
        this.nom = nom;
        this.des = des;
        this.docVer = docVer;
    }
    public IndicadorFichaEvaluacion(int indFicEvaId, String nom, String des) {
        this.indFicEvaId = indFicEvaId;
        this.nom = nom;
        this.des = des;
    }
    public IndicadorFichaEvaluacion(int indFicEvaId, ContenidoFichaEvaluacion contenidoFichaEvaluacion, String nom, String des, String docVer, Integer usuMod, Date fecMod, Character estReg, Set indicadoresEvaluarPersonals) {
       this.indFicEvaId = indFicEvaId;
       this.contenidoFichaEvaluacion = contenidoFichaEvaluacion;
       this.nom = nom;
       this.des = des;
       this.docVer = docVer;
       this.usuMod = usuMod;
       this.fecMod = fecMod;
       this.estReg = estReg;
       this.indicadoresEvaluarPersonals = indicadoresEvaluarPersonals;
    }
    public int getIndFicEvaId() {
        return this.indFicEvaId;
    }
    
    public void setIndFicEvaId(int indFicEvaId) {
        this.indFicEvaId = indFicEvaId;
    }
    public ContenidoFichaEvaluacion getContenidoFichaEvaluacion() {
        return this.contenidoFichaEvaluacion;
    }
    
    public void setContenidoFichaEvaluacion(ContenidoFichaEvaluacion contenidoFichaEvaluacion) {
        this.contenidoFichaEvaluacion = contenidoFichaEvaluacion;
    }

    public String getNom() {
        return this.nom;
    }
    
    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getDes() {
        return this.des;
    }
    
    public void setDes(String des) {
        this.des = des;
    }
    public String getDocVer() {
        return this.docVer;
    }
    
    public void setDocVer(String docVer) {
        this.docVer = docVer;
    }

    
   
    public Integer getUsuMod() {
        return this.usuMod;
    }
    
    public void setUsuMod(Integer usuMod) {
        this.usuMod = usuMod;
    }

    public Date getFecMod() {
        return this.fecMod;
    }
    
    public void setFecMod(Date fecMod) {
        this.fecMod = fecMod;
    }

    public Character getEstReg() {
        return this.estReg;
    }
    
    public void setEstReg(Character estReg) {
        this.estReg = estReg;
    }

    public Set<IndicadoresEvaluarPersonal> getIndicadoresEvaluarPersonals() {
        return this.indicadoresEvaluarPersonals;
    }
    
    public void setIndicadoresEvaluarPersonals(Set<IndicadoresEvaluarPersonal> indicadoresEvaluarPersonals) {
        this.indicadoresEvaluarPersonals = indicadoresEvaluarPersonals;
    }
    public IndicadorFichaEvaluacion copyFromOther(IndicadorFichaEvaluacion elemento) {
        if(elemento != null){
            this.setNom(elemento.getNom());
            this.setDes(elemento.getDes());
            this.setDocVer(elemento.getDocVer());
        }
        return this;
    }

    public int getValue() {
        return value;
    }

    public void setValue(short value) {
        this.value = value;
    }

    @Override
    public boolean equals(Object o){
        if(this == o)return true;
        else if(o!= null && o instanceof IndicadorFichaEvaluacion){
            IndicadorFichaEvaluacion ind = (IndicadorFichaEvaluacion)o;
            if(ind.getIndFicEvaId() == this.getIndFicEvaId() && ind.getNom().equals(this.getNom())) return true;
            else return false;
        }
        else{
            return false;
        }
    }
    @Override
    public String toString(){
        return "{\"id\":" + this.indFicEvaId + ",\"nom\":\"" + this.nom + 
                "\",\"des\":\"" + this.des + "\",\"docver\":\"" + this.docVer + "\"}";
    }


}


