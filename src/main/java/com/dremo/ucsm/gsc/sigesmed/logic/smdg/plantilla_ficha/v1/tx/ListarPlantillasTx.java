/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.smdg.plantilla_ficha.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.smdg.PlantillaFichaInstitucionalDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.smdg.PlantillaFichaInstitucional;
import com.dremo.ucsm.gsc.sigesmed.core.entity.smdg.PlantillaFichaInstitucional;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author Administrador
 */
public class ListarPlantillasTx implements ITransaction{
    public WebResponse execute(WebRequest wr) {        
        /*
        *  Parte para la operacion en la Base de Datos
        */        
        
//        JSONObject requestData = (JSONObject)wr.getData();
//        int orgId = requestData.getInt("orgId");
                
        List<Object[]> plantillas = null;
        PlantillaFichaInstitucionalDao plantillasDao = (PlantillaFichaInstitucionalDao)FactoryDao.buildDao("smdg.PlantillaFichaInstitucionalDao");
        
        try{
            plantillas = plantillasDao.listarPlantillas();
        
        }catch(Exception e){            
            return WebResponse.crearWebResponseError("No se pudo Listar las plantillas", e.getMessage() );
        }
        //Fin
             
        /*
        *  Repuesta Correcta
        */
        JSONArray miArray = new JSONArray();
        for(Object[] p : plantillas){
            JSONObject oResponse = new JSONObject();
            oResponse.put("plaide",p[0]);
            oResponse.put("placod",p[1]);
            oResponse.put("planom",p[2]);
            oResponse.put("platip",p[3]);
            oResponse.put("plafec",p[4]);
            miArray.put(oResponse);
        }
        
        return WebResponse.crearWebResponseExito("Se Listo correctamente",miArray);        
                
    }
}
