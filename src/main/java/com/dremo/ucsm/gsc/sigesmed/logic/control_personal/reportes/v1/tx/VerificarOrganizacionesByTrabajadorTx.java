/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.control_personal.reportes.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.logic.control_personal.configuracion.v1.tx.*;
import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.OrganizacionDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.cpe.LibroAsistenciaDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.cpe.PersonalDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Organizacion;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Trabajador;
import com.dremo.ucsm.gsc.sigesmed.core.entity.cpe.ConfiguracionControl;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import org.json.JSONArray;
import org.json.JSONObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 *
 * @author carlos
 */
public class VerificarOrganizacionesByTrabajadorTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
        
        
        String dni="";
        Integer tipoOrg;
        try{
             JSONObject requestData = (JSONObject)wr.getData();
            dni=requestData.getString("dni");
            tipoOrg=requestData.getInt("tipoOrg");
        }catch(Exception e){
            return WebResponse.crearWebResponseError("No se pudo verificar los datos", e.getMessage() );
        }

        PersonalDao personalDao = (PersonalDao)FactoryDao.buildDao("cpe.PersonalDao");
        List<Integer> organizacionesPadre=new ArrayList<>();
        while(tipoOrg<=4)
        {
            organizacionesPadre.add(tipoOrg);
            tipoOrg++;
        }
        List<Organizacion> organizaciones=null;
        try{
            organizaciones=personalDao.listarOrganizacionesActivasDelTrabajador(dni, organizacionesPadre);
        }catch(Exception e){
            return WebResponse.crearWebResponseError("No se pudo ingresar la configuracion ", e.getMessage() );
        }
        JSONArray miArray=new JSONArray();
        for(Organizacion org:organizaciones)
        {
            JSONObject oRes=new JSONObject();
            oRes.put("id", org.getOrgId());
            oRes.put("nom", org.getNom());
            miArray.put(oRes);
        }

        return WebResponse.crearWebResponseExito("Se verifico correctamente",miArray);        
        //Fin
    }
    
}

