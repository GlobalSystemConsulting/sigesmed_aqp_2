/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.cuadro_horas.plan_estudios.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.mech.PlanEstudiosDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mech.AreaModel;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mech.GradoModel;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mech.NivelModel;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mech.PlanEstudios;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import org.json.JSONObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.util.List;
import org.json.JSONArray;
/**
 *
 * @author abel
 */
public class BuscarPlanEstudiosPorOrganizacionTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
                
        int orgId = 0;        
        try{
            
            JSONObject requestData = (JSONObject)wr.getData();
            
            orgId = requestData.getInt("organizacionID");
        
        }catch(Exception e){
            System.out.println(e);
            return WebResponse.crearWebResponseError("No se pudo encontrar plan de estudios vigente, datos incorrectos", e.getMessage() );
        }
        
        /*
        *  Parte para la operacion en la Base de Datos
        */
        PlanEstudios plan = null;
        List<NivelModel> niveles = null;
        List<GradoModel> grados = null;
        List<AreaModel> areas = null;
        try{
            PlanEstudiosDao planDao = (PlanEstudiosDao)FactoryDao.buildDao("mech.PlanEstudiosDao");
            plan = planDao.buscarVigentePorOrganizacion(orgId);
            if(plan!=null){
                niveles = planDao.buscarNiveles(plan.getPlaEstId());
                grados = planDao.buscarGrados(plan.getPlaEstId());
                
                if(niveles != null && niveles.size()>0){
                    areas = planDao.buscarAreas(niveles.get(0).disenoCurID);
                    areas.addAll(planDao.buscarTalleres(niveles.get(0).disenoCurID));
                }
                    
            }
            
        }catch(Exception e){
            System.out.println(e);
            return WebResponse.crearWebResponseError("No se pudo encontrar plan de estudios vigente", e.getMessage() );
        }
        
        if(plan != null){
        
            JSONObject res = new JSONObject();
            res.put("planID", plan.getPlaEstId());
            res.put("anoEscolar", plan.getAñoEsc());
            res.put("nombre", plan.getNom());

            JSONArray aNiv = new JSONArray();
            int disenoID = 0;
            for(NivelModel n : niveles){
                JSONObject oNiv = new JSONObject();
                oNiv.put("planNivelID", n.ID );
                oNiv.put("descripcion", n.descripcion);
                oNiv.put("nivelID", n.nivelID );
                oNiv.put("jornadaID", n.jornadaID );
                oNiv.put("jornada", n.jornada );                
                oNiv.put("turnoID", ""+n.turnoID );
                oNiv.put("periodoID", ""+n.periodoID);
                
                disenoID = n.disenoCurID;                
                aNiv.put(oNiv);
            }
            res.put("diseñoCurricularID", disenoID);
            
            JSONArray aGra = new JSONArray();
            for(GradoModel g : grados){
                JSONObject oGra = new JSONObject();
                oGra.put("gradoID", g.gradoID );
                oGra.put("grado", g.grado );
                oGra.put("nivelID", g.nivelID );
                oGra.put("meta", g.secciones );
                
                aGra.put(oGra);
            }
            JSONArray aAre = new JSONArray();
            for(AreaModel a : areas){
                JSONObject oAre = new JSONObject();
                oAre.put("areaID", a.areaID );
                oAre.put("area", a.area );
                oAre.put("gradoID", a.gradoID );
                oAre.put("grado", a.grado );                
                aAre.put(oAre);
            }            
            
            res.put("niveles",aNiv);
            res.put("grados",aGra);
            res.put("areas",aAre);

            return WebResponse.crearWebResponseExito("Se encontro el plan de estudios vigente",res);
        }
        else{
            return WebResponse.crearWebResponseError("No se encontro el plan de estudios vigente");
        }
    }    
    
    
}
