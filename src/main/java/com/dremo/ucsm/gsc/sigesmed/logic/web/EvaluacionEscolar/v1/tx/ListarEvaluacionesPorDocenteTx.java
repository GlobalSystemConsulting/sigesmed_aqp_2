/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.web.EvaluacionEscolar.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.web.EvaluacionEscolarDao;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import org.json.JSONArray;
import org.json.JSONObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import com.dremo.ucsm.gsc.sigesmed.core.entity.web.EvaluacionEscolar;
import com.dremo.ucsm.gsc.sigesmed.core.entity.web.PreguntaEvaluacion;
import java.text.SimpleDateFormat;
import java.util.List;

/**
 *
 * @author abel
 */
public class ListarEvaluacionesPorDocenteTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
        
        /*
        *   Parte para la lectura, verificacion y validacion de datos
        */
        int planID = 0;
        int docenteID = 0;
        try{
            
            JSONObject requestData = (JSONObject)wr.getData();
            
            planID = requestData.getInt("planID");
            docenteID = requestData.getInt("docenteID");
        
        }catch(Exception e){
            System.out.println(e);
            return WebResponse.crearWebResponseError("No se pudo Listar las evaluaciones por docente", e.getMessage() );
        }
        //Fin        
        
        /*
        *  Parte para la operacion en la Base de Datos
        */
        List<EvaluacionEscolar> evaluaciones = null;
        EvaluacionEscolarDao evaluacionDao = (EvaluacionEscolarDao)FactoryDao.buildDao("web.EvaluacionEscolarDao");
        try{
            evaluaciones = evaluacionDao.buscarPorDocente(planID,docenteID);
        
        }catch(Exception e){
            System.out.println("No se pudo Listar las evaluaciones por docente \n"+e);
            return WebResponse.crearWebResponseError("No se pudo Listar las evaluaciones por docente", e.getMessage() );
        }
        //Fin        
        /*
        *  Repuesta Correcta
        */
        JSONArray miArray = new JSONArray();
        SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        int i = 0;
        for(EvaluacionEscolar evaluacion:evaluaciones ){
            JSONObject oResponse = new JSONObject();
            oResponse.put("evaluacionID",evaluacion.getEvaEscId() );
            oResponse.put("nombre",evaluacion.getNom());
            oResponse.put("descripcion",evaluacion.getDes());
            
            oResponse.put("calificacion",evaluacion.getSisCal());
            oResponse.put("automatico",evaluacion.getAut());
            oResponse.put("aleatorio",evaluacion.getOrdAle());
            oResponse.put("intentos",evaluacion.getNumInt());
            
            oResponse.put("docenteID",evaluacion.getUsuMod());
            
            oResponse.put("planID",evaluacion.getPlaEstId());
            oResponse.put("gradoID",evaluacion.getGraId());
            oResponse.put("seccionID",""+evaluacion.getSecId());
            oResponse.put("areaID",evaluacion.getAreCurId());
            
            oResponse.put("estado",""+evaluacion.getEstado());
            
            JSONArray aPre = new JSONArray();
            for(PreguntaEvaluacion p : evaluacion.getPreguntas()){
                JSONObject oMet = new JSONObject();
                oMet.put("preguntaID", p.getPreEvaId() );
                oMet.put("titulo", p.getTitulo() );
                oMet.put("pregunta", p.getPregunta() );
                oMet.put("respuesta", p.getRespuesta() );
                oMet.put("alternativa", p.getAlternativa() );
                oMet.put("orden", p.getOrden() );
                oMet.put("puntos", p.getPuntos() );
                oMet.put("tiempo", p.getTiempo() );
                oMet.put("tipo", ""+p.getTipo() );
                aPre.put(oMet);
            }
            oResponse.put("preguntas", aPre);
            
            oResponse.put("fechaEnvio",evaluacion.getFecEnv());
            if(evaluacion.getFecIni()!=null)
                oResponse.put("fechaInicio",sf.format(evaluacion.getFecIni()) );
            if(evaluacion.getFecFin()!=null)
                oResponse.put("fechaFin",sf.format(evaluacion.getFecFin()) );
            
            oResponse.put("i",i++);
            
            miArray.put(oResponse);
        }
        
        return WebResponse.crearWebResponseExito("Las evaluaciones se listaron correctamente",miArray);        
        //Fin
    }
    
}

