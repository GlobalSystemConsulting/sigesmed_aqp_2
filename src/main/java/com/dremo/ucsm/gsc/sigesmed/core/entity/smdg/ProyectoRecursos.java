/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.entity.smdg;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Administrador
 */
@Entity
@Table(name = "proyecto_recursos", schema = "institucional")

public class ProyectoRecursos implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "pru_id")
    private Integer pruId;
    @Column(name = "pru_nom")
    private String pruNom;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "pru_cos")
    private Double pruCos;
    @Column(name = "fec_mod")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fecMod;
    @Column(name = "usu_mod")
    private Integer usuMod;
    @Column(name = "est_reg")
    private String estReg = "A";
    
    @JoinColumn(name = "pac_id", referencedColumnName = "pac_id")
    @ManyToOne(fetch=FetchType.LAZY)
    private ProyectoActividades actividad;
    
    public ProyectoRecursos() {
    }

    public ProyectoRecursos(Integer pruId) {
        this.pruId = pruId;
    }

    public ProyectoRecursos(String pruNom, Double pruCos, ProyectoActividades actividad) {   
        this.pruNom = pruNom;
        this.pruCos = pruCos;        
        this.actividad = actividad;
    }
    
    public ProyectoRecursos(Integer pruId, String pruNom, Double pruCos, ProyectoActividades actividad) {   
        this.pruId = pruId;
        this.pruNom = pruNom;
        this.pruCos = pruCos;        
        this.actividad = actividad;
    }
    
    public Integer getPruId() {
        return pruId;
    }

    public void setPruId(Integer pruId) {
        this.pruId = pruId;
    }

    public String getPruNom() {
        return pruNom;
    }

    public void setPruNom(String pruNom) {
        this.pruNom = pruNom;
    }

    public Double getPruCos() {
        return pruCos;
    }

    public void setPruCos(Double pruCos) {
        this.pruCos = pruCos;
    }

    public Date getFecMod() {
        return fecMod;
    }

    public void setFecMod(Date fecMod) {
        this.fecMod = fecMod;
    }

    public Integer getUsuMod() {
        return usuMod;
    }

    public void setUsuMod(Integer usuMod) {
        this.usuMod = usuMod;
    }

    public String getEstReg() {
        return estReg;
    }

    public void setEstReg(String estReg) {
        this.estReg = estReg;
    }

    public ProyectoActividades getActividad() {
        return actividad;
    }

    public void setActividad(ProyectoActividades actividad) {
        this.actividad = actividad;
    }

//    @Override
//    public int hashCode() {
//        int hash = 0;
//        hash += (pruId != null ? pruId.hashCode() : 0);
//        return hash;
//    }
//
//    @Override
//    public boolean equals(Object object) {
//        // TODO: Warning - this method won't work in the case the id fields are not set
//        if (!(object instanceof ProyectoRecursos)) {
//            return false;
//        }
//        ProyectoRecursos other = (ProyectoRecursos) object;
//        if ((this.pruId == null && other.pruId != null) || (this.pruId != null && !this.pruId.equals(other.pruId))) {
//            return false;
//        }
//        return true;
//    }
//
//    @Override
//    public String toString() {
//        return "entidades.smdg.ProyectoRecursos[ pruId=" + pruId + " ]";
//    }
    
}
