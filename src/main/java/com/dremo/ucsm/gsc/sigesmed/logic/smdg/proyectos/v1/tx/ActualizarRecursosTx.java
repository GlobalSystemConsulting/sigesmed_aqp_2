/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.smdg.proyectos.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.smdg.ProyectoRecursosDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.smdg.ProyectoActividades;
import com.dremo.ucsm.gsc.sigesmed.core.entity.smdg.ProyectoRecursos;
import com.dremo.ucsm.gsc.sigesmed.core.entity.smdg.Proyectos;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONObject;

/**
 *
 * @author Administrador
 */
public class ActualizarRecursosTx implements ITransaction{
    @Override
    public WebResponse execute(WebRequest wr) {
                
        /*
        *   Parte para la lectura, verificacion y validacion de datos
        */
        
        JSONObject requestData = (JSONObject)wr.getData();
//        JSONArray recursoes = requestData.getJSONArray("recursoes");
        
        int recid = requestData.getInt("recid");
        int pacid = requestData.getInt("pacid");
        String recnom = requestData.optString("recnom");
        double reccos = requestData.optInt("reccos");
                        
        ProyectoRecursos recurso = new ProyectoRecursos(recid, recnom, reccos, new ProyectoActividades(pacid));
        
        //Fin
        
        /*
        *  Parte para la operacion en la Base de Datos
        */
        ProyectoRecursosDao recursoDao = (ProyectoRecursosDao)FactoryDao.buildDao("smdg.ProyectoRecursosDao");
        try{
            recursoDao.update(recurso);
        
        }catch(Exception e){
            System.out.println("No se pudo actualizar el recurso\n"+e);
            return WebResponse.crearWebResponseError("No se pudo actualizar el recurso", e.getMessage());
        }
        //Fin
        
        /*
        *  Repuesta Correcta
        */        
        return WebResponse.crearWebResponseExito("El recurso se actualizo correctamente");
        //Fin
    }
}
