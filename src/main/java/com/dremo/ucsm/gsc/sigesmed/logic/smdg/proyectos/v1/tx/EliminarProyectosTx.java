/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.smdg.proyectos.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.smdg.ProyectosDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.smdg.Proyectos;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import org.json.JSONObject;

/**
 *
 * @author Administrador
 */
public class EliminarProyectosTx implements ITransaction{
    @Override
    public WebResponse execute(WebRequest wr) {
        
        /*
        *   Parte para la lectura, verificacion y validacion de datos
        */
        int proid = 0;
        try{
            JSONObject requestData = (JSONObject)wr.getData();
            proid = requestData.getInt("proid");
        
        }catch(Exception e){
            System.out.println(e);
            return WebResponse.crearWebResponseError("No se pudo eliminar, datos incorrectos", e.getMessage() );
        }
        //Fin
        
        /*
        *  Parte para la operacion en la Base de Datos
        */
        ProyectosDao proDao = (ProyectosDao)FactoryDao.buildDao("smdg.ProyectosDao");
        try{
            proDao.delete(new Proyectos(proid));        
        }catch(Exception e){
            System.out.println("No se pudo eliminar el Proyecto\n"+e);
            return WebResponse.crearWebResponseError("No se pudo eliminar el Proyecto", e.getMessage() );
        }
        //Fin
        
        /*
        *  Repuesta Correcta
        */        
        return WebResponse.crearWebResponseExito("El Proyecto se elimino correctamente");
        //Fin
    }
}
