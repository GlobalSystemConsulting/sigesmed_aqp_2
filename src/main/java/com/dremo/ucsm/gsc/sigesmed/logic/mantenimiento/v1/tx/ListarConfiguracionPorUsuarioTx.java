/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.mantenimiento.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.mnt.AjustePaginaWebDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Usuario;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mnt.AjustePaginaWeb;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import org.json.JSONObject;

/**
 *
 * @author Administrador
 */
public class ListarConfiguracionPorUsuarioTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
        int usuarioID=0;
        try{
            JSONObject requestData=(JSONObject) wr.getData();
            usuarioID=requestData.getInt("usuarioID");
        }catch(Exception e){
            System.out.println("No se pudo leer los datos"+ e.getMessage());
            return WebResponse.crearWebResponseError("No se pudo leer los datos", e.getMessage());
        }
        AjustePaginaWebDao ajusteDao=(AjustePaginaWebDao)FactoryDao.buildDao("mnt.AjustePaginaWebDao");
        
        AjustePaginaWeb ajuste=ajusteDao.obtenerAjustePorIdUsuario(new Usuario(usuarioID));
        
        JSONObject rpta=new JSONObject();
        if(ajuste!=null){
            rpta.put("ladoIzq",ajuste.getAjusteIzq());
            rpta.put("ladoDer",ajuste.getAjusteDer());
            rpta.put("color",ajuste.getAjusteColor());
            return WebResponse.crearWebResponseExito("Se obtuvo la configuracion", rpta);
        }
        return WebResponse.crearWebResponseError("No se pudo obtener la configuracion");
    }
    
}
