/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.sma;

import com.dremo.ucsm.gsc.sigesmed.core.dao.GenericDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.GenericDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.dao.sma.ItemsDao;
import com.dremo.ucsm.gsc.sigesmed.core.datastore.HibernateUtil;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sma.Items;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author Administrador
 */
public class ItemsDaoHibernate extends GenericDaoHibernate<Items> implements ItemsDao{
    @Override
    public String buscarUltimoCodigo() {
        String codigo = null;

        return codigo;
    }
    @Override
    public Items obtenerPlantillaItems(int indId){
        Session session = HibernateUtil.getSessionFactory().openSession();
        Items item= null;
        Transaction t = session.beginTransaction();
        try{                                        
//            plantilla = (PlantillaFichaInstitucional)session.get(PlantillaFichaInstitucional.class, plaId);
            String hql = "SELECT g FROM Items i "                   
                    + "WHERE i.iteId = " + indId;
            Query query = session.createQuery(hql);
            query.setMaxResults(1);
            item = (Items)query.uniqueResult();            
            t.commit();
                      
        }catch(Exception e){
            t.rollback();
            System.out.println("No se pudo obtener el item \\n "+ e.getMessage());
            throw new UnsupportedOperationException("No se pudo obtener el item \\n "+ e.getMessage());            
        }
        finally{
            session.close();
        }
        
        return item;
    }
}
