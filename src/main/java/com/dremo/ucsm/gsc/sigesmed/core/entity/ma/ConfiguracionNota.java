/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.entity.ma;
import javax.persistence.*;
import java.util.Date;
/**
 *
 * @author abel
 */
@Entity
@Table(name = "configuracion_nota", schema = "pedagogico")
public class ConfiguracionNota {
    
    @Id
    @Column(name = "config_not_id",nullable = false, unique = true)
    private String config_not_id;
    
    @Column(name="not_min_num")
    private String not_min_num;
    
    @Column(name="not_max_num")
    private String not_max_num;
    
    @Column(name="fec_mod")
    private Date fec_mod;
    
    @Column(name="usu_mod")
    private int usu_mod;
    
    @Column(name="est_reg")
    private char estReg;
    
    @Column(name="des")
    private String des;

    public void setConfig_not_id(String config_not_id) {
        this.config_not_id = config_not_id;
    }

    public String getConfig_not_id() {
        return config_not_id;
    }


    public void setNot_min_num(String not_min_num) {
        this.not_min_num = not_min_num;
    }

    public void setNot_max_num(String not_max_num) {
        this.not_max_num = not_max_num;
    }

    public void setFec_mod(Date fec_mod) {
        this.fec_mod = fec_mod;
    }

    public void setUsu_mod(int usu_mod) {
        this.usu_mod = usu_mod;
    }

    public void setEst_reg(char est_reg) {
        this.estReg = est_reg;
    }

    public void setDes(String des) {
        this.des = des;
    }

   
    public String getNot_min_num() {
        return not_min_num;
    }

    public String getNot_max_num() {
        return not_max_num;
    }

    public Date getFec_mod() {
        return fec_mod;
    }

    public int getUsu_mod() {
        return usu_mod;
    }

    public char getEst_reg() {
        return estReg;
    }

    public String getDes() {
        return des;
    }

    public ConfiguracionNota(String config_not_id, String not_min_num, String not_max_num, Date fec_mod, int usu_mod, char est_reg, String des) {
        this.config_not_id = config_not_id;
        this.not_min_num = not_min_num;
        this.not_max_num = not_max_num;
        this.fec_mod = fec_mod;
        this.usu_mod = usu_mod;
        this.estReg = est_reg;
        this.des = des;
    }

    public ConfiguracionNota() {
    }
    
    
    
    
    
}
