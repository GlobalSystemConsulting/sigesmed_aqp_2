/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.web.BandejaMensaje.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.web.MensajeElectronicoDao;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import org.json.JSONObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;

/**
 *
 * @author abel
 */
public class ActualizarBandejaMensajeTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
                
        /*
        *   Parte para la lectura, verificacion y validacion de datos
        */
        int bandejaMensajeID = 0;
        try{
            JSONObject requestData = (JSONObject)wr.getData();
            bandejaMensajeID = requestData.getInt("bandejaMensajeID");
        
        }catch(Exception e){
            System.out.println("No se pudo actualizar el estado del mensaje, datos incorrectos\n"+e);
            return WebResponse.crearWebResponseError("No se pudo actualizar el estado del mensaje, datos incorrectos", e.getMessage() );
        }
        //Fin
        
        /*
        *  Parte para la operacion en la Base de Datos
        */
        MensajeElectronicoDao bandejaDao = (MensajeElectronicoDao)FactoryDao.buildDao("web.MensajeElectronicoDao");
        try{
            bandejaDao.verMensaje(bandejaMensajeID);
        
        }catch(Exception e){
            System.out.println("No se pudo marcar como visto la alerta\n"+e);
            return WebResponse.crearWebResponseError("No se pudo actualizar el estado del mensaje", e.getMessage() );
        }
        //Fin
        
        /*
        *  Repuesta Correcta
        */
        JSONObject oResponse = new JSONObject();
        oResponse.put("estado",""+Mensaje.ESTADO_VISTO);
        return WebResponse.crearWebResponseExito("el mensaje se vio correctamente",oResponse);
        //Fin
    }
    
}
