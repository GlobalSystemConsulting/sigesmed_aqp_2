/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.tramite_documentario.documento_estado_prioridad.v1.core;

import com.dremo.ucsm.gsc.sigesmed.core.service.WebComponent;
import com.dremo.ucsm.gsc.sigesmed.core.service.constantes.Sigesmed;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.IComponentRegister;
import com.dremo.ucsm.gsc.sigesmed.logic.tramite_documentario.documento_estado_prioridad.v1.tx.ActualizarEstadoExpedienteTx;
import com.dremo.ucsm.gsc.sigesmed.logic.tramite_documentario.documento_estado_prioridad.v1.tx.ActualizarPrioridadExpedienteTx;
import com.dremo.ucsm.gsc.sigesmed.logic.tramite_documentario.documento_estado_prioridad.v1.tx.ActualizarTipoDocumentoTx;
import com.dremo.ucsm.gsc.sigesmed.logic.tramite_documentario.documento_estado_prioridad.v1.tx.EliminarEstadoExpedienteTx;
import com.dremo.ucsm.gsc.sigesmed.logic.tramite_documentario.documento_estado_prioridad.v1.tx.EliminarPrioridadExpedienteTx;
import com.dremo.ucsm.gsc.sigesmed.logic.tramite_documentario.documento_estado_prioridad.v1.tx.EliminarTipoDocumentoTx;
import com.dremo.ucsm.gsc.sigesmed.logic.tramite_documentario.documento_estado_prioridad.v1.tx.InsertarEstadoExpedienteTx;
import com.dremo.ucsm.gsc.sigesmed.logic.tramite_documentario.documento_estado_prioridad.v1.tx.InsertarPrioridadExpedienteTx;
import com.dremo.ucsm.gsc.sigesmed.logic.tramite_documentario.documento_estado_prioridad.v1.tx.InsertarTipoDocumentoTx;
import com.dremo.ucsm.gsc.sigesmed.logic.tramite_documentario.documento_estado_prioridad.v1.tx.ListarEstadoExpedienteTx;
import com.dremo.ucsm.gsc.sigesmed.logic.tramite_documentario.documento_estado_prioridad.v1.tx.ListarPrioridadExpedienteTx;
import com.dremo.ucsm.gsc.sigesmed.logic.tramite_documentario.documento_estado_prioridad.v1.tx.ListarTipoDocumentoTx;

/**
 *
 * @author abel
 */
public class ComponentRegister implements IComponentRegister{
    @Override
    public WebComponent createComponent(){
        //Asiganando el modulo al que pertenece
        WebComponent component = new WebComponent(Sigesmed.MODULO_TRAMITE_DOCUMENTARIO);        
        
        //Registrando el Nombre del componente
        component.setName("tramite_datos");
        //Version del componente
        component.setVersion(1);
        //Lista de operaciones de logica, propias del componente        
        component.addTransactionPOST("insertarTipoDocumento", InsertarTipoDocumentoTx.class);
        component.addTransactionGET("listarTipoDocumentos", ListarTipoDocumentoTx.class);
        component.addTransactionDELETE("eliminarTipoDocumento", EliminarTipoDocumentoTx.class);
        component.addTransactionPUT("actualizarTipoDocumento", ActualizarTipoDocumentoTx.class);
        
        component.addTransactionPOST("insertarEstadoExpediente", InsertarEstadoExpedienteTx.class);
        component.addTransactionGET("listarEstadoExpedientes", ListarEstadoExpedienteTx.class);
        component.addTransactionDELETE("eliminarEstadoExpediente", EliminarEstadoExpedienteTx.class);
        component.addTransactionPUT("actualizarEstadoExpediente", ActualizarEstadoExpedienteTx.class);
        
        component.addTransactionPOST("insertarPrioridadExpediente", InsertarPrioridadExpedienteTx.class);
        component.addTransactionGET("listarPrioridadExpedientes", ListarPrioridadExpedienteTx.class);
        component.addTransactionDELETE("eliminarPrioridadExpediente", EliminarPrioridadExpedienteTx.class);
        component.addTransactionPUT("actualizarPrioridadExpediente", ActualizarPrioridadExpedienteTx.class);
        
        return component;
    }
}
