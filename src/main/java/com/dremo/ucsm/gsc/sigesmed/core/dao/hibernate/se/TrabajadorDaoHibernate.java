/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.se;

import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.GenericDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.TrabajadorDao;
import com.dremo.ucsm.gsc.sigesmed.core.datastore.HibernateUtil;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Trabajador;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author gscadmin
 */
public class TrabajadorDaoHibernate extends GenericDaoHibernate<Trabajador> implements TrabajadorDao{

    @Override
    public Trabajador buscarPorPerId(Integer perId) {
        Trabajador trabajador = null;        
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction t = session.beginTransaction();
                
        try{   
            String hql = "SELECT t FROM com.dremo.ucsm.gsm.sigesmed.core.entity.se.Trabajador AS t "
                    + "join fetch t.persona as p "
                    + "WHERE p.perId= '" + perId + "'";
            
            Query query = session.createQuery(hql);
            query.setMaxResults(1);
            trabajador = (Trabajador)query.uniqueResult();
            t.commit();
        
        }catch(Exception e){
            t.rollback();
            System.out.println("No se pudo  \\n "+ e.getMessage());
            throw new UnsupportedOperationException("No se pudo  \\n "+ e.getMessage());            
        }
        finally{
            session.close();
        }
        return trabajador;
    }

    @Override
    public Trabajador buscarPorId(Integer traId) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Trabajador t = (Trabajador)session.get(Trabajador.class, traId);
        session.close();
        return t;
    }

    @Override
    public Trabajador buscarPorUsuarioId(int codUsr) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            String hql = "SELECT t FROM com.dremo.ucsm.gsm.sigesmed.core.entity.se.Trabajador t"
                    + " INNER JOIN FETCH t.traCar tc"
                    + " INNER JOIN t.persona p"
                    + " INNER JOIN p.usuario u"
                    + " WHERE u.usuId =:cod";
            Query query = session.createQuery(hql);
            query.setParameter("cod", codUsr);
            query.setMaxResults(1);
            return (Trabajador) query.uniqueResult();
        }catch (Exception e){
            throw e;
        }finally {
            session.close();
        }
    }
    
}
