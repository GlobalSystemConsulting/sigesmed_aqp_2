/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.control_patrimonial.catalogo_bienes.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.scp.FamiliaGenericaDAO;
import com.dremo.ucsm.gsc.sigesmed.core.entity.scp.FamiliaGenerica;

import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import org.json.JSONObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;

import org.json.JSONArray;
import com.dremo.ucsm.gsc.sigesmed.core.service.base.FileJsonObject;
import com.dremo.ucsm.gsc.sigesmed.util.BuildFile;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Administrador
 */
public class ListarFamiliaTx implements ITransaction {

    @Override
    public WebResponse execute(WebRequest wr) {
        
         JSONArray miArray = new JSONArray();
         try{
            JSONObject requestData = (JSONObject)wr.getData();
            int fam_gen_id  = requestData.getInt("fam_gen_id");
            List<FamiliaGenerica> familias_genericas = null;
            
            FamiliaGenericaDAO fam_gen_dao = (FamiliaGenericaDAO)FactoryDao.buildDao("scp.FamiliaGenericaDAO");
            familias_genericas = fam_gen_dao.listarFamilias(fam_gen_id);
             
            for(FamiliaGenerica familia : familias_genericas){
               JSONObject oResponse = new JSONObject();
               oResponse.put("fam_gen_id",familia.getCla_gen_id());
               oResponse.put("gru_gen_id",familia.getGru_gen_id());
               oResponse.put("cla_gen_id",familia.getGru_gen_id());
               oResponse.put("cod_fa",familia.getCod());
               oResponse.put("nom",familia.getNom());
               miArray.put(oResponse);
           }
            
 
         }catch(Exception e){
            System.out.println("No se pudo Listar las Familias Genericas\n"+e);
            return WebResponse.crearWebResponseError("No se pudo Listar las Familias Genericas", e.getMessage() );
         }
         
        return WebResponse.crearWebResponseExito("Se Listo las Familias Genericas Correctamente",miArray); 
    
       // throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
