package com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.carpeta;

import com.dremo.ucsm.gsc.sigesmed.core.entity.Organizacion;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.Docente;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by Administrador on 27/12/2016.
 */
@Entity(name = "DocenteCarpetaMaestro")
@Table(name = "docente_carpeta_pedagogica", schema = "pedagogico")
public class DocenteCarpeta implements java.io.Serializable{
    @Id
    @Column(name="doc_car_ped_id", unique=true, nullable=false)
    @SequenceGenerator(name = "docente_carpeta_pedagogica_doc_car_ped_id_seq", sequenceName="pedagogico.docente_carpeta_pedagogica_doc_car_ped_id_seq" )
    @GeneratedValue(generator="docente_carpeta_pedagogica_doc_car_ped_id_seq")
    private int docCarPedId;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "doc_id", nullable = false)
    private Docente docente;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "rut_con_car_id")
    private RutaContenidoCarpeta ruta;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "org_id")
    private Organizacion organizacion;
    @Column(name = "ver")
    private Integer ver;
    @Column(name = "des",length = 256)
    private String des;
    @Column(name = "com")
    private Boolean com;
    @Column(name = "est_ava")
    private Integer estAva;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "car_dig_id")
    private CarpetaPedagogica carpeta;
    @Column(name = "usu_mod")
    private Integer usuMod;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "fec_mod", length=29)
    private Date fecMod;

    @Column(name = "est_reg",length = 1)
    private Character estReg;

    public DocenteCarpeta() {
    }

    public DocenteCarpeta(Integer ver, String des, Boolean com) {
        this.ver = ver;
        this.des = des;
        this.com = com;
    }

    public int getDocCarPedId() {
        return docCarPedId;
    }

    public void setDocCarPedId(int docCarPedId) {
        this.docCarPedId = docCarPedId;
    }

    public Docente getDocente() {
        return docente;
    }

    public void setDocente(Docente docente) {
        this.docente = docente;
    }

    public RutaContenidoCarpeta getRuta() {
        return ruta;
    }

    public void setRuta(RutaContenidoCarpeta ruta) {
        this.ruta = ruta;
    }

    public Organizacion getOrganizacion() {
        return organizacion;
    }

    public void setOrganizacion(Organizacion organizacion) {
        this.organizacion = organizacion;
    }

    public Integer getVer() {
        return ver;
    }

    public void setVer(Integer ver) {
        this.ver = ver;
    }

    public String getDes() {
        return des;
    }

    public void setDes(String des) {
        this.des = des;
    }

    public Boolean getCom() {
        return com;
    }

    public void setCom(Boolean com) {
        this.com = com;
    }

    public Integer getEstAva() {
        return estAva;
    }

    public void setEstAva(Integer estAva) {
        this.estAva = estAva;
    }

    public CarpetaPedagogica getCarpeta() {
        return carpeta;
    }

    public void setCarpeta(CarpetaPedagogica carpeta) {
        this.carpeta = carpeta;
    }

    public Integer getUsuMod() {
        return usuMod;
    }

    public void setUsuMod(Integer usuMod) {
        this.usuMod = usuMod;
    }

    public Date getFecMod() {
        return fecMod;
    }

    public void setFecMod(Date fecMod) {
        this.fecMod = fecMod;
    }

    public Character getEstReg() {
        return estReg;
    }

    public void setEstReg(Character estReg) {
        this.estReg = estReg;
    }
}
