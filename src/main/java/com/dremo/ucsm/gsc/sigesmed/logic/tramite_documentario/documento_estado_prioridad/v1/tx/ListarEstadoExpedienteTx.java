/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.tramite_documentario.documento_estado_prioridad.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.std.EstadoExpedienteDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.std.EstadoExpediente;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import org.json.JSONArray;
import org.json.JSONObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.util.List;

/**
 *
 * @author abel
 */
public class ListarEstadoExpedienteTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
        
        /*
        *  Parte para la operacion en la Base de Datos
        */
        List<EstadoExpediente> estados = null;
        EstadoExpedienteDao estadoDao = (EstadoExpedienteDao)FactoryDao.buildDao("std.EstadoExpedienteDao");
        try{
            estados = estadoDao.buscarTodos(EstadoExpediente.class);
        
        }catch(Exception e){
            System.out.println("No se pudo Listar los estados del expediente\n"+e);
            return WebResponse.crearWebResponseError("No se pudo Listar los estados del expediente", e.getMessage() );
        }
        //Fin
        
        /*
        *  Repuesta Correcta
        */
        JSONArray miArray = new JSONArray();
        for(EstadoExpediente estado:estados ){
            JSONObject oResponse = new JSONObject();
            oResponse.put("estadoExpedienteID",estado.getEstExpId() );
            oResponse.put("nombre",estado.getNom());
            oResponse.put("descripcion",estado.getDes());
            oResponse.put("fecha",estado.getFecMod().toString());
            oResponse.put("estado",""+estado.getEstReg());
            miArray.put(oResponse);
        }
        
        return WebResponse.crearWebResponseExito("Se Listo correctamente las estado de expediente",miArray);        
        //Fin
    }
    
}

