

package com.dremo.ucsm.gsc.sigesmed.logic.web.Tarea.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.web.TareaEscolarDao;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import org.json.JSONArray;
import org.json.JSONObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import com.dremo.ucsm.gsc.sigesmed.core.entity.web.TareaEscolar;
import java.util.List;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.plan_curricular.TareaIndicador;
import com.dremo.ucsm.gsc.sigesmed.core.dao.maestro.plan.TareaIndicadorDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.plan_curricular.TareaSesionAprendizaje;



public class ObtenerIndicadoresTx implements ITransaction{

 @Override
    public WebResponse execute(WebRequest wr) {

        int tar_id = 0;
        TareaEscolar tarea;
        int tarea_maestro_id;
        int ses_apr_id;
        int gra_id;
        //char sec_id;
        String sec_id;
        
        JSONArray miArray = new JSONArray();
        try{
            JSONObject requestData = (JSONObject)wr.getData();
            tar_id = requestData.getInt("tareaID");

            //OBTENEMOS LA TAREA A PARTIR DEL ID DE LA TAREA
             TareaEscolarDao tareaDao = (TareaEscolarDao)FactoryDao.buildDao("web.TareaEscolarDao");
             tarea = tareaDao.obtenertarea(tar_id);
             tarea_maestro_id = tarea.getid_tar();
             gra_id = tarea.getGraId();
             sec_id = String.valueOf(tarea.getSecId());
             ses_apr_id = tarea.getSes_id();
             JSONObject oResponse = new JSONObject();
             oResponse.put("ses_id",ses_apr_id);
             oResponse.put("gra_id",gra_id);
             oResponse.put("sec_id",sec_id);
             miArray.put(oResponse);
             
             /*
             //OBTENEMOS LA SESION

             TareaIndicadorDao tar_ind_dao = (TareaIndicadorDao) FactoryDao.buildDao("maestro.plan.TareaIndicadorDao");
             TareaSesionAprendizaje tar_ses = tar_ind_dao.listar_indicadores_tarea(tarea_maestro_id);

             //OBTENEMOS LA LISTA DE INDICADORES ASOCIADOS A LA TAREA DE SESION
             List<TareaIndicador> tar_ind = tar_ses.getTarea_indicador();
             for(TareaIndicador tar : tar_ind){
                JSONObject oResponse = new JSONObject();
                oResponse.put("ind_id",tar.getInd_apr_id());
                miArray.put(oResponse);

             }
             */
             
             
             
               return WebResponse.crearWebResponseExito("Se Listo los indicadores  correctamente",miArray);


        }catch(Exception e){
                  System.out.println(e);
            return WebResponse.crearWebResponseError("No se pudo obtener los indicadores asociados a dicha tarea", e.getMessage() );
        }



    }


}