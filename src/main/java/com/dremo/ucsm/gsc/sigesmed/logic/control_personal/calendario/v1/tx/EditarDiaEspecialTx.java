/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.control_personal.calendario.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.logic.control_personal.configuracion.v1.tx.*;
import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.cpe.CalendarioDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.cpe.HorarioDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.cpe.LibroAsistenciaDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Organizacion;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Trabajador;
import com.dremo.ucsm.gsc.sigesmed.core.entity.cpe.ConfiguracionControl;
import com.dremo.ucsm.gsc.sigesmed.core.entity.cpe.DiasEspeciales;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import org.json.JSONArray;
import org.json.JSONObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author carlos
 */
public class EditarDiaEspecialTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
        
        
        HorarioDao horarioDao = (HorarioDao)FactoryDao.buildDao("cpe.HorarioDao");
        DiasEspeciales nuevoDiaEsp=null;
        DiasEspeciales diaToDelete=null;
        Integer idLast;
        
        try{
            JSONObject requestData = (JSONObject)wr.getData();
             
            JSONObject dia_=requestData.getJSONObject("dia");
            Integer org=requestData.getInt("organizacionID");
            Organizacion organizacion=new Organizacion(org);
            String descripcion=dia_.getString("des");
            DateFormat fmt = new SimpleDateFormat("dd/MM/yyyy");
            String fecha=dia_.getString("fec");
            Date fechaDia=fmt.parse(fecha);
            Integer registro=dia_.getInt("reg");
            Integer idToDelete=dia_.getInt("id");
            
            nuevoDiaEsp=new DiasEspeciales(organizacion, fechaDia, new Date(), descripcion, registro+"",wr.getIdUsuario());
            diaToDelete=new DiasEspeciales(idToDelete);
        }catch(Exception e){
            return WebResponse.crearWebResponseError("No se pudo verificar los datos del dia", e.getMessage() );
        }
        try{
            CalendarioDao calendarioDao = (CalendarioDao)FactoryDao.buildDao("cpe.CalendarioDao");
            calendarioDao.delete(diaToDelete);
            calendarioDao.insert(nuevoDiaEsp);
            idLast=calendarioDao.getLastIdDiaEspecial();
            
        }catch(Exception e){
            return WebResponse.crearWebResponseError("No se pudo registrar el dia ", e.getMessage() );
        }

        JSONObject oResponse = new JSONObject();
        oResponse.put("id", idLast);
    
        return WebResponse.crearWebResponseExito("Se Edito correctamente",oResponse);        
        //Fin
    }
    
}

