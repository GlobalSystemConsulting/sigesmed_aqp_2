/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao.sad;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sad.UnidadOrganica;
//import com.dremo.ucsm.gsc.sigesmed.core.entity.sad.Area;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Area;
import com.dremo.ucsm.gsc.sigesmed.core.dao.GenericDao;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Jeferson 
 */
public interface UnidadOrganicaDAO extends GenericDao<UnidadOrganica> {
    
    public List<UnidadOrganica> buscarPorOrganizacion(int tipoOrganizacionID);
    public List<UnidadOrganica> buscarPorCodigo(int codigo);
    public Area buscarAreaPorCodigo(int codigo);
    /*Agregado*/
    public List<UnidadOrganica> buscarPorArea(int cod_area);
}
