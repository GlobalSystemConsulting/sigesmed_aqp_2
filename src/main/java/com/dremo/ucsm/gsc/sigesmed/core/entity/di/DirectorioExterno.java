/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.entity.di;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author Administrador
 */
@Entity
@Table(name = "directorio_externo",schema="institucional")
//@XmlRootElement
//@NamedQueries({
//    @NamedQuery(name = "DirectorioExterno.findAll", query = "SELECT d FROM DirectorioExterno d"),
//    @NamedQuery(name = "DirectorioExterno.findByDexId", query = "SELECT d FROM DirectorioExterno d WHERE d.dexId = :dexId"),
//    @NamedQuery(name = "DirectorioExterno.findByDexNombre", query = "SELECT d FROM DirectorioExterno d WHERE d.dexNombre = :dexNombre"),
//    @NamedQuery(name = "DirectorioExterno.findByDexDir", query = "SELECT d FROM DirectorioExterno d WHERE d.dexDir = :dexDir"),
//    @NamedQuery(name = "DirectorioExterno.findByDexTel", query = "SELECT d FROM DirectorioExterno d WHERE d.dexTel = :dexTel"),
//    @NamedQuery(name = "DirectorioExterno.findByDexEma", query = "SELECT d FROM DirectorioExterno d WHERE d.dexEma = :dexEma"),
//    @NamedQuery(name = "DirectorioExterno.findByFecMod", query = "SELECT d FROM DirectorioExterno d WHERE d.fecMod = :fecMod"),
//    @NamedQuery(name = "DirectorioExterno.findByUsuMod", query = "SELECT d FROM DirectorioExterno d WHERE d.usuMod = :usuMod"),
//    @NamedQuery(name = "DirectorioExterno.findByEstReg", query = "SELECT d FROM DirectorioExterno d WHERE d.estReg = :estReg")})
public class DirectorioExterno implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "dex_id")
    private Integer dexId;
    @Column(name = "dex_nombre")
    private String dexNombre;
    @Column(name = "dex_dir")
    private String dexDir;
    @Column(name = "dex_tel")
    private Integer dexTel;
    @Column(name = "dex_ema")
    private String dexEma;
    @Column(name = "fec_mod")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fecMod;
    @Column(name = "usu_mod")
    private Integer usuMod;
    @Column(name = "est_reg")
    private String estReg = "A";

    public DirectorioExterno() {
    }

    public DirectorioExterno(Integer dexId) {
        this.dexId = dexId;
    }
    
    public DirectorioExterno(String dexDir, String dexNombre) {
        this.dexNombre = dexNombre;
        this.dexDir = dexDir;
    }

    public DirectorioExterno(String dexNombre, String dexDir, Integer dexTel, String dexEma) {        
        this.dexNombre = dexNombre;
        this.dexDir = dexDir;
        this.dexTel = dexTel;
        this.dexEma = dexEma;        
    }

    public DirectorioExterno(Integer dexId, String dexNombre, String dexDir, Integer dexTel, String dexEma) {
        this.dexId = dexId;
        this.dexNombre = dexNombre;
        this.dexDir = dexDir;
        this.dexTel = dexTel;
        this.dexEma = dexEma;
    }
    
    
    
    public Integer getDexId() {
        return dexId;
    }

    public void setDexId(Integer dexId) {
        this.dexId = dexId;
    }

    public String getDexNombre() {
        return dexNombre;
    }

    public void setDexNombre(String dexNombre) {
        this.dexNombre = dexNombre;
    }

    public String getDexDir() {
        return dexDir;
    }

    public void setDexDir(String dexDir) {
        this.dexDir = dexDir;
    }

    public Integer getDexTel() {
        return dexTel;
    }

    public void setDexTel(Integer dexTel) {
        this.dexTel = dexTel;
    }

    public String getDexEma() {
        return dexEma;
    }

    public void setDexEma(String dexEma) {
        this.dexEma = dexEma;
    }

    public Date getFecMod() {
        return fecMod;
    }

    public void setFecMod(Date fecMod) {
        this.fecMod = fecMod;
    }

    public Integer getUsuMod() {
        return usuMod;
    }

    public void setUsuMod(Integer usuMod) {
        this.usuMod = usuMod;
    }

    public String getEstReg() {
        return estReg;
    }

    public void setEstReg(String estReg) {
        this.estReg = estReg;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (dexId != null ? dexId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DirectorioExterno)) {
            return false;
        }
        DirectorioExterno other = (DirectorioExterno) object;
        if ((this.dexId == null && other.dexId != null) || (this.dexId != null && !this.dexId.equals(other.dexId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entidades.DirectorioExterno[ dexId=" + dexId + " ]";
    }
    
}
