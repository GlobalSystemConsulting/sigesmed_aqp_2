/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.directorio.trabajador.v1.core;

import com.dremo.ucsm.gsc.sigesmed.core.service.WebComponent;
import com.dremo.ucsm.gsc.sigesmed.core.service.constantes.Sigesmed;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.IComponentRegister;
import com.dremo.ucsm.gsc.sigesmed.logic.directorio.trabajador.v1.tx.ActualizarTrabajadorTx;
import com.dremo.ucsm.gsc.sigesmed.logic.directorio.trabajador.v1.tx.EliminarTrabajadorTx;
import com.dremo.ucsm.gsc.sigesmed.logic.directorio.trabajador.v1.tx.InsertarTrabajadorTx;
import com.dremo.ucsm.gsc.sigesmed.logic.directorio.trabajador.v1.tx.ListarEstudiantesPorOrganizacionTx;
import com.dremo.ucsm.gsc.sigesmed.logic.directorio.trabajador.v1.tx.ListarPadresPorOrganizacionTx;
import com.dremo.ucsm.gsc.sigesmed.logic.directorio.trabajador.v1.tx.ListarTrabajadoresPorOrganizacionyTipoTx;
import com.dremo.ucsm.gsc.sigesmed.logic.directorio.trabajador.v1.tx.ObtenerDatosPersonalesTx;
import com.dremo.ucsm.gsc.sigesmed.logic.directorio.trabajador.v1.tx.ReporteApoderadosTx;
import com.dremo.ucsm.gsc.sigesmed.logic.directorio.trabajador.v1.tx.ReporteDirectorioExternoTx;
import com.dremo.ucsm.gsc.sigesmed.logic.directorio.trabajador.v1.tx.ReporteEstudiantesTx;
import com.dremo.ucsm.gsc.sigesmed.logic.directorio.trabajador.v1.tx.ReporteTx;


/**
 *
 * @author ucsm
 */

public class ComponentRegister implements IComponentRegister{
    @Override
    public WebComponent createComponent(){
        //Asiganando el modulo al que pertenece
        WebComponent component = new WebComponent(Sigesmed.MODULO_DIRECTORIO);        
        
        //Registrando el Nombre del componente
        component.setName("trabajador");
        //Version del componente
        component.setVersion(1);
        //Lista de operaciones de logica, propias del componente
        component.addTransactionPOST("insertarTrabajador", InsertarTrabajadorTx.class);      
        component.addTransactionDELETE("eliminarTrabajador", EliminarTrabajadorTx.class);
        component.addTransactionPUT("actualizarTrabajador", ActualizarTrabajadorTx.class);
        component.addTransactionGET("listarTrabajadoresPorOrganizacionyTipo", ListarTrabajadoresPorOrganizacionyTipoTx.class);
        component.addTransactionGET("listarEstudiantesPorOrganizacion", ListarEstudiantesPorOrganizacionTx.class);
        component.addTransactionGET("listarPadresPorOrganizacion", ListarPadresPorOrganizacionTx.class);
        component.addTransactionGET("reporte", ReporteTx.class);
        component.addTransactionGET("reporteApoderados", ReporteApoderadosTx.class);
        component.addTransactionGET("reporteEstudiantes", ReporteEstudiantesTx.class);
        component.addTransactionGET("reporteDirectorioExterno", ReporteDirectorioExternoTx.class);               
        component.addTransactionGET("obtenerDatosPersonales", ObtenerDatosPersonalesTx.class);
        
        
        return component;
    }
}
