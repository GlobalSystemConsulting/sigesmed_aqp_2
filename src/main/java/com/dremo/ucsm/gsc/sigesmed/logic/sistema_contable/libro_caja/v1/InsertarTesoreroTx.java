/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.sistema_contable.libro_caja.v1;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.sci.LibroCajaDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Organizacion;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Persona;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sci.LibroCaja;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sci.Tesorero;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.base.FileJsonObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.constantes.Sigesmed;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import com.dremo.ucsm.gsc.sigesmed.util.BuildFile;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.json.JSONObject;

/**
 *
 * @author RE
 */
public class InsertarTesoreroTx implements ITransaction{   
    
    @Override    
    public WebResponse execute(WebRequest wr)  {
                
        /*
        *   Parte para la lectura, verificacion y validacion de datos
        */
        Object tes=null;
        Tesorero nuevoTesorero = null;
        LibroCaja libro=null;
        String datos;
        
        Object obj=null;
        FileJsonObject docTesorero= null;
        String nomDocAdj="";
        JSONObject jsonArchivo=null;
    
        try{            
             
            JSONObject requestData = (JSONObject)wr.getData();           
            int personaID = requestData.getInt("personaID"); 
            int libroID = requestData.getInt("libroID");
             datos= requestData.getString("datos");
             String fechaI = requestData.getString("fechaInicio");  
             String fechaC = requestData.getString("fechaCierre");   
            
            String rDirectoral = requestData.getString("rd");  
             //verificamos si existe un archivo adjunto
            JSONObject jDoc = (JSONObject)requestData.opt("doc");        
            
            jsonArchivo = jDoc.optJSONObject("archivo");
            
            
                   
            nuevoTesorero = new Tesorero((short)0,new LibroCaja(libroID),new Persona(personaID),new Date(fechaI),new Date(fechaC),rDirectoral,null,new Date(), wr.getIdUsuario(),'A');
           
       
            
        }catch(Exception e){
          
            return WebResponse.crearWebResponseError("No se pudo registrar, datos incorrectos", e.getMessage() );
        }
         //Fin
        
        /*
        *   Parte de Logica de Negocio    
        *
        */
        
     

        LibroCajaDao tesoreroDao = (LibroCajaDao)FactoryDao.buildDao("sci.LibroCajaDao");
        
        //Fin
        
        /*
        *  Parte para la operacion en la Base de Datos
        */
        try{
            tes=tesoreroDao.llave(Tesorero.class,"tesId");
            if(tes!=null){
                nuevoTesorero.setId((short)((short)tes+1));
            }
            else{
                nuevoTesorero.setId((short)1);
            }
            if( jsonArchivo !=null && jsonArchivo.length() > 0 ){
                           
                docTesorero = new FileJsonObject( jsonArchivo ,((int)nuevoTesorero.getId())+"_resoluciónTesorero");               
                nomDocAdj = docTesorero.getName();
                nuevoTesorero.setNomDocAdj(nomDocAdj);
            }  
            tesoreroDao.insertarTesorero(nuevoTesorero);        
        }catch(Exception e){
            return WebResponse.crearWebResponseError("No se pudo registrar el Tesorero ", e.getMessage() );
        }
        //Fin
        
        if(docTesorero !=null){
            BuildFile.buildFromBase64("contable", docTesorero.getName(), docTesorero.getData());
        }
        /*
        *  Repuesta Correcta
        */
        
        DateFormat fechaHora = new SimpleDateFormat("yyyy/MM/dd");

        JSONObject oResponse = new JSONObject();
        oResponse.put("personaID",nuevoTesorero.getPersona().getPerId());
        oResponse.put("datos",datos);      
        oResponse.put("fechaInicio",fechaHora.format(nuevoTesorero.getFecIni())); 
        oResponse.put("fechaCierre",fechaHora.format(nuevoTesorero.getFecFin()));  
        oResponse.put("rd",nuevoTesorero.getNumRes());         
        oResponse.put("nomDocAdj",nuevoTesorero.getNomDocAdj());
        oResponse.put("url",Sigesmed.UBI_ARCHIVOS+"/contable/");
        

        return WebResponse.crearWebResponseExito("El registro del Tesorero se realizo correctamente", oResponse);
        //Fin
    }    
    
    
}