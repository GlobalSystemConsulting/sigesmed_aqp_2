package com.dremo.ucsm.gsc.sigesmed.core.entity.mech;

import java.util.Date;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="area_curricular" )
public class AreaCurricular  implements java.io.Serializable {

    @Id
    @Column(name="are_cur_id", unique=true, nullable=false)
    @SequenceGenerator(name = "secuencia_areacurricular", sequenceName="area_curricular_are_cur_id_seq" )
    @GeneratedValue(generator="secuencia_areacurricular")
    private int areCurId;
    @Column(name="abr",length=4)
    private String abr;
    @Column(name="nom",length=64)
    private String nom;
    @Column(name="des", length=256)
    private String des;
    
    @Column(name="est_tal")
    private boolean esTal;
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="fec_mod", nullable=false, length=29)
    private Date fecMod;    
    @Column(name="usu_mod")
    private int usuMod;
    @Column(name="est_reg")
    private char estReg;
    
    @Column(name="dis_cur_id")
    private int disCurId;    
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="dis_cur_id",updatable = false,insertable = false)
    private DisenoCurricularMECH disenoCurricular;
    
    @OneToMany(mappedBy="area",fetch=FetchType.LAZY)
    List<DistribucionHoraArea> distribucionHoraArea;
    
    public AreaCurricular() {
    }
    public AreaCurricular(int areCurId) {
        this.areCurId = areCurId;
    }
    public AreaCurricular(int areCurId,String abr, String nom, String des,boolean esTal ,int disCurId, Date fecMod, int usuMod, char estReg) {
       this.areCurId = areCurId;
       this.abr = abr;
       this.nom = nom;
       this.des = des;
       this.disCurId = disCurId;
       this.esTal = esTal;
       this.fecMod = fecMod;
       this.usuMod = usuMod;
       this.estReg = estReg;
    }
   
     
    public int getAreCurId() {
        return this.areCurId;
    }    
    public void setAreCurId(int areCurId) {
        this.areCurId = areCurId;
    }
    
    public String getAbr() {
        return this.abr;
    }
    public void setAbr(String abr) {
        this.abr = abr;
    }
    public String getNom() {
        return this.nom;
    }
    public void setNom(String nom) {
        this.nom = nom;
    }    
    public String getDes() {
        return this.des;
    }
    public void setDes(String des) {
        this.des = des;
    }
    
    public boolean getEsTal() {
        return this.esTal;
    }
    public void setEsTal(boolean esTal) {
        this.esTal = esTal;
    }
    
    
    public Date getFecMod() {
        return this.fecMod;
    }
    public void setFecMod(Date fecMod) {
        this.fecMod = fecMod;
    }
    
    public int getUsuMod() {
        return this.usuMod;
    }
    public void setUsuMod(int usuMod) {
        this.usuMod = usuMod;
    }
    
    public char getEstReg() {
        return this.estReg;
    }
    public void setEstReg(char estReg) {
        this.estReg = estReg;
    }
    
    public int getDisCurId() {
        return this.disCurId;
    }    
    public void setDisCurId(int disCurId) {
        this.disCurId = disCurId;
    }
    
    public DisenoCurricularMECH getDisenoCurricular() {
        return this.disenoCurricular;
    }
    public void setDisenoCurricular(DisenoCurricularMECH disenoCurricular) {
        this.disenoCurricular = disenoCurricular;
    }

    public List<DistribucionHoraArea> getDistribucionHoraArea() {
        return distribucionHoraArea;
    }

    public void setDistribucionHoraArea(List<DistribucionHoraArea> distribucionHoraArea) {
        this.distribucionHoraArea = distribucionHoraArea;
    }
    
    
}


