package com.dremo.ucsm.gsc.sigesmed.logic.capacitacion.curso_capacitacion.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.capacitacion.AsistenciaParticipanteDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.capacitacion.JustificacionInasistenciaCapacitacionDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.capacitacion.AsistenciaParticipante;
import com.dremo.ucsm.gsc.sigesmed.core.entity.capacitacion.JustificacionInasistenciaCapacitacion;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.base.FileJsonObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.constantes.Sigesmed;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import com.dremo.ucsm.gsc.sigesmed.util.BuildFile;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

public class RegistrarJustificacionTx implements ITransaction {
    
    private static final Logger logger = Logger.getLogger(RegistrarJustificacionTx.class.getName());

    @Override
    public WebResponse execute(WebRequest wr) {
        try {
            JSONObject data = (JSONObject) wr.getData();
            AsistenciaParticipanteDao asistenciaParticipanteDao = (AsistenciaParticipanteDao) FactoryDao.buildDao("capacitacion.AsistenciaParticipanteDao");
            JustificacionInasistenciaCapacitacionDao justificacionInasistenciaCapacitacionDao = (JustificacionInasistenciaCapacitacionDao) FactoryDao.buildDao("capacitacion.JustificacionInasistenciaCapacitacionDao");
            AsistenciaParticipante attendance = asistenciaParticipanteDao.buscarPorId(data.getInt("asi"));
            attendance.setEstReg('P');
            asistenciaParticipanteDao.update(attendance);
            
            JSONArray array = data.getJSONArray("adj");
            JSONArray attachments = new JSONArray();
            
            for (int i = 0; i < array.length(); i++) {
                JustificacionInasistenciaCapacitacion justificacion = new JustificacionInasistenciaCapacitacion(attendance, "Nombre_archivo");
                justificacionInasistenciaCapacitacionDao.insert(justificacion);
                
                FileJsonObject file = new FileJsonObject(array.getJSONObject(i).optJSONObject("arc"),
                        "doc_" + data.getInt("usuCod") + "_jus_" + justificacion.getJusInaCapId() + "_" + array.getJSONObject(i).getString("nom"));
                BuildFile.buildFromBase64(HelpTraining.attachments_Justification, file.getName(), file.getData());
                justificacion.setNom(file.getName());
                justificacionInasistenciaCapacitacionDao.update(justificacion);
                
                JSONObject object = new JSONObject();
                object.put("cod", justificacion.getJusInaCapId());
                
                String nomFile = justificacion.getNom();
                nomFile = nomFile.substring(nomFile.lastIndexOf("_jus_") + 6);
                nomFile = nomFile.substring(nomFile.indexOf("_") + 1);
                nomFile = nomFile.substring(0, nomFile.lastIndexOf("."));
                
                object.put("nom", nomFile);
                object.put("url", data.getString("url") + Sigesmed.UBI_ARCHIVOS + HelpTraining.attachments_J_Address + justificacion.getNom());

                attachments.put(object);
            }
            
            return WebResponse.crearWebResponseExito("Los adjuntos fueron creados correctamente", WebResponse.OK_RESPONSE).setData(attachments);
        } catch (Exception e) {
            logger.log(Level.SEVERE, "RegistrarJustificacionTx", e);
            return WebResponse.crearWebResponseError("No se pudo registrar los adjuntos", WebResponse.BAD_RESPONSE);
        }        
    }
}