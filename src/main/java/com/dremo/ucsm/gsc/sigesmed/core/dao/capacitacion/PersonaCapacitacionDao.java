package com.dremo.ucsm.gsc.sigesmed.core.dao.capacitacion;

import com.dremo.ucsm.gsc.sigesmed.core.dao.GenericDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.capacitacion.Persona;
import java.util.List;

public interface PersonaCapacitacionDao extends GenericDao<Persona> {
    List<Persona> listarPersonasCapacitacion();
    Persona buscarPorDni(String dni);
    Persona buscarPorId(int codPer);
}
