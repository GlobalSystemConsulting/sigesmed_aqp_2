package com.dremo.ucsm.gsc.sigesmed.logic.maestro.curriculabanco.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.maestro.plan.AreaCurricularDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.maestro.plan.CompetenciaAprendizajeDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.plan_curricular.CompetenciaAprendizaje;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.plan_curricular.TipoCurriculaEnum;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import org.json.JSONObject;

import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by Administrador on 14/10/2016.
 */
public class EditarCompetenciaBancoTx implements ITransaction {
    private static final Logger logger = Logger.getLogger(EditarCompetenciaBancoTx.class.getName());
    @Override
    public WebResponse execute(WebRequest wr) {
        JSONObject data = (JSONObject) wr.getData();

        return editarCompetencia(data);
    }

    private WebResponse editarCompetencia(JSONObject data) {
        try{
            CompetenciaAprendizajeDao competenciaDao = (CompetenciaAprendizajeDao) FactoryDao.buildDao("maestro.plan.CompetenciaAprendizajeDao");
            CompetenciaAprendizaje competencia = competenciaDao.buscarCompetenciPorCodigo(data.getInt("cod"));
            competencia.setNom(data.optString("nom",competencia.getNom()));
            competencia.setDes(data.optString("des",competencia.getDes()));
            competencia.setFecMod(new Date());
            competencia.setDcn(TipoCurriculaEnum.valueOf(data.optString("cur")));

            if(data.optInt("are",-1) != -1){
                AreaCurricularDao areaDao = (AreaCurricularDao) FactoryDao.buildDao("maestro.plan.AreaCurricularDao");
                competencia.setArea(areaDao.buscarPorId(data.getInt("are")));
            }else{
                competencia.setArea(null);
            }
            competenciaDao.update(competencia);
            return  WebResponse.crearWebResponseExito("Se edito correctamente los datos",WebResponse.OK_RESPONSE);
        }catch (Exception e){
            logger.log(Level.SEVERE,"editarcompetencia",e);
            return  WebResponse.crearWebResponseError("Error al editar los datos de la competencia",e.getMessage());
        }
    }
}
