package com.dremo.ucsm.gsc.sigesmed.logic.maestro.curriculabanco.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.maestro.plan.AreaCurricularDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.maestro.plan.DisenoCurricularDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.plan_curricular.AreaCurricular;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import org.json.JSONObject;

import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by Administrador on 19/10/2016.
 */
public class EditarAreaCurricularTx implements ITransaction{
    private static final Logger logger = Logger.getLogger( EditarAreaCurricularTx.class.getName());
    @Override
    public WebResponse execute(WebRequest wr) {
        JSONObject data = (JSONObject) wr.getData();
        return editarArea(data);
    }

    private WebResponse editarArea(JSONObject data) {
        try{
            AreaCurricularDao areaDao = (AreaCurricularDao) FactoryDao.buildDao("maestro.plan.AreaCurricularDao");
            DisenoCurricularDao disenoDao = (DisenoCurricularDao) FactoryDao.buildDao("maestro.plan.DisenoCurricularDao");

            AreaCurricular area = areaDao.buscarPorId(data.getInt("cod"));
            area.setNom(data.optString("nom", area.getNom()));
            area.setDes(data.optString("des", area.getDes()));
            area.setAbr(data.optString("abr",area.getAbr()));
            area.setDisenoCurr(disenoDao.buscarPorId(data.optInt("curr", area.getDisenoCurr().getDisCurId())));

            area.setFecMod(new Date());

            areaDao.update(area);
            return WebResponse.crearWebResponseExito("Exito al realizar el registro", WebResponse.OK_RESPONSE);
        }catch (Exception e){
            logger.log(Level.SEVERE,"editarCapacitacion",e);
            return WebResponse.crearWebResponseError("Error al realizar el registro",e.getMessage());
        }
    }
}
