/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.mep;

import com.dremo.ucsm.gsc.sigesmed.core.dao.mep.FichaEvaluacionPersonalDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.GenericDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.datastore.HibernateUtil;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mep.*;

import java.io.*;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.dremo.ucsm.gsc.sigesmed.core.service.ServicioREST;
import com.dremo.ucsm.gsc.sigesmed.logic.mep.v1.tx.MEP;
import org.hibernate.*;
import org.hibernate.criterion.CriteriaSpecification;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.hibernate.sql.JoinType;
import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONTokener;

/**
 *
 * @author Administrador
 */
public class FichaEvaPerslDaoHibernate extends GenericDaoHibernate<FichaEvaluacionPersonal> implements FichaEvaluacionPersonalDao{
    
    private static Logger  logger = Logger.getLogger(FichaEvaPerslDaoHibernate.class.getName());
    public FichaEvaPerslDaoHibernate(){

    }
    @Override
    public void insertFichaEvaluacion(FichaEvaluacionPersonal fev){
        List<FichaEvaluacionPersonal> fichasRegistradas = getFichas();
        if(fichasRegistradas.contains(fev)){
            fev.setFicEvaPerId(-1);
            return;
        }
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction tx = session.beginTransaction();
        try{
            /*if(!fev.getTipTra().equals(TipoRol.DOCENTE)){
                insertEscalas(fev,MEP.ESCALAS);
                insertRangosFicha(fev, MEP.RANGOS);
                insertarContenidoFichaPersonal(fev,getJSONContenidoFicha(fev.getTipTra().name()));
            }else{
                insertEscalas(fev,MEP.ESCALAS_DOCENTE);
                insertRangosFicha(fev,MEP.RANGOS_DOCENTE);
                insertContenidoFichaDocente(fev);
            }*/
            session.persist(fev);
            tx.commit();

        }catch(HibernateException e){
            tx.rollback();
            logger.log(Level.SEVERE,logger.getName() + ": insertFichaEvaluacion()",e);
        }
    }
    private void insertarContenidoFichaPersonal(FichaEvaluacionPersonal fep, JSONObject jsonContenido){
        insertContenidoTipo(fep, jsonContenido, "funciones", 'f');
        insertContenidoTipo(fep,jsonContenido,"competencias",'c');
    }
    private void insertContenidoTipo(FichaEvaluacionPersonal fep, JSONObject cont,String nomfic,char tipo){
        JSONArray jsonContenido = cont.getJSONArray(nomfic);
        for(int i = 0; i < jsonContenido.length(); i++){
            JSONObject jsonCont = jsonContenido.getJSONObject(i);
            ContenidoFichaEvaluacion objCont = new ContenidoFichaEvaluacion(tipo,
                    jsonCont.getString("nom"),new Date(),'A',fep);
            JSONArray jsonIndicadores = jsonCont.getJSONArray("indicadores");
            for(int j = 0; j < jsonIndicadores.length(); j++){
                JSONObject jsonIndicador = jsonIndicadores.getJSONObject(j);
                IndicadorFichaEvaluacion objIndicador = new IndicadorFichaEvaluacion(jsonIndicador.getString("nom"),
                        jsonIndicador.getString("des"), jsonIndicador.getString("docVer"), objCont);
                objIndicador.setEstReg('A');
                objIndicador.setFecMod(new Date());
                objCont.getIndicadorFichaEvaluacions().add(objIndicador);
            }
            fep.getContenidoFichaEvaluacions().add(objCont);
        }
    }
    public JSONObject getJSONContenidoFicha(String nombre){

        try {
            //logger.log(Level.INFO,"esta es la ruta del docuemento: {0}",ServicioREST.PATH_SIGESMED+"\\WEB-INF\\classes\\fep_otros.json");
            Reader reader = new InputStreamReader(new FileInputStream(ServicioREST.PATH_SIGESMED+ File.separator +"WEB-INF" + File.separator +"classes" + File.separator + "fep_otros.json"),"UTF-8");
            JSONArray array = new JSONArray(new JSONTokener(reader));
            for(int i = 0; i< array.length(); i++){
                JSONObject jsonFicha = array.getJSONObject(i);
                if(jsonFicha.getString("rol").equals(nombre)) return jsonFicha;
            }
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        return null;
    }

    private void insertEscalas(FichaEvaluacionPersonal fev, String[] escalas){
        for(String escala : escalas){
            String[] datosEscala = escala.split(":");
            fev.getEscalaValoracionFichas().add(new EscalaValoracionFicha(datosEscala[0],datosEscala[1],Integer.parseInt(datosEscala[2]),new Date(),'A',fev));
        }
    }
    private void insertContenidoFichaDocente(FichaEvaluacionPersonal fev){

        try {
            //getClass().getClassLoader().getResource("fep_docente.json").getFile()
            Reader reader = new InputStreamReader(new FileInputStream(ServicioREST.PATH_SIGESMED+ File.separator +"WEB-INF" + File.separator +"classes" + File.separator + "fep_docente.json"),"UTF-8");
            JSONObject obj = new JSONObject(new JSONTokener(reader));
            JSONArray array = obj.getJSONArray("dominios");
            for(int i = 0; i < array.length(); i++){
                JSONObject dominio = array.getJSONObject(i);
                JSONArray competencias = dominio.getJSONArray("competencias");
                ContenidoFichaEvaluacion padreCont = new ContenidoFichaEvaluacion(dominio.getString("tip").charAt(0),dominio.getString("nom"),new Date(),'A',fev);
                for(int j = 0; j < competencias.length();j++){
                    JSONObject competencia = competencias.getJSONObject(j);
                    JSONArray indicadores = competencia.getJSONArray("inds");
                    ContenidoFichaEvaluacion objCont = new ContenidoFichaEvaluacion(competencia.getString("tip").charAt(0),
                            competencia.getString("nom"),new Date(),'A',fev);
                    objCont.setParCont(padreCont);
                    for(int k = 0; k < indicadores.length(); k++){
                        JSONObject indicador = indicadores.getJSONObject(k);
                        objCont.getIndicadorFichaEvaluacions().add(new IndicadorFichaEvaluacion(indicador.getString("nom"),
                                indicador.getString("des"), indicador.getString("doc"), objCont));
                    }
                    fev.getContenidoFichaEvaluacions().add(objCont);
                }
            }
        } catch (FileNotFoundException e) {
            logger.log(Level.SEVERE, "error grave" ,e);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

    }
    private void insertRangosFicha(FichaEvaluacionPersonal fev, String[] rangos){
        for(String rango : rangos){
            String[] datosRango = rango.split(":");
            fev.getRangoPorcentualFichas().add(new RangoPorcentualFicha(Integer.parseInt(datosRango[0]),Integer.parseInt(datosRango[1]),datosRango[2],new Date(),'A',fev));
        }
    }

    @Override
    public ElementoFicha insertElementoFicha(int id, ElementoFicha elemento) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            FichaEvaluacionPersonal fevp = (FichaEvaluacionPersonal)session.get(FichaEvaluacionPersonal.class, id);
            if(fevp != null){
                elemento.setEstReg('A');
                elemento.setFecMod(new Date());
                Transaction miTx = session.beginTransaction();
                elemento.setFichaEvaluacionPersonal(fevp);
                session.persist(elemento);
                miTx.commit();
            } else {
                return null;
            }
        }catch(Exception e){
            logger.log(Level.SEVERE, logger.getName() + ": insertElementoFicha", e);
            return null;
        }finally{
            session.close();
        }
        return elemento;
    }

    public boolean insertIndicadorFichaEvaluacion(int idCont,IndicadorFichaEvaluacion indicador){
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            ContenidoFichaEvaluacion cfe = (ContenidoFichaEvaluacion)session.get(ContenidoFichaEvaluacion.class, idCont);
            if(cfe != null){
                indicador.setEstReg('A');
                indicador.setFecMod(new Date());
                Transaction miTx = session.beginTransaction();
                indicador.setContenidoFichaEvaluacion(cfe);
                session.persist(indicador);
                miTx.commit();
                return true;
            }else return false;
            
        }catch(HibernateException e){
            logger.log(Level.SEVERE,logger.getName() + ": insertIndicadorFichaEvaluacion()",e);
            return false;
        }
    }
    @Override
    public List<ElementoFicha> getElementos(int idFicha, Class<? extends ElementoFicha> elemento,String... variables) {
        FichaEvaluacionPersonal ficha = getFichaById(idFicha);
        if(elemento.isInstance(new ContenidoFichaEvaluacion()) && ficha.getTipTra().getCarNom().equals("Docente")) return getContenidoFichaDocente(idFicha,variables);
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            Criteria criteria = session.createCriteria(elemento);
            Criteria fCriteria = criteria.createCriteria("fichaEvaluacionPersonal");
            fCriteria.add(Restrictions.eq("ficEvaPerId", idFicha));
            if(elemento.isInstance(new ContenidoFichaEvaluacion())){
                criteria.setFetchMode("hijosContenido",FetchMode.JOIN);
                criteria.setFetchMode("indicadorFichaEvaluacions",FetchMode.JOIN);
                criteria.setFetchMode("hijosContenido.hijosContenido",FetchMode.JOIN);
                criteria.setFetchMode("hijosContenido.indicadorFichaEvaluacions",FetchMode.JOIN);
            }
            criteria.add(Restrictions.eq("estReg",'A'));
            if(variables != null && variables.length > 0) criteria.addOrder(Order.asc(variables[0]));
            criteria.setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY);
            List<ElementoFicha> elementos = criteria.list();
            return elementos;
        }catch(HibernateException e){
            logger.log(Level.SEVERE,logger.getName() + ": getElementos()" ,e);
        }finally{
            session.close();
        }
        return null;
    }
    private List<ElementoFicha> getContenidoFichaDocente(int idFicha,String... variables){
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            Criteria criteria = session.createCriteria(ContenidoFichaEvaluacion.class);
            criteria.setFetchMode("hijosContenido",FetchMode.JOIN);
            criteria.setFetchMode("indicadorFichaEvaluacions",FetchMode.JOIN);
            criteria.setFetchMode("hijosContenido.hijosContenido",FetchMode.JOIN);
            criteria.setFetchMode("hijosContenido.indicadorFichaEvaluacions",FetchMode.JOIN);
            //criteria.createAlias("fichaEvaluacionPersonal","f",JoinType.INNER_JOIN);
            Criteria fCriteria = criteria.createCriteria("fichaEvaluacionPersonal");
            fCriteria.add(Restrictions.eq("ficEvaPerId", idFicha));
            criteria.add(Restrictions.eq("estReg",'A'));
            criteria.add(Restrictions.eq("tip",'d'));
            criteria.setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY);
            List<ElementoFicha> elementos = criteria.list();
            logger.log(Level.INFO,logger.getName() + ": size() : {0}" ,elementos.size());
            return elementos;
        }catch(HibernateException e){
            logger.log(Level.SEVERE,logger.getName() + ": getContenidoFichaDocente()" ,e);
        }finally{
            session.close();
        }
        return null;
    }
    public ContenidoFichaEvaluacion getContenidoById(int idContenido){
        Session session = HibernateUtil.getSessionFactory().openSession();
        ContenidoFichaEvaluacion contenido = null;
        try{
            Criteria criteria = session.createCriteria(ContenidoFichaEvaluacion.class);
            criteria.setFetchMode("hijosContenido",FetchMode.JOIN);
            criteria.setFetchMode("indicadorFichaEvaluacions",FetchMode.JOIN);
            criteria.setFetchMode("hijosContenido.hijosContenido",FetchMode.JOIN);
            criteria.setFetchMode("hijosContenido.indicadorFichaEvaluacions",FetchMode.JOIN);
            criteria.add(Restrictions.eq("conFicEvaId",idContenido));
            contenido = (ContenidoFichaEvaluacion)criteria.uniqueResult();
        }catch (Exception e){
            logger.log(Level.SEVERE,logger.getName() + ": getContenidoFichaDocente()" ,e);
            return null;
        }
        return  contenido;
    }
    public FichaEvaluacionPersonal getFichaByCargo(String cargo){
        Session session = HibernateUtil.getSessionFactory().openSession();
        FichaEvaluacionPersonal fev  = null;
        try{
            Criteria criteria = session.createCriteria(FichaEvaluacionPersonal.class);
            criteria.setFetchMode("escalaValoracionFichas", FetchMode.JOIN);
            criteria.setFetchMode("rangoPorcentualFichas", FetchMode.JOIN);
            criteria.setFetchMode("contenidoFichaEvaluacions", FetchMode.JOIN);
            criteria.setFetchMode("contenidoFichaEvaluacions.hijosContenido", FetchMode.JOIN);
            criteria.setFetchMode("contenidoFichaEvaluacions.indicadorFichaEvaluacions", FetchMode.JOIN);
            criteria.add(Restrictions.eq("estReg",'A'));
            //criteria.add(Restrictions.like("tipTra",TipoRol.valueOf(cargo)));
            criteria.createCriteria("tipTra").add(Restrictions.eq("carNom",cargo));
            criteria.setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY);
            fev = (FichaEvaluacionPersonal)criteria.uniqueResult();
            if(cargo.toUpperCase().equals("DOCENTE")){//TipoRol.valueOf(cargo) == TipoRol.DOCENTE
                FichaEvaluacionPersonal fichaDocente = new FichaEvaluacionPersonal(fev.getFicEvaPerId(),fev.getTipTra(),
                        fev.getNom(),fev.getFecCre(),fev.getDes());
                fichaDocente.setEscalaValoracionFichas(fev.getEscalaValoracionFichas());
                fichaDocente.setRangoPorcentualFichas(fev.getRangoPorcentualFichas());
                Iterator<ContenidoFichaEvaluacion> itc = fev.getContenidoFichaEvaluacions().iterator();
                while(itc.hasNext()){
                    ContenidoFichaEvaluacion contenidoFichaEvaluacion = itc.next();
                    if(contenidoFichaEvaluacion.getTip() == 'd'){
                        fichaDocente.getContenidoFichaEvaluacions().add(contenidoFichaEvaluacion);
                    }
                }
                return fichaDocente;
            }
        }catch(HibernateException e){
            logger.log(Level.SEVERE,logger.getName() + ": getFichaByCargo",e);
            return null;
        }finally {
            session.close();
        }
        return fev;
    }
    public List<Object[]> getFichaEvaluacionDocente(){
        Session session = HibernateUtil.getSessionFactory().openSession();

        try{
            String sql = "select  f.fic_eva_per_id,f.nom,f.des,f.com,c.con_fic_eva_id,c.nom from pedagogico.ficha_evaluacion_personal f inner join pedagogico.contenido_ficha_evaluacion c on f.fic_eva_per_id = c.fic_eva_per_id\n" +
                    "  where f.tip_tra like 'Docente%' and c.tip='d' and f.est_reg='A';";
            SQLQuery query = session.createSQLQuery(sql);

            //Criteria criteria = session.createCriteria(FichaEvaluacionPersonal.class);
            //criteria.setFetchMode("escalaValoracionFichas", FetchMode.JOIN);
            //criteria.setFetchMode("rangoPorcentualFichas", FetchMode.JOIN);
            /*criteria.setFetchMode("contenidoFichaEvaluacions", FetchMode.JOIN);
            criteria.setFetchMode("contenidoFichaEvaluacions.parCont", FetchMode.JOIN);
            criteria.setFetchMode("contenidoFichaEvaluacions.hijosContenido",FetchMode.JOIN);
            criteria.createCriteria("contenidoFichaEvaluacions").add(Restrictions.eq("tip", 'd'));*/
            //criteria.setFetchMode("contenidoFichaEvaluacions",FetchMode.JOIN);
            //criteria.createAlias("fi.contenidoFichaEvaluacions","do");
            //criteria.add(Restrictions.eq("do.tip", 'd'));
            //criteria.add(Restrictions.like("tipTra",TipoRol.DOCENTE));
            //criteria.add(Restrictions.eq("estReg",'A'));
            /*criteria.createCriteria("contenidoFichaEvaluacions")
                    .setFetchMode("indicadorFichaEvaluacions", FetchMode.JOIN)
                    .setFlushMode(FlushMode.MANUAL).setReadOnly(true).setCacheable(true)
                    .add(Restrictions.eq("tip", 'd')).add(Restrictions.eq("estReg",'A'));
            criteria.add(Restrictions.eq("estReg",'A'));
            criteria.add(Restrictions.like("tipTra",TipoRol.DOCENTE));*/
            /*criteria.setFetchMode("contenidoFichaEvaluacions",FetchMode.JOIN);
            criteria.setFetchMode("contenidoFichaEvaluacions.parCont",FetchMode.JOIN);
            criteria.setFetchMode("contenidoFichaEvaluacions.hijosContenido",FetchMode.JOIN);*/
            //criteria.setFetchMode("contenidoFichaEvaluacions.indicadorFichaEvaluacions", FetchMode.JOIN);
            /*criteria.setFetchMode("contenidoFichaEvaluacions",FetchMode.JOIN);*/

            //criteria.createAlias("contenidoFichaEvaluacions","dominio",JoinType.INNER_JOIN);
            //criteria.add(Restrictions.eq("dominio.tip",'d'));
            query.addRoot( "f", FichaEvaluacionPersonal.class);

            //query.addEntity("f", FichaEvaluacionPersonal.class);
            query.addFetch("c","f","contenidoFichaEvaluacions");

            List<Object[]> fichas = query.list();
            for(Object[] tuple: fichas){
                FichaEvaluacionPersonal ficha = (FichaEvaluacionPersonal)tuple[0];
                //logger.log(Level.INFO, "ficha: {0}",ficha.toString());
                ContenidoFichaEvaluacion cont = (ContenidoFichaEvaluacion)ficha.getContenidoFichaEvaluacions().iterator().next();
                logger.log(Level.INFO, "contenido: {0}",cont.toString());
                /*if(cont.getTip()=='d')
                //logger.log(Level.INFO, "ficha: {0}",ficha.toString());
                    logger.log(Level.INFO, "contenido: {0}",cont.toString());*/
            }
            return fichas;

        }catch(HibernateException e){
            logger.log(Level.SEVERE,logger.getName() + ": getFichaByCargo",e);
            return null;
        }finally {
            session.close();
        }
    }
    @Override
    public List<FichaEvaluacionPersonal> getFichas() {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{

            Criteria query = session.createCriteria(FichaEvaluacionPersonal.class)
                    .addOrder(Order.asc("ficEvaPerId"));
            query.add(Restrictions.eq("estReg", 'A'));
            List<FichaEvaluacionPersonal> lfevp = query.list();
            for(FichaEvaluacionPersonal fep :lfevp )
                fep.verificarRelaciones();   
            return lfevp;
        }catch(Exception e){
            logger.log(Level.SEVERE,logger.getName() + ": listarPorFechaCreacion",e);
        }finally{
            session.close();
        }
        return null;
    }

    @Override
    public FichaEvaluacionPersonal getFichaById(int id) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            Criteria query = session.createCriteria(FichaEvaluacionPersonal.class);
            query.add(Restrictions.eq("ficEvaPerId",id));
            FichaEvaluacionPersonal fevp = (FichaEvaluacionPersonal)query.uniqueResult();
            //fevp.verificarRelaciones();
            return fevp;
        }catch(Exception e){
            logger.log(Level.SEVERE,logger.getName() + ": getFichaById",e);
            return null;
        }finally{
            session.close();
        }

    }
    public boolean deleteFichaEvaluacionById(int idFicha){
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction tx = session.beginTransaction();
        try{
            FichaEvaluacionPersonal fep = (FichaEvaluacionPersonal)session.get(FichaEvaluacionPersonal.class,idFicha);
            fep.setEstReg('E');
            fep.setFecMod(new Date());
            changeEstateElementosFicha(fep);
            session.update(fep);
            tx.commit();
            return true;
        }catch(Exception e){
            tx.rollback();
            logger.log(Level.SEVERE,logger.getName() + ": deleteFichaEvaluacionById",e);
            return false;
        }finally{
            session.close();
        }
    }
    public void changeEstateElementosFicha(FichaEvaluacionPersonal fep){
        Iterator<RangoPorcentualFicha> irangos = fep.getRangoPorcentualFichas().iterator();
        while(irangos.hasNext()){
            RangoPorcentualFicha rango = irangos.next();
            rango.setEstReg('E');
            rango.setFecMod(new Date());
        }
        
        Iterator<EscalaValoracionFicha> iescalas = fep.getEscalaValoracionFichas().iterator();
        while(iescalas.hasNext()){
            EscalaValoracionFicha escalas = iescalas.next();
            escalas.setEstReg('E');
            escalas.setFecMod(new Date());
        }

        Iterator<ContenidoFichaEvaluacion> icontenido = fep.getContenidoFichaEvaluacions().iterator();
        while(icontenido.hasNext()){
            ContenidoFichaEvaluacion cont = icontenido.next();
            cont.setEstReg('E');
            cont.setFecMod(new Date());
            Iterator<IndicadorFichaEvaluacion> iterind = cont.getIndicadorFichaEvaluacions().iterator();
            while(iterind.hasNext()){
                IndicadorFichaEvaluacion indicador  = iterind.next();
                indicador.setEstReg('E');
                indicador.setFecMod(new Date());
            }
        }
    }
    public boolean deleteElementoFichaById(int idElemento,Class<ElementoFicha> elemento){
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction tx = session.beginTransaction();
        try{
            ElementoFicha eficha = (ElementoFicha)session.get(elemento,idElemento);
            if(elemento.isInstance(ContenidoFichaEvaluacion.class)){
                ContenidoFichaEvaluacion contenido = (ContenidoFichaEvaluacion)eficha;
                Iterator<IndicadorFichaEvaluacion> iter = contenido.getIndicadorFichaEvaluacions().iterator();
                while(iter.hasNext()){
                    iter.next().setEstReg('E');
                    iter.next().setFecMod(new Date());
                }
            }
            eficha.setEstReg('E');
            session.update(eficha);
            tx.commit();
            return true;
        }catch(Exception e){
            tx.rollback();
            logger.log(Level.SEVERE,logger.getName() + ": deleteFichaEvaluacionById",e);
            return false;
        }finally{
            session.close();
        }
    }
    public boolean deleteIndicadorFichaById(int idIndicador){
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction tx = session.beginTransaction();
        try{
            IndicadorFichaEvaluacion indicador = (IndicadorFichaEvaluacion)session.get(IndicadorFichaEvaluacion.class,idIndicador);
            indicador.setEstReg('E');
            indicador.setFecMod(new Date());
            session.update(indicador);
            tx.commit();
            return true;
        }catch(Exception e){
            tx.rollback();
            logger.log(Level.SEVERE,logger.getName() + ": deleteIndicadorFichaById",e);
            return false;
        }finally{
            session.close();
        }
    }
    public FichaEvaluacionPersonal updateFichaEvaluacionPersonal(int idFicha, FichaEvaluacionPersonal fev){
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction tx = session.beginTransaction();
        try{
            FichaEvaluacionPersonal source = (FichaEvaluacionPersonal)session.get(FichaEvaPerslDaoHibernate.class,idFicha);
            source.copyFromOther(fev);
            source.setFecMod(new Date());
            session.update(source);
            tx.commit();
            return source;
        }catch(Exception e){
            tx.rollback();
            logger.log(Level.SEVERE,logger.getName() + ": updateFichaEvaluacionPersonal",e);
            return null;
        }finally{
            session.close();
        }
    }
    public ElementoFicha updateElementoFicha(int idElemento,ElementoFicha elemento){
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction tx = session.beginTransaction();
        try{

            ElementoFicha source = (ElementoFicha)session.get(elemento.getClass(), idElemento);
            source.copyFromOther(elemento);
            source.setFecMod(new Date());
            session.update(source);
            tx.commit();
            return elemento;
        }catch(Exception e){
            tx.rollback();
            logger.log(Level.SEVERE,logger.getName() + ": updateFichaEvaluacionPersonal",e);
            return null;
        }finally{
            session.close();
        }
    }
    public IndicadorFichaEvaluacion updateIndicadorFicha(int idIndicador, IndicadorFichaEvaluacion elemento){
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction tx = session.beginTransaction();
        try{
            IndicadorFichaEvaluacion source = (IndicadorFichaEvaluacion)session.get(IndicadorFichaEvaluacion.class,idIndicador);
            source.copyFromOther(elemento);
            source.setFecMod(new Date());
            session.update(source);
            tx.commit();
            return source;
        }catch(Exception e){
            tx.rollback();
            logger.log(Level.SEVERE,logger.getName() + ": updateIndicadorFicha",e);
            return null;
        }finally{
            session.close();
        }
    }
}
