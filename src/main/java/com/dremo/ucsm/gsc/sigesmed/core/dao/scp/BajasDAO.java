/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao.scp;
import com.dremo.ucsm.gsc.sigesmed.core.dao.GenericDao;
import java.util.Date;
import java.util.List;
import com.dremo.ucsm.gsc.sigesmed.core.entity.scp.Bajas;
import com.dremo.ucsm.gsc.sigesmed.core.entity.scp.AltasDetalle;

/**
 *
 * @author Administrador
 */
public interface BajasDAO extends GenericDao<Bajas> {
    
    public List<Bajas> listar_bajas_bienes(int org_id);
    public Bajas obtenerBaja(int cod_bie);
    public AltasDetalle alta_baja(int cod_bie);
    public int contar_bajas(int org_id);
}
