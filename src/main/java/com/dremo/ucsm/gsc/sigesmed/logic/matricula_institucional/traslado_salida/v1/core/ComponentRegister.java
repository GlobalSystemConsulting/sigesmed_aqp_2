package com.dremo.ucsm.gsc.sigesmed.logic.matricula_institucional.traslado_salida.v1.core;

import com.dremo.ucsm.gsc.sigesmed.core.service.WebComponent;
import com.dremo.ucsm.gsc.sigesmed.core.service.constantes.Sigesmed;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.IComponentRegister;
import com.dremo.ucsm.gsc.sigesmed.logic.matricula_institucional.traslado_salida.v1.tx.GenerarTrasladoSalidaTx;
import com.dremo.ucsm.gsc.sigesmed.logic.matricula_institucional.traslado_salida.v1.tx.ListarTrasladosIngresosTx;

public class ComponentRegister implements IComponentRegister {

    @Override

    public WebComponent createComponent() {
        WebComponent maComponent = new WebComponent(Sigesmed.SUBMODULO_MATRICULA_INSTITUCIONAL);
        maComponent.setName("trasladoSalida");
        maComponent.setVersion(1);
        maComponent.addTransactionGET("listarTrasladosIngresos", ListarTrasladosIngresosTx.class);
        maComponent.addTransactionPOST("generarTrasladoSalida", GenerarTrasladoSalidaTx.class);

        return maComponent;
    }
}
