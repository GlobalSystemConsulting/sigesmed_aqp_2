/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao.mpf;

import com.dremo.ucsm.gsc.sigesmed.core.dao.std.*;
import com.dremo.ucsm.gsc.sigesmed.core.dao.*;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Organizacion;
import com.dremo.ucsm.gsc.sigesmed.core.entity.ma.EstudianteAsistencia;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mpf.GradoIeEstudiante;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mpf.ListaUtiles;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.Seccion;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.plan_curricular.AreaCurricular;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mech.Grado;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mpf.AreaModel;

import com.dremo.ucsm.gsc.sigesmed.core.entity.mech.NivelModel;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mech.PlanEstudios;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mpf.AreaExonerada;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mpf.Estudiante;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mpf.Matricula;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mpf.SaludControles;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mpf.BandejaTarea;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mpf.EstudianteAsistenciaModel;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mpf.RegistroAuxiliarCompetencia;
import java.util.Date;
import java.util.List;
/**
 *
 * @author Carlos
 */
public interface EstudianteDao extends GenericDao<Matricula>{
    public List<Matricula> listarHijosDisponibles(Integer idPersona);
    public Matricula getDatosEstudiante(Long matriculaId);
    public Matricula getDatosMatricula(Long matriculaId);
    public Matricula getDatosOnlyMatricula(Long matriculaId);
    public Matricula getDatosMatriculaAndPlan(Long matriculaId);
    public List ListarTutores(Integer grado,Character seccion, Integer planEstudios); 
    public List<SaludControles> getSaludControlesByEstudiante(Estudiante estudiante);
    public List<AreaExonerada> listarCursosExonerados(Estudiante estudiante);
    public List<AreaModel> buscarAreasByPlanEstudios(int plaEstudiosId,int gradoId,char seccionId);
    public List<AreaModel> buscarAreasByPlanEstudios(int plaEstudiosId,int gradoId,char seccionId,List<Integer> exoneraciones);
    public AreaModel buscarAreasById(int planID,int gradoID,int areaId);
    public ListaUtiles listarUtilesByArea(Grado gradoId,Seccion Seccion,AreaCurricular area,Organizacion org);
    public ListaUtiles listarUtiles(Grado gradoId,Seccion Seccion,Organizacion org);
    public PlanEstudios getPlanEstudiosById(Integer id);
    public List<BandejaTarea> buscarTareasPorAlumnoAndArea(int planID, Long alumnoID, int areaId);
    public List<EstudianteAsistenciaModel> buscarAsistenciaByAlumno(Long matriculaID,Integer area,Date desde,Date hasta);
    public GradoIeEstudiante getGradoIeeActual(Matricula matricula);
    public List<RegistroAuxiliarCompetencia> listarNotasByCompetencias(GradoIeEstudiante gradoIee);
    public String getNotaAreaByPeriodo(int  gradoIee,int areaId,int etapa);
    public Matricula getEstudiante(Long persona);
    public String getNotaPromAreaFinal(int  gradoIee,int areaId);
    public String getNotaRecAreaFinal(int  gradoIee,int areaId);
    
//    List<Area>
}
