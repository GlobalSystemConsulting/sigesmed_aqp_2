/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.configuracion_inicial.modulo_sistema.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.ModuloSistemaDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.ModuloSistema;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import org.json.JSONObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.util.Date;

/**
 *
 * @author Administrador
 */
public class InsertarModuloSistemaTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
                
        /*
        *   Parte para la lectura, verificacion y validacion de datos
        */
        ModuloSistema nuevoModulo = null;
        try{
            JSONObject requestData = (JSONObject)wr.getData();
            String codigo = requestData.getString("codigo");
            String nombre = requestData.getString("nombre");
            String descripcion = requestData.getString("descripcion");
            String icono = requestData.getString("icono");
            String estado = requestData.getString("estado");
            nuevoModulo = new ModuloSistema((int)0, codigo, nombre, descripcion,icono, new Date(), 1, estado.charAt(0), null);
        
        }catch(Exception e){
            return WebResponse.crearWebResponseError("No se pudo registrar, datos incorrectos", e.getMessage() );
        }
        //Fin
        
        
        /*
        *  Parte para la operacion en la Base de Datos
        */
        ModuloSistemaDao moduloDao = (ModuloSistemaDao)FactoryDao.buildDao("ModuloSistemaDao");
        try{
            moduloDao.insert(nuevoModulo);
        
        }catch(Exception e){
            System.out.println("No se pudo registrar\n"+e);
            return WebResponse.crearWebResponseError("No se pudo registrar", e.getMessage() );
        }
        //Fin
        
        
        /*
        *  Repuesta Correcta
        */
        JSONObject oResponse = new JSONObject();
        oResponse.put("moduloID",nuevoModulo.getModSisId());
        oResponse.put("fecha",nuevoModulo.getFecMod().toString());
        return WebResponse.crearWebResponseExito("El registro del Modulo se realizo correctamente", oResponse);
        //Fin
    }
    
}
