/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.cuadro_horas.horario_escolar.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.mech.HorarioEscolarDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mech.DistribucionHoraGrado;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import org.json.JSONObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import org.json.JSONArray;
/**
 *
 * @author abel
 */
public class BuscarDocentesDisponiblesTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
                
        /*
        *   Parte para la lectura, verificacion y validacion de datos
        */
        int planID = 0;
        //int gradoID = 0;
        char diaID = ' ';
        Date desde = null;
        Date hasta = null;
        try{
            
            JSONObject r = (JSONObject)wr.getData();
            
            planID = r.getInt("planID");
            //gradoID = r.getInt("gradoID");
            diaID = r.getString("diaID").charAt(0);            
            desde = new SimpleDateFormat("H:mm").parse( r.getString("horaInicio") );
            hasta = new SimpleDateFormat("H:mm").parse( r.getString("horaFin") );
        
        }catch(Exception e){
            System.out.println(e);
            return WebResponse.crearWebResponseError("No se pudo registrar el Horario Escolar, datos incorrectos", e.getMessage() );
        }
        //Fin
        
        /*
        *  Parte para la operacion en la Base de Datos
        */
        List<DistribucionHoraGrado> distribucion = null;
        try{
            HorarioEscolarDao horarioDao = (HorarioEscolarDao)FactoryDao.buildDao("mech.HorarioEscolarDao");
            
            distribucion = horarioDao.buscarDocentesPorIntervaloHora(planID, diaID, desde, hasta);            
        }catch(Exception e){
            System.out.println(e);
            return WebResponse.crearWebResponseError("No se pudo registrar el Horario Escolar", e.getMessage() );
        }
        //Fin
        
        JSONArray miArray = new JSONArray();
        
        for(DistribucionHoraGrado d:distribucion ){
            JSONObject oResponse = new JSONObject();
            
            oResponse.put("disGradoID",d.getDisHorGraId());
            oResponse.put("hora",d.getHorAsi());
            oResponse.put("gradoID",d.getGraId());
            oResponse.put("plazaID",d.getPlaMagId());
            /*
            JSONArray aDisA = new JSONArray();
            for(DistribucionHoraArea da : d.getDistribucionAreas()){
                JSONObject oDisA = new JSONObject();
                
                oDisA.put("disAreaID", da.getDisHorAreId() );
                oDisA.put("hora", da.getHorAsi() );
                oDisA.put("areaID", da.getAreCurId() );
                oDisA.put("disGradoID", da.getDisHorGraId() );
                aDisA.put(oDisA);
            }
            oResponse.put("areas", aDisA);*/
            
            miArray.put(oResponse);
        }
        return WebResponse.crearWebResponseExito("La distribucion de las plazas se listo correctamente",miArray);
    }
}