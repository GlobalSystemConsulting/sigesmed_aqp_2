/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao.se;

import com.dremo.ucsm.gsc.sigesmed.core.dao.GenericDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.FichaEscalafonaria;
import java.util.List;

/**
 *
 * @author gscadmin
 */
public interface FichaEscalafonariaDao extends GenericDao<FichaEscalafonaria>{
    public FichaEscalafonaria obtenerDatosPersonales(int orgId, int perId);
    public FichaEscalafonaria buscarPorDNI(String perDNI);
    public List<FichaEscalafonaria> ListarxOrganizacion(int orgId);
    public FichaEscalafonaria buscarPorId(Integer ficEscId);
    public FichaEscalafonaria buscarPorUsuId(Integer usuId);
}
