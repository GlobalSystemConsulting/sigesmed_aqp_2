/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.ma.utiles.v1.tx;
import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.OrganizacionDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.UsuarioDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.ma.ArticuloEscolarDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.ma.ListaArticuloDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.ma.ListaUtilesDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Organizacion;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Usuario;
import com.dremo.ucsm.gsc.sigesmed.core.entity.ma.ArticuloEscolar;
import com.dremo.ucsm.gsc.sigesmed.core.entity.ma.ListaArticulo;
import com.dremo.ucsm.gsc.sigesmed.core.entity.ma.ListaUtiles;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.Seccion;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.plan_curricular.AreaCurricular;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mech.Grado;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mech.Nivel;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.base.EntityUtil;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONArray;
import org.json.JSONObject;
/**
 *
 * @author ucsm
 */
public class RegistrarListaUtilesTx implements ITransaction{
    
    private final static Logger logger = Logger.getLogger(RegistrarListaUtilesTx.class.getName());
    
    @Override
    public WebResponse execute(WebRequest wr) {
        System.out.print("Entro a RegistrarListaUtilesTx ... ");
        JSONObject jsonData = (JSONObject)wr.getData();
        int idOrg = jsonData.getInt("org");
        int nivLista = jsonData.getInt("niv");   
        int idUsr = jsonData.getInt("user");
        String secLista = jsonData.getString("sec");
        int graLista = jsonData.getInt("gra");
        int areLista = jsonData.getInt("are");
        System.out.print(" ver areLista " + areLista);
        JSONObject jsonLista = jsonData.getJSONObject("lis");
        JSONObject jsonAniLista = jsonLista.getJSONObject("ani");
        //JSONArray  jsonArti = jsonData.getJSONArray("art");
        JSONObject  jsonArti = jsonData.getJSONObject("art");
        
        System.out.print("jsonLista tiene : " +jsonLista.getString("tit"));
        
        return registrarLista(idOrg,idUsr,jsonLista,jsonAniLista,nivLista,graLista,secLista,areLista,jsonArti);
    }
    private WebResponse registrarLista(int idOrg, int idUsr,JSONObject jsonLista,JSONObject jsonAniList, int idNiv, int idGra, String secc, int idAre,JSONObject jsonArti){
        try{
           
            ListaUtilesDao listaUtilesDao = (ListaUtilesDao) FactoryDao.buildDao("ma.ListaUtilesDao");
            OrganizacionDao organizacionDao = (OrganizacionDao)FactoryDao.buildDao("OrganizacionDao");
            UsuarioDao usuarioDao = (UsuarioDao) FactoryDao.buildDao("UsuarioDao");
            ListaArticuloDao listaArticuloDao = (ListaArticuloDao) FactoryDao.buildDao("ma.ListaArticuloDao");
            
            Organizacion organizacion = organizacionDao.buscarConTipoOrganizacionYPadre(idOrg);
            Usuario usuario = usuarioDao.buscarPorId(idUsr);
            Nivel nivel = listaUtilesDao.buscarNivelPorId(idNiv);
            Grado grado = listaUtilesDao.buscarGradoPorId(idGra);
            AreaCurricular area = listaUtilesDao.buscarAreaCurricularPorId(idAre);
            char idSec = secc.charAt(0);
            Seccion seccion = listaUtilesDao.buscarSeccionPorId(idSec);
            
            String tipoLi = jsonLista.getString("tip");
            char tipList = tipoLi.charAt(0);
            JSONObject an = jsonLista.getJSONObject("ani");
            
            // Insertamos la lista de utiles
            ListaUtiles listaUtiles = new ListaUtiles(an.getString("anio"),
                    jsonLista.getString("tit"),tipList,jsonLista.getString("rec"),
                    jsonLista.getDouble("cos"),organizacion,usuario,nivel,grado,area,seccion);
            
            listaUtiles.setFecMod(new Date());
            listaUtiles.setEstReg('A');
            
            listaUtilesDao.insert(listaUtiles);
            
            //Insertamos el articulos de lista
            registrarArticuloDeLista(jsonArti.getJSONArray("data"), listaUtiles, listaArticuloDao);
            
            // Generamos el json de respuesta
            String strjson = EntityUtil.objectToJSONString(new String[]{"listUtiId","tituloLista","tipoLista","recomendacionLista","precioTotal"},new String[]{"id","tit","tip","rec","pre"},listaUtiles);
            JSONObject respuesta = new JSONObject(strjson);
            
            return WebResponse.crearWebResponseExito("Se realizo el registro correctamente",WebResponse.OK_RESPONSE).setData(
                    respuesta);
            
        }catch(Exception e){
            logger.log(Level.SEVERE,"registrarLista",e);
            return WebResponse.crearWebResponseError("Error al realizar el registro",WebResponse.BAD_RESPONSE);
        }
    }
    public static void registrarArticuloDeLista(JSONArray jsonArticulos, ListaUtiles listaUtiles,ListaArticuloDao listaArticuloDao){
        ArticuloEscolarDao articuloEscolarDao = (ArticuloEscolarDao)FactoryDao.buildDao("ma.ArticuloEscolarDao");
        //listaArticuloDao.insert(new ListaUtiles(listaUtiles);
        
        for(int i = 0; i < jsonArticulos.length(); i++){
            JSONObject jsonArticulo = jsonArticulos.getJSONObject(i);
            ArticuloEscolar articuloEscolar = articuloEscolarDao.buscarPorId(jsonArticulo.getInt("id"));
            try{
                ListaArticulo listaArticulo = new ListaArticulo(listaUtiles,articuloEscolar,jsonArticulo.getInt("can"),jsonArticulo.getString("des"),jsonArticulo.getDouble("pre"));
                listaArticuloDao.insert(listaArticulo);
            }catch (Exception e){
                logger.log(Level.SEVERE,"registrarArticuloDeLista",e);
            }

        }
    }
}
