package com.dremo.ucsm.gsc.sigesmed.core.entity.maestro;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by Administrador on 19/12/2016.
 */
@Entity(name = "SeccionMaestro")
@Table(name = "seccion",schema = "institucional")
public class Seccion implements java.io.Serializable {
    @Id
    @Column(name = "sec_id",nullable = false,unique = true,length = 1)
    private char sedId;
    @Column(name = "abr",length = 2)
    private Character abr;
    @Column(name = "nom",length = 32)
    private String nom;

    @Column(name = "usu_mod")
    private Integer usuMod;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "fec_mod")
    private Date fecMod;

    @Column(name = "est_reg", length = 1)
    private Character estReg;

    public Seccion() {
    }

    public Seccion(char sedId) {
        this.sedId = sedId;
    }

    public Seccion(char sedId, Character abr, String nom) {
        this.sedId = sedId;
        this.abr = abr;
        this.nom = nom;

        this.fecMod = new Date();
        this.estReg = 'A';
    }

    public char getSedId() {
        return sedId;
    }

    public void setSedId(char sedId) {
        this.sedId = sedId;
    }

    public Character getAbr() {
        return abr;
    }

    public void setAbr(Character abr) {
        this.abr = abr;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public Integer getUsuMod() {
        return usuMod;
    }

    public void setUsuMod(Integer usuMod) {
        this.usuMod = usuMod;
    }

    public Date getFecMod() {
        return fecMod;
    }

    public void setFecMod(Date fecMod) {
        this.fecMod = fecMod;
    }

    public Character getEstReg() {
        return estReg;
    }

    public void setEstReg(Character estReg) {
        this.estReg = estReg;
    }
}
