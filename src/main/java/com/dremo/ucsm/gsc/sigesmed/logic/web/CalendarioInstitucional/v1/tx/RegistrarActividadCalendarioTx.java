package com.dremo.ucsm.gsc.sigesmed.logic.web.CalendarioInstitucional.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.OrganizacionDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.UsuarioSessionDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.web.ActividadCalendarioDao;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import org.json.JSONObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import com.dremo.ucsm.gsc.sigesmed.core.entity.web.ActividadCalendario;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.DatatypeConverter;

public class RegistrarActividadCalendarioTx implements ITransaction {

    private static final Logger logger = Logger.getLogger(RegistrarActividadCalendarioTx.class.getName());

    @Override
    public WebResponse execute(WebRequest wr) {
        try {
            JSONObject data = (JSONObject) wr.getData();

            Date ini = DatatypeConverter.parseDateTime(data.getString("ini")).getTime();
            Date fin = DatatypeConverter.parseDateTime(data.getString("fin")).getTime();
            char tip = data.getString("tip").charAt(0);
            ActividadCalendarioDao actividadDao = (ActividadCalendarioDao) FactoryDao.buildDao("web.ActividadCalendarioDao");
            ActividadCalendario activity = new ActividadCalendario(data.getString("tit"), data.getString("des"), tip, ini, fin);

            if (tip == 'P') {
                activity.setUsuario(data.getInt("usu"));
            } else {
                activity.setTipAct(data.getString("rol").charAt(0));
                activity.setEstReg('P');
            }

            actividadDao.insert(activity);

            if (tip != 'P') {
                register(activity, data.getInt("org"), data.getInt("fun"));
            }

            JSONObject object = new JSONObject();

            object.put("title", activity.getTit());
            object.put("start", activity.getFecIni().getTime());
            object.put("end", activity.getFecFin().getTime());

            switch (activity.getTipAct()) {
                case 'P':
                    object.put("color", "#257e4a");
                    break;
                case 'D':
                    object.put("color", "#25267e");
                    break;
                case 'U':
                    object.put("color", "#7e2d25");
                    break;
                case 'I':
                    object.put("color", "#64257e");
                    break;
            }

            object.put("cod", activity.getActCalId());
            object.put("tip", String.valueOf(activity.getTipAct()));
            object.put("des", activity.getDes());
            object.put("est", activity.getFecFin().before(new Date()) ? "F" : "A");

            return WebResponse.crearWebResponseExito("La actividad fue creada correctamente", WebResponse.OK_RESPONSE).setData(object);
        } catch (Exception e) {
            logger.log(Level.SEVERE, "registrarActividad", e);
            return WebResponse.crearWebResponseError("No se pudo registrar la actividad", WebResponse.BAD_RESPONSE);
        }
    }

    private void register(ActividadCalendario activity, int orgCod, int funCod) {
        UsuarioSessionDao usuarioSessionDao = (UsuarioSessionDao) FactoryDao.buildDao("UsuarioSessionDao");
        List<Integer> users = usuarioSessionDao.listarUsuariosCalendario(orgCod, funCod);
        ActividadCalendarioDao actividadDao = (ActividadCalendarioDao) FactoryDao.buildDao("web.ActividadCalendarioDao");

        for (int userCod : users) {
            ActividadCalendario newActivity = new ActividadCalendario(activity);
            newActivity.setUsuario(userCod);
            newActivity.setActividadPadre(activity);
            newActivity.setFuncion(funCod);
            actividadDao.insert(newActivity);
        }

        OrganizacionDao organizacionDao = (OrganizacionDao) FactoryDao.buildDao("OrganizacionDao");
        List<Integer> organizations = organizacionDao.listarOrganizacionesCalendario(orgCod);

        for (int org : organizations) {
            ActividadCalendario activityTemplate = new ActividadCalendario(activity);
            activityTemplate.setEstReg('P');
            activityTemplate.setActividadPadre(activity);
            actividadDao.insert(activityTemplate);
            register(activityTemplate, org, funCod);
        }
    }
}
