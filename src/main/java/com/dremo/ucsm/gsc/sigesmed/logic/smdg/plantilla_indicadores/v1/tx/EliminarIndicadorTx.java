/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.smdg.plantilla_indicadores.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.smdg.PlantillaIndicadoresDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.smdg.PlantillaGrupo;
import com.dremo.ucsm.gsc.sigesmed.core.entity.smdg.PlantillaIndicadores;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import org.json.JSONObject;

/**
 *
 * @author Administrador
 */
public class EliminarIndicadorTx implements ITransaction{
    @Override
    public WebResponse execute(WebRequest wr) {
        
        /*
        *   Parte para la lectura, verificacion y validacion de datos
        */
        int gruId = 0;
        int indId = 0;
        try{
            JSONObject requestData = (JSONObject)wr.getData();
            gruId = requestData.getInt("gruId");
            indId = requestData.getInt("indId");
        
        }catch(Exception e){
            System.out.println(e);
            return WebResponse.crearWebResponseError("No se pudo eliminar, datos incorrectos", e.getMessage() );
        }
        //Fin
        
        /*
        *  Parte para la operacion en la Base de Datos
        */
        PlantillaIndicadoresDao indicadorDao = (PlantillaIndicadoresDao)FactoryDao.buildDao("smdg.PlantillaIndicadoresDao");
        try{
            indicadorDao.delete(new PlantillaIndicadores(indId,new PlantillaGrupo(gruId)));
        
        }catch(Exception e){
            System.out.println("No se pudo eliminar el Indicador\n"+e);
            return WebResponse.crearWebResponseError("No se pudo eliminar el Indicador", e.getMessage() );
        }
        //Fin
        
        /*
        *  Repuesta Correcta
        */        
        return WebResponse.crearWebResponseExito("El Indicador se elimino correctamente");
        //Fin
    }
    
}
