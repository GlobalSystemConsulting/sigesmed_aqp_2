/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.cuadro_horas.horario_escolar.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.mech.HorarioEscolarDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mech.HorarioEscolar;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import org.json.JSONObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.text.SimpleDateFormat;
import java.util.Date;
/**
 *
 * @author abel
 */
public class InsertarHorarioEscolarTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
                
        /*
        *   Parte para la lectura, verificacion y validacion de datos
        */
        HorarioEscolar horario = null;
        try{
            
            JSONObject r = (JSONObject)wr.getData();
            Date horaInicio = new SimpleDateFormat("HH:mm:ss").parse( r.getString("horaInicio") );
            
            int planNivelID = r.getInt("planNivelID");
            int gradoID = r.getInt("gradoID");
            char seccionID = r.getString("seccionID").charAt(0);
            
            horario = new HorarioEscolar(0,horaInicio,null,planNivelID,gradoID,seccionID,new Date(),wr.getIdUsuario(),'A');
        
        }catch(Exception e){
            System.out.println(e);
            return WebResponse.crearWebResponseError("No se pudo registrar el Horario Escolar, datos incorrectos", e.getMessage() );
        }
        //Fin
        
        /*
        *  Parte para la operacion en la Base de Datos
        */
        try{
            HorarioEscolarDao horarioDao = (HorarioEscolarDao)FactoryDao.buildDao("mech.HorarioEscolarDao");
            horarioDao.insert(horario);
            
        }catch(Exception e){
            System.out.println(e);
            return WebResponse.crearWebResponseError("No se pudo registrar el Horario Escolar", e.getMessage() );
        }
        //Fin
        
        /*
        *  Repuesta Correcta
        */
        JSONObject res = new JSONObject();
        res.put("horarioID", ""+horario.getHorarioId());
        return WebResponse.crearWebResponseExito("El registro del Horario Escolar se realizo correctamente",res);
        //Fin
    }    
    
    
}
