package com.dremo.ucsm.gsc.sigesmed.core.dao.maestro.acomp;

import com.dremo.ucsm.gsc.sigesmed.core.dao.GenericDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Organizacion;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.acomp.AcompanamientoDocente;

import java.util.List;

/**
 * Created by Administrador on 05/01/2017.
 */
public interface AcompanamientoDocenteDao extends GenericDao<AcompanamientoDocente> {
    List<Organizacion> listarOrganizcionesHijas(int idPadre);
    AcompanamientoDocente buscarAcompanamiento(int id);
    List<AcompanamientoDocente> buscarAcompanamientosDocente(int idDoc);

}
