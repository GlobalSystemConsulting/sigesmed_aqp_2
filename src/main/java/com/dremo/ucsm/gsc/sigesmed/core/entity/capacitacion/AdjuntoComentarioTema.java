package com.dremo.ucsm.gsc.sigesmed.core.entity.capacitacion;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "adjunto_comentario_tema", schema = "pedagogico")
public class AdjuntoComentarioTema implements Serializable {
    @Id
    @Column(name = "adj_com_tem_id", unique = true, nullable = false)
    @SequenceGenerator(name = "secuencia_adjunto_comentario_tema", sequenceName = "pedagogico.adjunto_comentario_tema_adj_com_tem_id_seq")
    @GeneratedValue(generator = "secuencia_adjunto_comentario_tema")
    private int adjComTemId;
    
    @Column(name = "nom", nullable = false)
    private String nom;
    
    @Column(name = "usu_mod", nullable = false)
    private int usuMod;
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "fec_mod", nullable = false)
    private Date fecMod;
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "com_tem_cap_id")
    private ComentarioTemaCapacitacion comentarioTema;

    public AdjuntoComentarioTema() {}
    
    public AdjuntoComentarioTema(String nom, ComentarioTemaCapacitacion comentarioTema, int usuMod) {
        this.nom = nom;
        this.usuMod = usuMod;
        this.fecMod = new Date();
        this.comentarioTema = comentarioTema;
    }

    public int getAdjComTemId() {
        return adjComTemId;
    }

    public void setAdjComTemId(int adjComTemId) {
        this.adjComTemId = adjComTemId;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public int getUsuMod() {
        return usuMod;
    }

    public void setUsuMod(int usuMod) {
        this.usuMod = usuMod;
    }

    public Date getFecMod() {
        return fecMod;
    }

    public void setFecMod(Date fecMod) {
        this.fecMod = fecMod;
    }

    public ComentarioTemaCapacitacion getComentarioTema() {
        return comentarioTema;
    }

    public void setComentarioTema(ComentarioTemaCapacitacion comentarioTema) {
        this.comentarioTema = comentarioTema;
    }

    @Override
    public String toString() {
        return "AdjuntoComentarioTema{" + "adjComTemId=" + adjComTemId + ", nom=" + nom + '}';
    }
    
    
}
