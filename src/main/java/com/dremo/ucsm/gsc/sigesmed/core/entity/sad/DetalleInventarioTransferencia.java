/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.entity.sad;


import java.util.Date;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
/**
 *
 * @author Jeferson
 */
@Entity
@Table(name="detalle_inventario_transferencia", schema="administrativo")
public class DetalleInventarioTransferencia implements java.io.Serializable {
    
    @Id
    @Column(name="det_inv_tra", unique= true , nullable=false)
    @SequenceGenerator(name="secuencia_det_inv_tra",sequenceName="administrativo.detalle_inventario_transferencia_det_inv_tra_seq")
    @GeneratedValue(generator="secuencia_det_inv_tra")
    private int det_inv_tra;
    
    
    @Column(name="inv_tra_id", insertable = false , updatable=false)
    private int inv_tra_id;
    
    
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumns({
        @JoinColumn(name="inv_tra_id",referencedColumnName="inv_tra_id",insertable=true,updatable=false),
        @JoinColumn(name="ser_doc_id",referencedColumnName="ser_doc_id",insertable=true,updatable=false)
    })
    private InventarioTransferencia inv_tra;
    
    
    @Column(name="ser_doc_id", insertable=false ,  updatable=false)
    private int ser_doc_id;
    
    
    @Column(name="fec_cre")
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date fec_cre;
    
    
    @Column(name="asu")
    private String asu;
    
    @Column(name="met_lin")
    private int met_lin;
    
    /*
    @Column(name="res_con")
    private int res_con;
    */
    
    /*
    @Column(name="loc")
    private int loc;
    */
    
    @Column(name="alm")
    private String alm;
    
    @Column(name="est")
    private int est;
    
    @Column(name="bal")
    private int bal;
    
    @Column(name="fil")
    private int fil;

    public int getDet_inv_tra() {
        return det_inv_tra;
    }

    public int getInv_tra_id() {
        return inv_tra_id;
    }

    public InventarioTransferencia getInv_tra() {
        return inv_tra;
    }

    public int getSer_doc_id() {
        return ser_doc_id;
    }

    public Date getFec_cre() {
        return fec_cre;
    }

    public String getAsu() {
        return asu;
    }

    public int getMet_lin() {
        return met_lin;
    }

    public String getAlm() {
        return alm;
    }

    public int getEst() {
        return est;
    }

    public int getBal() {
        return bal;
    }

    public void setDet_inv_tra(int det_inv_tra) {
        this.det_inv_tra = det_inv_tra;
    }

    public void setInv_tra_id(int inv_tra_id) {
        this.inv_tra_id = inv_tra_id;
    }

    public void setInv_tra(InventarioTransferencia inv_tra) {
        this.inv_tra = inv_tra;
    }

    public void setSer_doc_id(int ser_doc_id) {
        this.ser_doc_id = ser_doc_id;
    }

    public void setFec_cre(Date fec_cre) {
        this.fec_cre = fec_cre;
    }

    public void setAsu(String asu) {
        this.asu = asu;
    }

    public void setMet_lin(int met_lin) {
        this.met_lin = met_lin;
    }

    public void setAlm(String alm) {
        this.alm = alm;
    }

    public void setEst(int est) {
        this.est = est;
    }

    public void setBal(int bal) {
        this.bal = bal;
    }

    public void setFil(int fil) {
        this.fil = fil;
    }

    public void setCue(int cue) {
        this.cue = cue;
    }

    public void setCaj(int caj) {
        this.caj = caj;
    }

    public void setArchivos_inventario_transferencia(List<ArchivosInventarioTransferencia> archivos_inventario_transferencia) {
        this.archivos_inventario_transferencia = archivos_inventario_transferencia;
    }

    
    public int getFil() {
        return fil;
    }

    public int getCue() {
        return cue;
    }

    public int getCaj() {
        return caj;
    }

    public List<ArchivosInventarioTransferencia> getArchivos_inventario_transferencia() {
        return archivos_inventario_transferencia;
    }
    /*
    public List<TablaRetencionInventario> getTabla_retencion_inventario() {
        return tabla_retencion_inventario;
    }
    */
    
    @Column(name="cue")
    private int cue;
    
    @Column(name="caj")
    private int caj;
    
    @OneToMany(mappedBy="detalle_inventario", cascade=CascadeType.ALL)
    private List<ArchivosInventarioTransferencia> archivos_inventario_transferencia;
    
    /*
    @OneToMany(mappedBy="detalle_inv_trans" , cascade=CascadeType.PERSIST)
    private List<TablaRetencionInventario> tabla_retencion_inventario ;
    */
    
    public DetalleInventarioTransferencia(){
        
    }
    public DetalleInventarioTransferencia(int det_inv_tra_id){
        this.det_inv_tra = det_inv_tra_id;
    }
    public DetalleInventarioTransferencia(int det_inv_tra_id , int inv_trans_id , int ser_doc_id){
        this.det_inv_tra = det_inv_tra_id;
        this.inv_tra_id = inv_trans_id;
        this.ser_doc_id = ser_doc_id;
        this.inv_tra = new InventarioTransferencia(inv_trans_id);
     //   this.serie = new SerieDocumental(ser_doc_id);
    }
    
    public DetalleInventarioTransferencia(int det_inv_tra_id , InventarioTransferencia it , int ser_doc_id , Date fec_cre , String asu , int met_lin  , String alm , int est , int bal ,int fil , int cue , int caj){
        this.det_inv_tra = det_inv_tra_id;
     
        this.inv_tra = it;
        this.fec_cre = fec_cre;
        this.asu = asu;
        this.met_lin = met_lin;
    //    this.res_con = res_con;
    //    this.loc = loc;
        this.alm = alm;
        this.est = est;
        this.bal = bal;
        this.fil = fil;
        this.cue = cue;
        this.caj = caj;
        
    }
    
    public List<ArchivosInventarioTransferencia> getArchivoInventarioTrans(){
        return this.archivos_inventario_transferencia;
    }
    public void setArchivosInventarioTrans(List<ArchivosInventarioTransferencia> archivos_inv_trans){
        this.archivos_inventario_transferencia = archivos_inv_trans;
    }
    /*
    public List<TablaRetencionInventario> getTablaRetencionInventario(){
        return this.tabla_retencion_inventario;
    }
    public void setTablaRetencionInventario(List<TablaRetencionInventario> tabla_retencion){
        this.tabla_retencion_inventario = tabla_retencion;
    }
    */
    public void setdet_inv_tra(int det_inv){
        this.det_inv_tra = det_inv;
    }
    public int getdet_inv_tra(){
        return this.det_inv_tra;
    }
    public Date getFecha(){
        return this.fec_cre;
    }
}
