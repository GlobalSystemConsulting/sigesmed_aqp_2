package com.dremo.ucsm.gsc.sigesmed.logic.scec.comision.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.scec.CargoDeComisionDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.scec.ComisionDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.scec.IntegranteComisionDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.scec.CargoDeComision;
import com.dremo.ucsm.gsc.sigesmed.core.entity.scec.Comision;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import static com.dremo.ucsm.gsc.sigesmed.logic.scec.comision.v1.tx.RegistrarComisionTx.registrarPresidente;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.Date;
import java.util.logging.Logger;

/**
 * Created by geank on 29/09/16.
 */
public class EditarComisionTx implements ITransaction {
    private static Logger logger = Logger.getLogger(EditarComisionTx.class.getName());
    @Override
    public WebResponse execute(WebRequest wr) {
        JSONObject data = (JSONObject) wr.getData();
        JSONObject jsonComision = data.getJSONObject("com");
        JSONArray jsonCargos = data.getJSONArray("car");
        JSONObject jsonPresidente = data.getJSONObject("pres");
        return editarComision(jsonComision,jsonCargos,jsonPresidente);
    }
    public WebResponse editarComision(JSONObject jsonComision, JSONArray jsonCargos, JSONObject jsonPresidente){
        try{
            ComisionDao comisionDao = (ComisionDao) FactoryDao.buildDao("scec.ComisionDao");
            CargoDeComisionDao cargoDeComisionDao = (CargoDeComisionDao)FactoryDao.buildDao("scec.CargoDeComisionDao");
            IntegranteComisionDao integranteComisionDao2 = (IntegranteComisionDao)FactoryDao.buildDao("scec.IntegranteComisionDao");

            Comision comision = comisionDao.buscarPorId(jsonComision.getInt("cod"));
            comision.setNomCom(jsonComision.optString("nom",comision.getNomCom()));
            comision.setDesCom(jsonComision.optString("des",comision.getDesCom()));
            comision.setFecMod(new Date());
            comisionDao.update(comision);

            //buscamos los cargos asignados
            //primero eliminamos
            cargoDeComisionDao.eliminarCargosDeComision(comision.getComId());
            //volvemos a insertar
            RegistrarComisionTx.registrarCargoDeComision(jsonCargos,comision,cargoDeComisionDao);
            //RegistrarComisionTx.registrarPresidente(jsonPresidente,comision);

            return WebResponse.crearWebResponseExito("exito al realizar la operacion",WebResponse.OK_RESPONSE);
        }catch (Exception e){
            return WebResponse.crearWebResponseError("error al realizar la operacion", WebResponse.BAD_RESPONSE);
        }
    }
}
