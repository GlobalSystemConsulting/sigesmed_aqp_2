/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.di;

import com.dremo.ucsm.gsc.sigesmed.core.dao.di.ApoderadoDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.GenericDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.datastore.HibernateUtil;
import com.dremo.ucsm.gsc.sigesmed.core.entity.di.Apoderado;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author Administrador
 */
public class ApoderadoDaoHibernate extends GenericDaoHibernate<Apoderado> implements ApoderadoDao {
    
    @Override
    public String buscarUltimoCodigo() {
        String codigo = null;
//        Session session = HibernateUtil.getSessionFactory().openSession();
//        try{
//            //listar ultimo codigo de tramite
//            String hql = "SELECT a.apoId FROM Apoderado a ORDER BY a.apoId DESC ";
//            Query query = session.createQuery(hql);
//            query.setMaxResults(1);
//            codigo = ((String)query.uniqueResult());
//            
//            //solo cuando no hay ningun registro aun
//            if(codigo==null)
//                codigo = "0000";
//        
//        }catch(Exception e){
//            System.out.println("No se pudo encontrar el ultimo codigo \\n "+ e.getMessage());
//            throw new UnsupportedOperationException("No se pudo encontrar el ultimo codigo \\n "+ e.getMessage());            
//        }
//        finally{
//            session.close();
//        }
        return codigo;
    }
    
    @Override
    public List<Apoderado> datosApoderado(){
        List<Apoderado> apos = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction t = session.beginTransaction();
        try{
            
            String hql = "SELECT a FROM Apoderado a JOIN FETCH a.persona";
            Query query = session.createQuery(hql);            
            apos = query.list();
            t.commit();
                    
        }catch(Exception e){
            t.rollback();
            System.out.println("No se pudo encontrar el ultimo codigo \\n "+ e.getMessage());
            throw new UnsupportedOperationException("No se pudo encontrar el ultimo codigo \\n "+ e.getMessage());            
        }
        finally{
            session.close();
        }
        return apos;
    }
    
    @Override
    public List listarApoderados(String s, int orgId){
        
        String[] cmp = s.split(",");
        s="";
        for(int i = 0; i<cmp.length - 1; ++i){
            if(cmp[i].equals("o.nom")){
                s += cmp[i] + " as orgnom,";
            } 
            else    s+=cmp[i] + ",";
        }
        if(cmp[cmp.length-1].equals("o.nom")){
            s += cmp[cmp.length-1] + " as orgnom";
        }
        else{
            s += cmp[cmp.length-1];
        }
        
        List apos = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction t = session.beginTransaction();
        try{
            
            SQLQuery query = session.createSQLQuery("select "+s+" from pedagogico.apoderado a "
                    + "join pedagogico.persona p on a.apo_id = p.per_id "
                    + "join pedagogico.matricula m on m.apo_id = p.per_id "                   
                    + "join organizacion o on m.org_id = o.org_id " 
                    + "WHERE (m.org_id = " + orgId + " OR o.org_pad_id= " + orgId + ")");
                                
            apos = query.list();
            t.commit();
                    
        }catch(Exception e){
            t.rollback();
            System.out.println("No se pudo encontrar el ultimo codigo \\n "+ e.getMessage());
            throw new UnsupportedOperationException("No se pudo encontrar el ultimo codigo \\n "+ e.getMessage());            
        }
        finally{
            session.close();
        }
        return apos;
    }
    
}
