package com.dremo.ucsm.gsc.sigesmed.core.entity.capacitacion;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "respuesta_encuesta", schema = "pedagogico")
public class RespuestaEncuesta implements Serializable {

    @Id
    @Column(name = "pre_eva_cap_id", unique = true, nullable = false)
    private int preEvaCapId;

    @Id
    @Column(name = "eva_par_cap_id", unique = true, nullable = false)
    private int evaParCapId;

    @Column(name = "res_enc", nullable = false)
    private String resEnc;

    public RespuestaEncuesta() {
    }

    public RespuestaEncuesta(int preEvaCapId, int evaParCapId, String resEnc) {
        this.preEvaCapId = preEvaCapId;
        this.evaParCapId = evaParCapId;
        this.resEnc = resEnc;
    }

    public int getPreEvaCapId() {
        return preEvaCapId;
    }

    public void setPreEvaCapId(int preEvaCapId) {
        this.preEvaCapId = preEvaCapId;
    }

    public int getEvaParCapId() {
        return evaParCapId;
    }

    public void setEvaParCapId(int evaParCapId) {
        this.evaParCapId = evaParCapId;
    }

    public String getResEnc() {
        return resEnc;
    }

    public void setResEnc(String resEnc) {
        this.resEnc = resEnc;
    }

    @Override
    public String toString() {
        return "RespuestaEncuesta{" + "resEnc=" + resEnc + '}';
    }
    
    
}
