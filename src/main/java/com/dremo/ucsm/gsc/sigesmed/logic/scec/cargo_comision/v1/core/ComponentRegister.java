/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.scec.cargo_comision.v1.core;

import com.dremo.ucsm.gsc.sigesmed.core.service.WebComponent;
import com.dremo.ucsm.gsc.sigesmed.core.service.constantes.Sigesmed;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.IComponentRegister;
import com.dremo.ucsm.gsc.sigesmed.logic.scec.cargo_comision.v1.tx.ActualizarCargoTx;
import com.dremo.ucsm.gsc.sigesmed.logic.scec.cargo_comision.v1.tx.EliminarCargoTx;
import com.dremo.ucsm.gsc.sigesmed.logic.scec.cargo_comision.v1.tx.ListarCargosTx;
import com.dremo.ucsm.gsc.sigesmed.logic.scec.cargo_comision.v1.tx.RegistrarCargoTx;

/**
 *
 * @author ucsm
 */
public class ComponentRegister implements IComponentRegister{
    
    @Override

    public WebComponent createComponent() {
        WebComponent scecComponent = new WebComponent(Sigesmed.SUBMODULO_COMISIONES);
        scecComponent.setName("cargos");
        scecComponent.setVersion(1);

        scecComponent.addTransactionPOST("registrarCargo",RegistrarCargoTx.class);
        scecComponent.addTransactionGET("listarCargos",ListarCargosTx.class);
        scecComponent.addTransactionPUT("actualizarCargo", ActualizarCargoTx.class);
        scecComponent.addTransactionDELETE("eliminarCargo", EliminarCargoTx.class);
        return scecComponent;
    }
 
}
