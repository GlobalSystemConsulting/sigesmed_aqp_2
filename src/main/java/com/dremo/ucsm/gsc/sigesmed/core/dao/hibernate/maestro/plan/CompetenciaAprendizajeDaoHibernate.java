package com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.maestro.plan;

import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.GenericDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.dao.maestro.plan.CompetenciaAprendizajeDao;
import com.dremo.ucsm.gsc.sigesmed.core.datastore.HibernateUtil;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.plan_curricular.CompetenciaAprendizaje;
import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.criterion.CriteriaSpecification;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.hibernate.engine.query.spi.sql.NativeSQLQuerySpecification;
import org.hibernate.sql.JoinType;
import org.hibernate.transform.DistinctResultTransformer;

import java.util.List;

/**
 * Created by Administrador on 13/10/2016.
 */
public class CompetenciaAprendizajeDaoHibernate extends GenericDaoHibernate<CompetenciaAprendizaje> implements CompetenciaAprendizajeDao{
    @Override
    public List<CompetenciaAprendizaje> buscarCompetenciasConCapacidades() {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            /*Criteria criteria  = session.createCriteria(CompetenciaAprendizaje.class)
                    .setFetchMode("area", FetchMode.JOIN)
                    .setFetchMode("capacidades", FetchMode.JOIN)
                    .add(Restrictions.eq("estReg", 'A'))
                    .addOrder(Order.asc("comId"));*/
                    /*.createAlias("capacidades", "caps", JoinType.LEFT_OUTER_JOIN)
                    .createAlias("caps.cap", "c", JoinType.LEFT_OUTER_JOIN).add(Restrictions.or(Restrictions.eq("caps.estReg", 'A'),
                            Restrictions.isNull("caps.estReg")))*/
                    //.setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY);-----
            /*criteria.createCriteria("capacidades").createCriteria("cap").add(Restrictions.or(Restrictions.eq("estReg", 'A'),
                    Restrictions.isNull("estReg")));
            criteria.setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY);*/
                    //.createCriteria("capacidades").createCriteria("cap").add(Restrictions.eq("estReg", 'A'));
            //criteria.add(Restrictions.eq("estReg", 'A'));
            //criteria.addOrder(Order.asc("comId"));
            String hql = "SELECT comp FROM CompetenciaAprendizaje  comp LEFT JOIN FETCH comp.area LEFT JOIN FETCH comp.capacidades caps LEFT JOIN FETCH caps.cap cap WHERE comp.estReg = 'A' AND (caps.estReg ='A' OR caps.estReg is NULL)";
            Query query2 = session.createQuery(hql);
            query2.setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY);
            return query2.list();
            //return criteria.list();
        }catch (Exception e){
            throw  e;
        }finally {
            session.close();
        }
    }

    @Override
    public CompetenciaAprendizaje buscarCompetenciPorCodigo(int cod) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            /*Criteria criteria  = session.createCriteria(CompetenciaAprendizaje.class)
                    .createAlias("capacidades","caps", JoinType.INNER_JOIN)
                    .add(Restrictions.eq("comId",cod));
            criteria.add(Restrictions.eq("estReg", 'A'));*/
            String hql = "SELECT comp FROM CompetenciaAprendizaje  comp LEFT JOIN FETCH comp.capacidades caps  WHERE comp.comId =:comid AND comp.estReg = 'A'";
            Query query2 = session.createQuery(hql);
            query2.setInteger("comid",cod);
            return (CompetenciaAprendizaje)query2.uniqueResult();
        }catch (Exception e){
            throw  e;
        }finally {
            session.close();
        }
    }

    @Override
    public List<CompetenciaAprendizaje> buscarCompetenciaPorArea(int idArea) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            /*Criteria query  = session.createCriteria(CompetenciaAprendizaje.class)
                    .setFetchMode("capacidades", FetchMode.JOIN)
                    .add(Restrictions.eq("estReg", 'A'));
            query.createCriteria("area").add(Restrictions.eq("areCurId",idArea));
            //query.createAlias("capacidades","caps",JoinType.INNER_JOIN).add(Restrictions.eq("caps.estReg",'A'));
            query.createCriteria("capacidades.cap","cap",JoinType.INNER_JOIN).add(Restrictions.eq("estReg", 'A'));
            query.setFetchMode("cap.indicadores", FetchMode.JOIN);
            query.createAlias("cap.indicadores","indicadores",JoinType.INNER_JOIN);
            //query.createCriteria("cap.indicadores","inds",JoinType.LEFT_OUTER_JOIN).add(Restrictions.eq("estReg", 'A'));
            query.setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY);*/
            String hql = "SELECT comp FROM CompetenciaAprendizaje  comp INNER JOIN FETCH comp.capacidades caps INNER JOIN FETCH caps.cap cap LEFT JOIN  cap.indicadores inds WHERE comp.area.areCurId =:areaid AND comp.estReg = 'A' AND caps.estReg = 'A' AND cap.estReg ='A' AND (inds.estReg ='A' OR inds.estReg is NULL)";
            Query query2 = session.createQuery(hql);
            query2.setInteger("areaid",idArea);
            query2.setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY);
            return query2.list();
        }catch (Exception e){
            throw e;
        }finally {
            session.close();
        }
    }

    @Override
    public CompetenciaAprendizaje buscarCompetenciaPorNombre(String nombre) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            String hql = "SELECT comp FROM CompetenciaAprendizaje comp WHERE comp.nom =:nomcom";
            Query query = session.createQuery(hql);
            query.setString("nomcom",nombre);
            query.setMaxResults(1);
            return (CompetenciaAprendizaje) query.uniqueResult();
        }catch (Exception e){
            throw e;
        }finally {
            session.close();
        }
    }
}
