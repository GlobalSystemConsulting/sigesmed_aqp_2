/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao.mnt;

import com.dremo.ucsm.gsc.sigesmed.core.dao.GenericDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.FuncionSistema;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mnt.Peticion;

/**
 *
 * @author Administrador
 */
public interface PeticionDao extends GenericDao<Peticion>{
    char obtenerTipoPeticionPorIdMensaje(int mensajeID);
    FuncionSistema obtenerFuncionPorIdMensaje(int mensajeID);
    Peticion obtenerPeticionPorIdmensaje(int mensajeID);
}
