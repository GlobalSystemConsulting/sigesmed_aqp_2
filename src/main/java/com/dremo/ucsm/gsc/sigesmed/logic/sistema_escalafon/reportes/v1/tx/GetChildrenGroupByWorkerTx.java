/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.reportes.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.ParientesDao;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author gscadmin
 */
public class GetChildrenGroupByWorkerTx implements ITransaction{

    private static final Logger logger = Logger.getLogger(GetChildrenGroupByWorkerTx.class.getName());

    @Override
    public WebResponse execute(WebRequest wr) {
        JSONObject data = (JSONObject) wr.getData();
        Integer orgId = data.optInt("orgId");
        
        JSONArray resultado = new JSONArray();
        ParientesDao parientesDao = (ParientesDao)FactoryDao.buildDao("se.ParientesDao");
        
        try{
            resultado = parientesDao.numeroHijosxTrabajador(orgId);
        
        }catch(Exception e){
            logger.log(Level.SEVERE,"Listar desplazamientos",e);
            System.out.println("No se pudo listar los desplazamientos\n"+e);
            return WebResponse.crearWebResponseError("No se pudo listar los desplazamientos", e.getMessage() );
        }
        return WebResponse.crearWebResponseExito("Los desplazamientos fueron listados exitosamente", resultado);
    }
    
}
