package com.dremo.ucsm.gsc.sigesmed.logic.maestro.plananual.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.maestro.plan.*;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.plan_curricular.*;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.base.EntityUtil;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by Administrador on 24/10/2016.
 */
public class DetalleProgramacionAnualTx implements ITransaction {
    private static final Logger logger = Logger.getLogger(DetalleProgramacionAnualTx.class.getName());

    private static final String FUNC_LISTAR = "listar";
    private static final String FUNC_EDITAR = "editar";
    private static final String FUNC_REGISTRAR = "registrar";
    private static final String FUNC_ELIMINAR = "eliminar";
    //
    private static final String ELEM_OBJ = "obj";
    private static final String ELEM_VAL = "val";
    private static final String ELEM_MET = "met";
    private static final String ELEM_UNI = "uni";
    @Override
    public WebResponse execute(WebRequest wr) {
        try{
            String func = wr.getMetadataValue("func");
            String elemDet = wr.getMetadataValue("elem");

            JSONObject data = (JSONObject) wr.getData();
            switch (func){
                case FUNC_LISTAR :return listarElementoProgramacionAnual(elemDet,data);
                case FUNC_REGISTRAR : return registrarElementoProgramacionAnual(elemDet,data);
                case FUNC_ELIMINAR : return eliminarElementoProgramacionAnual(elemDet, data);
                case FUNC_EDITAR : return editarElementoProgramacionAnual(elemDet, data);
                default: return WebResponse.crearWebResponseError("No existe la funcion solicitada");
            }
        }catch (Exception e){
            return WebResponse.crearWebResponseError("Error al leer los parametros enviados");
        }
    }



    private WebResponse listarElementoProgramacionAnual(String elemDet,JSONObject data) {
        try{
            switch (elemDet){
                case ELEM_OBJ : return listarObjetivosProgramacionAnual(data);
                case ELEM_UNI : return listarUnidadDidactica(data);
                case ELEM_VAL: return listarValoresUnidad(data);
                default:return WebResponse.crearWebResponseError("No exite el elemento solicitado");
            }
        }catch (Exception e){
            return WebResponse.crearWebResponseError("Error al listar elemento");
        }
    }
    private WebResponse registrarElementoProgramacionAnual(String elemDet, JSONObject data){
        try{
            switch (elemDet){
                case ELEM_OBJ : return regitrarObjetivoPLan(data);
                case ELEM_UNI: return registrarUnidadDidactica(data);
                case ELEM_VAL: return registrarValoresUnidad(data);
                default: return WebResponse.crearWebResponseError("No exite el elemento solicitado");
            }
        }catch (Exception e){
            return WebResponse.crearWebResponseError("Error al listar elemento");
        }
    }
    private WebResponse eliminarElementoProgramacionAnual(String elemDet, JSONObject data){
        try{
            switch (elemDet){
                case ELEM_OBJ : return eliminarObjetivoPLan(data);
                case ELEM_UNI: return eliminarUnidadDidactica(data);
                case ELEM_VAL: return eliminarValoresUnidad(data);
                default: return WebResponse.crearWebResponseError("No exite el elemento solicitado");
            }
        }catch (Exception e){
            return WebResponse.crearWebResponseError("Error al listar elemento");
        }
    }
    private WebResponse editarElementoProgramacionAnual(String elemDet, JSONObject data) {
        try{
            switch (elemDet){
                case ELEM_UNI: return editarUnidadDidactica(data);
                case ELEM_VAL: return editarValoresUnidad(data);
                default: return WebResponse.crearWebResponseError("No exite el elemento solicitado");
            }
        }catch (Exception e){
            return WebResponse.crearWebResponseError("Error al listar elemento");
        }
    }
    /**CRUD Objetivos*/
    private WebResponse listarObjetivosProgramacionAnual(JSONObject data) {
        try {
            String tip = data.optString("tip");
            String mod = data.getString("mod");
            ProgramacionAnualDao progDao = (ProgramacionAnualDao) FactoryDao.buildDao("maestro.plan.ProgramacionAnualDao");
            List<ObjetivoProgramacion> obj;
            if(mod.equals("all")){
                JSONArray objPr = new JSONArray(EntityUtil.listToJSONString(
                        new String[]{"objPrId","nom","des"},
                        new String[]{"cod","nom","des"},
                        progDao.buscarObjetivosDeProgramacion("pr")
                ));
                JSONArray objEsp = new JSONArray(EntityUtil.listToJSONString(
                        new String[]{"objPrId","nom","des"},
                        new String[]{"cod","nom","des"},
                        progDao.buscarObjetivosDeProgramacion("es")
                ));
                return WebResponse.crearWebResponseExito("Se listo con exito", new JSONObject().put("pr",objPr).put("es", objEsp));
            }else if(mod.equals("prog-tip")){
                obj = progDao.buscarObjetivosDeProgramacion(data.getInt("cod"),tip);
                return WebResponse.crearWebResponseExito("Se listaron todos los objetivos",new JSONArray(EntityUtil.listToJSONString(
                        new String[]{"objPrId","nom","des"},
                        new String[]{"cod","nom","des"},
                        obj
                )));
            }else if(mod.equals("prog-all")){
                obj = progDao.buscarObjetivosDeProgramacion(data.getInt("cod"));
                JSONArray arrayObj = new JSONArray();
                for(ObjetivoProgramacion o: obj){
                    JSONObject jsonObj = new JSONObject(EntityUtil.objectToJSONString(
                            new String[]{"objPrId","nom","des","tip"},
                            new String[]{"cod","nom","des","tip"},
                            o
                    ));
                    jsonObj.put("edit",false);
                    arrayObj.put(jsonObj);
                }
                return WebResponse.crearWebResponseExito("Se listo con exito", arrayObj);
            }else{
                return WebResponse.crearWebResponseError("Modo desconocido de objetivo");
            }

        }catch (Exception e){
            logger.log(Level.SEVERE,"listarObjetivosProgramacionAnual",e);
            return WebResponse.crearWebResponseError("Error al listar los objetivos anuales");
        }
    }
    private WebResponse regitrarObjetivoPLan(JSONObject data) {
        try{
            String tip = data.getString("tip");
            String nom = data.getString("nom");
            String des = data.optString("des");

            ProgramacionAnualDao progDao = (ProgramacionAnualDao) FactoryDao.buildDao("maestro.plan.ProgramacionAnualDao");
            ObjetivoProgramacion objPro = new ObjetivoProgramacion(nom,des,tip);

            ProgramacionAnual prog = progDao.buscarProgramacionConObjetivos(data.getInt("cod"));
            List<ObjetivoProgramacion> objetivos = progDao.buscarObjetivosDeProgramacion(tip);

            if(objetivos.contains(objPro)){
                if(!prog.getObjetivos().contains(objPro)){
                    prog.getObjetivos().add(objetivos.get(objetivos.indexOf(objPro)));
                }else{
                    return WebResponse.crearWebResponseError("El objetivo ya esta registrado en la programacion seleccionada.");
                }
            }else {
                ObjetivoProgramacionDao objDao = (ObjetivoProgramacionDao) FactoryDao.buildDao("maestro.plan.ObjetivoProgramacionDao");
                objDao.insert(objPro);
                prog.getObjetivos().add(objPro);
            }

            progDao.update(prog);

            return WebResponse.crearWebResponseExito("Se registro correctamente el objetivo",new JSONObject().put("cod",objPro.getObjPrId()));
        }catch (Exception e){
            logger.log(Level.INFO,"regitrarObjetivoPLan",e);
            return WebResponse.crearWebResponseError("No se pudo regitrar el objetivo");
        }
    }
    private WebResponse eliminarObjetivoPLan(JSONObject data){
        try{
            int idObj = data.getInt("cod");
            int idProg = data.getInt("prog");
            ProgramacionAnualDao progDao = (ProgramacionAnualDao) FactoryDao.buildDao("maestro.plan.ProgramacionAnualDao");
            ObjetivoProgramacionDao objDao = (ObjetivoProgramacionDao) FactoryDao.buildDao("maestro.plan.ObjetivoProgramacionDao");

            ProgramacionAnual prog = progDao.buscarProgramacionConObjetivos(idProg);
            prog.getObjetivos().remove(objDao.buscarObjetivoPorId(idObj));

            progDao.update(prog);
            return WebResponse.crearWebResponseExito("Se quito correctamente el objetivo");
        }catch (Exception e){
            logger.log(Level.INFO,"regitrarObjetivoPLan",e);
            return WebResponse.crearWebResponseError("No se pudo eliminar el objetivo ");
        }
    }
    /**CRUD Unidades Didacticas*/
    private WebResponse registrarUnidadDidactica(JSONObject data){
        try{
            String titulo = data.getString("nom");
            String tip = data.getString("tip");
            String sit = data.getString("sit");
            long fecIni = data.getLong("ini");
            long fecFin = data.getLong("fin");
            int idPer = data.optInt("per");
            JSONArray aprendizajesEsperados = data.optJSONArray("competencias");
            UnidadDidacticaDao unidadDao = (UnidadDidacticaDao)FactoryDao.buildDao("maestro.plan.UnidadDidacticaDao");
            ProgramacionAnualDao progDao = (ProgramacionAnualDao) FactoryDao.buildDao("maestro.plan.ProgramacionAnualDao");
            CompetenciaCapacidadDao comCapDao = (CompetenciaCapacidadDao) FactoryDao.buildDao("maestro.plan.CompetenciaCapacidadDao");
            //new UnidadDidactica(titulo,tip.charAt(0),sit,new Date(fecIni),new Date(fecFin))
            UnidadDidactica unidad = null;
            switch (tip.toUpperCase()){
                case "M" :unidad = new ModuloAprendizaje(titulo,tip.charAt(0),sit,new Date(fecIni),new Date(fecFin)); break;
                case "P" :unidad = new ProyectoAprendizaje(titulo,tip.charAt(0),sit,new Date(fecIni),new Date(fecFin)); break;
                case "U" :unidad = new UnidadAprendizaje(titulo,tip.charAt(0),sit,new Date(fecIni),new Date(fecFin)); break;
                default: return WebResponse.crearWebResponseError("No se puede registrar la unidad");
            }
            ProgramacionAnual prog = progDao.buscarProgramacionConObjetivos(data.getInt("cod"));
            unidad.setProgramacionAnual(prog);
            unidad.setPeriodo(unidadDao.buscarPeriodoEvaluacion(idPer));
            unidadDao.insert(unidad);
            if(aprendizajesEsperados !=  null){
                for(int i = 0; i < aprendizajesEsperados.length(); i++){
                    JSONObject competencia = aprendizajesEsperados.getJSONObject(i);
                    JSONArray capacidades = competencia.getJSONArray("capacidades");
                    for(int j = 0; j < capacidades.length(); j++){
                        JSONObject capacidad =  capacidades.getJSONObject(j);
                        CompetenciaCapacidad objComCap = comCapDao.buscarCapacidadDeCompetencia(capacidad.getInt("id"), competencia.getInt("id"));
                        List<IndicadorAprendizaje> indicadoresJson = objComCap.getCap().getIndicadores();
                        CompetenciasUnidadDidactica comUniDi = new CompetenciasUnidadDidactica(unidad,objComCap,"");
                        try{
                            unidadDao.registrarCapacidades(comUniDi);
                            capacidad.put("indicadores", new JSONArray(EntityUtil.listToJSONString(
                                    new String[]{"indAprId","nomInd"},
                                    new String[]{"id","nom"},
                                    indicadoresJson
                            )));
                        }catch (Exception e){
                            logger.log(Level.SEVERE,"Duplicidad de clave",e);
                            capacidades.remove(j);
                            --j;
                        }
                    }
                }
            }
            JSONObject rspData = new JSONObject().put("cod",unidad.getUniDidId());
            return WebResponse.crearWebResponseExito("Se registro la unidad didactica",rspData);
        }catch (Exception e){
            logger.log(Level.SEVERE,"registrarUnidadDidactica",e);
            return WebResponse.crearWebResponseError("No se puede registrar la unidad");
        }
    }
    private WebResponse listarUnidadDidactica(JSONObject data){
        try{
            int idPlan = data.getInt("cod");
            UnidadDidacticaDao unidadDao = (UnidadDidacticaDao)FactoryDao.buildDao("maestro.plan.UnidadDidacticaDao");
            List<UnidadDidactica> unidades = unidadDao.buscarUnidadesPorPlan(idPlan);
            JSONArray array = new JSONArray();
            for(UnidadDidactica unidad : unidades){
                JSONObject objUnidad = new JSONObject(EntityUtil.objectToJSONString(
                        new String[]{"uniDidId","tit","tip","sitSig","fecIni","fecFin"},
                        new String[]{"cod","nom","tip","sit","ini","fin"},
                        unidad
                ));
                objUnidad.put("per",unidad.getPeriodo().getPerPlaEstId());
                array.put(objUnidad);
            }
            return WebResponse.crearWebResponseExito("Exito al listar las unidades", array);
        }catch (Exception e){
            logger.log(Level.SEVERE,"eliminarUnidadDidactica",e);
            return WebResponse.crearWebResponseError("No se puede listar los elementos de la unidad");
        }
    }
    private WebResponse editarUnidadDidactica(JSONObject data){
        try{
            UnidadDidacticaDao unidadDao = (UnidadDidacticaDao)FactoryDao.buildDao("maestro.plan.UnidadDidacticaDao");
            int idUnidad = data.getInt("cod");
            String tip = data.getString("tip");
            UnidadDidactica unidad = unidadDao.buscarUnidadDidactica(idUnidad,tip);

            String titulo = data.optString("nom", unidad.getTit());

            String sit = data.optString("sit", unidad.getSitSig());
            long fecIni = data.optLong("ini", unidad.getFecIni().getTime());
            long fecFin = data.optLong("fin", unidad.getFecFin().getTime());

            unidad.setTit(titulo);
            unidad.setTip(tip.charAt(0));
            unidad.setSitSig(sit);
            unidad.setFecIni(new Date(fecIni));
            unidad.setFecFin(new Date(fecFin));
            unidad.setFecMod(new Date());

            unidadDao.update(unidad);
            return WebResponse.crearWebResponseExito("Se edito la unidad didactica");
        }catch (Exception e){
            logger.log(Level.SEVERE,"editarUnidadDidactica",e);
            return WebResponse.crearWebResponseError("No se puede editar la unidad");
        }
    }
    private WebResponse eliminarUnidadDidactica(JSONObject data){
        try{
            UnidadDidacticaDao unidadDao = (UnidadDidacticaDao)FactoryDao.buildDao("maestro.plan.UnidadDidacticaDao");
            int idUnidad = data.getInt("cod");
            String tip = data.getString("tip");
            UnidadDidactica unidad = unidadDao.buscarUnidadDidactica(idUnidad,tip);
            unidadDao.delete(unidad);

            return WebResponse.crearWebResponseExito("Se edito la unidad didactica");
        }catch (Exception e){
            logger.log(Level.SEVERE,"eliminarUnidadDidactica",e);
            return WebResponse.crearWebResponseError("No se puede eliminar la unidad");
        }
    }
    /**CRUD valores*/
    private WebResponse registrarValoresUnidad(JSONObject data){
        try{
            String val = data.getString("val");
            String cont = data.getString("con");
            String sit = data.getString("sit");


            ProgramacionAnualDao progDao = (ProgramacionAnualDao) FactoryDao.buildDao("maestro.plan.ProgramacionAnualDao");
            ProgramacionAnual prog = progDao.buscarProgramacionConValores(data.getInt("cod"));

            ValoresActitudes valor = new ValoresActitudes(val,cont,sit);

            ValorProgramacionAnualDao valorDao = (ValorProgramacionAnualDao) FactoryDao.buildDao("maestro.plan.ValorProgramacionAnualDao");
            valorDao.insert(valor);
            prog.getValores().add(valor);
            progDao.update(prog);

            JSONObject rspData = new JSONObject().put("cod",valor.getValActId());
            return WebResponse.crearWebResponseExito("Se registro el valor didactica",rspData);
        }catch (Exception e){
            logger.log(Level.SEVERE,"registrarValoresUnidad",e);
            return WebResponse.crearWebResponseError("No se puede registrar el valor");
        }
    }
    private WebResponse listarValoresUnidad(JSONObject data){
        try{
            ProgramacionAnualDao progDao = (ProgramacionAnualDao) FactoryDao.buildDao("maestro.plan.ProgramacionAnualDao");
            List<ValoresActitudes> vals = progDao.buscarValoresDeProgramacion(data.getInt("cod"));

            return WebResponse.crearWebResponseExito("Se listaron todos los valores", new JSONArray(EntityUtil.listToJSONString(
                    new String[]{"valActId", "val", "cont","sitSig"},
                    new String[]{"cod", "val", "con","sit"},
                    vals
            )));
        }catch (Exception e){
            logger.log(Level.SEVERE,"eliminarUnidadDidactica",e);
            return WebResponse.crearWebResponseError("No se puede listar los elementos de la unidad");
        }
    }
    private WebResponse editarValoresUnidad(JSONObject data){
        try{
            String val = data.getString("val");
            String cont = data.getString("con");
            String sit = data.getString("sit");
            int cod = data.getInt("cod");
            //int progId = data.getInt("prog");
            //ProgramacionAnualDao progDao = (ProgramacionAnualDao) FactoryDao.buildDao("maestro.plan.ProgramacionAnualDao");
            ValorProgramacionAnualDao valorDao = (ValorProgramacionAnualDao) FactoryDao.buildDao("maestro.plan.ValorProgramacionAnualDao");
            //ProgramacionAnual prog = progDao.buscarProgramacionConValores(progId);

            ValoresActitudes valor = valorDao.buscarValorPorId(cod);

            valor.setCont(cont);
            valor.setVal(val);
            valor.setSitSig(sit);

            valorDao.update(valor);
            //progDao.update(prog);

            return WebResponse.crearWebResponseExito("Se edito el valor didactica");
        }catch (Exception e){
            logger.log(Level.SEVERE,"registrarValoresUnidad",e);
            return WebResponse.crearWebResponseError("No se puede editar el valor");
        }
    }
    private WebResponse eliminarValoresUnidad(JSONObject data){
        try{
            int idProg = data.getInt("prog");
            int cod = data.getInt("cod");

            ProgramacionAnualDao progDao = (ProgramacionAnualDao) FactoryDao.buildDao("maestro.plan.ProgramacionAnualDao");
            ValorProgramacionAnualDao valorDao = (ValorProgramacionAnualDao) FactoryDao.buildDao("maestro.plan.ValorProgramacionAnualDao");

            ProgramacionAnual prog = progDao.buscarProgramacionConValores(idProg);
            ValoresActitudes valor = valorDao.buscarValorPorId(cod);
            prog.getValores().remove(valor);
            progDao.update(prog);

            valorDao.deleteAbsolute(valor);
            return WebResponse.crearWebResponseExito("Se elimino el valor");
        }catch (Exception e){
            logger.log(Level.SEVERE,"registrarValoresUnidad",e);
            return WebResponse.crearWebResponseError("No se puede eliminar el valor");
        }
    }
}
