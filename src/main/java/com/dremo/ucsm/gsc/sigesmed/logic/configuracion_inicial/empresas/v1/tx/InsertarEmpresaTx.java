/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.configuracion_inicial.empresas.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.EmpresaDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.cpe.LibroAsistenciaDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Empresa;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import org.json.JSONArray;
import org.json.JSONObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.util.Date;

/**
 *
 * @author carlos
 */
public class InsertarEmpresaTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
        
        Empresa nuevaEmpresa=null;
        
        try{
           JSONObject requestData = (JSONObject)wr.getData();
           JSONObject empresa=requestData.getJSONObject("empresa");
           String razon_social=empresa.getString("razonSocial");
           String ruc=empresa.getString("ruc");
           String pag_web=empresa.getString("pagWeb");
           String telefono=empresa.getString("telefono");
           char estado=empresa.getString("estado").charAt(0);
           
           nuevaEmpresa=new Empresa(ruc, razon_social, telefono, pag_web, new Date(), wr.getIdUsuario(), estado);
                   
           
        }catch(Exception e){
            return WebResponse.crearWebResponseError("No se pudo verificar los datos", e.getMessage() );
        }

        
        EmpresaDao empresaDao = (EmpresaDao)FactoryDao.buildDao("EmpresaDao");
        
        try{
           empresaDao.insert(nuevaEmpresa);
        }catch(Exception e){
            return WebResponse.crearWebResponseError("No se pudo Insertar la Empresa ", e.getMessage() );
        }

        JSONObject oRes=new JSONObject();
        
        oRes.put("empresaID",nuevaEmpresa.getEmpId());
        oRes.put("razonSocial",nuevaEmpresa.getRazonSocial());
        oRes.put("ruc",nuevaEmpresa.getRuc());
        oRes.put("estado",nuevaEmpresa.getEstReg()+"");
        oRes.put("pagWeb",nuevaEmpresa.getWeb());
        oRes.put("telefono",nuevaEmpresa.getNum());    
        
        return WebResponse.crearWebResponseExito("Se Inserto correctamente",oRes);        
        //Fin
    }
    
}

