/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.documentos_comunicacion.configuracion.v1.core;

import com.dremo.ucsm.gsc.sigesmed.core.service.WebComponent;
import com.dremo.ucsm.gsc.sigesmed.core.service.constantes.Sigesmed;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.IComponentRegister;
import com.dremo.ucsm.gsc.sigesmed.logic.documentos_comunicacion.configuracion.v1.tx.EditarPlantillaTx;
import com.dremo.ucsm.gsc.sigesmed.logic.documentos_comunicacion.configuracion.v1.tx.EliminarPlantillaTx;
import com.dremo.ucsm.gsc.sigesmed.logic.documentos_comunicacion.configuracion.v1.tx.ListarImagenesTx;
import com.dremo.ucsm.gsc.sigesmed.logic.documentos_comunicacion.configuracion.v1.tx.ListarPlantillasTx;
import com.dremo.ucsm.gsc.sigesmed.logic.documentos_comunicacion.configuracion.v1.tx.RegistrarPlantillaTx;




/**
 *
 * @author ucsm
 */
public class ComponentRegister implements IComponentRegister{
    @Override
    public WebComponent createComponent(){
        //Asiganando el modulo al que pertenece
        WebComponent component = new WebComponent(Sigesmed.MODULO_DOCUMENTOS_COMUNICACION);        
        
        //Registrnado el Nombre del componente
        component.setName("configuracion");
        //Version del componente
        component.setVersion(1);
        //Lista de operaciones de logica, propias del componente
        component.addTransactionPOST("editarPlantilla", EditarPlantillaTx.class);
        component.addTransactionGET("listarPlantillas", ListarPlantillasTx.class);
        component.addTransactionPUT("registrarPlantilla", RegistrarPlantillaTx.class);
        component.addTransactionPUT("eliminarPlantilla", EliminarPlantillaTx.class);
        component.addTransactionGET("listarImagenes", ListarImagenesTx.class);
      
        
        return component;
    }
}
