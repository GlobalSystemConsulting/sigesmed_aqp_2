package com.dremo.ucsm.gsc.sigesmed.logic.maestro.unidad.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.maestro.plan.EvaluacionesUnidadDidacticaDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.plan_curricular.EvaluacionesUnidadDidactica;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import org.json.JSONObject;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by Administrador on 03/01/2017.
 */
public class EliminarEvaluacionesUnidadTx implements ITransaction{
    private static Logger logger = Logger.getLogger(EliminarEvaluacionesUnidadTx.class.getName());
    @Override
    public WebResponse execute(WebRequest wr) {
        JSONObject data = (JSONObject) wr.getData();
        int idEvaluacion = data.getInt("id");
        return eliminarEvaluacion(idEvaluacion);
    }

    private WebResponse eliminarEvaluacion(int idEvaluacion) {
        try{
            EvaluacionesUnidadDidacticaDao evaDao = (EvaluacionesUnidadDidacticaDao) FactoryDao.buildDao("maestro.plan.EvaluacionesUnidadDidacticaDao");
            EvaluacionesUnidadDidactica evaluacion = evaDao.buscarEvaluacionId(idEvaluacion);
            evaDao.delete(evaluacion);
            return WebResponse.crearWebResponseExito("no se puede eliminar la evaluacion");
        }catch (Exception e){
            logger.log(Level.SEVERE,"eliminarEvaluacion",e);
            return WebResponse.crearWebResponseError("no se puede eliminar la evaluacion");
        }
    }
}
