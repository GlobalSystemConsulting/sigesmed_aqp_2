/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.ma.utiles.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.OrganizacionDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.UsuarioDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.ma.ListaArticuloDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.ma.ListaUtilesDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.ma.ArticuloEscolar;
import com.dremo.ucsm.gsc.sigesmed.core.entity.ma.ListaArticulo;
import com.dremo.ucsm.gsc.sigesmed.core.entity.ma.ListaUtiles;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONObject;

/**
 *
 * @author ucsm
 */
public class RegistrarPlantillaListaTx implements ITransaction{
    
    private final static Logger logger = Logger.getLogger(RegistrarPlantillaListaTx.class.getName());
    
    @Override
    public WebResponse execute(WebRequest wr) {
        JSONObject jsonData = (JSONObject)wr.getData();
        int idLista = jsonData.getInt("idLis");
        return registrarPlantLista(idLista);
    }
    
    private WebResponse registrarPlantLista(int idLis){
        try{
            ListaUtilesDao listaUtilesDao = (ListaUtilesDao) FactoryDao.buildDao("ma.ListaUtilesDao");
            OrganizacionDao organizacionDao = (OrganizacionDao)FactoryDao.buildDao("OrganizacionDao");
            UsuarioDao usuarioDao = (UsuarioDao) FactoryDao.buildDao("UsuarioDao");
            ListaArticuloDao listaArticuloDao = (ListaArticuloDao) FactoryDao.buildDao("ma.ListaArticuloDao");
            //Obtener lista de utiles a clonar
            ListaUtiles lisUtiles = listaUtilesDao.buscarPorId(idLis);
            
            //Insertamos lista nueva
            String titulo = "copia "+lisUtiles.getTituloLista();
            
            ListaUtiles listaUtilesNueva = new ListaUtiles(lisUtiles.getAnioLista(),titulo,lisUtiles.getTipoLista(),lisUtiles.getRecomendacionLista(),lisUtiles.getPrecioTotal(),lisUtiles.getOrganizacion(),lisUtiles.getUsuId(),lisUtiles.getNivel(),lisUtiles.getGrado(),lisUtiles.getArea(),lisUtiles.getSeccion());
            
            listaUtilesNueva.setFecMod(new Date());
            listaUtilesNueva.setEstReg('A');
            
            listaUtilesDao.insert(listaUtilesNueva);
            
            //Obtener cargos de lista de utiles a clonar e insertar articulos en lista nueva
            List<ArticuloEscolar> artiDeLista = listaArticuloDao.listarArticulosDeLista(idLis);
            registrarArticulos(artiDeLista,listaUtilesNueva,lisUtiles,listaArticuloDao);
            
            return WebResponse.crearWebResponseError("Exito al realizar el registro",WebResponse.BAD_RESPONSE);
        }catch(Exception e){
            logger.log(Level.SEVERE,"registrarLista",e);
            return WebResponse.crearWebResponseError("Error al realizar el registro",WebResponse.BAD_RESPONSE);
        }
    }
    public static void registrarArticulos(List<ArticuloEscolar> artiDeLista,  ListaUtiles listaUtiles,ListaUtiles listaAnterior,ListaArticuloDao listaArticuloDao){
        ListaArticulo lart;
        for (int i=0;i<artiDeLista.size();i++){
            try{
                lart = listaArticuloDao.buscarArticulo(listaAnterior.getListUtiId(), artiDeLista.get(i).getArtEscId());
                ListaArticulo listaArticulo = new ListaArticulo(listaUtiles,artiDeLista.get(i),lart.getCanArt(),lart.getDesArt(),lart.getPreArt());
                listaArticuloDao.insert(listaArticulo);
            }catch (Exception e){
                logger.log(Level.SEVERE,"registrarArticuloDeLista",e);
            }
        }
    }
}
