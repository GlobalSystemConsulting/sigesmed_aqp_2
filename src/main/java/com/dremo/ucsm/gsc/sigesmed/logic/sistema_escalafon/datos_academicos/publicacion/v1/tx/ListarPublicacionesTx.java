/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.datos_academicos.publicacion.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.PublicacionDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Publicacion;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author gscadmin
 */
public class ListarPublicacionesTx implements ITransaction{

    private static final Logger logger = Logger.getLogger(ListarPublicacionesTx.class.getName());
    
    @Override
    public WebResponse execute(WebRequest wr) {
        /*
        *  Parte para la operacion en la Base de Datos
        */        
        
        JSONObject requestData = (JSONObject)wr.getData();
        Integer ficEscId = requestData.getInt("ficEscId");
                
        List<Publicacion> publicaciones = null;
        PublicacionDao publicacionDao = (PublicacionDao)FactoryDao.buildDao("se.PublicacionDao");
        
        try{
            publicaciones = publicacionDao.listarxFichaEscalafonaria(ficEscId);
        
        }catch(Exception e){
            logger.log(Level.SEVERE,"Listar publicaciones",e);
            System.out.println("No se pudo listar las publicaciones\n"+e);
            return WebResponse.crearWebResponseError("No se pudo listar las publicaciones", e.getMessage() );
        }
        
        
        //Fin
             
        /*
        *  Repuesta Correcta
        */
        JSONArray miArray = new JSONArray();
        for(Publicacion p:publicaciones ){
            JSONObject oResponse = new JSONObject();
            oResponse.put("pubId", p.getPubId());
            oResponse.put("nomEdi", p.getNomEdi());
            oResponse.put("tipPub", p.getTipPub());
            oResponse.put("titPub", p.getTipPub());
            oResponse.put("graPar", p.getGraPar());
            oResponse.put("lug", p.getLug());
            oResponse.put("fecPub", p.getFecPub());
            oResponse.put("numReg", p.getNumReg());
            
            miArray.put(oResponse);
        }
        
        return WebResponse.crearWebResponseExito("Las publicaciones fueron listadas exitosamente", miArray);
    }
    
}
