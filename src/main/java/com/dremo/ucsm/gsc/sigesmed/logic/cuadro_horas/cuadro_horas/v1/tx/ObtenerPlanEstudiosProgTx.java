/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.cuadro_horas.cuadro_horas.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import org.json.JSONArray;
import org.json.JSONObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.util.List;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.plan_curricular.ProgramacionAnual;
import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.maestro.plan.ProgramacionAnualDao;


/**
 *
 * @author abel
 */
public class ObtenerPlanEstudiosProgTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
        
            int prog_an_id;
        try{
            JSONObject requestData = (JSONObject)wr.getData();
            prog_an_id = requestData.getInt("prog_id");
            
            ProgramacionAnualDao prog_an_dao = (ProgramacionAnualDao)FactoryDao.buildDao("maestro.plan.ProgramacionAnualDao");
            ProgramacionAnual prog_an = prog_an_dao.obtenerPlanProgr(prog_an_id);
            
             JSONObject oResponse = new JSONObject();
             oResponse.put("plan_id",prog_an.getPla_est_id());
              return WebResponse.crearWebResponseExito("Se Obtuvo el Plan de Estudios Correctamente",oResponse);        
            
        }catch(Exception e){
            System.out.println(e);
            return WebResponse.crearWebResponseError("No se pudo Obtener el Plan de Estudios", e.getMessage() ); 
        }
        
        
    //    throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    
    
    
    
}
