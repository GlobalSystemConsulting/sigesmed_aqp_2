/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.control_personal.reportes.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.logic.documentos_gestion.entorno_inicio.v1.tx.*;
import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.cpe.AsistenciaAulaDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.cpe.AsistenciaDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.cpe.InasistenciaDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Organizacion;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Trabajador;
import com.dremo.ucsm.gsc.sigesmed.core.entity.cpe.AsistenciaAula;
import com.dremo.ucsm.gsc.sigesmed.core.entity.cpe.DiasEspeciales;
import com.dremo.ucsm.gsc.sigesmed.core.entity.cpe.Docente;
import com.dremo.ucsm.gsc.sigesmed.core.entity.cpe.Inasistencia;
import com.dremo.ucsm.gsc.sigesmed.core.entity.cpe.PlazaMagisterial;
import com.dremo.ucsm.gsc.sigesmed.core.service.ServicioREST;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.constantes.Sigesmed;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import com.dremo.ucsm.gsc.sigesmed.util.DateUtil;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author Carlos
 */
public class ReporteHEMesTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
        
        /*
        *   Parte para la lectura, verificacion y validacion de datos
        */
        
        Organizacion organizacion;
        Integer planNivelId;
        JSONObject requestData = (JSONObject)wr.getData();
        Integer mes;
        String nombreOrg="";
        String nivelNom="";
        try{
            
            Integer organizacionId = requestData.getInt("organizacionID");  
            nombreOrg = requestData.getString("organizacionNombre");  
            mes=requestData.getInt("mes");
            planNivelId=requestData.getInt("nivel");
            nivelNom=requestData.getString("nivelNombre");
            organizacion = new Organizacion(organizacionId);
            
    
        }catch(Exception e){
            System.out.println("No se pudo verificar los datos \n"+e);
            return WebResponse.crearWebResponseError("No se pudo verificar los datos ", e.getMessage() );
        }
        
        Calendar c1 = GregorianCalendar.getInstance();
        int anho=DateUtil.getAnioActual();
        int dia=DateUtil.getDiaActual();
        
        
        String mes_="";
        switch (mes) {
            case 1:
                c1.set(anho, Calendar.JANUARY, dia);
                mes_ = "Enero";
                break;
            case 2:
                c1.set(anho, Calendar.FEBRUARY, dia);
                mes_ = "Febrero";
                break;
            case 3:
                c1.set(anho, Calendar.MARCH, dia);
                mes_ = "Marzo";
                break;
            case 4:
                c1.set(anho, Calendar.APRIL, dia);
                mes_ = "Abril";
                break;
            case 5:
                c1.set(anho, Calendar.MAY, dia);
                mes_ = "Mayo";
                break;
            case 6:
                c1.set(anho, Calendar.JUNE, dia);
                mes_ = "Junio";
                break;
            case 7:
                c1.set(anho, Calendar.JULY, dia);
                mes_ = "Julio";
                break;
            case 8:
                c1.set(anho, Calendar.AUGUST, dia);
                mes_ = "Agosto";
                break;
            case 9:
                c1.set(anho, Calendar.SEPTEMBER, dia);
                mes_ = "Septiembre";
                break;
            case 10:
                c1.set(anho, Calendar.OCTOBER, dia);
                mes_ = "Octubre";
                break;
            case 11:
                c1.set(anho, Calendar.NOVEMBER, dia);
                mes_ = "Noviembre";
                break;
            case 12:
                c1.set(anho, Calendar.DECEMBER, dia);
                mes_ = "Diciembre";
                break;
        }
        
        Date fechaInicio=DateUtil.getInicioMes(c1.getTime());
        Date fechaFin=DateUtil.getFinMes(c1.getTime());
        
        /**VERIFICAR SABADOS Y DOMINGOS DEL MES**/
        
        Calendar calendar = Calendar.getInstance();
        calendar.set(DateUtil.getAnioActual(), mes-1, 1);
        int daysInMonth = calendar.getActualMaximum(Calendar.DAY_OF_MONTH);
        //1 dia laborable
        //2 sabado o domingo
        //3 dia feriadp
        char[] diasEfectivosMes=new char[daysInMonth];//dias calendarios
        
       
       
        for (int day = 1; day <= daysInMonth; day++) {
            calendar.set(DateUtil.getAnioActual(),mes-1, day);
            int dayOfWeek = calendar.get(Calendar.DAY_OF_WEEK);
            if (dayOfWeek == Calendar.SUNDAY || dayOfWeek == Calendar.SATURDAY) {
                diasEfectivosMes[day-1]='2';
            }else
            {
                 diasEfectivosMes[day-1]='1';
            }
            
        }
        /**FIN VERIFICAR SABADOS Y DOMINGOS DEL MES**/
        
        AsistenciaAulaDao asistenciaAulaDao= (AsistenciaAulaDao)FactoryDao.buildDao("cpe.AsistenciaAulaDao");
        InasistenciaDao inasistenciaDao= (InasistenciaDao)FactoryDao.buildDao("cpe.InasistenciaDao");
        AsistenciaDao asistenciaDao= (AsistenciaDao)FactoryDao.buildDao("cpe.AsistenciaDao");
        List<Docente> docentes=new ArrayList<>();
        List<DiasEspeciales> diasNoLaborables=null;
     
        try{
            diasNoLaborables=inasistenciaDao.getDiasEspeciales(organizacion, fechaInicio, fechaFin, Sigesmed.ESTADO_DIA_ESPECIAL_NO_LABORABLE);
            docentes=asistenciaAulaDao.listarDocenteAndPlazas(organizacion.getOrgId(), planNivelId);
            
        }catch(Exception e){
            System.out.println("No se pudo verificar la asistencia \n"+e);
            return WebResponse.crearWebResponseError("No se pudo verificar la asistencia ", e.getMessage() );
        }
        /**Actualizar dias no laborables **/
            for(DiasEspeciales day_:diasNoLaborables)
            {
                int idx=DateUtil.obtenerDiaSegunFecha(day_.getFecha())-1;
                if(diasEfectivosMes[idx]=='1')
                diasEfectivosMes[idx]='3';
            }
        /****/
        
        
        
        JSONArray miArray = new JSONArray();
        JSONObject oResponse = new JSONObject();
        /*Reporte*/
        String pathFinal = ServicioREST.PATH_SIGESMED+ File.separator +"archivos"+File.separator +"control_personal"+File.separator +"FORMATO_MES.xlsx";
        System.out.println("Working Directory = " +pathFinal);
                
        InputStream inp;
        
        
        
        try {
            inp = new FileInputStream(pathFinal);
            
            Workbook wb = WorkbookFactory.create(inp);
            Sheet sheet = wb.getSheetAt(0);
            
            Row rowData1 = sheet.getRow(5);
            rowData1.createCell(3).setCellValue(nombreOrg);
            
            Row rowData2 = sheet.getRow(6);
            rowData2.createCell(22).setCellValue(mes_);
            rowData2.createCell(3).setCellValue(nivelNom);
            
            int rowIndex=14;
            int nro=1;
            int sumatoriaHEtoDictar=0;
            int sumatoriaHEMensual=0;
            CellStyle cellStyle = wb.createCellStyle();
            cellStyle.setBorderBottom(CellStyle.BORDER_THIN);
            cellStyle.setBorderTop(CellStyle.BORDER_THIN);
            cellStyle.setBorderRight(CellStyle.BORDER_THIN);
            cellStyle.setBorderLeft(CellStyle.BORDER_THIN);
            
            for (Docente data : docentes) {
               
                Row row = sheet.createRow(rowIndex++);
                
                row.createCell(0).setCellValue(nro++);
                row.getCell(0).setCellStyle(cellStyle);
                row.createCell(1).setCellValue(data.getPersona().getNombrePersonaAP());
                row.getCell(1).setCellStyle(cellStyle);
                int jl = 0;
                int jpe = 0;
                for (PlazaMagisterial pm : data.getPlazas()) {
                    jl = jl + pm.getJornadaLaboral();
                    jpe = jpe + pm.getJornadaPedagogica();
                    PlazaMagisterial plaza=asistenciaAulaDao.getPlazaMagisterialAndDistribucion(pm.getPlaMagId());
                    
                    row.createCell(3).setCellValue(plaza.getDistribucionGrados().get(0).getGrado().getAbr()+"");
                    row.getCell(3).setCellStyle(cellStyle);
                    
                    row.createCell(4).setCellValue(plaza.getDistribucionGrados().get(0).getSecId()+"");
                    row.getCell(4).setCellStyle(cellStyle);
                }
                
                row.createCell(2).setCellValue(jl);
                row.getCell(2).setCellStyle(cellStyle);
               
                sumatoriaHEtoDictar+=jpe;
                row.createCell(5).setCellValue(jpe);   
                row.getCell(5).setCellStyle(cellStyle);
                
                
                /**DIAS EFECTIVOS DEL DOCENTE**/
                Integer[] horasEfectivos=new Integer[daysInMonth];//horas efectivos por dia
                List<AsistenciaAula> asistenciasDocente=asistenciaAulaDao.listarAsistenciaAula(data.getDocId(), fechaInicio, fechaFin);
                for(AsistenciaAula asi:asistenciasDocente)
                {
                    if(asi.getHorasPedagogicas()!=null)
                    {
                        if(horasEfectivos[DateUtil.obtenerDiaSegunFecha(asi.getHoraIngreso())-1]==null)
                        {
                            horasEfectivos[DateUtil.obtenerDiaSegunFecha(asi.getHoraIngreso())-1]=asi.getHorasPedagogicas();
                        }
                        else
                        {
                            horasEfectivos[DateUtil.obtenerDiaSegunFecha(asi.getHoraIngreso())-1]+=asi.getHorasPedagogicas();
                        }
                    }
                    
                }
                /**DIAS INASISTIDOS DEL DOCENTE**/
                //1 no justificado
                //2 sin justificar
                char[] inasistencias=new char[daysInMonth];
                List<Inasistencia> inasistenciasDoc=asistenciaDao.listarInasistencias(data.getPersona(),organizacion, fechaInicio, fechaFin);
                for(Inasistencia ina:inasistenciasDoc)
                {
                    if(ina.getJustificacion()==null)
                    {
                        inasistencias[DateUtil.obtenerDiaSegunFecha(ina.getInaFecha())-1]='1';
                    }
                    else{
                        inasistencias[DateUtil.obtenerDiaSegunFecha(ina.getInaFecha())-1]='2';
                    }
                }
                
                int columnaDias=6;
                int i=0;
                int sumatoriaHE=0;
                for(char idx_:diasEfectivosMes)
                {
                    if(idx_=='3')//si es feriado
                    {
                        row.createCell(columnaDias).setCellValue("F");
                        row.getCell(columnaDias).setCellStyle(cellStyle);
                        columnaDias++;
                    } else if(idx_=='1')//si es laborable
                    {
                        if(horasEfectivos[i]==null)
                        {
                            if(inasistencias[i]=='1')
                                row.createCell(columnaDias).setCellValue("I");
                            else if(inasistencias[i]=='2')
                            {
                                row.createCell(columnaDias).setCellValue("J");
                            }
                            else
                            {
                                row.createCell(columnaDias).setCellValue("");
                            }
                                        
                        }
                        else                            
                        {
                            row.createCell(columnaDias).setCellValue(horasEfectivos[i]);
                            sumatoriaHE+=horasEfectivos[i];
                        }
                        row.getCell(columnaDias).setCellStyle(cellStyle);
                        columnaDias++;
                        
                    }
                    
                    i++;
                }
                sumatoriaHEMensual+=sumatoriaHE;
                
                row.createCell(29).setCellValue(sumatoriaHE);
                row.getCell(29).setCellStyle(cellStyle);
            }
            Row rowSumatorias=sheet.getRow(rowIndex+1);
            rowSumatorias.getCell(5).setCellValue(sumatoriaHEtoDictar);
            
            rowSumatorias.getCell(29).setCellValue(sumatoriaHEMensual);
           
            
            FileOutputStream fileOut = new FileOutputStream(ServicioREST.PATH_SIGESMED+ File.separator +"archivos"+File.separator +"control_personal"+File.separator+"reporteMes.xlsx");
            wb.write(fileOut);
            fileOut.close();
            
        } catch (FileNotFoundException ex) {
            Logger.getLogger(ReporteHEMesTx.class.getName()).log(Level.SEVERE, null, ex);
            return WebResponse.crearWebResponseError("No se pudo generar el Reporte ", ex.getMessage() );
        } catch (IOException ex) {
            Logger.getLogger(ReporteHEMesTx.class.getName()).log(Level.SEVERE, null, ex);
            return WebResponse.crearWebResponseError("No se pudo generar el Reporte ", ex.getMessage() );
        } catch (InvalidFormatException ex) {
            Logger.getLogger(ReporteHEMesTx.class.getName()).log(Level.SEVERE, null, ex);
            return WebResponse.crearWebResponseError("No se pudo generar el Reporte ", ex.getMessage() );
        } catch (EncryptedDocumentException ex) {
            Logger.getLogger(ReporteHEMesTx.class.getName()).log(Level.SEVERE, null, ex);
            return WebResponse.crearWebResponseError("No se pudo generar el Reporte ", ex.getMessage() );
        }
        
        
        /*Fin Reporte*/
        
        
        
        
        miArray.put(oResponse);

        return WebResponse.crearWebResponseExito("Se genero el Reporte correctamente", miArray);
        //Fin
    }
}
