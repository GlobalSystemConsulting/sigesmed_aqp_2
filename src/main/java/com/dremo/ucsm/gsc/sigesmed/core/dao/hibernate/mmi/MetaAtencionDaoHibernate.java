package com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.mmi;

import com.dremo.ucsm.gsc.sigesmed.core.datastore.HibernateUtil;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mech.MetaAtencion;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

public class MetaAtencionDaoHibernate extends GenericMMIDaoHibernate<MetaAtencion> {

    public List<MetaAtencion> buscarGradoIE(int idOrg, int anyoEst) throws ParseException {
        Session session = HibernateUtil.getSessionFactory().openSession();      
        Transaction transaction = session.beginTransaction();
        Date myDate = getDate4MetaAtencion(anyoEst);
        List<MetaAtencion> result = new ArrayList<>();
        String hql;
        Query query;
        try {
            hql = "FROM MetaAtencionMMI AS ma "
                    + "JOIN PlanEstudiosMMI AS pe ON pe.orgId = :myOrg AND pe.anoEsc = :myAnyoEsc "
                    + "JOIN PlanNivelMMI AS pn ON pn.plaEstId = pe.plaEstId";
            query = session.createQuery(hql);
            query.setInteger("myOrg", idOrg);
            query.setDate("myAnyoEsc", myDate);
            result = query.list();
            transaction.commit();
        } catch (Exception ex) {
            transaction.rollback();
            throw ex;
        } finally {
            session.close();
        }
        return result;
    }

    private Date getDate4MetaAtencion(int year) throws ParseException {
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        String parse = "01/01/"+year;
        Date d = sdf.parse(parse);
        return d;
    }
}
