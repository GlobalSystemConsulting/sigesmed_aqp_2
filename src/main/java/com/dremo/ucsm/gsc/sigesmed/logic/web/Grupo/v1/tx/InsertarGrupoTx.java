/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.web.Grupo.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.web.GrupoDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.UsuarioSession;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import org.json.JSONObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import com.dremo.ucsm.gsc.sigesmed.core.entity.web.Grupo;
import java.util.ArrayList;
import java.util.Date;
import org.json.JSONArray;

/**
 *
 * @author abel
 */
public class InsertarGrupoTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
                
        /*
        *   Parte para la lectura, verificacion y validacion de datos
        */        
        
        Grupo nuevoGrupo = null;
        try{
            
            JSONObject requestData = (JSONObject)wr.getData();
            boolean tipo = requestData.getBoolean("tipo");
            String nombre = requestData.getString("nombre");
            int organizacionID = requestData.getInt("organizacionID");
            String estado = requestData.getString("estado");
            
            JSONArray roles = requestData.optJSONArray("usuarios");
            
            nuevoGrupo = new Grupo(0, tipo, nombre, organizacionID, new Date(), wr.getIdUsuario(), estado.charAt(0));
            if( roles != null ){
                nuevoGrupo.setUsuarios(new ArrayList<UsuarioSession>());
                for(int i = 0; i < roles.length();i++){
                    JSONObject bo = roles.getJSONObject(i);
                    nuevoGrupo.getUsuarios().add( new UsuarioSession( bo.getInt("sessionID") ) );
                }
            }            
        
        }catch(Exception e){
            System.out.println(e);
            return WebResponse.crearWebResponseError("No se pudo registrar el grupo, datos incorrectos", e.getMessage() );
        }
        //Fin
        
        /*
        *   Parte de Logica de Negocio
        *   descripcion: El Sitema debe generar el codigo para el nuevo Tipo de Tramite antes de insertar a la BD
        */
        
        
        //Fin
        
        /*
        *  Parte para la operacion en la Base de Datos
        */
        
        GrupoDao grupoDao = (GrupoDao)FactoryDao.buildDao("web.GrupoDao");
        try{
            grupoDao.insert(nuevoGrupo);            
        }catch(Exception e){
            System.out.println(e);
            return WebResponse.crearWebResponseError("No se pudo registrar el grupo", e.getMessage() );
        }
        //Fin
        
        /*
        *  Repuesta Correcta
        */
        JSONObject oResponse = new JSONObject();
        oResponse.put("grupoID",nuevoGrupo.getGruId());
        return WebResponse.crearWebResponseExito("El registro del grupo se realizo correctamente", oResponse);
        //Fin
    }    
    
    
}
