/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.sma.programacion_monitoreo.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.sma.MonitoreoDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sma.Monitoreo;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sma.Organizacion;
import com.dremo.ucsm.gsc.sigesmed.core.service.ServicioREST;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.base.FileJsonObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.constantes.Sigesmed;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import com.dremo.ucsm.gsc.sigesmed.util.BuildFile;
import java.io.File;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONObject;

/**
 *
 * @author Felipe
 */
public class ActualizarMonitoreoTx implements ITransaction{
    
    private static final Logger logger = Logger.getLogger(ActualizarMonitoreoTx.class.getName());

@Override
    public WebResponse execute(WebRequest wr) {
        JSONObject data = (JSONObject) wr.getData();
        Integer usuarioId = wr.getIdUsuario();
        return editarMonitoreo(data, usuarioId);
    }

    private WebResponse editarMonitoreo(JSONObject data, int usuarioId) {
        try{
            Integer monId = data.getInt("monId");
            Integer numEsp = data.getInt("numEsp");
            Integer idIE = data.getInt("idIE");
            String nomIE = data.getString("nomIE");
            String nivIE = data.getString("nivIE");

            MonitoreoDao monitoreoDao = (MonitoreoDao) FactoryDao.buildDao("sma.MonitoreoDao");
            Monitoreo monitoreo = monitoreoDao.buscarPorId(monId);

            monitoreo.setNumEsp(numEsp);
            monitoreo.setFecMod(new Date());
            monitoreo.setUsuMod(usuarioId);
            

            //Datos del archivo
            String nomArchivo = data.optString("nomArchivo");
            JSONObject arcJSON = data.optJSONObject("archivo");
            
            FileJsonObject file = null;

            if(arcJSON != null && arcJSON.length() > 0){
                file = new FileJsonObject(arcJSON, monId + "_doc_mon_");
            }else{
                return WebResponse.crearWebResponseError("No se pudo leer el archivo, datos incorrectos");  
            }
            
            monitoreo.setMniUrl(file.concatName("" + monId) );
            BuildFile.buildFromBase64("monitoreo_y_acompañamiento", monitoreo.getMniUrl(), file.getData());
            monitoreoDao.update(monitoreo);
            
            
            DateFormat sdo = new SimpleDateFormat("dd-MM-yyyy");
            
            JSONObject oResponse = new JSONObject();
            oResponse.put("monId", monitoreo.getMniId());
            oResponse.put("codMon", monitoreo.getMniCod());
            oResponse.put("anioMon", monitoreo.getMniYy());
            oResponse.put("nomMon", monitoreo.getMniNom());
            oResponse.put("idIE", idIE);
            oResponse.put("nomIE", nomIE);
            oResponse.put("nivIE", nivIE);
            oResponse.put("nombreURL", monitoreo.getMniUrl());
            oResponse.put("fecReg", sdo.format(monitoreo.getFecReg()));
            oResponse.put("numEsp", monitoreo.getNumEsp());
            oResponse.put("numDoc", monitoreo.getNumDoc());
            oResponse.put("estMon", monitoreo.getEstMon());

            return WebResponse.crearWebResponseExito("Se actualizo correctamente el monitoreo");
        }catch (Exception e){
            logger.log(Level.SEVERE,"actualizarMonitoreo",e);
            return WebResponse.crearWebResponseError("No se pudo actualizar el monitoreo");
        }
    }
    
    
}
