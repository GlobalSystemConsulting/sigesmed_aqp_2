/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.sma.plantilla_compromisos.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.sma.CompromisosGestionDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sma.CompromisosGestion;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sma.PlantillaFicha;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import org.json.JSONObject;

/**
 *
 * @author Administrador
 */
public class InsertarCompromisoTx implements ITransaction{
    @Override
    public WebResponse execute(WebRequest wr) {
                
        /*
        *   Parte para la lectura, verificacion y validacion de datos
        */
        CompromisosGestion compromiso = null;
        try{
            JSONObject requestData = (JSONObject)wr.getData();
            
            int plaId = requestData.getInt("plaId");
            String comNom = requestData.optString("comNom");
                        
            compromiso = new CompromisosGestion(comNom, new PlantillaFicha(plaId));
            
        
        }catch(Exception e){            
            return WebResponse.crearWebResponseError("No se pudo registrar, datos incorrectos", e.getMessage() );
        }
        //Fin
                
        /*
        *  Parte para la operacion en la Base de Datos
        */
        CompromisosGestionDao compromisoDao = (CompromisosGestionDao)FactoryDao.buildDao("sma.CompromisosGestionDao");
        try{
            compromisoDao.insert(compromiso);
        
        }catch(Exception e){
            System.out.println("No se pudo registrar\n"+e);
            return WebResponse.crearWebResponseError("No se pudo registrar", e.getMessage() );
        }
        //Fin        
        
        /*
        *  Repuesta Correcta
        */
        JSONObject oResponse = new JSONObject();
        oResponse.put("gruId",compromiso.getCgeId());
      
        return WebResponse.crearWebResponseExito("El registro del compromiso se realizo correctamente", oResponse);
        //Fin
    }
    
}
