/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao;

import com.dremo.ucsm.gsc.sigesmed.core.entity.Rol;
import java.util.List;

/**
 *
 * @author abel
 */
public interface RolDao extends GenericDao<Rol>{
    
    public List<Rol> buscarConFunciones();
    List<Rol> buscarPorOrganizacion(int orgID);
    public void eliminarFunciones(int rolID);    
}
