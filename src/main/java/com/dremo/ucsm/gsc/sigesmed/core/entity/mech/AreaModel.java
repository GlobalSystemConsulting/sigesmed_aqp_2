/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.entity.mech;

/**
 *
 * @author abel
 */
public class AreaModel {
    public int areaID;
    public int gradoID;
    
    public String area;
    public String grado;
    
    public AreaModel(int areaID,String area,int gradoID,String grado){
        this.areaID = areaID;
        this.area = area;
        this.gradoID = gradoID;
        this.grado = grado;
    }
}
