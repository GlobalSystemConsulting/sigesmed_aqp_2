package com.dremo.ucsm.gsc.sigesmed.core.entity.capacitacion;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity(name = "OrganizacionCapacitacion")
@Table(name = "organizacion",schema = "public")
public class Organizacion implements Serializable {
    @Id
    @Column(name = "org_id", unique = true, nullable = false)
    @SequenceGenerator(name = "secuencia_organizacion", sequenceName = "organizacion_org_id_seq" )
    @GeneratedValue(generator = "secuencia_organizacion")
    private int orgId;
    
    @Column(name = "cod", nullable = false, length = 16)
    private String cod;
    
    @Column(name = "nom", nullable = false, length = 64)
    private String nom;
    
    @Column(name = "ali", nullable = false, length = 64)
    private String ali;
    
    @Column(name = "est_reg", length = 1)
    private Character estReg;

    @OneToMany(mappedBy = "organizacionAut")
    List<CursoCapacitacion> capacitacionesAut = new ArrayList<>();
    
    @OneToMany(mappedBy = "organizacion")
    List<CursoCapacitacion> capacitacionesOrg = new ArrayList<>();
    
    public int getOrgId() {
        return orgId;
    }

    public void setOrgId(int orgId) {
        this.orgId = orgId;
    }

    public String getCod() {
        return cod;
    }

    public void setCod(String cod) {
        this.cod = cod;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getAli() {
        return ali;
    }

    public void setAli(String ali) {
        this.ali = ali;
    }

    public Character getEstReg() {
        return estReg;
    }

    public void setEstReg(Character estReg) {
        this.estReg = estReg;
    }

    public List<CursoCapacitacion> getCapacitacionesAut() {
        return capacitacionesAut;
    }

    public void setCapacitacionesAut(List<CursoCapacitacion> capacitacionesAut) {
        this.capacitacionesAut = capacitacionesAut;
    }

    public List<CursoCapacitacion> getCapacitacionesOrg() {
        return capacitacionesOrg;
    }

    public void setCapacitacionesOrg(List<CursoCapacitacion> capacitacionesOrg) {
        this.capacitacionesOrg = capacitacionesOrg;
    }
}
