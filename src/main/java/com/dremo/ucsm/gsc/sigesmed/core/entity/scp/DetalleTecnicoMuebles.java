/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.entity.scp;
import java.util.Date;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.OneToOne;
import java.util.List;


/**
 *
 * @author Jeferson
 */
@Entity
@Table(name="detalle_tecnico_muebles", schema="administrativo")
public class DetalleTecnicoMuebles implements java.io.Serializable {
    
    @Id
    @Column(name="cod_bie")
    private int cod_bie;
    

    
    @Column(name="marc")
    private String marc;
    
    @Column(name="mod")
    private String mod;
    
    @Column(name="cond")
    private String cond;
    
    @Column(name="dim")
    private int dim;
    
    @Column(name="ser")
    private String ser;
    
    @Column(name="col")
    private String col;
    
    @Column(name="tip")
    private String tip;
    
    @Column(name="nro_mot")
    private String nro_mot;
    
    @Column(name="nro_pla")
    private String nro_pla;
    
    
    @Column(name="nro_cha")
    private String nro_cha;
    
    @Column(name="raza")
    private String raza;
    
    @Column(name="edad")
    private int edad;
    
    @Column(name="rut_imag_1")
    private String rut_imag_1;
    
    @Column(name="rut_imag_2")
    private String rut_imag_2;
    
    @Column(name="rut_aut_img")
    private String rut_aut_img;
    
    @Column(name="est_reg")
    private char est_reg;

    
    
    public void setCod_bie(int cod_bie) {
        this.cod_bie = cod_bie;
    }

    public void setMarc(String marc) {
        this.marc = marc;
    }

    public void setMod(String mod) {
        this.mod = mod;
    }

    public void setCond(String cond) {
        this.cond = cond;
    }

    public void setDim(int dim) {
        this.dim = dim;
    }

    public void setSer(String ser) {
        this.ser = ser;
    }

    public void setCol(String col) {
        this.col = col;
    }

    public void setTip(String tip) {
        this.tip = tip;
    }

    public void setNro_mot(String nro_mot) {
        this.nro_mot = nro_mot;
    }

    public void setNro_pla(String nro_pla) {
        this.nro_pla = nro_pla;
    }

    public void setNro_cha(String nro_cha) {
        this.nro_cha = nro_cha;
    }

    public void setRaza(String raza) {
        this.raza = raza;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }

    public void setRut_imag_1(String rut_imag_1) {
        this.rut_imag_1 = rut_imag_1;
    }

    public void setRut_imag_2(String rut_imag_2) {
        this.rut_imag_2 = rut_imag_2;
    }

    public void setRut_aut_img(String rut_aut_img) {
        this.rut_aut_img = rut_aut_img;
    }

    public void setEst_reg(char est_reg) {
        this.est_reg = est_reg;
    }

   

    public int getCod_bie() {
        return cod_bie;
    }

    public String getMarc() {
        return marc;
    }

    public String getMod() {
        return mod;
    }

    public String getCond() {
        return cond;
    }

    public int getDim() {
        return dim;
    }

    public String getSer() {
        return ser;
    }

    public String getCol() {
        return col;
    }

    public String getTip() {
        return tip;
    }

    public String getNro_mot() {
        return nro_mot;
    }

    public String getNro_pla() {
        return nro_pla;
    }

    public String getNro_cha() {
        return nro_cha;
    }

    public String getRaza() {
        return raza;
    }

    public int getEdad() {
        return edad;
    }

    public String getRut_imag_1() {
        return rut_imag_1;
    }

    public String getRut_imag_2() {
        return rut_imag_2;
    }

    public String getRut_aut_img() {
        return rut_aut_img;
    }

    public char getEst_reg() {
        return est_reg;
    }

    public DetalleTecnicoMuebles(int cod_bie, String marc, String mod, String cond, int dim, String ser, String col, String tip, String nro_mot, String nro_pla, String nro_cha, String raza, int edad, String rut_imag_1, String rut_imag_2, String rut_aut_img, char est_reg) {
        this.cod_bie = cod_bie;
        this.marc = marc;
        this.mod = mod;
        this.cond = cond;
        this.dim = dim;
        this.ser = ser;
        this.col = col;
        this.tip = tip;
        this.nro_mot = nro_mot;
        this.nro_pla = nro_pla;
        this.nro_cha = nro_cha;
        this.raza = raza;
        this.edad = edad;
        this.rut_imag_1 = rut_imag_1;
        this.rut_imag_2 = rut_imag_2;
        this.rut_aut_img = rut_aut_img;
        this.est_reg = est_reg;
    }

    public DetalleTecnicoMuebles() {
    }

}
