/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.ma;

import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.GenericDaoHibernate;
import static com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.mep.MepReportDaoHibernate.logger;
import com.dremo.ucsm.gsc.sigesmed.core.dao.ma.ArticuloEscolarDao;
import com.dremo.ucsm.gsc.sigesmed.core.datastore.HibernateUtil;
import com.dremo.ucsm.gsc.sigesmed.core.entity.ma.ArticuloEscolar;
import java.util.List;
import java.util.logging.Level;
import org.hibernate.Query;
import org.hibernate.Session;

/**
 *
 * @author ucsm
 */
public class ArticuloEscolarDaoHibernate extends GenericDaoHibernate<ArticuloEscolar> implements ArticuloEscolarDao{

    @Override
    public ArticuloEscolar buscarPorId(int id) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            ArticuloEscolar articuloEscolar = (ArticuloEscolar)session.get(ArticuloEscolar.class,id);
            return articuloEscolar;
        }catch (Exception e){
            logger.log(Level.SEVERE,"buscarPorId",e);
            throw e;
        }finally {
            session.close();
        }
    }

    @Override
    public ArticuloEscolar buscarArticuloPorNombre(String nombre) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            System.out.println(" entro a buscaArticulo()");
            String sql = "SELECT c FROM ArticuloEscolar c WHERE c.estReg!='E' AND c.nombreArticulo LIKE upper(:nombre)";
            //SQLQuery query = session.createSQLQuery(sql);
  
            Query query = session.createQuery(sql);
            query.setString("nombre", nombre);
            query.setMaxResults(1);
            return (ArticuloEscolar) query.uniqueResult();
        }catch (Exception e){
            throw e;
        }finally {
            session.close();
        }
    }
    
    @Override
    public List<ArticuloEscolar> buscarArtiPorNombre(String nombre) {
        List<ArticuloEscolar> listArti = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            String hql = "SELECT c FROM ArticuloEscolar c WHERE c.nomArticulo LIKE upper(:nombre) AND c.estReg!='E'";
            Query query = session.createQuery(hql);
            query.setString("nombre", '%'+nombre+'%');
            listArti = query.list();
        }catch (Exception e){
            System.out.println("No se pudo encontrar el articulo \\n "+ e.getMessage());
            throw new UnsupportedOperationException("No se pudo encontrar el articulo\\n "+ e.getMessage());            
        }finally {
            session.close();
        }
        return listArti;
    }
}
