/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.tramite_documentario.documento_estado_prioridad.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.std.TipoDocumentoDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import org.json.JSONObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import com.dremo.ucsm.gsc.sigesmed.core.entity.std.TipoDocumento;
import java.util.Date;

/**
 *
 * @author abel
 */
public class ActualizarTipoDocumentoTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
                
        /*
        *   Parte para la lectura, verificacion y validacion de datos
        */
        TipoDocumento tipoDocumentoAct = null;
        try{
            JSONObject requestData = (JSONObject)wr.getData();
            int tipoDocumentoID = requestData.getInt("tipoDocumentoID");
            String nombre = requestData.getString("nombre");
            String descripcion = requestData.getString("descripcion");
            String estado = requestData.getString("estado");
            
            tipoDocumentoAct = new TipoDocumento(tipoDocumentoID, nombre, descripcion, new Date(), wr.getIdUsuario(), estado.charAt(0));
        
        }catch(Exception e){
            System.out.println(e);
            return WebResponse.crearWebResponseError("No se pudo actualizar, datos incorrectos");
        }
        //Fin
        
        /*
        *  Parte para la operacion en la Base de Datos
        */
        TipoDocumentoDao documentoDao = (TipoDocumentoDao)FactoryDao.buildDao("std.TipoDocumentoDao");
        try{
            documentoDao.update(tipoDocumentoAct);
        
        }catch(Exception e){
            System.out.println("No se pudo actualizar el Tipo de Documento\n"+e);
            return WebResponse.crearWebResponseError("No se pudo actualizar el Tipo Documento", e.getMessage() );
        }
        //Fin
        
        /*
        *  Repuesta Correcta
        */        
        return WebResponse.crearWebResponseExito("El Tipo de Documento se actualizo correctamente");
        //Fin
    }
    
}
