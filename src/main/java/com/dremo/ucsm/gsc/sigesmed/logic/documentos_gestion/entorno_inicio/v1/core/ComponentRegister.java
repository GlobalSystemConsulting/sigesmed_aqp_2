/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.documentos_gestion.entorno_inicio.v1.core;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebComponent;
import com.dremo.ucsm.gsc.sigesmed.core.service.constantes.Sigesmed;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.IComponentRegister;

import com.dremo.ucsm.gsc.sigesmed.logic.documentos_gestion.entorno_inicio.v1.tx.InsertarDocumentosGestionTx;
import com.dremo.ucsm.gsc.sigesmed.logic.documentos_gestion.entorno_inicio.v1.tx.ListarDocumentosGestionTx;
import com.dremo.ucsm.gsc.sigesmed.logic.documentos_gestion.entorno_inicio.v1.tx.ListarArbolDirsTx;
import com.dremo.ucsm.gsc.sigesmed.logic.documentos_gestion.entorno_inicio.v1.tx.MostrarContentDirTx;
import com.dremo.ucsm.gsc.sigesmed.logic.documentos_gestion.entorno_inicio.v1.tx.InsertarDirectorioGestionTx;
import com.dremo.ucsm.gsc.sigesmed.logic.documentos_gestion.entorno_inicio.v1.tx.ListarTiposEspTx;
import com.dremo.ucsm.gsc.sigesmed.logic.documentos_gestion.entorno_inicio.v1.tx.ListarEntidadesTx;
import com.dremo.ucsm.gsc.sigesmed.logic.documentos_gestion.entorno_inicio.v1.tx.EliminarTipoDocTx;
import com.dremo.ucsm.gsc.sigesmed.logic.documentos_gestion.entorno_inicio.v1.tx.InsertarTipoDocTx;
import com.dremo.ucsm.gsc.sigesmed.logic.documentos_gestion.entorno_inicio.v1.tx.ActualizarTipDocTx;
import com.dremo.ucsm.gsc.sigesmed.logic.documentos_gestion.entorno_inicio.v1.tx.ActualizarDirGesTx;
import com.dremo.ucsm.gsc.sigesmed.logic.documentos_gestion.entorno_inicio.v1.tx.EliminarDirectorioTx;
import com.dremo.ucsm.gsc.sigesmed.logic.documentos_gestion.entorno_inicio.v1.tx.ListarPapeleraTx;
import com.dremo.ucsm.gsc.sigesmed.logic.documentos_gestion.entorno_inicio.v1.tx.RestaurarDocTx;
import com.dremo.ucsm.gsc.sigesmed.logic.documentos_gestion.entorno_inicio.v1.tx.EliminarTotalDocTx;
import com.dremo.ucsm.gsc.sigesmed.logic.documentos_gestion.entorno_inicio.v1.tx.EliminarDocumentoTx;
import com.dremo.ucsm.gsc.sigesmed.logic.documentos_gestion.entorno_inicio.v1.tx.ConstruirUrlArchTx;
import com.dremo.ucsm.gsc.sigesmed.logic.documentos_gestion.entorno_inicio.v1.tx.ListarUsersPorArchTx;
import com.dremo.ucsm.gsc.sigesmed.logic.documentos_gestion.entorno_inicio.v1.tx.BuscarPersonaTx;
import com.dremo.ucsm.gsc.sigesmed.logic.documentos_gestion.entorno_inicio.v1.tx.ListarCargosTx;
import com.dremo.ucsm.gsc.sigesmed.logic.documentos_gestion.entorno_inicio.v1.tx.RegistrarComparticionTx;
import com.dremo.ucsm.gsc.sigesmed.logic.documentos_gestion.entorno_inicio.v1.tx.EliminarComparticionTx;
import com.dremo.ucsm.gsc.sigesmed.logic.documentos_gestion.entorno_inicio.v1.tx.ConstruirReporteGeneralTx;
import com.dremo.ucsm.gsc.sigesmed.logic.documentos_gestion.entorno_inicio.v1.tx.CrearCopiaDocumentoTx;
import com.dremo.ucsm.gsc.sigesmed.logic.documentos_gestion.entorno_inicio.v1.tx.ListarHistorialPorDocumentoTx;
import com.dremo.ucsm.gsc.sigesmed.logic.documentos_gestion.entorno_inicio.v1.tx.ProtegerDocumentoTx;
import com.dremo.ucsm.gsc.sigesmed.logic.documentos_gestion.entorno_inicio.v1.tx.ReporteTx;
/**
 *
 * @author ucsm
 */
public class ComponentRegister implements IComponentRegister{
    @Override
    public WebComponent createComponent(){
        //Asiganando el modulo al que pertenece
        WebComponent component = new WebComponent(Sigesmed.MODULO_DOCUMENTOS_GESTION);        
        
        //Registrnado el Nombre del componente
        component.setName("entornoInicio");
        //Version del componente
        component.setVersion(1);
        //Lista de operaciones de logica, propias del componente
        component.addTransactionPOST("insertarDocumentosGestion", InsertarDocumentosGestionTx.class);
        component.addTransactionGET("listarDocumentosGestion", ListarDocumentosGestionTx.class);
        component.addTransactionGET("listarArbolDirs", ListarArbolDirsTx.class);
        component.addTransactionGET("mostrarContentDir", MostrarContentDirTx.class);
        component.addTransactionPOST("insertarDirectorioGestion", InsertarDirectorioGestionTx.class);
        component.addTransactionGET("listarTiposEsp", ListarTiposEspTx.class);
        component.addTransactionGET("listarEntidades", ListarEntidadesTx.class);
        component.addTransactionDELETE("eliminarTipoDoc", EliminarTipoDocTx.class);
        component.addTransactionPOST("insertarTipoDoc", InsertarTipoDocTx.class);
        component.addTransactionPUT("actualizarTipoDoc", ActualizarTipDocTx.class);
        component.addTransactionPOST("protegerDocumento", ProtegerDocumentoTx.class);
        component.addTransactionPUT("crearCopiaDocumento",CrearCopiaDocumentoTx.class);
        component.addTransactionPUT("actualizarDirGes", ActualizarDirGesTx.class);
        component.addTransactionDELETE("eliminarDirectorio", EliminarDirectorioTx.class);
        component.addTransactionGET("listarPapelera", ListarPapeleraTx.class);
        component.addTransactionPUT("restaurarDoc", RestaurarDocTx.class);
        component.addTransactionDELETE("eliminarTotalDoc", EliminarTotalDocTx.class);
       
        component.addTransactionDELETE("eliminarDocumento", EliminarDocumentoTx.class);
        component.addTransactionGET("construirUrlArch", ConstruirUrlArchTx.class);
        component.addTransactionGET("listarUsersPorArch", ListarUsersPorArchTx.class);
        component.addTransactionPOST("reporte", ReporteTx.class);
        

        component.addTransactionGET("buscarPersonaByNombreYOrganizacion", BuscarPersonaTx.class);
        component.addTransactionGET("listarCargos", ListarCargosTx.class);
        component.addTransactionPOST("registrarComparticion", RegistrarComparticionTx.class);
        component.addTransactionDELETE("eliminarComparticion", EliminarComparticionTx.class);
        component.addTransactionGET("construirReporteGeneral", ConstruirReporteGeneralTx.class);
        component.addTransactionGET("listarHistorialPorDocumento", ListarHistorialPorDocumentoTx.class);
        return component;
    }
}
