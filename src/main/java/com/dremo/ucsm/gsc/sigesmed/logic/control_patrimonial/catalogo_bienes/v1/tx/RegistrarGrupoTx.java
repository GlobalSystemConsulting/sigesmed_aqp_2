/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.control_patrimonial.catalogo_bienes.v1.tx;
import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import org.json.JSONObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import com.dremo.ucsm.gsc.sigesmed.core.entity.scp.CatalogoBienes;

import com.dremo.ucsm.gsc.sigesmed.core.entity.scp.GrupoGenerico;
import com.dremo.ucsm.gsc.sigesmed.core.dao.scp.CatalogoBienesDAO;
import com.dremo.ucsm.gsc.sigesmed.core.dao.scp.GrupoGenericoDAO;
import org.json.JSONArray;
import com.dremo.ucsm.gsc.sigesmed.core.service.base.FileJsonObject;
import com.dremo.ucsm.gsc.sigesmed.util.BuildFile;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Administrador
 */
public class RegistrarGrupoTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
        
        GrupoGenerico grupo_generico = null;
        JSONObject requestData = (JSONObject)wr.getData();
        JSONArray grupos = requestData.getJSONArray("grupos");
        for(int i = 0; i < grupos.length();i++){
             JSONObject grupo =grupos.getJSONObject(i);
             String cod = grupo.getString("cod");
             String nom = grupo.getString("nom");
             Date fec_mod = new Date();
             int usu_mod = grupo.getInt("usu_mod");
             char est_reg = 'A';
             grupo_generico = new GrupoGenerico(0,cod,nom,fec_mod,usu_mod,est_reg); 
             GrupoGenericoDAO gru_gen_dao = (GrupoGenericoDAO)FactoryDao.buildDao("scp.GrupoGenericoDAO");
             gru_gen_dao.insert(grupo_generico);
        }
        
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
