package com.dremo.ucsm.gsc.sigesmed.core.entity.capacitacion;

import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import org.hibernate.annotations.DynamicUpdate;

@Entity
@Table(name = "objetivos_capacitacion", schema = "pedagogico")
@DynamicUpdate(value = true)
public class ObjetivoCapacitacion implements java.io.Serializable {

    @Id
    @Column(name = "obj_cap_id", unique = true, nullable = false)
    @SequenceGenerator(name = "secuencia_objetivos_capacitacion", sequenceName = "pedagogico.objetivos_capacitacion_obj_cap_id_seq")
    @GeneratedValue(generator = "secuencia_objetivos_capacitacion")
    private int objCapId;

    @Column(name = "tip", nullable = false, length = 1)
    private Character tip;

    @Column(name = "des")
    private String des;

    @Column(name = "fue", length = 15)
    private String fue;

    @ManyToMany(mappedBy = "objetivosCapacitacion")
    private Set<CursoCapacitacion> cursosCapacitacion = new HashSet<>(0);

    public ObjetivoCapacitacion() {
    }

    public ObjetivoCapacitacion(Character tip, String des, String fue) {
        this.tip = tip;
        this.des = des;
        this.fue = fue;
    }

    public int getObjCapId() {
        return objCapId;
    }

    public void setObjCapId(int objCapId) {
        this.objCapId = objCapId;
    }

    public Character getTip() {
        return tip;
    }

    public void setTip(Character tip) {
        this.tip = tip;
    }

    public String getDes() {
        return des;
    }

    public void setDes(String des) {
        this.des = des;
    }

    public String getFue() {
        return fue;
    }

    public void setFue(String fue) {
        this.fue = fue;
    }

    public Set<CursoCapacitacion> getCursosCapacitacion() {
        return cursosCapacitacion;
    }

    public void setCursosCapacitacion(Set<CursoCapacitacion> cursosCapacitacion) {
        this.cursosCapacitacion = cursosCapacitacion;
    }
}
