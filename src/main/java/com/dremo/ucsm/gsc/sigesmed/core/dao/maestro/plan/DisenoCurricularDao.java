package com.dremo.ucsm.gsc.sigesmed.core.dao.maestro.plan;

import com.dremo.ucsm.gsc.sigesmed.core.dao.GenericDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.plan_curricular.DisenoCurricular;

/**
 * Created by Administrador on 19/10/2016.
 */
public interface DisenoCurricularDao extends GenericDao<DisenoCurricular> {
    DisenoCurricular buscarPorId(int id);
}
