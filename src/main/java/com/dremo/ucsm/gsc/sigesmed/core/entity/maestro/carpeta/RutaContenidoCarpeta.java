package com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.carpeta;

import com.dremo.ucsm.gsc.sigesmed.core.entity.Organizacion;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Usuario;

import javax.persistence.*;
import java.util.List;

/**
 * Created by Administrador on 28/12/2016.
 */
@Entity(name = "RutaContenidoCarpetaMaestro")
@Table(name = "ruta_contenido_carpeta", schema = "pedagogico")
public class RutaContenidoCarpeta implements java.io.Serializable{
    @Id
    @Column(name = "rut_con_car_id", unique = true, nullable = false)
    @SequenceGenerator(name = "ruta_contenido_carpeta_rut_con_car_id_seq", sequenceName = "pedagogico.ruta_contenido_carpeta_rut_con_car_id_seq")
    @GeneratedValue(generator = "ruta_contenido_carpeta_rut_con_car_id_seq")
    private int rutConCarId;
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "con_sec_car_ped_id", nullable = false)
    private ContenidoSeccionCarpeta contenido;

    @OneToMany(mappedBy = "ruta",fetch = FetchType.LAZY)
    List<DocenteCarpeta> contenidosDocente;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "org_id", nullable = false)
    private Organizacion organizacion;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "usu_id", nullable = false)
    private Usuario usuario;
    @Column(name = "tip_usu", nullable = false)
    private Integer tipUsu;
    @Column(name = "pat", nullable = false,length = 50)
    private String pat;
    @Column(name = "nom_fil", nullable = false,length = 50)
    private String nomFil;

    public RutaContenidoCarpeta() {
    }

    public RutaContenidoCarpeta(String nomFil, String pat, Integer tipUsu) {
        this.nomFil = nomFil;
        this.pat = pat;
        this.tipUsu = tipUsu;
    }

    public RutaContenidoCarpeta(String pat, String nomFil, Integer tipUsu, Usuario usuario, Organizacion organizacion, ContenidoSeccionCarpeta contenido) {
        this.pat = pat;
        this.nomFil = nomFil;
        this.tipUsu = tipUsu;
        this.usuario = usuario;
        this.organizacion = organizacion;
        this.contenido = contenido;
    }

    public int getRutConCarId() {
        return rutConCarId;
    }

    public void setRutConCarId(int rutConCarId) {
        this.rutConCarId = rutConCarId;
    }

    public ContenidoSeccionCarpeta getContenido() {
        return contenido;
    }

    public void setContenido(ContenidoSeccionCarpeta contenido) {
        this.contenido = contenido;
    }

    public Organizacion getOrganizacion() {
        return organizacion;
    }

    public void setOrganizacion(Organizacion organizacion) {
        this.organizacion = organizacion;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public Integer getTipUsu() {
        return tipUsu;
    }

    public void setTipUsu(Integer tipUsu) {
        this.tipUsu = tipUsu;
    }

    public String getPat() {
        return pat;
    }

    public void setPat(String pat) {
        this.pat = pat;
    }

    public String getNomFil() {
        return nomFil;
    }

    public void setNomFil(String nomFil) {
        this.nomFil = nomFil;
    }

    public List<DocenteCarpeta> getContenidosDocente() {
        return contenidosDocente;
    }

    public void setContenidosDocente(List<DocenteCarpeta> contenidosDocente) {
        this.contenidosDocente = contenidosDocente;
    }
}
