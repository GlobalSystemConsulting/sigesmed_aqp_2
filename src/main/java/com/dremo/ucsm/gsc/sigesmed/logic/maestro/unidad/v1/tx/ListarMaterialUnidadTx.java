package com.dremo.ucsm.gsc.sigesmed.logic.maestro.unidad.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.maestro.plan.UnidadDidacticaDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.plan_curricular.MaterialRecursoProgramacion;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.plan_curricular.ProductosUnidadAprendizaje;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.base.EntityUtil;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by Administrador on 13/12/2016.
 */
public class ListarMaterialUnidadTx implements ITransaction{
    private static Logger logger = Logger.getLogger(ListarMaterialUnidadTx.class.getName());
    @Override
    public WebResponse execute(WebRequest wr) {
        JSONObject data = (JSONObject) wr.getData();
        int idUnidad = data.getInt("unid");
        return listarMaterialesUnidad(idUnidad);
    }

    private WebResponse listarMaterialesUnidad(int idUnidad) {
        try{
            UnidadDidacticaDao unidadDao = (UnidadDidacticaDao) FactoryDao.buildDao("maestro.plan.UnidadDidacticaDao");
            List<MaterialRecursoProgramacion> materiales = unidadDao.listarMateriales(idUnidad);
            JSONObject jsonResult = new JSONObject();
            jsonResult.put("mats",new JSONArray(EntityUtil.listToJSONString(
                    new String[]{"matId","nom","tip","des","can"},
                    new String[]{"id","nom","tip","des","can"},
                    materiales
            )));
            List<ProductosUnidadAprendizaje> productos = unidadDao.listarProductosUnidad(idUnidad);
            jsonResult.put("prods",new JSONArray(EntityUtil.listToJSONString(
                    new String[]{"proUniAprId", "nom","editable"},
                    new String[]{"id", "nom","edit"},
                    productos
            )));
            return WebResponse.crearWebResponseExito("Se listo correctamente la lista de materiales",jsonResult);
        }catch (Exception e){
            logger.log(Level.SEVERE,"listarmaterialesUnidad",e);
            return WebResponse.crearWebResponseError("no se pudo listar la lista de materiales");
        }
    }
}
