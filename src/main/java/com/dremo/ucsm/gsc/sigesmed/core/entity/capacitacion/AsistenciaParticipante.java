package com.dremo.ucsm.gsc.sigesmed.core.entity.capacitacion;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "asistencia_participantes_capacitacion", schema = "pedagogico")
public class AsistenciaParticipante implements Serializable {
    @Id
    @Column(name = "asi_par_cap_id", unique = true, nullable = false)
    @SequenceGenerator(name = "secuencia_asistencia_participantes_capacitacion", sequenceName = "pedagogico.asistencia_participantes_capacitacion_asi_par_cap_id_seq")
    @GeneratedValue(generator = "secuencia_asistencia_participantes_capacitacion")
    private int asiParCapId;
    
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "per_id")
    private Persona persona;
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "hor_sed_cap_id")
    private HorarioSedeCapacitacion horario;
    
    @Column(name = "est_reg", length = 1)
    private Character estReg;
    
    @Column(name = "fec")
    private Date fec;
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "fec_mod")
    private Date fecMod;
    
    @Temporal(TemporalType.TIME)
    @Column(name = "hor_reg")
    private Date horReg;
    
    @Column(name = "usu_mod")
    private Integer usuMod;
    
    @OneToMany(mappedBy = "asistencia", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    List<JustificacionInasistenciaCapacitacion> justificaciones = new ArrayList<>();

    public AsistenciaParticipante() {}
    
    public AsistenciaParticipante(int docId, HorarioSedeCapacitacion horario, Character estReg, Date fec) {
        this.persona = new Persona();
        this.persona.setPerId(docId);
        this.horario = horario;
        this.estReg = estReg;
        this.fec = fec;
        this.fecMod = new Date();
        this.horReg = new Date();
    }
    
    public int getAsiParCapId() {
        return asiParCapId;
    }

    public void setAsiParCapId(int asiParCapId) {
        this.asiParCapId = asiParCapId;
    }

    public Persona getPersona() {
        return persona;
    }

    public void setPersona(Persona persona) {
        this.persona = persona;
    }

    public HorarioSedeCapacitacion getHorario() {
        return horario;
    }

    public void setHorario(HorarioSedeCapacitacion horario) {
        this.horario = horario;
    }

    public Character getEstReg() {
        return estReg;
    }

    public void setEstReg(Character estReg) {
        this.estReg = estReg;
    }

    public Date getFec() {
        return fec;
    }

    public void setFec(Date fec) {
        this.fec = fec;
    }

    public Date getFecMod() {
        return fecMod;
    }

    public void setFecMod(Date fecMod) {
        this.fecMod = fecMod;
    }

    public Date getHorReg() {
        return horReg;
    }

    public void setHorReg(Date horReg) {
        this.horReg = horReg;
    }

    public Integer getUsuMod() {
        return usuMod;
    }

    public void setUsuMod(Integer usuMod) {
        this.usuMod = usuMod;
    }

    public List<JustificacionInasistenciaCapacitacion> getJustificaciones() {
        return justificaciones;
    }

    public void setJustificaciones(List<JustificacionInasistenciaCapacitacion> justificaciones) {
        this.justificaciones = justificaciones;
    }
}
