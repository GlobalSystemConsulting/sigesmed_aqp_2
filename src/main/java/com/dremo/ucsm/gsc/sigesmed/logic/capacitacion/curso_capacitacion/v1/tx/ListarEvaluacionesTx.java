package com.dremo.ucsm.gsc.sigesmed.logic.capacitacion.curso_capacitacion.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.capacitacion.EvaluacionCursoCapacitacionDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.capacitacion.EvaluacionParticipanteCapacitacionDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.capacitacion.PreguntaEvaluacionCapacitacionDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.capacitacion.RespuestaEncuestaDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.capacitacion.EvaluacionCursoCapacitacion;
import com.dremo.ucsm.gsc.sigesmed.core.entity.capacitacion.EvaluacionParticipanteCapacitacion;
import com.dremo.ucsm.gsc.sigesmed.core.entity.capacitacion.PreguntaEvaluacionCapacitacion;
import com.dremo.ucsm.gsc.sigesmed.core.entity.capacitacion.RespuestaEncuesta;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.base.EntityUtil;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

public class ListarEvaluacionesTx implements ITransaction {

    private static final Logger logger = Logger.getLogger(ListarEvaluacionesTx.class.getName());

    @Override
    public WebResponse execute(WebRequest wr) {
        JSONObject data = (JSONObject) wr.getData();
        int opt = data.getInt("opt");
        WebResponse response = null;

        switch (opt) {
            case 0:
                response = listarEvaluaciones(data.getInt("sedCod"));
                break;

            case 1:
                response = listarEvaluacionesBanPre(data.getInt("sedCod"));
                break;

            case 2:
                response = verificarExistencia(data.getInt("evaCod"));
                break;

            case 3:
                response = listarEvaluacionesDocente(data.getInt("sedCod"), data.getInt("codDoc"));
                break;

            case 4:
                response = listarEncuestas(data.getInt("sedCod"));
                break;

            case 5:
                response = gestionarEncuestas(data.getInt("evaCod"));
                break;
        }

        return response;
    }

    private WebResponse listarEvaluaciones(int sedCod) {
        try {
            EvaluacionCursoCapacitacionDao evaluacionCursoCapacitacionDao = (EvaluacionCursoCapacitacionDao) FactoryDao.buildDao("capacitacion.EvaluacionCursoCapacitacionDao");
            List<EvaluacionCursoCapacitacion> evaluaciones = evaluacionCursoCapacitacionDao.buscarPorSede(sedCod);

            JSONArray array = new JSONArray();

            for (EvaluacionCursoCapacitacion test : evaluaciones) {
                JSONObject object = new JSONObject();

                if (test.getFecFin().before(new Date()) && test.getTip().equals('P')) {
                    test.setEstReg('F');
                    evaluacionCursoCapacitacionDao.update(test);
                    calificarEvaluaciones(test.getEvaCurCapId());
                }

                object.put("id", test.getEvaCurCapId());
                object.put("ini", test.getFecIni());
                object.put("fin", test.getFecFin());
                object.put("tip", test.getTip());
                object.put("est", test.getEstReg());
                object.put("obs", test.getObs());
                object.put("nom", test.getNom());
                object.put("ord", test.isOrdAle());
                object.put("int", test.getNumInt());

                array.put(object);
            }

            return WebResponse.crearWebResponseExito("Se listo correctamente", WebResponse.OK_RESPONSE).setData(array);
        } catch (Exception e) {
            logger.log(Level.SEVERE, "listarEvaluaciones", e);
            return WebResponse.crearWebResponseError("Error al listar las evaluaciones", WebResponse.BAD_RESPONSE);
        }
    }

    private WebResponse listarEvaluacionesBanPre(int sedCod) {
        try {
            EvaluacionCursoCapacitacionDao evaluacionCursoCapacitacionDao = (EvaluacionCursoCapacitacionDao) FactoryDao.buildDao("capacitacion.EvaluacionCursoCapacitacionDao");
            List<EvaluacionCursoCapacitacion> tests = evaluacionCursoCapacitacionDao.buscarPorSede(sedCod);

            return WebResponse.crearWebResponseExito("Se listo correctamente", WebResponse.OK_RESPONSE)
                    .setData(new JSONArray(EntityUtil.listToJSONString(
                                            new String[]{"evaCurCapId", "nom"},
                                            new String[]{"cod", "nom"},
                                            tests
                                    )));
        } catch (Exception e) {
            logger.log(Level.SEVERE, "listarEvaluacionesBanPre", e);
            return WebResponse.crearWebResponseError("Error al listar las evaluaciones", WebResponse.BAD_RESPONSE);
        }
    }

    private WebResponse verificarExistencia(int evaCod) {
        try {
            PreguntaEvaluacionCapacitacionDao preguntaEvaluacionCapacitacionDao = (PreguntaEvaluacionCapacitacionDao) FactoryDao.buildDao("capacitacion.PreguntaEvaluacionCapacitacionDao");
            JSONObject object = new JSONObject();
            object.put("state", preguntaEvaluacionCapacitacionDao.existenPreguntas(evaCod));

            return WebResponse.crearWebResponseExito("Se listo correctamente", WebResponse.OK_RESPONSE).setData(object);
        } catch (Exception e) {
            logger.log(Level.SEVERE, "verificarExistencia", e);
            return WebResponse.crearWebResponseError("Error al listar las evaluaciones", WebResponse.BAD_RESPONSE);
        }
    }

    private WebResponse listarEvaluacionesDocente(int sedCod, int perId) {
        try {
            EvaluacionCursoCapacitacionDao evaluacionCursoCapacitacionDao = (EvaluacionCursoCapacitacionDao) FactoryDao.buildDao("capacitacion.EvaluacionCursoCapacitacionDao");
            List<EvaluacionCursoCapacitacion> tests = evaluacionCursoCapacitacionDao.buscarIniciados(sedCod);
            EvaluacionParticipanteCapacitacionDao evaluacionParticipanteCapacitacionDao = (EvaluacionParticipanteCapacitacionDao) FactoryDao.buildDao("capacitacion.EvaluacionParticipanteCapacitacionDao");

            JSONArray array = new JSONArray();
            for (EvaluacionCursoCapacitacion evaluacion : tests) {
                JSONObject object = new JSONObject();
                object.put("id", evaluacion.getEvaCurCapId());
                object.put("tip", evaluacion.getTip());
                object.put("est", evaluacion.getEstReg());
                object.put("obs", evaluacion.getObs());
                object.put("nom", evaluacion.getNom());
                object.put("ord", evaluacion.isOrdAle());
                object.put("int", evaluacion.getNumInt());
                object.put("opo", evaluacionParticipanteCapacitacionDao.contarEvaluaciones(evaluacion.getEvaCurCapId(), perId));
                EvaluacionParticipanteCapacitacion evaPar = evaluacionParticipanteCapacitacionDao.buscarUltima(evaluacion.getEvaCurCapId(), perId);

                if (evaluacion.getEstReg().equals('I') || evaluacion.getEstReg().equals('F')) {
                    object.put("estUlt", evaPar.getEstReg());
                    object.put("not", evaPar.getNotObt());
                } else {
                    object.put("estUlt", "N");
                }

                array.put(object);
            }

            return WebResponse.crearWebResponseExito("Se listo correctamente", WebResponse.OK_RESPONSE).setData(array);
        } catch (Exception e) {
            logger.log(Level.SEVERE, "listarEvaluacionesDocente", e);
            return WebResponse.crearWebResponseError("Error al listar las evaluaciones", WebResponse.BAD_RESPONSE);
        }
    }

    private void calificarEvaluaciones(int codEva) {
        EvaluacionParticipanteCapacitacionDao evaluacionParticipanteCapacitacionDao = (EvaluacionParticipanteCapacitacionDao) FactoryDao.buildDao("capacitacion.EvaluacionParticipanteCapacitacionDao");
        List<Integer> evaluacion = evaluacionParticipanteCapacitacionDao.listarParticipantes(codEva);

        for (Integer docId : evaluacion) {
            EvaluacionParticipanteCapacitacion evaPar = evaluacionParticipanteCapacitacionDao.buscarUltima(codEva, docId);
            if (evaPar.getEstReg().equals('A')) {
                evaPar.setEstReg('C');
                evaluacionParticipanteCapacitacionDao.update(evaPar);
            }
        }
    }

    private WebResponse listarEncuestas(int sedCod) {
        try {
            EvaluacionCursoCapacitacionDao evaluacionCursoCapacitacionDao = (EvaluacionCursoCapacitacionDao) FactoryDao.buildDao("capacitacion.EvaluacionCursoCapacitacionDao");
            List<EvaluacionCursoCapacitacion> tests = evaluacionCursoCapacitacionDao.listarEncuestas(sedCod);

            return WebResponse.crearWebResponseExito("Se listo correctamente", WebResponse.OK_RESPONSE)
                    .setData(new JSONArray(EntityUtil.listToJSONString(
                                            new String[]{"evaCurCapId", "nom"},
                                            new String[]{"cod", "nom"},
                                            tests
                                    )));
        } catch (Exception e) {
            logger.log(Level.SEVERE, "listarEncuestas", e);
            return WebResponse.crearWebResponseError("Error al listar las encuestas", WebResponse.BAD_RESPONSE);
        }
    }

    private WebResponse gestionarEncuestas(int evaCod) {
        try {
            PreguntaEvaluacionCapacitacionDao preguntaEvaluacionCapacitacionDao = (PreguntaEvaluacionCapacitacionDao) FactoryDao.buildDao("capacitacion.PreguntaEvaluacionCapacitacionDao");
            RespuestaEncuestaDao respuestaEncuestaDao = (RespuestaEncuestaDao) FactoryDao.buildDao("capacitacion.RespuestaEncuestaDao");
            List<PreguntaEvaluacionCapacitacion> preguntas = preguntaEvaluacionCapacitacionDao.buscarPorEvaluacion(evaCod);

            JSONArray array = new JSONArray();
            for (PreguntaEvaluacionCapacitacion pregunta : preguntas) {
                JSONObject objPre = new JSONObject();
                objPre.put("nom", pregunta.getNom());
                objPre.put("des", pregunta.getDes());
                objPre.put("tip", pregunta.getTip());

                JSONArray labels = new JSONArray();
                JSONArray series = new JSONArray();

                List<RespuestaEncuesta> respuestas = respuestaEncuestaDao.buscarPorPregunta(pregunta.getPreEvaCapId());
                objPre.put("siz", respuestas.size() > 0);

                switch (pregunta.getTip()) {
                    case 'M':
                        Map<String, Integer> answers = new HashMap<>();
                        int value = 0;

                        for (RespuestaEncuesta respuesta : respuestas) {
                            StringTokenizer options = new StringTokenizer(respuesta.getResEnc(), "#$");
                            String key = "";

                            while (options.hasMoreTokens()) {
                                key = options.nextToken();
                                value = 0;

                                if (answers.containsKey(key)) {
                                    value = answers.get(key);
                                }

                                value++;
                                answers.put(key, value);
                            }
                        }

                        value = 0;

                        JSONArray opciones = new JSONArray();
                        StringTokenizer options = new StringTokenizer(pregunta.getOpc(), "#$");

                        while (options.hasMoreTokens()) {
                            opciones.put(options.nextToken());
                            labels.put("Pregunta " + (value + 1));

                            if (answers.containsKey(String.valueOf(value))) {
                                series.put(answers.get(String.valueOf(value)));
                            } else {
                                series.put(0);
                            }

                            value++;
                        }

                        objPre.put("opc", opciones);
                        break;

                    default:
                        answers = new HashMap<>();

                        for (RespuestaEncuesta respuesta : respuestas) {
                            value = 0;

                            if (answers.containsKey(respuesta.getResEnc())) {
                                value = answers.get(respuesta.getResEnc());
                            }

                            value++;
                            answers.put(respuesta.getResEnc().toUpperCase(), value);
                        }

                        Iterator<Map.Entry<String, Integer>> iterator = answers.entrySet().iterator();
                        while (iterator.hasNext()) {
                            Map.Entry<String, Integer> pair = iterator.next();
                            labels.put(pair.getKey());
                            series.put(pair.getValue());
                        }
                        break;
                }

                objPre.put("lab", labels);
                objPre.put("ser", series);
                array.put(objPre);
            }

            return WebResponse.crearWebResponseExito("Se listo correctamente", WebResponse.OK_RESPONSE).setData(array);
        } catch (Exception e) {
            logger.log(Level.SEVERE, "gestionarEncuestas", e);
            return WebResponse.crearWebResponseError("Error al listar el resultado de la encuesta", WebResponse.BAD_RESPONSE);
        }
    }
}
