/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.archivo_digital.inventario_transferencia.v1.core;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebComponent;
import com.dremo.ucsm.gsc.sigesmed.core.service.constantes.Sigesmed;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.IComponentRegister;


import com.dremo.ucsm.gsc.sigesmed.logic.archivo_digital.inventario_transferencia.v1.tx.EditarInventarioEliminacionTx;
import com.dremo.ucsm.gsc.sigesmed.logic.archivo_digital.inventario_transferencia.v1.tx.EditarInventarioTransferenciaTx;
import com.dremo.ucsm.gsc.sigesmed.logic.archivo_digital.inventario_transferencia.v1.tx.EliminarInventarioTransferenciaTx;
import com.dremo.ucsm.gsc.sigesmed.logic.archivo_digital.inventario_transferencia.v1.tx.ListarInventarioTransferenciaTx;
import com.dremo.ucsm.gsc.sigesmed.logic.archivo_digital.inventario_transferencia.v1.tx.RegistrarInventarioEliminacionTx;
import com.dremo.ucsm.gsc.sigesmed.logic.archivo_digital.inventario_transferencia.v1.tx.RegistrarInventarioTransferenciaTx;
import com.dremo.ucsm.gsc.sigesmed.logic.archivo_digital.inventario_transferencia.v1.tx.RegistrarPrestamoSerieTx;
import com.dremo.ucsm.gsc.sigesmed.logic.archivo_digital.inventario_transferencia.v1.tx.ListarPrestamoSerieTx;
import com.dremo.ucsm.gsc.sigesmed.logic.archivo_digital.inventario_transferencia.v1.tx.RegistrarArchivoTx;
import com.dremo.ucsm.gsc.sigesmed.logic.archivo_digital.inventario_transferencia.v1.tx.ListarArchivosTx;
/**
 *
 * @author Jeferson
 */
public class ComponentRegister implements IComponentRegister {

    @Override
    public WebComponent createComponent() {
        
         WebComponent component = new WebComponent(Sigesmed.SISTEMA_ARCHIVO_DIGITAL);
          //Registrando el Nombre del Componente
        component.setName("inventario_transferencia");
        //version del componente
        component.setVersion(1);
         
        
        component.addTransactionPUT("editar_inventario_eliminacion", EditarInventarioEliminacionTx.class);
        component.addTransactionPUT("editar_inventario_transferencia",EditarInventarioTransferenciaTx.class);
        component.addTransactionDELETE("eliminar_inventario_transferencia", EliminarInventarioTransferenciaTx.class);
        component.addTransactionGET("listar_inventario_transferencia", ListarInventarioTransferenciaTx.class);
        component.addTransactionPOST("registrar_inventario_eliminacion", RegistrarInventarioEliminacionTx.class);
        component.addTransactionPOST("registrar_inventario_transferencia", RegistrarInventarioTransferenciaTx.class);
        component.addTransactionPOST("registrar_prestamo_inventario",RegistrarPrestamoSerieTx.class);
        component.addTransactionGET("listar_prestamos_serie",ListarPrestamoSerieTx.class);
        component.addTransactionGET("registrar_archivo",RegistrarArchivoTx.class);
        component.addTransactionGET("listar_archivos",ListarArchivosTx.class);
        return component;
    }
    
}
