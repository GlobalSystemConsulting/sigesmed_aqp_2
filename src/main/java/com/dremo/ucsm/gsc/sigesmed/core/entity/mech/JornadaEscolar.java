package com.dremo.ucsm.gsc.sigesmed.core.entity.mech;

import java.util.Date;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="jornada_escolar")
public class JornadaEscolar  implements java.io.Serializable {

    @Id
    @Column(name="jor_esc_id", unique=true, nullable=false)
    @SequenceGenerator(name = "secuencia_jornada_escolar", sequenceName="jornada_escolar_jor_esc_id_seq" )
    @GeneratedValue(generator="secuencia_jornada_escolar")
    private int jorEscId;
    @Column(name="abr",length=4)
    private String abr;
    @Column(name="nom",length=64)
    private String nom;
    @Column(name="des", length=256)
    private String des;
    @Column(name="hor_obl")
    private int horObl;
    @Column(name="hor_lib_dis")
    private int horLibDis;
    @Column(name="hor_tut")
    private int horTut;
    @Column(name="hor_tot")
    private int horTot;
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="fec_mod", nullable=false, length=29)
    private Date fecMod;    
    @Column(name="usu_mod")
    private int usuMod;
    @Column(name="est_reg")
    private char estReg;
    
    @Column(name="dis_cur_id")
    private int disCurId;    
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="dis_cur_id",updatable = false,insertable = false)
    private DisenoCurricularMECH disenoCurricular;
    
    @Column(name="niv_id")
    private int nivId;
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="niv_id",updatable = false,insertable = false)
    private Nivel nivel;
    
    @OneToMany(mappedBy="jornada")
    private List<AreaCurricularHora> areaHoras;

    public JornadaEscolar() {
    }
    public JornadaEscolar(int jorEscId) {
        this.jorEscId = jorEscId;
    }
    public JornadaEscolar(int jorEscId,String abr, String nom, String des, int horObl, int horLibDis, int horTut, int horTot,int disCurId, int nivId, Date fecMod, int usuMod, char estReg) {
       this.jorEscId = jorEscId;
       this.abr = abr;
       this.nom = nom;
       this.des = des;
       
       this.horObl = horObl;
       this.horLibDis = horLibDis;
       this.horTut = horTut;
       this.horTot = horTot;
       
       this.disCurId = disCurId;
       this.nivId = nivId;
       
       this.fecMod = fecMod;
       this.usuMod = usuMod;
       this.estReg = estReg;
    }
   
     
    public int getJorEscId() {
        return this.jorEscId;
    }    
    public void setJorEscId(int jorEscId) {
        this.jorEscId = jorEscId;
    }
    
    public String getAbr() {
        return this.abr;
    }
    public void setAbr(String abr) {
        this.abr = abr;
    }
    public String getNom() {
        return this.nom;
    }
    public void setNom(String nom) {
        this.nom = nom;
    }    
    public String getDes() {
        return this.des;
    }
    public void setDes(String des) {
        this.des = des;
    }
    
    public int getHorObl() {
        return this.horObl;
    }
    public void setHorObl(int horObl) {
        this.horObl = horObl;
    }
    
    public int getHorLibDis() {
        return this.horLibDis;
    }
    public void setHorLibDis(int horLibDis) {
        this.horLibDis = horLibDis;
    }
    public int getHorTut() {
        return this.horTut;
    }
    public void setHorTut(int horTut) {
        this.horTut = horTut;
    }
    public int getHorTot() {
        return this.horTot;
    }
    public void setHorTot(int horTot) {
        this.horTot = horTot;
    }
    
    public Date getFecMod() {
        return this.fecMod;
    }
    public void setFecMod(Date fecMod) {
        this.fecMod = fecMod;
    }
    
    public int getUsuMod() {
        return this.usuMod;
    }
    public void setUsuMod(int usuMod) {
        this.usuMod = usuMod;
    }
    
    public char getEstReg() {
        return this.estReg;
    }
    public void setEstReg(char estReg) {
        this.estReg = estReg;
    }
    
    public int getDisCurId() {
        return this.disCurId;
    }    
    public void setDisCurId(int disCurId) {
        this.disCurId = disCurId;
    }
    
    public DisenoCurricularMECH getDisenoCurricular() {
        return this.disenoCurricular;
    }
    public void setDisenoCurricular(DisenoCurricularMECH disenoCurricular) {
        this.disenoCurricular = disenoCurricular;
    }
    
    public int getNivId() {
        return this.nivId;
    }    
    public void setNivId(int nivId) {
        this.nivId = nivId;
    }
    
    public Nivel getNivel() {
        return this.nivel;
    }
    public void setNivel(Nivel nivel) {
        this.nivel = nivel;
    }
    
    public List<AreaCurricularHora> getAreaHoras() {
        return this.areaHoras;
    }
    public void setAreaHoras(List<AreaCurricularHora> areaHoras) {
        this.areaHoras = areaHoras;
    }
}


