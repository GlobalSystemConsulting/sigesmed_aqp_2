package com.dremo.ucsm.gsc.sigesmed.logic.capacitacion.curso_capacitacion.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.capacitacion.PerfilCapacitadorDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.capacitacion.PerfilCapacitador;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

public class ListarPerfilesCapacitacionTx implements ITransaction {
    private static final Logger logger = Logger.getLogger(ListarPerfilesCapacitacionTx.class.getName());
    
    @Override
    public WebResponse execute(WebRequest wr) {
        try{
            JSONObject data =  (JSONObject) wr.getData();        
            int curCapId = data.getInt("cod");
            
            PerfilCapacitadorDao perfilDao = (PerfilCapacitadorDao) FactoryDao.buildDao("capacitacion.PerfilCapacitadorDao");
            List<PerfilCapacitador> perfilesGenerales = perfilDao.listarPerfilesCursoCapacitacion(curCapId);

            JSONObject perCapacitacion = new JSONObject();
            
            if(perfilesGenerales.size() > 0) {
                JSONArray perfiles = new JSONArray();
                JSONArray funciones = new JSONArray();
                JSONArray requisitos = new JSONArray();
                
                for(PerfilCapacitador perfil : perfilesGenerales) {
                    JSONObject obj = new JSONObject();
                    obj.put("id", perfil.getPerCapId());
                    obj.put("des", perfil.getDes());
                    obj.put("sel", false);

                    switch(perfil.getTip()) {
                        case 'P': perfiles.put(obj);
                                    break;
                        
                        case 'F': funciones.put(obj);
                                    break;
                        
                        case 'R': requisitos.put(obj);
                                    break;
                    }
                }
                
                perCapacitacion.put("perfiles", perfiles);
                perCapacitacion.put("funciones", funciones);
                perCapacitacion.put("requisitos", requisitos);
            }
            
            return WebResponse.crearWebResponseExito("Se listo correctamente",WebResponse.OK_RESPONSE)
                    .setData(perCapacitacion);
        } catch (Exception e ){
            logger.log(Level.SEVERE,"ListarPerfilesCapacitacionTx",e);
            return WebResponse.crearWebResponseError("Error al listar los perfiles",WebResponse.BAD_RESPONSE);
        }
    }    
}
