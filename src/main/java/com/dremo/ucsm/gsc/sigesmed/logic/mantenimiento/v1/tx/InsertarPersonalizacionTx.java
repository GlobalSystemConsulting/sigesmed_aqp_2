/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.mantenimiento.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.mnt.AjustePaginaWebDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Usuario;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mnt.AjustePaginaWeb;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import org.json.JSONObject;

/**
 *
 * @author Administrador
 */
public class InsertarPersonalizacionTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
        AjustePaginaWeb ajuste=null;
        String ladIzq="";
        String ladDer="";
        boolean def=false;
        int color=0;
        int usuarioID=0;
        try{
            JSONObject requestData=(JSONObject)wr.getData();
            def=requestData.getBoolean("default");
            usuarioID=requestData.getInt("usuarioID");
            if(!def){                
                if(requestData.getBoolean("ladIzq")){
                    ladIzq="S";
                }else{
                    ladIzq="N";
                }
                if(requestData.getBoolean("ladDer")){
                    ladDer="S";
                }else{
                    ladDer="N";
                }
                color=requestData.getInt("color");
            }
        }catch(Exception e){
            System.out.println("No se pudo guardar la configuración, datos incorrectos");
            return WebResponse.crearWebResponseError("No se pudo guardar la configuración, datos incorrectos", e.getMessage());
        }
        
        //Buscar si existe uno anterior para desactivarlo
        AjustePaginaWebDao ajusteDao=(AjustePaginaWebDao)FactoryDao.buildDao("mnt.AjustePaginaWebDao");
        
        AjustePaginaWeb ajusteTemp=ajusteDao.obtenerAjustePorIdUsuario(new Usuario(usuarioID));
        if(def){//Poner a defecto            
            if(ajusteTemp!=null){
                ajusteTemp.setEstReg('E');
                ajusteDao.update(ajusteTemp);                
            }
            
        }else{ //Actualizar configuracion actual.
            if(ajusteTemp!=null){//Existe configuracion anterior
                ajusteTemp.setAjusteColor(color);
                ajusteTemp.setAjusteDer(ladDer);
                ajusteTemp.setAjusteIzq(ladIzq);
                ajusteDao.update(ajusteTemp);  
            }else{
                ajuste=new AjustePaginaWeb(0, new Usuario(usuarioID), ladIzq, ladDer, color, 'A');
                ajusteDao.insert(ajuste);
            }
        }
        return WebResponse.crearWebResponseExito("El registro de la personalización se realizo correctamente, vuelva a conectarse para visualizar los cambios");
    }
    
}
