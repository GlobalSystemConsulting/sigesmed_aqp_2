/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.datos_academicos.colegiatura.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.ColegiaturaDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Colegiatura;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONObject;

/**
 *
 * @author gscadmin
 */
public class EliminarColegiaturaTx implements ITransaction{

    private static final Logger logger = Logger.getLogger(EliminarColegiaturaTx.class.getName());

    @Override
    public WebResponse execute(WebRequest wr) {
        Integer colId = 0;
        
        try{
            JSONObject requestData = (JSONObject)wr.getData();
            colId = requestData.getInt("colId");          
        
        }catch(Exception e){
            System.out.println(e);
            logger.log(Level.SEVERE,"eliminarColegiatura",e);
            return WebResponse.crearWebResponseError("No se pudo eliminar", e.getMessage() );
        }
        
        ColegiaturaDao colegiaturaDao = (ColegiaturaDao)FactoryDao.buildDao("se.ColegiaturaDao");
        try{
            colegiaturaDao.deleteAbsolute(new Colegiatura(colId));
        
        }catch(Exception e){
            System.out.println("No se pudo eliminar la colegiatura\n" + e);
            return WebResponse.crearWebResponseError("No se pudo eliminar la colegiatura", e.getMessage() );
        }
        return WebResponse.crearWebResponseExito("La colegiatura se elimino correctamente");
    }
    
}
