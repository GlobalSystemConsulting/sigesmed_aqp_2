
package com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.capacitacion;

import com.dremo.ucsm.gsc.sigesmed.core.dao.capacitacion.MaterialCursoCapacitacionDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.GenericDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.entity.capacitacion.MaterialCursoCapacitacion;


public class MaterialCursoCapacitacionDaoHibernate extends GenericDaoHibernate<MaterialCursoCapacitacion> implements MaterialCursoCapacitacionDao {
    
}
