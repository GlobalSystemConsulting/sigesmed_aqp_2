/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.legajo_personal.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.LegajoPersonalDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.LegajoPersonal;
import com.dremo.ucsm.gsc.sigesmed.core.service.ServicioREST;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.constantes.Sigesmed;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.io.File;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONObject;

/**
 *
 * @author gscadmin
 */
public class EliminarLegajoTx implements ITransaction{

    private static final Logger logger = Logger.getLogger(EliminarLegajoTx.class.getName());

    @Override
    public WebResponse execute(WebRequest wr) {
        JSONObject data = (JSONObject) wr.getData();
        int eliminarLegajo = data.getInt("legId");
        return eliminarLegajo(eliminarLegajo);
    }

    private WebResponse eliminarLegajo(int legId) {
        try{
            LegajoPersonalDao legPerDao = (LegajoPersonalDao)FactoryDao.buildDao("se.LegajoPersonalDao");
            LegajoPersonal legajo = legPerDao.buscarPorId(legId);
            legPerDao.delete(legajo);
            
            if(legajo.getUrl() != null && legajo.getUrl().length() > 0){
                
                File file = new File(ServicioREST.PATH_SIGESMED + File.separator + Sigesmed.UBI_ARCHIVOS + File.separator + Legajo.LEGAJO_PATH + File.separator + legajo.getUrl());
                file.delete();
            }
            return WebResponse.crearWebResponseExito("El legajo personal se elimino correctamente");
        }catch (Exception e){
            logger.log(Level.SEVERE,"eliminarLegajo",e);
            return WebResponse.crearWebResponseError("No se pudo eliminar el legajo", e.getMessage());
        }
    }
    
    
}
