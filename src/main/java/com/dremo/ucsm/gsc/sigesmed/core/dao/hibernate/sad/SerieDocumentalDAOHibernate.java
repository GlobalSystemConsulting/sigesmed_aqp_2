/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.sad;

import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.GenericDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.datastore.HibernateUtil;
import com.dremo.ucsm.gsc.sigesmed.core.dao.sad.SerieDocumentalDAO;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sad.SerieDocumental;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sad.InventarioTransferencia;
import com.dremo.ucsm.gsc.sigesmed.core.dao.sad.InventarioTransferenciaDAO;
import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;

import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
/**
 *
 * @author Jeferson
 */
public class SerieDocumentalDAOHibernate extends GenericDaoHibernate<SerieDocumental> implements SerieDocumentalDAO {

    @Override
    public List<SerieDocumental> buscarPorOrganizacion() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<SerieDocumental> buscarPorArea(int id_area) {
        List<SerieDocumental> series = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            // Listamos las series pertenecientes a dicha area
            String hql = "SELECT DISTINCT sd FROM SerieDocumental sd JOIN FETCH sd.area WHERE sd.area.areId=:p1 and sd.area.estReg!='E'";
            Query query = session.createQuery(hql);
            query.setParameter("p1",id_area );
            series = query.list();
            
        }catch(Exception e){
             System.out.println("No se pudo Listar los Tipos de Area \\n "+ e.getMessage());
             throw new UnsupportedOperationException("No se pudo Listar los Tipos de Area \\n "+ e.getMessage());
        }
        finally{
            session.close();
        }
        return series;
        
    }

    @Override
    public List<SerieDocumental> buscarPorOrganizacion(int tipoOrganizacionID) {
        
        
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<SerieDocumental> buscarPorCodigo(int codigo) {
        List<SerieDocumental> serie= null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            String hql = "SELECT DISTINCT sd FROM SerieDocumental WHERE sd.codigo=:p1";
            Query query = session.createQuery(hql);
            query.setParameter("p1",codigo );
            serie = query.list();
        }
        catch(Exception e){
             System.out.println("No se pudo Listar las Series Documentales buscadas por el codigo \\n "+ e.getMessage());
             throw new UnsupportedOperationException("No se pudo Listar las Series documentales buscadas por el codigo \\n "+ e.getMessage());
        }
         finally{
            session.close();
        }
        return serie;
    }

    @Override
    public List<SerieDocumental> buscarPorNombre(String nombre) {
        
        List<SerieDocumental> serie= null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            String hql = "SELECT DISTINCT sd FROM SerieDocumental WHERE sd.nombre=:p1";
            Query query = session.createQuery(hql);
            query.setParameter("p1",nombre );
            serie = query.list();
        }
        catch(Exception e){
             System.out.println("No se pudo Listar las Series Documentales buscadas por el nombre \\n "+ e.getMessage());
             throw new UnsupportedOperationException("No se pudo Listar las Series documentales buscadas por el nombre \\n "+ e.getMessage());
        }
         finally{
            session.close();
        }
        return serie;
        
       // throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void registrarPrestamo(SerieDocumental serie) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<SerieDocumental> listaPrestamosOrganizacion() {
        
        
        
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<SerieDocumental> buscarPorUnidadOrganica(int uni_orgID) {
        
        List<SerieDocumental> series= null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
         //   String hql = "SELECT * FROM serie_documental WHERE uni_org_id=:p1";
          //  String hql = "SELECT DISTINCT sd FROM SerieDocumental sd JOIN FETCH sd.uni_org WHERE sd.uni_org.uni_org_id=:p1";
            String hql = "SELECT sd FROM SerieDocumental sd  WHERE sd.uni_org_id=:p1";
            Query query = session.createQuery(hql);
        //     Query query = session.createSQLQuery(sql);
            query.setParameter("p1",uni_orgID );
            series = query.list();
            
        }
        catch(Exception e){
            System.out.println("No se pudo Listar las Series Documentales perteneciente a dicha unidad Organica\\n "+ e.getMessage());
            throw new UnsupportedOperationException("No se pudo Listar las Series Documentales perteneciente a dicha unidad Organica \\n "+ e.getMessage()); 
        }
        finally{
            session.close();
        }
        return series;
       // throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<SerieDocumental> listaInventariosTransferencia() {
        List<SerieDocumental> series = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            String hql = "SELECT DISTINCT sd FROM SerieDocumental sd LFT JOIN FETCH sd.inventarios_transferencia it WHERE sd.estReg!='E' or sd IS NULL )";
            Query query = session.createQuery(hql);
            series = query.list();
            
        }catch(Exception e){
            System.out.println("No se pudo listar los Iventarios de Transferencia \\n" + e.getMessage());
            throw new UnsupportedOperationException("No se pudo Listar los inventario de Transferencia \\n "+ e.getMessage());           
        }
        finally{
            session.close();
        }
        return series;
      //  throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void registrarInventario(SerieDocumental serie) {
       
         InventarioTransferenciaDAO inv_tra_dao = (InventarioTransferenciaDAO)FactoryDao.buildDao("InventarioTransferenciaDAO");
      
         try{
                for(InventarioTransferencia it : serie.getInventarios()  ){
                    
                    inv_tra_dao.insert(it);
                     
                }
        }catch(Exception e){
               System.out.println("No se pudo Registrar Los Inventario de Transferencia\\n" + e.getMessage());
            throw new UnsupportedOperationException("No se pudo Registrar los inventario de Transferencia \\n "+ e.getMessage());           
        }
        
    }

   
   
}
