/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.smdg.proyectos.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.smdg.ProyectoActividadesDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.smdg.ProyectoActividades;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author Administrador
 */
public class ListarActividadesTx implements ITransaction{
    @Override
    public WebResponse execute(WebRequest wr) {        
        
        JSONObject requestData = (JSONObject)wr.getData();
        int proid = requestData.getInt("proid");
                
        List<ProyectoActividades> actividades = null;
        ProyectoActividadesDao actividadesDao = (ProyectoActividadesDao)FactoryDao.buildDao("smdg.ProyectoActividadesDao");
        
        try{
            actividades = actividadesDao.listarActividadesxProyecto(proid);
        
        }catch(Exception e){            
            return WebResponse.crearWebResponseError("No se pudo Listar las actividades", e.getMessage() );
        }
        //Fin
             
        /*
        *  Repuesta Correcta
        */
        JSONArray miArray = new JSONArray();
//        for(Object[] p : actividades){
//            JSONObject oResponse = new JSONObject();
//            oResponse.put("proid",p[0]);
//            oResponse.put("pronom",p[1]);
//            oResponse.put("protip",p[2]);
//            oResponse.put("prores",p[3]);
//            oResponse.put("proini",p[4]);
//            oResponse.put("profin",p[5]);
//            oResponse.put("prodoc",p[6]);
//            miArray.put(oResponse);
//        }
        
        return WebResponse.crearWebResponseExito("Se Listo correctamente",miArray);        
                
    }
}
