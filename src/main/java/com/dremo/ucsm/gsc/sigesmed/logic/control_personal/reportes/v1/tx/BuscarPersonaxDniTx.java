/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.control_personal.reportes.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.logic.directorio.persona.v1.tx.*;
import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.cpe.PersonalDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.di.PersonaDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Persona;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author Administrador
 */
public class BuscarPersonaxDniTx implements ITransaction{
    
    @Override
    public WebResponse execute(WebRequest wr) {        
        /*
        *  Parte para la operacion en la Base de Datos
        */        
        
        JSONObject requestData = (JSONObject)wr.getData();
        String perDni = requestData.getString("perDni");
        
        Persona pe = null;
        PersonalDao personalDao = (PersonalDao)FactoryDao.buildDao("cpe.PersonalDao");
        
        try{
            pe = personalDao.buscarPersonaxDNI(perDni);
        
        }catch(Exception e){
            System.out.println("No se encontro a la persona con el DNI dado \n"+e);
            return WebResponse.crearWebResponseError("No se encontro a la persona con el DNI dado ", e.getMessage() );
        }
        //Fin
             
        /*
        *  Repuesta Correcta
        */
        //JSONArray miArray = new JSONArray();
        
        JSONObject oResponse = new JSONObject();
                
        if (pe != null){
            oResponse.put("id",pe.getPerId());
            oResponse.put("dni",pe.getDni());
            oResponse.put("materno",pe.getApeMat());
            oResponse.put("paterno",pe.getApePat());
            oResponse.put("nombre",pe.getNom());
            oResponse.put("datos",pe.getNombrePersonaAP());
            
            return WebResponse.crearWebResponseExito("La persona si existe ",oResponse);        
        }
                
        //oResponse.put("traId",pariente.getTrabajador().getTraId());            
        //miArray.put(oResponse);
        return WebResponse.crearWebResponseExito("La persona no existe",oResponse);        
                
        
                
    }   
    
}
